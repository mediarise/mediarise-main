<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наборы иконок");
?>
<!-- Section -->
<div class="section section_bg section_page-info">
	<div class="section__container section__container_bg">
		<div class="container">
			<div class="page-info">
				<div class="row">
					<div class="col_sm_7">
							<div class="page-info__title">
								<h1><?$APPLICATION->ShowTitle();?></h1>
							</div>											
					</div>
					<div class="col_sm_5">
						<div class="page-info__back">
							<a href="<?=SITE_DIR?>">Вернуться на главную</a>
						</div>						
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>