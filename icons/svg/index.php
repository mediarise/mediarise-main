<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Набор цветных svg иконок");
?>

<!-- Section -->
<div class="section section_bg section_page-info">
	<div class="section__container section__container_bg">
		<div class="container">
			<div class="page-info">
				<div class="row">
					<div class="col_sm_7">
							<div class="page-info__title">
								<h1><?$APPLICATION->ShowTitle();?></h1>
							</div>											
					</div>
					<div class="col_sm_5">
						<div class="page-info__back">
							<a href="<?=SITE_DIR?>">Вернуться на главную</a>
						</div>						
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Icon set -->
<div class="icon-set">
	<div class="container">
		<div class="row">
			
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="4" fill="#0288D1" width="10" height="15"/><rect x="2" y="6" fill="#29B6F6" width="4" height="11"/><rect x="18" y="6" fill="#29B6F6" width="4" height="11"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="7" y="4" fill="#039BE5" width="5" height="15"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_carousel</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4FC3F7" d="M12.04,2H12C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10h0.04c5.5-0.02,9.96-4.49,9.96-10S17.54,2.02,12.04,2zM12.04,20H12c-4.41,0-8-3.59-8-8s3.59-8,8-8h0.04C16.43,4.02,20,7.6,20,12C20,16.4,16.43,19.98,12.04,20z"/><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.3" fill="#2196F3" d="M22,12c0,5.51-4.46,9.98-9.96,10v-2c4.39-0.02,7.96-3.6,7.96-8c0-4.4-3.57-7.98-7.96-8V2C17.54,2.02,22,6.49,22,12z"/><rect x="11" y="11" fill="#2196F3" width="2" height="6"/><rect x="11" y="7" fill="#2196F3" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>info_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4CAF50" d="M8.193,3.3C8.384,3.492,8.49,3.738,8.49,3.994c0.043,1.075,0.223,2.525,0.57,3.565C9.182,7.904,9.096,8.314,8.82,8.59l-2.582,2.58C4.843,8.45,4.908,5.541,5,3.003h2.486C7.742,3.003,7.996,3.104,8.193,3.3z"/><path fill="#4CAF50" d="M20.699,15.807c-0.191-0.189-0.438-0.297-0.693-0.297c-1.074-0.043-2.525-0.223-3.564-0.57c-0.346-0.119-0.756-0.033-1.031,0.24l-2.581,2.582c2.722,1.396,5.631,1.33,8.168,1.238v-2.486C20.996,16.258,20.896,16.004,20.699,15.807z"/><path fill="#388E3C" d="M19.938,19C11.701,19,5,12.299,5,4.063V3.003H3.997c-0.248-0.007-0.502,0.092-0.7,0.29C3.1,3.492,3,3.808,3,4.063C3,13.416,10.583,21,19.938,21c0.254,0,0.572-0.1,0.771-0.297s0.297-0.453,0.289-0.699V19H19.938z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#FB8C00" points="18,11 23,6 18,1 18,4 14,4 14,8 18,8 "/></svg>			</div>
			<div class="icon-set__id">
				<span>phone_forwarded</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path fill="#546E7A" d="M15.05,16.29l2.86-3.07c0.38-0.39,0.72-0.79,1.04-1.18c0.319-0.39,0.59-0.78,0.819-1.17c0.23-0.39,0.41-0.78,0.541-1.17C20.439,9.31,20.5,8.91,20.5,8.52c0-0.53-0.09-1.02-0.27-1.46c-0.181-0.44-0.44-0.81-0.78-1.11c-0.341-0.31-0.771-0.54-1.261-0.71C17.68,5.08,17.109,5,16.471,5c-0.691,0-1.311,0.11-1.851,0.32c-0.54,0.21-1,0.51-1.36,0.88c-0.369,0.37-0.65,0.8-0.84,1.3c-0.18,0.47-0.27,0.97-0.28,1.5h2.139c0.011-0.31,0.051-0.6,0.131-0.87c0.09-0.29,0.23-0.54,0.4-0.75c0.18-0.21,0.41-0.37,0.68-0.49c0.27-0.12,0.6-0.18,0.96-0.18c0.31,0,0.579,0.05,0.81,0.15s0.43,0.25,0.59,0.43c0.16,0.18,0.28,0.4,0.371,0.65C18.3,8.19,18.35,8.46,18.35,8.75c0,0.22-0.029,0.43-0.08,0.65c-0.06,0.22-0.149,0.45-0.289,0.7c-0.141,0.25-0.32,0.53-0.561,0.83c-0.23,0.3-0.52,0.65-0.88,1.03l-4.17,4.55V18H21v-1.71H15.05z"/><rect x="2" y="11" fill="#E53935" width="8" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>exposure_minus_2</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#66BB6A" d="M11.99,2C6.47,2,2,6.48,2,12c0,5.52,4.47,10,9.99,10C17.52,22,22,17.52,22,12C22,6.48,17.52,2,11.99,2z"/><circle fill="#66BB6A" cx="12" cy="12" r="8"/><circle fill="#CFD8DC" cx="15.5" cy="9.5" r="1.5"/><circle fill="#CFD8DC" cx="8.5" cy="9.5" r="1.5"/><path fill="#CFD8DC" d="M12,17.5c2.33,0,4.311-1.46,5.109-3.5H6.89C7.69,16.04,9.67,17.5,12,17.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>tag_faces</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FBC02D" d="M12,7c1.1,0,2-0.9,2-2c0-0.38-0.1-0.73-0.29-1.03L12,1l-1.71,2.97C10.1,4.27,10,4.62,10,5C10,6.1,10.9,7,12,7z"/><path fill="#E57373" d="M5,15c-1.1,0-2,0.9-2,2v4h18v-4c0-1.1-0.9-2-2-2H5z"/><path fill="#EF9A9A" d="M18,15v-3c0-1.1-0.9-2-2-2h-3V8h-2v2H8c-1.1,0-2,0.9-2,2v3H18z"/><rect x="1" y="21" fill="#EF9A9A" width="22" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>cake</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#90A4AE" points="22,18 22,3 2,3 2,18 0,18 0,20 24,20 24,18 "/><rect x="10" y="17" fill="#B0BEC5" width="4" height="1"/><rect x="4" y="5" fill="#E3F2FD" width="16" height="10"/><rect y="18" fill="#546E7A" width="24" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>laptop_chromebook</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#607D8B" d="M3,21h18v-2H3V21z M11,17h10v-2H11V17z M3,3v2h18V3H3z M11,9h10V7H11V9z M11,13h10v-2H11V13z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#FFC107" points="3,8 3,16 7,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>format_indent_increase</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#9575CD" cx="12" cy="12" r="4"/><path fill="#546E7A" d="M5,15H3v4c0,1.1,0.9,2,2,2h4v-2H5V15z"/><path fill="#546E7A" d="M5,5h4V3H5C3.9,3,3,3.9,3,5v4h2V5z"/><path fill="#546E7A" d="M19,3h-4v2h4v4h2V5C21,3.9,20.1,3,19,3z"/><path fill="#546E7A" d="M19,19h-4v2h4c1.1,0,2-0.9,2-2v-4h-2V19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>center_focus_strong</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#D32F2F" d="M12.021,1.917c-3.306,0-5.978,2.673-5.978,5.977c0,4.484,5.978,11.1,5.978,11.1s5.977-6.616,5.977-11.1C17.998,4.59,15.325,1.917,12.021,1.917z M7.751,7.895c0-2.348,1.921-4.269,4.27-4.269c2.355,0,4.268,1.921,4.268,4.269c0,2.442-2.492,6.164-4.268,8.434C10.218,14.032,7.751,10.354,7.751,7.895z"/><path fill="#F44336" d="M12.021,3.626c-2.349,0-4.27,1.921-4.27,4.269c0,2.459,2.467,6.138,4.27,8.434c1.775-2.27,4.268-5.992,4.268-8.434C16.289,5.547,14.377,3.626,12.021,3.626z M12.021,10.029c-1.179,0-2.136-0.957-2.136-2.135s0.957-2.135,2.136-2.135c1.178,0,2.134,0.957,2.134,2.135C14.155,9.073,13.199,10.029,12.021,10.029z"/><circle fill="#FFFFFF" cx="12.021" cy="7.895" r="2.135"/></g><rect x="5" y="20" fill="#D32F2F" width="14" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>pin_drop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M-618-1464H782v3600H-618V-1464z M0,0h24v24H0V0z"/><path fill="#7E57C2" d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M4,12c0-4.4,3.6-8,8-8c1.8,0,3.5,0.6,4.9,1.7L5.7,16.9C4.6,15.5,4,13.8,4,12z M12,20c-1.8,0-3.5-0.6-4.9-1.7L18.3,7.1C19.4,8.5,20,10.2,20,12C20,16.4,16.4,20,12,20z"/></svg>			</div>
			<div class="icon-set__id">
				<span>dnd_forwardslash</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M11.46,22.54H1.12V0.78h10.34V3H2v3h9.46v2H3C2.45,8,2,8.45,2,9v6c0,0.55,0.45,1,1,1h8.46v2H2v3h9.46V22.54z"/><rect x="2" y="18" fill="#9CCC65" width="19" height="3"/><path fill="#26A69A" d="M21,9v6c0,0.55-0.45,1-1,1H3c-0.55,0-1-0.45-1-1V9c0-0.55,0.45-1,1-1h17C20.55,8,21,8.45,21,9z"/><rect x="2" y="3" fill="#29B6F6" width="19" height="3"/><rect x="2" y="3" fill="#4FC3F7" width="9.46" height="3"/><path fill="#4DB6AC" d="M3,8h8.46v8H3c-0.55,0-1-0.45-1-1V9C2,8.45,2.45,8,3,8z"/><rect x="2" y="18" fill="#81C784" width="9.46" height="3"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_day</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0-0.75h24v24H0V-0.75z"/><polygon fill="#607D8B" points="18,9.25 13.415,13.835 10.585,13.835 6,9.25 7.41,7.84 12,12.422 16.59,7.84 "/><rect x="10.999" y="12.833" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 10.7032 32.1014)" fill="#90A4AE" width="2.001" height="2.001"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_arrow_down</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M19.005,11c0,3.42-2.72,6.24-6,6.72V21h-2v-3.28c-3.28-0.489-6-3.31-6-6.72h1.7c0,2.99,2.52,5.08,5.26,5.1h0.04c2.761,0,5.3-2.1,5.3-5.1H19.005z"/><path fill="#546E7A" d="M19.005,11c0,3.42-2.72,6.24-6,6.72V21h-1.04v-4.9h0.04c2.761,0,5.3-2.1,5.3-5.1H19.005z"/></g><path fill="#1565C0" d="M12,2c-1.66,0-3,1.34-3,3v6c0,1.66,1.34,3,3,3s2.99-1.34,2.99-3L15,5C15,3.34,13.66,2,12,2z M13.189,11.1c0,0.66-0.529,1.2-1.189,1.2s-1.2-0.54-1.2-1.2V4.9c0-0.66,0.54-1.2,1.2-1.2s1.2,0.54,1.2,1.2L13.189,11.1z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>mnone</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M19,8H5c-1.66,0-3,1.34-3,3v6h4v4h12v-4h4v-6C22,9.34,20.66,8,19,8z"/><rect x="8" y="14" fill="#CFD8DC" width="8" height="5"/><circle fill="#CFD8DC" cx="19" cy="11" r="1"/><rect x="6" y="3" fill="#CFD8DC" width="12" height="4"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="5.969" y="12.719" fill="#CFD8DC" width="12.031" height="8.281"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_print_shop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1565C0" d="M13,3H6v18h4v-6h3c3.31,0,6-2.69,6-6S16.31,3,13,3z M13.2,11H10V7h3.2c1.1,0,2,0.9,2,2S14.3,11,13.2,11z"/><rect x="6" y="3" opacity="0.4" fill="#1E88E5" width="3.99" height="18"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_parking</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFA000" d="M7,14c-1.66,0-3,1.34-3,3c0,1.311-1.16,2-2,2c0.92,1.22,2.49,2,4,2c2.21,0,4-1.79,4-4C10,15.34,8.66,14,7,14z"/><path fill="#E64A19" d="M20.71,4.63l-1.34-1.34c-0.39-0.39-1.021-0.39-1.41,0L9,12.25L11.75,15l8.96-8.96C21.1,5.65,21.1,5.02,20.71,4.63z"/></svg>			</div>
			<div class="icon-set__id">
				<span>brush</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#F48FB1" points="22,7 22,20 14,20 14,16 10,16 10,20 2,20 2,7 5,7 5,4 19,4 19,7 "/><polygon fill="#F06292" points="22,7 22,9.97 2,9.97 2,7 5,7 5,4 19,4 19,7 "/><polygon fill="#CFD8DC" points="11,10 9,10 9,11 11,11 11,12 8,12 8,9 10,9 10,8 8,8 8,7 11,7 "/><polygon fill="#CFD8DC" points="16,12 15,12 15,10 13,10 13,7 14,7 14,9 15,9 15,7 16,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>local_convenience_store</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M22.92,22.61H11.96V22H12c5.52,0,10-4.48,10-10c0-5.52-4.48-10-10-10h-0.04V1.58h10.96V22.61z"/><path opacity="0.9" fill="#1565C0" d="M22,12c0,5.52-4.48,10-10,10h-0.04C6.46,21.98,2,17.51,2,12s4.46-9.98,9.96-10H12C17.52,2,22,6.48,22,12z"/><path opacity="0.3" fill="#1565C0" d="M19.155,19.174c-3.902,3.902-10.238,3.902-14.141,0l-0.028-0.029L19.128,5.003l0.028,0.028C23.06,8.934,23.06,15.27,19.155,19.174z"/></svg>			</div>
			<div class="icon-set__id">
				<span>lens</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M18,7V4c0-1.1-0.9-2-2-2H8C6.9,2,6,2.9,6,4v3H5v6l3,6v3h8v-3l3-6V7H18z M14,7V5h-1v2h-2V5h-1v2H8V4h8v3H14z"/><rect x="10" y="5" fill="#FFD54F" width="1" height="2"/><rect x="13" y="5" fill="#FFD54F" width="1" height="2"/><path fill="#FFCA28" d="M18,4v3h-2V4H8v3H6V4c0-1.1,0.9-2,2-2h8C17.1,2,18,2.9,18,4z"/><polygon fill="#546E7A" points="19,7 19,13 16,19 16,19.06 8,19.06 8,19 5,13 5,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_input_hdmi</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#7E57C2" d="M19,9c0,5.04-6.45,12.38-6.96,12.96C12.01,21.99,12,22,12,22S5,14.25,5,9c0-3.87,3.13-7,7-7h0.04C15.89,2.02,19,5.14,19,9z"/><path opacity="0.3" fill="#5E35B1" d="M19,9c0,5.04-6.45,12.38-6.96,12.96V2C15.89,2.02,19,5.14,19,9z"/><circle fill="#CFD8DC" cx="12" cy="9" r="2.5"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>room</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#BCAAA4" d="M21.72,17.41l-4.34,4.34c-0.2,0.19-0.45,0.29-0.71,0.29s-0.51-0.09-0.71-0.29l-3.98-3.98l-0.55,0.551L8,21.75c-0.39,0.39-1.02,0.39-1.41,0l-4.34-4.34c-0.39-0.391-0.39-1.021,0-1.41l3.98-3.98L2.25,8.04c-0.39-0.39-0.39-1.02,0-1.41l4.34-4.34C6.78,2.1,7.04,2,7.29,2C7.55,2,7.8,2.1,8,2.29l3.98,3.98l0.66-0.66l3.32-3.32c0.39-0.39,1.021-0.39,1.41,0l4.34,4.34c0.39,0.39,0.39,1.02,0,1.41l-3.04,3.05l-0.939,0.93L21.72,16C22.11,16.39,22.11,17.02,21.72,17.41z"/><circle fill="#D7CCC8" cx="12" cy="10" r="1"/><polygon fill="#A1887F" points="7.29,10.96 3.66,7.34 7.29,3.71 10.91,7.33 	"/><circle fill="#D7CCC8" cx="10" cy="12" r="1"/><circle fill="#D7CCC8" cx="12" cy="14" r="1"/><circle fill="#D7CCC8" cx="14" cy="12" r="1"/><polygon fill="#A1887F" points="16.66,20.34 13.029,16.72 16.66,13.09 20.279,16.71 	"/></g><g id="Capa_2"><path fill="#A1887F" d="M11.98,6.27l-5.75,5.75L2.25,8.04c-0.39-0.39-0.39-1.02,0-1.41l4.34-4.34C6.78,2.1,7.04,2,7.29,2C7.55,2,7.8,2.1,8,2.29L11.98,6.27z"/><path fill="#A1887F" d="M21.72,17.41l-4.34,4.34c-0.2,0.19-0.45,0.29-0.71,0.29s-0.51-0.09-0.71-0.29l-3.98-3.98l-0.55,0.551l-0.01-0.011l4.83-4.81l0.41-0.41l1.07-1.07L21.72,16C22.11,16.39,22.11,17.02,21.72,17.41z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>healing</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path d="M7,9v6h4l5,5V4l-5,5H7z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#64B5F6" d="M11,9H7v6h4l5,5V4L11,9z"/><path fill="none" d="M4,0h24v24H4V0z"/><polygon fill="#90A4AE" points="16,4 16,20 11,15 11,9 "/><rect x="7" y="9" fill="#78909C" width="4" height="6"/></svg>			</div>
			<div class="icon-set__id">
				<span>volume_mute</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#1976D2" d="M21,5v6c0,5.55-3.84,10.74-9,12c-5.16-1.26-9-6.45-9-12V5l9-4L21,5z"/><path fill="#1E88E5" d="M12,1v22c-5.16-1.26-9-6.45-9-12V5L12,1z"/><path fill="#42A5F5" d="M11.984,3.172l-7,3.125L5,12c0,0,0.219,7.5,7,8.906c0,0,4.766-0.516,7-8.938l0.063-5.594L11.984,3.172z"/><path fill="#FFE082" d="M12,11.99h7c-0.529,4.119-3.094,7.869-7,8.939V12H5V6.3l7-3.11V11.99z"/></g><g id="Capa_3"></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>security</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#42A5F5" d="M3.9,12c0-1.71,1.39-3.1,3.1-3.1h4V7H7c-2.76,0-5,2.24-5,5s2.24,5,5,5h4v-1.9H7C5.29,15.1,3.9,13.71,3.9,12z"/><rect x="8" y="11" fill="#1976D2" width="8" height="2"/><path fill="#42A5F5" d="M17,7h-4v1.9h4c1.71,0,3.1,1.39,3.1,3.1s-1.39,3.1-3.1,3.1h-4V17h4c2.76,0,5-2.24,5-5S19.76,7,17,7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>link</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FF5722" d="M17,17v3.67C17,21.4,16.4,22,15.66,22H8.33C7.6,22,7,21.4,7,20.67V17h4v3l1.6-3H17z"/><polygon fill="#FFFFFF" fill-opacity="0.3" points="15,12.5 12.6,17 11,20 11,14.5 9,14.5 13,7 13,12.5 "/><path fill="#FF5722" fill-opacity="0.3" d="M17,5.33V17h-4.4l2.4-4.5h-2V7l-4,7.5h2V17H7V5.33C7,4.6,7.6,4,8.33,4H10V2h4v2h1.67C16.4,4,17,4.6,17,5.33z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_charging_20</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#BBDEFB" points="11.92,7.33 3,16.25 3,21 7.75,21 16.67,12.08 "/><polygon fill="#7986CB" points="6.92,19 5,19 5,17.08 10.915,11.165 12.835,13.085 "/><path fill="#311B92" d="M18.09,13.5l1.41-1.41l-1.92-1.92l3.12-3.12c0.399-0.4,0.399-1.03,0.01-1.42l-2.34-2.34c-0.39-0.39-1.021-0.39-1.41,0l-3.12,3.12L11.91,4.5L10.5,5.91L18.09,13.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>colorize</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#43A047" d="M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M19.14,6.91C17.16,5.4,14.68,4.5,12,4.5c-2.68,0-5.16,0.9-7.14,2.41C3.14,8.21,1.8,9.96,1,12c0.82,2.09,2.21,3.88,3.98,5.18c0,0,0,0.011,0.01,0.021c1.96,1.439,4.39,2.3,7.01,2.3s5.05-0.86,7.01-2.3c0.01-0.01,0.01-0.021,0.01-0.021c1.771-1.3,3.16-3.09,3.98-5.18C22.2,9.96,20.86,8.21,19.14,6.91z M12,17c-2.76,0-5-2.24-5-5s2.24-5,5-5s5,2.24,5,5S14.76,17,12,17z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z"/></g><g id="Capa_2"><path fill="#66BB6A" d="M15,12c0,1.66-1.34,3-3,3s-3-1.34-3-3s1.34-3,3-3S15,10.34,15,12z"/><path fill="#43A047" d="M15,12c0,1.66-1.34,3-3,3s-3-1.34-3-3s1.34-3,3-3S15,10.34,15,12z"/><path fill="#81C784" d="M19.14,6.91C17.16,5.4,14.68,4.5,12,4.5c-2.68,0-5.16,0.9-7.14,2.41c-1.02,1.43-1.61,3.17-1.61,5.05c0,1.96,0.64,3.76,1.73,5.22c0,0,0,0.011,0.01,0.021c1.96,1.439,4.39,2.3,7.01,2.3s5.05-0.86,7.01-2.3c0.01-0.01,0.01-0.021,0.01-0.021c1.091-1.46,1.73-3.26,1.73-5.22C20.75,10.08,20.16,8.34,19.14,6.91z M12,17c-2.76,0-5-2.24-5-5s2.24-5,5-5s5,2.24,5,5S14.76,17,12,17z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>visibility</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/><rect x="7" y="11" fill="#E53935" width="10" height="2"/><path fill="#EF5350" d="M12,2h-0.03C6.46,2.02,2,6.49,2,12s4.46,9.98,9.97,10H12c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M12,20h-0.03C7.57,19.98,4,16.4,4,12c0-4.4,3.57-7.98,7.97-8H12c4.41,0,8,3.59,8,8S16.41,20,12,20z"/><path opacity="0.3" fill="#D32F2F" d="M22,12c0,5.52-4.48,10-10,10h-0.03v-2H12c4.41,0,8-3.59,8-8s-3.59-8-8-8h-0.03V2H12C17.52,2,22,6.48,22,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>remove_circle_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1976D2" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><circle fill="#BBDEFB" cx="12" cy="12" r="8"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#81C784" d="M12,6v6c0,0,5.516-2.297,5.516-2.344C17.516,9.609,16.172,6.094,12,6z"/><path fill="#DCE775" d="M17.516,9.656l-5.542,2.375c0,0,4.032,4.439,4.077,4.425C16.096,16.439,19.25,13.578,17.516,9.656z"/><path fill="#FFB74D" d="M16.051,16.456l-4.041-4.487c0,0-4.261,4.221-4.244,4.266C7.784,16.275,11.422,20.047,16.051,16.456z"/></svg>			</div>
			<div class="icon-set__id">
				<span>timelapse</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="22" fill="#455A64" width="2" height="2"/><rect x="11" y="22" fill="#546E7A" width="2" height="2"/><rect x="11" y="2" fill="#64B5F6" width="2" height="10"/><path fill="#42A5F5" d="M16.561,4.44l-1.451,1.45C16.84,6.94,18,8.83,18,11c0,3.311-2.689,6-6,6c-3.31,0-6-2.689-6-6c0-2.17,1.16-4.06,2.88-5.12L7.44,4.44C5.36,5.88,4,8.28,4,11c0,4.42,3.58,8,8,8s8-3.58,8-8C20,8.28,18.641,5.88,16.561,4.44z"/><rect x="15" y="22" fill="#455A64" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_power</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#64B5F6" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.11,4,6.6,5.64,5.35,8.04C2.34,8.36,0,10.91,0,14c0,3.311,2.69,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z"/><polygon fill="#1976D2" points="14,13 14,17 10,17 10,13 7,13 12,8 17,13 "/></svg>			</div>
			<div class="icon-set__id">
				<span>cloud_upload</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19,6H5C3.9,6,3,6.9,3,8v8c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V8C21,6.9,20.1,6,19,6z M19,16H5V8h14V16z"/><path fill="none" d="M43-23h24V1H43V-23z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_16_9</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z"/><rect x="5" y="5" fill="#BBDEFB" width="14" height="14"/><path fill="none" d="M0,0h24v24H0V0z"/><circle fill="#039BE5" cx="12" cy="10" r="2.25"/><path fill="#0288D1" d="M16.5,16.25c0-1.5-3-2.25-4.5-2.25s-4.5,0.75-4.5,2.25V17h9V16.25z"/></svg>			</div>
			<div class="icon-set__id">
				<span>portrait</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M16,4.8c0.99,0,1.8-0.81,1.8-1.8S16.99,1.2,16,1.2c-1,0-1.8,0.81-1.8,1.8S15,4.8,16,4.8z"/><circle fill="#37474F" cx="19" cy="17" r="5"/><path fill="#B0BEC5" d="M19,20.5c-1.93,0-3.5-1.57-3.5-3.5s1.57-3.5,3.5-3.5s3.5,1.57,3.5,3.5S20.93,20.5,19,20.5z"/><path fill="#78909C" d="M14.8,10H19V8.2h-3.2l-1.93-3.27c-0.3-0.5-0.841-0.83-1.46-0.83c-0.47,0-0.89,0.19-1.2,0.5l-3.7,3.7C7.19,8.6,7,9.03,7,9.5c0,0.63,0.33,1.16,0.85,1.47L11.2,13v5H13v-6.48l-2.25-1.67l2.32-2.33L14.8,10z"/><circle fill="#37474F" cx="5" cy="17" r="5"/><path fill="#B0BEC5" d="M5,20.5c-1.93,0-3.5-1.57-3.5-3.5s1.57-3.5,3.5-3.5s3.5,1.57,3.5,3.5S6.93,20.5,5,20.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>directions_bike</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#C5CAE9" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><polygon fill="#00ACC1" points="6.5,9 10,5.5 13.5,9 11,9 11,13 9,13 9,9 "/><polygon fill="#43A047" points="17.5,15 14,18.5 10.5,15 13,15 13,11 15,11 15,15 "/></svg>			</div>
			<div class="icon-set__id">
				<span>swap_vert_circle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M20,2H4C2.9,2,2.01,2.9,2.01,4L2,22l4-4h14c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2z"/><path fill="#607D8B" d="M6,9h12v2H6V9z M14,14H6v-2h8V14z M18,8H6V6h12V8z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>chat</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polyline opacity="0.5" fill="#BBDEFB" points="20,4 20,20 4,20 4,4 20,4 "/><path fill="#BBDEFB" d="M19,5v14H5V5H19 M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>check_box_outline_blank</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M12,3.2c0,0-7.156,7.675-5.875,12.144C6.516,16.828,7.797,19.828,12,20c0,0,6,0.063,5.984-6.406C17.328,8.703,12.141,3.484,12,3.2z"/><polygon fill="#9FA8DA" points="4,6.55 5.27,5.27 19.859,19.859 18.59,21.13 "/><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_color_reset</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#546E7A" points="18,7 11,7 16,12 11,17 18,17 18,20 6,20 6,18 7.08,17 12.5,12 7.08,7 6,6 6,4 18,4 "/><polygon opacity="0.4" fill="#78909C" points="18,4 18,7 7.08,7 6,6 6,4 "/><polygon opacity="0.4" fill="#78909C" points="16,12 11,17 7.08,17 12.5,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>format_textdirection_l_to_r</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="13.9" fill="#B0BEC5" width="10" height="6.1"/><rect x="6" y="14" fill="#BBDEFB" width="6" height="4"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="4" y="4" fill="#E53935" width="16" height="2"/><polygon fill="#D1C4E9" points="6.01,7 6.01,13.99 3,13.99 3,12 4,7 "/><rect x="5.97" y="7" fill="#E53935" width="3.04" height="6.99"/><rect x="8.96" y="7" fill="#D1C4E9" width="3.05" height="6.99"/><rect x="11.96" y="7" fill="#E53935" width="3.04" height="6.99"/><rect x="14.96" y="7" fill="#D1C4E9" width="3.04" height="6.99"/><polygon fill="#E53935" points="21,12 21,13.99 17.96,13.99 17.96,7 20,7 "/><rect x="18" y="14" fill="#B0BEC5" width="2" height="6"/></svg>			</div>
			<div class="icon-set__id">
				<span>store_mall_directory</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#BBDEFB" points="11,13 11,19 6,19 6,21 18,21 18,19 13.001,19 13,13 21,5 21,3 3,3 3,5 "/><polygon fill="#009688" points="6,8 18,8 13,13 11,13 "/><polygon fill="#80CBC4" points="18,8 6,8 3,5 21,5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>local_bar</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#607D8B" d="M20,4h-3.17L15,2H9L7.17,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4zM15,15.5V13H9v2.5L5.5,12L9,8.5V11h6V8.5l3.5,3.5L15,15.5z"/><polygon fill="#CFD8DC" points="15,15.5 15,13 9,13 9,15.5 5.5,12 9,8.5 9,11 15,11 15,8.5 18.5,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>switch_camera</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M16.984,1.01L6.984,1c-1.101,0-2,0.857-2,1.906v17.147c0,1.048,0.899,1.905,2,1.905h10c1.1,0,2-0.857,2-1.905V2.906C18.984,1.858,18.084,1.01,16.984,1.01z"/><rect x="6.984" y="4.811" fill="#BBDEFB" width="10" height="13.336"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#29B6F6" d="M2,16v2c2.76,0,5,2.24,5,5h2C9,19.13,5.87,16,2,16z"/><path fill="#29B6F6" d="M2,20v3h3C5,21.34,3.66,20,2,20z"/><path fill="#29B6F6" d="M2,12v2c4.97,0,9,4.03,9,9h2C13,16.92,8.08,12,2,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>tap_and_play</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="11" fill="#7E57C2" width="2" height="2"/><rect x="11" y="15" fill="#7E57C2" width="2" height="2"/><rect x="7" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="7" fill="#7E57C2" width="2" height="2"/><rect x="15" y="11" fill="#7E57C2" width="2" height="2"/><rect x="11" y="3" fill="#7E57C2" width="2" height="2"/><rect x="15" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="11" fill="#7E57C2" width="2" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><rect x="3" y="7" fill="#7E57C2" width="2" height="2"/><rect x="19" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><rect x="3" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="19" fill="#512DA8" width="18" height="2"/><rect x="3" y="15" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_bottom</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="16" fill="#9575CD" width="6" height="2"/><rect x="3" y="6" fill="#BA68C8" width="18" height="2"/><rect x="3" y="11" fill="#F06292" width="12" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>sort</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M21.005,16.53v3.5c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.569,3.57c0.109,0.35,0.029,0.74-0.25,1l-2.199,2.21c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.318,0.439,0.63,0.682,0.939c0.129,0.157,0.25,0.301,0.369,0.448c0.289,0.341,0.59,0.658,0.899,0.959c0.22,0.211,0.448,0.42,0.688,0.632c0.119,0.108,0.239,0.221,0.369,0.312c0.133,0.102,0.25,0.2,0.383,0.3c0.131,0.109,0.26,0.2,0.397,0.29c0.144,0.103,0.276,0.188,0.42,0.29c0.276,0.2,0.58,0.381,0.88,0.54c0.188,0.108,0.393,0.221,0.58,0.319l2.2-2.199c0.276-0.271,0.67-0.352,1.021-0.24c1.118,0.37,2.318,0.57,3.568,0.57C20.555,15.53,21.005,15.979,21.005,16.53z"/><g id="Capa_2"><path fill="#455A64" d="M21.005,19.71v0.32c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h0.688l0.012,0.06c0,0-0.131,3.77,1.92,7.72c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.318,0.439,0.63,0.682,0.939c0.129,0.157,0.25,0.301,0.369,0.448c0.289,0.341,0.59,0.658,0.899,0.959c0.22,0.211,0.448,0.42,0.688,0.632c0.119,0.102,0.239,0.198,0.369,0.312c0.133,0.102,0.25,0.2,0.383,0.3c0.131,0.104,0.271,0.189,0.397,0.29c0.144,0.103,0.276,0.188,0.42,0.29c0.276,0.184,0.58,0.37,0.88,0.54c0.188,0.108,0.393,0.221,0.58,0.319c2.051,1.067,4.604,1.869,7.764,2.211L21.005,19.71z"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="#FFEE58" d="M17.817,2.394c-0.16-0.161-0.375-0.254-0.615-0.254l-3.046-0.01c-0.247-0.001-0.469,0.103-0.625,0.266c-0.152,0.156-0.248,0.367-0.25,0.602l-0.002,3.046c-0.001,0.24,0.094,0.458,0.252,0.619l3.908,3.926c0.154,0.154,0.371,0.25,0.611,0.251c0.237,0.003,0.456-0.091,0.613-0.253l3.055-3.037c0.161-0.16,0.258-0.378,0.254-0.615c0.005-0.237-0.098-0.463-0.252-0.62L17.817,2.394z M15.458,3.658c0,0.191-0.077,0.353-0.204,0.472c-0.117,0.111-0.273,0.182-0.449,0.178c-0.363-0.001-0.654-0.294-0.651-0.655c-0.002-0.175,0.066-0.329,0.181-0.449c0.116-0.123,0.283-0.202,0.475-0.201C15.169,3.001,15.46,3.295,15.458,3.658z"/><path opacity="0.2" fill="#8D6E63" enable-background="new    " d="M21.721,6.317c0.154,0.158,0.259,0.382,0.252,0.618c0.004,0.238-0.093,0.456-0.254,0.616l-1.542,1.532L15.254,4.13c0.127-0.12,0.204-0.281,0.204-0.472c0.002-0.363-0.289-0.656-0.649-0.654c-0.191-0.001-0.356,0.079-0.474,0.201l-0.804-0.808c0.156-0.163,0.378-0.267,0.625-0.266l3.046,0.01c0.239,0,0.454,0.093,0.615,0.254L21.721,6.317z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>local_phone</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path opacity="0.8" fill="#FFAB91" enable-background="new    " d="M3.007,8.001l-0.008,11c0,1.1,0.891,2,2,2h14c1.1,0,2-0.9,2-2v-11H3.007z"/><rect x="12.021" y="11.999" opacity="0.8" fill="#42A5F5" enable-background="new    " width="5" height="5"/><path opacity="0.8" fill="#C2185B" enable-background="new    " d="M20.999,5.001c0-1.1-0.9-2-2-2h-14c-1.109,0-1.99,0.9-1.99,2l-0.002,3h17.992V5.001z"/><path fill="#A1887F" enable-background="new    " d="M7.999,5.001h-2v-4h2V5.001z M17.999,1.001h-2v4h2V1.001z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><rect x="11.928" y="11.984" fill="#CFD8DC" width="2.557" height="5.015"/></svg>			</div>
			<div class="icon-set__id">
				<span>insert_invitation</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M23,5l-0.01,12c0,0.99-0.721,1.81-1.66,1.97C21.22,18.99,21.11,19,21,19h-5v2H8v-2H3c-0.11,0-0.22-0.01-0.33-0.03C1.72,18.81,1,17.99,1,17V5c0-1.1,0.9-2,2-2h18C22.1,3,23,3.9,23,5z"/><path fill="#455A64" d="M23,5l-0.01,12c0,0.99-0.721,1.81-1.66,1.97H2.67C1.72,18.81,1,17.99,1,17V5c0-1.1,0.9-2,2-2h18C22.1,3,23,3.9,23,5z"/><rect x="3" y="5" fill="#E3F2FD" width="18" height="12"/><rect x="8" y="8" fill="#64B5F6" width="11" height="2"/><rect x="8" y="12" fill="#F48FB1" width="11" height="2"/><rect x="5" y="8" fill="#42A5F5" width="2" height="2"/><rect x="5" y="12" fill="#F06292" width="2" height="2"/><rect y="1.25" fill="none" width="0" height="0.094"/></svg>			</div>
			<div class="icon-set__id">
				<span>dvr</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#FFD54F" points="15,7.5 15,2 9,2 9,7.5 12,10.5 "/><polygon fill="#4FC3F7" points="7.5,9 2,9 2,15 7.5,15 10.5,12 "/><polygon fill="#81C784" points="9,16.5 9,22 15,22 15,16.5 12,13.5 "/><polygon fill="#EF5350" points="16.5,9 13.5,12 16.5,15 22,15 22,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>gamepad</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4CAF50" d="M5,16c0,3.87,3.13,7,7,7s7-3.13,7-7v-4H5V16z"/><path fill="#4CAF50" d="M16.12,4.37l2.101-2.1L17.4,1.44L15.1,3.75C14.16,3.28,13.12,3,12,3S9.84,3.28,8.91,3.75L6.6,1.44L5.78,2.27l2.1,2.1C6.14,5.64,5,7.68,5,10v1h14v-1C19,7.68,17.859,5.64,16.12,4.37z"/><circle fill="#FFFFFF" cx="9" cy="8" r="1"/><circle fill="#FFFFFF" cx="15" cy="8" r="1"/></svg>			</div>
			<div class="icon-set__id">
				<span>adb</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#BDBDBD" d="M17,10V6c0-2.76-2.24-5-5-5S7,3.24,7,6h1.9c0-1.71,1.39-3.1,3.1-3.1s3.1,1.39,3.1,3.1v4H17z"/><path fill="#90A4AE" d="M18,10H6c-1.1,0-2,0.9-2,2v8c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2v-8C20,10.9,19.1,10,18,10z"/><path fill="#607D8B" d="M12,18c1.1,0,2-0.9,2-2s-0.9-2-2-2c-1.1,0-2,0.9-2,2S10.9,18,12,18z"/></svg>			</div>
			<div class="icon-set__id">
				<span>lock_open</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M20,6h-2.18C17.93,5.69,18,5.35,18,5c0-1.66-1.34-3-3-3c-1.05,0-1.96,0.54-2.5,1.35l-0.32,0.42l-0.06,0.09L12,4.02l-0.5-0.68c-0.47-0.69-1.22-1.19-2.09-1.32C9.29,2.01,9.17,2,9.04,2C8.83,2,8.62,2.02,8.43,2.06c-0.1,0.02-0.2,0.05-0.3,0.08C7.94,2.19,7.76,2.27,7.58,2.36c-0.26,0.14-0.5,0.31-0.7,0.52C6.34,3.42,6,4.17,6,5c0,0.35,0.07,0.69,0.18,1H4C2.89,6,2.01,6.89,2.01,8L2,19c0,1.11,0.89,2,2,2h16c1.11,0,2-0.89,2-2V8C22,6.89,21.11,6,20,6z M15,4c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S14.45,4,15,4z M9,4c0.55,0,1,0.45,1,1S9.55,6,9,6S8,5.55,8,5S8.45,4,9,4z"/><rect x="4" y="17" fill="#CFD8DC" width="16" height="2"/><polygon fill="#CFD8DC" points="20,14 4,14 4,8 9.08,8 7,10.83 8.62,12 11,8.76 12,7.4 13,8.76 15.38,12 17,10.83 14.92,8 20,8 "/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="1.979" y="13.979" fill="#66BB6A" width="19.989" height="3"/><rect x="10.472" y="5.487" fill="#66BB6A" width="3" height="15.45"/><path fill="#43A047" d="M14.98,1.98c-1.25,0-2.32,0.74-2.8,1.79c0,0.02-0.01,0.03-0.02,0.05v0.01l-0.04,0.03l-0.14,0.11L11.8,3.73c-0.43-0.92-1.33-1.58-2.39-1.71C9.29,2.01,9.17,2,9.04,2C8.83,2,8.62,2.02,8.43,2.06c-0.1,0.02-0.2,0.05-0.3,0.08C7.94,2.19,7.76,2.27,7.58,2.36c-0.26,0.14-0.5,0.31-0.7,0.52C6.32,3.42,5.98,4.17,5.98,5c0,0.35,0.06,0.69,0.18,1c0.42,1.17,1.55,2,2.88,2h0.04L7,10.83L8.62,12L12,7.4l3.38,4.6L17,10.83L14.92,8l-0.03-0.03c0.03,0.01,0.061,0.01,0.091,0.01C16.3,7.98,17.42,7.16,17.84,6c0.13-0.32,0.19-0.66,0.19-1.02C18.03,3.33,16.66,1.98,14.98,1.98z M9,6C8.45,6,8,5.55,8,5s0.45-1,1-1s1,0.45,1,1S9.55,6,9,6z M15,6c-0.55,0-1-0.45-1-1s0.45-1,1-1s1,0.45,1,1S15.55,6,15,6z"/></svg>			</div>
			<div class="icon-set__id">
				<span>redeem</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4FC3F7" d="M12,2C6.49,2,2,6.49,2,12s4.49,10,10,10s10-4.49,10-10S17.51,2,12,2z"/><path fill="#2196F3" d="M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/><polygon fill="#CFD8DC" points="13,7 11,7 11,11 7,11 7,13 11,13 11,17 13,17 13,13 17,13 17,11 13,11 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>control_point</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#4CAF50" d="M20,8h-2.811C16.74,7.22,16.12,6.55,15.37,6.04L17,4.41L15.59,3l-2.17,2.17C12.96,5.06,12.49,5,12,5s-0.96,0.06-1.41,0.17L8.41,3L7,4.41l1.62,1.63C7.88,6.55,7.26,7.22,6.81,8H4v2h2.09C6.04,10.33,6,10.66,6,11v1H4v2h2v1c0,0.34,0.04,0.67,0.09,1H4v2h2.81c1.04,1.79,2.97,3,5.19,3c2.221,0,4.15-1.21,5.189-3H20v-2h-2.09c0.05-0.33,0.09-0.66,0.09-1v-1h2v-2h-2v-1c0-0.34-0.04-0.67-0.09-1H20V8z"/><path fill="#9CCC65" d="M14,16h-4v-2h4V16z M14,12h-4v-2h4V12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>bug_report</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#00BCD4" d="M15.995,11c1.66,0,2.99-1.34,2.99-3s-1.33-3-2.99-3s-3,1.34-3,3S14.335,11,15.995,11z M15.995,13c-2.33,0-7,1.17-7,3.5V19h14v-2.5C22.995,14.17,18.325,13,15.995,13z"/><path fill="#1976D2" d="M8,11c1.66,0,2.99-1.34,2.99-3S9.66,5,8,5S5,6.34,5,8S6.34,11,8,11z M8,13c-2.33,0-7,1.17-7,3.5V19h14v-2.5C15,14.17,10.33,13,8,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>group</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path fill="#546E7A" d="M16.141,12.5c0,1-0.101,1.85-0.301,2.55c-0.199,0.7-0.48,1.271-0.83,1.7c-0.359,0.439-0.789,0.75-1.3,0.95c-0.51,0.2-1.069,0.3-1.7,0.3c-0.62,0-1.18-0.1-1.69-0.3c-0.51-0.2-0.95-0.511-1.31-0.95c-0.36-0.439-0.65-1.01-0.85-1.7c-0.2-0.7-0.3-1.55-0.3-2.55v-2.04c0-1,0.1-1.85,0.3-2.55C8.36,7.21,8.64,6.65,9,6.22c0.36-0.43,0.8-0.74,1.31-0.93C10.81,5.1,11.38,5,12,5c0.63,0,1.189,0.1,1.7,0.29c0.51,0.19,0.95,0.5,1.31,0.93c0.36,0.43,0.641,0.99,0.84,1.69c0.2,0.7,0.301,1.54,0.301,2.55v2.04H16.141z M14.029,10.14c0-0.64-0.049-1.18-0.129-1.62c-0.09-0.44-0.221-0.79-0.4-1.06C13.33,7.19,13.109,7,12.859,6.88C12.609,6.75,12.32,6.69,12,6.69s-0.61,0.06-0.86,0.18s-0.47,0.31-0.64,0.58c-0.17,0.27-0.31,0.62-0.4,1.06c-0.09,0.44-0.13,0.98-0.13,1.62v2.67c0,0.64,0.05,1.181,0.14,1.62c0.09,0.45,0.23,0.811,0.4,1.09c0.17,0.28,0.39,0.48,0.64,0.61s0.54,0.19,0.87,0.19s0.621-0.061,0.871-0.19s0.459-0.33,0.629-0.61c0.17-0.279,0.301-0.64,0.391-1.09s0.13-0.99,0.13-1.62v-2.66H14.029z"/></svg>			</div>
			<div class="icon-set__id">
				<span>exposure_zero</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#78909C" points="12.971,12.9 12.971,2.9 10.97,2.9 10.97,12.9 7.97,12.9 11.97,16.9 15.971,12.9 "/><rect x="3.97" y="18.9" fill="#546E7A" width="16" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#78909C" points="11.97,2.9 11.97,16.9 7.97,12.9 10.97,12.9 10.97,2.9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>vertical_align_bottom</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#00838F" d="M20.939,11C20.48,6.83,17.17,3.52,13,3.06V1h-2v2.06C6.83,3.52,3.52,6.83,3.06,11H1v2h2.06c0.46,4.17,3.77,7.48,7.94,7.939V23h2v-2.061c4.17-0.459,7.48-3.77,7.939-7.939H23v-2H20.939z"/><path fill="#B2EBF2" d="M12,19c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,19,12,19z"/><path fill="none" d="M0,0h24v24H0V0z"/><circle fill="#006064" cx="12" cy="12" r="4"/></svg>			</div>
			<div class="icon-set__id">
				<span>my_location</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_2_"><g id="Capa_1_1_"><path fill="#78909C" d="M7,9H3v6h4l5,5V4L7,9z M14,7.97v8.05c1.48-0.729,2.5-2.25,2.5-4.021C16.5,10.23,15.48,8.71,14,7.97zM14,3.23v2.06c2.891,0.86,5,3.54,5,6.71s-2.109,5.85-5,6.71v2.06c4.01-0.908,7-4.486,7-8.77C21,7.72,18.01,4.14,14,3.23z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#90A4AE" points="12,4 12,20 7,15 7,9 		"/></g><g id="Capa_2"><rect x="3" y="9" fill="#78909C" width="4" height="6"/></g></g><g id="Capa_2_1_"><g id="Capa_3"><polygon fill="#9FA8DA" points="3.033,4.25 4.299,2.969 21.033,19.688 19.768,20.969 		"/></g></g><path fill="none" d="M26.667-2.829h24v24h-24V-2.829z"/></svg>			</div>
			<div class="icon-set__id">
				<span>volume_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#455A64" d="M7,18c-1.1,0-1.99,0.9-1.99,2S5.9,22,7,22s2-0.9,2-2S8.1,18,7,18z"/><path fill="#D32F2F" d="M7.2,14.63l-0.03,0.12c0,0.141,0.11,0.25,0.25,0.25H19v2H7c-1.1,0-2-0.9-2-2c0-0.35,0.09-0.68,0.25-0.96l1.35-2.45L3.03,4.06L3,4H1V2h3.27l0.94,2H20c0.55,0,1,0.45,1,1c0,0.17-0.04,0.34-0.12,0.48l-3.58,6.49c-0.34,0.62-1,1.03-1.75,1.03H8.1L7.2,14.63z"/><path fill="#455A64" d="M17,18c-1.1,0-1.99,0.9-1.99,2S15.9,22,17,22s2-0.9,2-2S18.1,18,17,18z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#607D8B" points="5.21,4 1,4 1,2 4.27,2 	"/><path fill="#607D8B" d="M19,15v2H7c-1.1,0-2-0.9-2-2c0-0.35,0.09-0.68,0.25-0.96l1.35-2.45c0,0,0.44,1.36,1.45,1.41H8.1l-0.9,1.63l-0.03,0.12c0,0.141,0.11,0.25,0.25,0.25H19z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>shopping_cart</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M20,4h-3.17L15,2H9L7.17,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4z"/><circle fill="#1A237E" cx="12.008" cy="11.977" r="4.961"/><path fill="#F06292" d="M12,7c1.63,0,3.061,0.79,3.98,2H12c-1.66,0-3,1.34-3,3c0,0.35,0.07,0.689,0.18,1H7.1C7.04,12.68,7,12.34,7,12C7,9.24,9.24,7,12,7z"/><path fill="#4FC3F7" d="M12,17c-1.63,0-3.06-0.79-3.98-2H12c1.66,0,3-1.34,3-3c0-0.35-0.07-0.69-0.18-1h2.08c0.07,0.32,0.1,0.66,0.1,1C17,14.76,14.76,17,12,17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>party_mode</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="2.708" y="4.25" fill="#C5CAE9" width="18.667" height="15.375"/><polygon fill="#9FA8DA" points="19,12 17,12 17,15 14,15 14,17 19,17 "/><polygon fill="#9FA8DA" points="7,9 10,9 10,7 5,7 5,12 7,12 "/><path fill="#5C6BC0" d="M21,3H3C1.9,3,1,3.9,1,5v14c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2V5C23,3.9,22.1,3,21,3z M21,19.01H3V4.99h18V19.01z"/><path fill="none" d="M0,0v24h24V0H0z M3,4.99h18v14.02H3V4.99z"/></svg>			</div>
			<div class="icon-set__id">
				<span>aspect_ratio</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path opacity="0.6" fill="#F57C00" enable-background="new    " d="M2.979,19c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2h-14c-1.11,0-2,0.9-2,2S2.979,17.167,2.979,19z M4.979,5h14v14h-14V5z"/><polygon fill="#FB8C00" points="13.979,3 13.979,5 17.568,5 7.739,14.83 9.149,16.24 18.979,6.41 18.979,10 20.979,10 20.979,3 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>open_in_new</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#7B1FA2" points="7.88,3.39 6.6,1.86 2,5.71 3.29,7.24 "/><polygon fill="#7B1FA2" points="22,5.72 17.4,1.86 16.109,3.39 20.71,7.25 "/><path fill="#AB47BC" d="M12,4c-4.97,0-9,4.03-9,9s4.02,9,9,9c4.971,0,9-4.03,9-9S16.971,4,12,4z"/><path fill="#BBDEFB" d="M12,20c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,20,12,20z"/><polygon fill="#7B1FA2" points="9,11 12.63,11 9,15.2 9,17 15,17 15,15 11.37,15 15,10.8 15,9 9,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>snooze</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#00BCD4" fill-opacity="0.36" d="M0,20h24v4H0V20z"/><path fill="#607D8B" d="M11,3L5.5,17h2.25l1.12-3h6.25l1.12,3h2.25L13,3H11z M9.62,12L12,5.67L14.38,12H9.62z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_color_text</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#607D8B" d="M5,16h3v3h2v-5H5V16z M16,8V5h-2v5h5V8H16z"/><path fill="#455A64" d="M8,8H5v2h5V5H8V8z M14,19h2v-3h3v-2h-5V19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>fullscreen_exit</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#B0BEC5" d="M14,1H4C2.9,1,2,1.9,2,3v12c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V3C16,1.9,15.1,1,14,1z"/><path fill="#E0E0E0" d="M19,5H8C6.9,5,6,5.9,6,7v14c0,1.1,0.9,2,2,2h11c1.1,0,2-0.9,2-2V7C21,5.9,20.1,5,19,5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>content_copy</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFD54F" d="M10,12c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S11.1,12,10,12z"/><path fill="#FFCA28" d="M6,8c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S7.1,8,6,8z"/><path fill="#FFCA28" d="M6,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S7.1,16,6,16z"/><path fill="#FFD54F" d="M18,8c1.1,0,2-0.9,2-2s-0.9-2-2-2s-2,0.9-2,2S16.9,8,18,8z"/><path fill="#FFCA28" d="M14,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S15.1,16,14,16z"/><path fill="#FFD54F" d="M18,12c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S19.1,12,18,12z"/><path fill="#FFCA28" d="M14,8c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S15.1,8,14,8z"/><path fill="#FFD54F" d="M10,4C8.9,4,8,4.9,8,6s0.9,2,2,2s2-0.9,2-2S11.1,4,10,4z"/></svg>			</div>
			<div class="icon-set__id">
				<span>grain</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#BA68C8" d="M7.52,21.48C4.25,19.939,1.91,16.76,1.55,13h-1.5C0.56,19.16,5.71,24,12,24l0.66-0.03l-3.81-3.81L7.52,21.48z"/><path fill="#29B6F6" d="M8.41,14.96c-0.19,0-0.37-0.03-0.52-0.08c-0.16-0.06-0.29-0.13-0.4-0.239c-0.11-0.101-0.2-0.221-0.26-0.371c-0.06-0.14-0.09-0.3-0.09-0.47h-1.3c0,0.36,0.07,0.681,0.21,0.95c0.14,0.27,0.33,0.5,0.56,0.689c0.24,0.181,0.51,0.318,0.82,0.41C7.73,15.95,8.05,16,8.39,16c0.37,0,0.72-0.05,1.03-0.15c0.32-0.1,0.6-0.25,0.83-0.438c0.23-0.189,0.42-0.432,0.55-0.723C10.93,14.4,11,14.08,11,13.72c0-0.188-0.02-0.38-0.07-0.56c-0.05-0.18-0.12-0.35-0.23-0.51c-0.1-0.16-0.24-0.3-0.4-0.433c-0.17-0.13-0.37-0.229-0.61-0.31c0.2-0.09,0.37-0.2,0.52-0.33s0.27-0.27,0.37-0.42c0.1-0.15,0.17-0.3,0.22-0.46c0.05-0.16,0.07-0.32,0.07-0.48c0-0.36-0.06-0.68-0.18-0.96c-0.12-0.28-0.29-0.51-0.51-0.69c-0.2-0.19-0.47-0.33-0.77-0.43C9.1,8.05,8.76,8,8.39,8c-0.36,0-0.69,0.05-1,0.16C7.09,8.27,6.82,8.42,6.6,8.61C6.39,8.8,6.22,9.02,6.09,9.28c-0.12,0.26-0.18,0.54-0.18,0.85h1.3c0-0.17,0.03-0.32,0.09-0.45c0.06-0.13,0.14-0.25,0.25-0.34c0.11-0.09,0.23-0.17,0.38-0.22c0.15-0.05,0.3-0.08,0.48-0.08c0.4,0,0.7,0.1,0.89,0.31c0.19,0.2,0.29,0.49,0.29,0.86c0,0.18-0.03,0.34-0.08,0.49c-0.05,0.15-0.14,0.27-0.25,0.37c-0.11,0.1-0.25,0.18-0.41,0.24c-0.16,0.06-0.36,0.09-0.58,0.09H7.5v1.03h0.77c0.22,0,0.42,0.02,0.6,0.07c0.18,0.05,0.33,0.13,0.45,0.23c0.12,0.107,0.22,0.237,0.29,0.397c0.07,0.16,0.1,0.353,0.1,0.57c0,0.407-0.12,0.72-0.35,0.93C9.13,14.859,8.81,14.96,8.41,14.96z"/><path fill="#29B6F6" d="M17.71,10.24c-0.181-0.47-0.431-0.87-0.75-1.2s-0.7-0.59-1.14-0.77C15.391,8.09,14.9,8,14.359,8H12v8h2.3c0.55,0,1.062-0.09,1.511-0.27c0.449-0.183,0.84-0.433,1.16-0.763c0.319-0.329,0.569-0.729,0.739-1.188c0.17-0.472,0.261-0.99,0.261-1.57v-0.4C17.971,11.23,17.88,10.71,17.71,10.24z M16.57,12.2c0,0.42-0.051,0.79-0.141,1.13c-0.102,0.33-0.24,0.62-0.432,0.85c-0.189,0.23-0.432,0.41-0.71,0.53c-0.29,0.12-0.62,0.181-0.99,0.181h-0.909V9.12h0.971c0.721,0,1.271,0.23,1.641,0.69c0.38,0.46,0.57,1.12,0.57,1.99V12.2z"/><path fill="#BA68C8" d="M12,0l-0.66,0.03l3.811,3.81l1.33-1.33c3.271,1.55,5.609,4.72,5.959,8.48h1.5C23.439,4.84,18.29,0,12,0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>3d_rotation</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4E342E" d="M18.993,3.019h-4.181c-0.42-1.16-1.521-2-2.819-2c-1.299,0-2.4,0.84-2.82,2h-4.18c-1.1,0-2,0.9-2,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2v-14C20.993,3.918,20.093,3.019,18.993,3.019z"/><path fill="#D7CCC8" d="M19.701,6.556c0-1.355-1.102-2.454-2.455-2.454H6.781c-1.356,0-2.455,1.099-2.455,2.454v10.676c0,1.354,1.099,2.453,2.455,2.453h10.465c1.354,0,2.455-1.099,2.455-2.453V6.556z"/><circle fill="#F1F8E9" cx="11.993" cy="4.019" r="1"/></g><path fill="#039BE5" d="M12,7.346c1.66,0,3,1.34,3,3c0,1.66-1.34,3-3,3s-3-1.34-3-3C9,8.686,10.34,7.346,12,7.346z"/><path fill="#0288D1" d="M18,19H6v-1.4c0-2,4-3.1,6-3.1s6,1.1,6,3.1V19z"/><path fill="#FFA000" d="M7.01,6.24l9.984,0.031l0.002-1.236v-2h-2.18c-0.42-1.16-1.521-2-2.82-2s-2.4,0.84-2.82,2h-2.18v2L7.01,6.24z M11.998,3.035c0.549,0,1,0.45,1,1c0,0.55-0.451,1-1,1c-0.551,0-1-0.45-1-1C10.998,3.484,11.447,3.035,11.998,3.035z"/></svg>			</div>
			<div class="icon-set__id">
				<span>assignment_ind</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26A69A" d="M12,2C6.47,2,2,6.47,2,12s4.47,10,10,10c5.529,0,10-4.47,10-10S17.529,2,12,2z M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/></svg>			</div>
			<div class="icon-set__id">
				<span>panorama_fisheye</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#FFEE58" points="6.76,4.84 4.96,3.05 3.55,4.46 5.34,6.25 "/><rect x="1" y="10.5" fill="#FFEE58" width="3" height="2"/><rect x="11" y="0.55" fill="#FFEE58" width="2" height="2.95"/><rect x="17.853" y="3.384" transform="matrix(0.7071 0.7071 -0.7071 0.7071 8.809 -11.9671)" fill="#FFEE58" width="1.994" height="2.531"/><polygon fill="#FFEE58" points="17.24,18.16 19.029,19.96 20.439,18.55 18.641,16.76 "/><rect x="20" y="10.5" fill="#FFEE58" width="3" height="2"/><path fill="#FDD835" d="M12,5.5c-3.31,0-6,2.69-6,6c0,3.311,2.69,6,6,6c3.311,0,6-2.689,6-6C18,8.19,15.311,5.5,12,5.5z"/><rect x="11" y="19.5" fill="#FFEE58" width="2" height="2.95"/><polygon fill="#FFEE58" points="3.55,18.54 4.96,19.95 6.75,18.15 5.34,16.74 "/></svg>			</div>
			<div class="icon-set__id">
				<span>wb_sunny</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#81C784" d="M20,2H4C2.9,2,2.01,2.9,2.01,4L2,22l4-4h14c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2z"/><rect x="6" y="12" fill="#455A64" width="2" height="2"/><rect x="6" y="9" fill="#455A64" width="2" height="2"/><rect x="6" y="6" fill="#455A64" width="2" height="2"/><rect x="10" y="12" fill="#78909C" width="5" height="2"/><rect x="10" y="9" fill="#78909C" width="8" height="2"/><rect x="10" y="6" fill="#78909C" width="8" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>speaker_notes</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36">      
<g data-iconmelon="Smallicons:e4585c28e46f8cd7fe09ef1a5fca34db">
<path fill="#BDC2C5" d="M13.501 5.864l3.002-3.91 3.009 3.91h2.488l-4.083-5.31c-.754-.739-2.073-.739-2.827 0l-.092.103-3.998 5.207h2.501z"></path>
<path fill="#F5BB69" d="M2 5.721h28c1.104 0 2 .896 2 2v20c0 1.105-.896 2-2 2h-28c-1.104 0-2-.895-2-2v-20c0-1.106.896-2 2-2z"></path>
<path fill="#37A18E" d="M4 7.721h24c1.104 0 2 .896 2 2v16c0 1.105-.896 2-2 2h-24c-1.104 0-2-.895-2-2v-16c0-1.106.896-2 2-2z"></path>
<path fill="#E4E7E7" d="M10.787 18.68c.584-.604 1.222-1.26 1.222-2.471 0-1.463-1.232-2.473-2.485-2.473-1.337 0-2.517.819-2.517 2.793 0 .262.073.354.282.354h.919c.219 0 .271-.131.271-.332 0-.688.49-1.213 1.055-1.213.501 0 .888.263.888.881 0 .617-.658 1.261-1.326 1.914-1.013.974-1.9 1.865-1.9 1.865-.094.084-.188.238-.188.5v.879c0 .273.042.347.282.347h4.322c.24 0 .303-.132.303-.347v-.975c0-.215-.062-.346-.303-.346h-2.204c0 .004.763-.744 1.379-1.376zm7.824-1.834h-1.744v-1.735c0-.232-.077-.375-.374-.375h-1.008c-.297 0-.374.143-.374.375v1.735h-1.73c-.232 0-.375.078-.375.375v1.011c0 .299.143.375.375.375h1.73v1.735c0 .232.077.375.374.375h1.008c.297 0 .374-.143.374-.375v-1.735h1.744c.231 0 .373-.076.373-.375v-1.011c0-.297-.14-.375-.373-.375zm6.002 3.216h-2.203s.763-.75 1.379-1.382c.584-.604 1.223-1.26 1.223-2.473 0-1.461-1.232-2.471-2.486-2.471-1.336 0-2.517.817-2.517 2.793 0 .262.073.356.282.356h.919c.22 0 .271-.133.271-.334 0-.688.49-1.213 1.056-1.213.501 0 .887.263.887.881 0 .617-.657 1.261-1.325 1.912-1.014.978-1.9 1.867-1.9 1.867-.094.084-.188.238-.188.5v.879c0 .273.042.347.282.347h4.321c.24 0 .304-.132.304-.347v-.975c-.002-.209-.064-.34-.305-.34z"></path>
</g>
</svg>						</div>
			<div class="icon-set__id">
				<span>chalkboard</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#9E9E9E" d="M13.41,13l-9.5-9.5c-1.56,1.56-1.56,4.09,0,5.66l6.67,6.67L12,14.41l6.88,6.88l1.41-1.41L13.41,13z"/><path fill="#BDBDBD" d="M14.88,11.53c1.53,0.71,3.681,0.21,5.271-1.38c1.91-1.91,2.279-4.65,0.81-6.12c-1.46-1.46-4.2-1.1-6.12,0.81c-1.59,1.59-2.09,3.74-1.38,5.27L3.7,19.87l1.41,1.41L14.88,11.53z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_restaurant</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_2"><rect x="5.203" y="11.281" fill="#90A4AE" width="14.281" height="9.719"/></g><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#607D8B" d="M20,5h-3.17L15,3H9L7.17,5H4C2.9,5,2,5.9,2,7v12c0,1.1,0.9,2,2,2h7v-2.09C8.17,18.43,6,15.97,6,13h2c0,2.21,1.79,4,4,4s4-1.79,4-4h2c0,2.97-2.17,5.43-5,5.91V21h7c1.1,0,2-0.9,2-2V7C22,5.9,21.1,5,20,5z"/><path fill="#1565C0" d="M14,13c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2V9c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2V13z"/><path fill="none" d="M17.708,2.416h24v24h-24V2.416z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>perm_camera_m24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#7E57C2" width="2" height="2"/><rect x="7" y="3" fill="#7E57C2" width="2" height="2"/><rect x="7" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="19" fill="#7E57C2" width="2" height="2"/><rect x="11" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="15" fill="#7E57C2" width="2" height="2"/><rect x="3" y="7" fill="#7E57C2" width="2" height="2"/><rect x="11" y="15" fill="#7E57C2" width="2" height="2"/><rect x="15" y="11" fill="#7E57C2" width="2" height="2"/><rect x="19" y="3" fill="#512DA8" width="2" height="18"/><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><rect x="15" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="11" fill="#7E57C2" width="2" height="2"/><rect x="11" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="7" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_right</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z"/><rect x="7" y="10" fill="#BA68C8" width="2" height="7"/><rect x="11" y="7" fill="#AB47BC" width="2" height="10"/><rect x="15" y="13" fill="#BA68C8" width="2" height="4"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>poll</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90CAF9" d="M20,4H4C2.9,4,2.01,4.9,2.01,6L2,18c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4z"/><rect x="4" y="14" fill="#FFEE58" width="11" height="4"/><rect x="4" y="9" fill="#66BB6A" width="11" height="4"/><rect x="16" y="9" fill="#26A69A" width="4" height="9"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>web</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M18,2H6C4.9,2,4,2.9,4,4v16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V4C20,2.9,19.1,2,18,2z M11,12l-2.5-1.5L6,12V4h5V12z"/><polygon fill="#FFD54F" points="11,12 8.56,10.54 8.5,10.5 6,12 5.97,1 10.98,1 "/><polygon opacity="0.5" fill="#FFCA28" points="11,12 8.56,10.54 8.56,1 10.98,1 "/></svg>			</div>
			<div class="icon-set__id">
				<span>class</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M17,1.01L7,1C5.9,1,5,1.9,5,3v18c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V3C19,1.9,18.1,1.01,17,1.01z"/><rect x="7" y="5" fill="#BBDEFB" width="10" height="14"/><rect x="11" y="7" fill="#37474F" width="2" height="2"/><rect x="11" y="11" fill="#37474F" width="2" height="6"/></svg>			</div>
			<div class="icon-set__id">
				<span>perm_device_info</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4.667" y="5.167" fill="#FFCC80" width="14.458" height="13.25"/><circle fill="#FFEB3B" cx="12" cy="12" r="4"/><path fill="#FFEB3B" d="M20,8.69V4h-4.689L12,0.69L8.69,4H4v4.69L0.69,12L4,15.311V20h4.69L12,23.311L15.311,20H20v-4.689L23.311,12L20,8.69z M12,18c-3.31,0-6-2.689-6-6c0-3.31,2.69-6,6-6c3.311,0,6,2.69,6,6C18,15.311,15.311,18,12,18z"/></svg>			</div>
			<div class="icon-set__id">
				<span>brightness_7</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><circle fill="#4FC3F7" cx="6" cy="14" r="1"/><circle fill="#4FC3F7" cx="6" cy="18" r="1"/><circle fill="#4FC3F7" cx="6" cy="10" r="1"/><path fill="#81D4FA" d="M3,9.5c-0.28,0-0.5,0.22-0.5,0.5s0.22,0.5,0.5,0.5s0.5-0.22,0.5-0.5S3.28,9.5,3,9.5z"/><circle fill="#4FC3F7" cx="6" cy="6" r="1"/><path fill="#81D4FA" d="M21,10.5c0.279,0,0.5-0.22,0.5-0.5S21.279,9.5,21,9.5s-0.5,0.22-0.5,0.5S20.721,10.5,21,10.5z"/><circle fill="#4FC3F7" cx="14" cy="6" r="1"/><path fill="#81D4FA" d="M14,3.5c0.279,0,0.5-0.22,0.5-0.5S14.279,2.5,14,2.5S13.5,2.72,13.5,3S13.721,3.5,14,3.5z"/><path fill="#81D4FA" d="M3,13.5c-0.28,0-0.5,0.22-0.5,0.5s0.22,0.5,0.5,0.5s0.5-0.22,0.5-0.5S3.28,13.5,3,13.5z"/><path fill="#81D4FA" d="M10,20.5c-0.28,0-0.5,0.22-0.5,0.5s0.22,0.5,0.5,0.5s0.5-0.22,0.5-0.5S10.28,20.5,10,20.5z"/><path fill="#81D4FA" d="M10,3.5c0.28,0,0.5-0.22,0.5-0.5S10.28,2.5,10,2.5S9.5,2.72,9.5,3S9.72,3.5,10,3.5z"/><circle fill="#4FC3F7" cx="10" cy="6" r="1"/><circle fill="#039BE5" cx="10" cy="14" r="1.5"/><circle fill="#4FC3F7" cx="18" cy="14" r="1"/><circle fill="#4FC3F7" cx="18" cy="18" r="1"/><circle fill="#4FC3F7" cx="18" cy="10" r="1"/><circle fill="#4FC3F7" cx="18" cy="6" r="1"/><path fill="#81D4FA" d="M21,13.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S21.279,13.5,21,13.5z"/><circle fill="#4FC3F7" cx="14" cy="18" r="1"/><path fill="#81D4FA" d="M14,20.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S14.279,20.5,14,20.5z"/><circle fill="#039BE5" cx="10" cy="10" r="1.5"/><circle fill="#4FC3F7" cx="10" cy="18" r="1"/><circle fill="#039BE5" cx="14" cy="14" r="1.5"/><circle fill="#039BE5" cx="14" cy="10" r="1.5"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>blur_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M24,0v9.05c0-0.05-2.03-3.43-2.03-3.43L17.98,2.28c0,0-3.921-0.3-3.961-0.23c0.07,0.06,0.141,0.13,0.19,0.2C18.67,3.25,22,7.24,22,12c0,3.18-1.48,6.01-3.8,7.84c0,0.09,0,0.141,0,0.141l0.359,0.05c0,0,1.471-0.12,1.58-0.37c0.11-0.25,2.45-4.19,2.45-4.19S24,9.09,24,9.05V24H0V13.42c0.02,0,3.38,8.1,3.38,8.1l7.65,0.98l-0.01-0.55C5.96,21.46,2,17.19,2,12c0-1.15,0.19-2.25,0.56-3.28L2.47,8.64c0,0-0.19,0.2-0.2,0.25C2.25,8.94,0.03,13.42,0,13.42V0H24z"/><path fill="#01579B" d="M22,12c0,3.18-1.48,6.01-3.8,7.84C16.5,21.19,14.34,22,12,22c-0.33,0-0.66-0.02-0.98-0.05C5.96,21.46,2,17.19,2,12c0-1.15,0.19-2.25,0.56-3.28C3.91,4.81,7.63,2,12,2c0.76,0,1.5,0.09,2.21,0.25C18.67,3.25,22,7.24,22,12z"/><path fill="#03A9F4" d="M11,19.93c-3.95-0.49-7-3.85-7-7.93c0-0.62,0.08-1.21,0.21-1.79L9,15v1c0,1.1,0.9,2,2,2V19.93z"/><path fill="#03A9F4" d="M17.9,17.391C17.641,16.58,16.9,16,16,16h-1v-3c0-0.55-0.45-1-1-1H8v-2h2c0.55,0,1-0.45,1-1V7h2c1.1,0,2-0.9,2-2V4.59c2.93,1.19,5,4.06,5,7.41C20,14.08,19.2,15.97,17.9,17.391z"/></g><path fill="#03A9F4" d="M22,12c0,3.18-1.48,6.01-3.8,7.84c-0.01-0.439-0.061-1.79-0.29-2.47c-0.01-0.03-0.021-0.061-0.03-0.09c-0.31-0.761-1.1-2.19-1.1-2.19L14.77,7.53l0.82-2.5l-0.5-0.4L15,4.56c0,0,0.11-1.32-0.79-2.31C18.67,3.25,22,7.24,22,12z"/><path fill="#03A9F4" d="M11.02,21.95C5.96,21.46,2,17.19,2,12c0-1.15,0.19-2.25,0.56-3.28l1.64,1.45l4.83,4.78c0,0-0.02,0.069-0.03,0.2c-0.04,0.26-0.07,0.729,0.03,1.21c0.06,0.26,0.16,0.529,0.32,0.77c0.07,0.101,0.15,0.2,0.24,0.28c0.08,0.09,0.16,0.149,0.25,0.22c0.03,0.021,0.07,0.04,0.1,0.061c0.07,0.04,0.14,0.079,0.21,0.109c0.23,0.101,0.5,0.17,0.82,0.2h0.01L11,19.8v0.13L11.02,21.95z"/></svg>			</div>
			<div class="icon-set__id">
				<span>publ24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#F44336" points="19,4 12,11 6.5,5.5 6.5,9 5,9 5,3 11,3 11,4.5 7.5,4.5 12,9 18,3 	"/><polygon fill="#E57373" points="19,4 12,11 11.97,10.97 11.97,8.97 12,9 18,3 	"/></g><g id="Capa_2"><g><path fill="#EF5350" d="M24,17.784c0,0.28-0.109,0.53-0.29,0.71l-2.479,2.479c-0.181,0.181-0.431,0.29-0.71,0.29c-0.271,0-0.521-0.1-0.7-0.279c-0.8-0.73-1.69-1.36-2.67-1.851c-0.33-0.16-0.561-0.51-0.561-0.899v-3.101c-1.449-0.47-3-0.72-4.6-0.72s-3.15,0.25-4.6,0.73v3.1c0,0.399-0.23,0.74-0.561,0.899c-0.97,0.49-1.87,1.11-2.66,1.851c-0.18,0.17-0.43,0.28-0.699,0.28c-0.28,0-0.53-0.11-0.711-0.29l-2.34-2.341l-0.14-0.14c-0.18-0.18-0.29-0.43-0.29-0.71s0.11-0.53,0.29-0.71c3.05-2.891,7.17-4.67,11.71-4.67s8.66,1.779,11.71,4.67C23.88,17.264,24,17.504,24,17.784z"/><path fill="#E53935" d="M24,17.784c0,0.28-0.109,0.53-0.29,0.71l-0.47,0.47c-2.24-1.96-4.48-3.149-6.65-3.8c-3.38-1.02-6.569-0.73-9.199,0.01c-3.98,1.12-6.7,3.26-6.971,3.47l-0.14-0.14c-0.18-0.18-0.29-0.43-0.29-0.71s0.11-0.53,0.29-0.71c3.05-2.891,7.17-4.67,11.71-4.67s8.66,1.779,11.71,4.67C23.88,17.264,24,17.504,24,17.784z"/></g></g></svg>			</div>
			<div class="icon-set__id">
				<span>phone_missed</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M5,7h11l3.55,5L16,17H5V7z"/><path d="M17.63,5.84C17.27,5.33,16.67,5,16,5h-2.96L5,5.01C3.9,5.01,3,5.9,3,7v10c0,1.1,0.9,1.99,2,1.99L13.04,19H16c0.67,0,1.27-0.33,1.63-0.84L22,12L17.63,5.84z M16,17H5V7h11l3.55,5L16,17z"/><g><path fill="none" d="M0,0v24h24V0H0z M17.63,18.16C17.27,18.67,16.67,19,16,19h-2.96L5,18.99c-1.1,0-2-0.891-2-1.99V7c0-1.1,0.9-1.99,2-1.99L13.04,5H16c0.67,0,1.27,0.33,1.63,0.84L22,12L17.63,18.16z"/><path fill="#F57C00" d="M5,17h9.46l-1.42,2L5,18.99c-1.1,0-2-0.891-2-1.99V7c0-1.1,0.9-1.99,2-1.99L13.04,5l1.42,2H5V17z"/><path fill="#FF5722" d="M22,12l-4.37,6.16C17.27,18.67,16.67,19,16,19h-2.96l1.42-2H16l3.55-5L16,7h-1.54l-1.42-2H16c0.67,0,1.27,0.33,1.63,0.84L22,12z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>label_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3.958" y="4" fill="#B3E5FC" width="16.042" height="16.125"/><path fill="#546E7A" d="M4,4h7V2H4C2.9,2,2,2.9,2,4v7h2V4z"/><polygon fill="#66BB6A" points="10,13 6,18 18,18 15,14 12.971,16.71 "/><circle fill="#FFEE58" cx="15.5" cy="8.5" r="1.5"/><path fill="#78909C" d="M20,2h-7v2h7v7h2V4C22,2.9,21.1,2,20,2z"/><path fill="#546E7A" d="M20,20h-7v2h7c1.1,0,2-0.9,2-2v-7h-2V20z"/><path fill="#78909C" d="M4,13H2v7c0,1.1,0.9,2,2,2h7v-2H4V13z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>now_wallpaper</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#795548" d="M20.54,5.23l-1.39-1.68C18.88,3.21,18.471,3,18,3H6C5.53,3,5.12,3.21,4.84,3.55L3.46,5.23C3.17,5.57,3,6.02,3,6.5V19c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V6.5C21,6.02,20.83,5.57,20.54,5.23z"/><polygon fill="#FB8C00" points="12,17.5 6.5,12 10,12 10,10 14,10 14,12 17.5,12 "/><polygon fill="#5D4037" points="5.12,5 5.93,4 17.93,4 18.87,5 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>archive</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="10" y="4" fill="#8E24AA" width="4" height="16"/><rect x="4" y="12" fill="#BA68C8" width="4" height="8"/><rect x="16" y="9" fill="#AB47BC" width="4" height="11"/></svg>			</div>
			<div class="icon-set__id">
				<span>equalizer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M17,1.01L7,1C5.9,1,5,1.9,5,3v18c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V3C19,1.9,18.1,1.01,17,1.01z"/><rect x="7" y="5" fill="#BBDEFB" width="10" height="14"/><polygon fill="#66BB6A" points="16,13 13,13 13,8 11,8 11,13 8,13 12,17 "/></svg>			</div>
			<div class="icon-set__id">
				<span>system_update</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="2.99" y="5" opacity="0.7" fill="#C5CAE9" enable-background="new    " width="18.01" height="14"/><path opacity="0.6" fill="#BA68C8" enable-background="new    " d="M3,21h18c1.1,0,2-0.9,2-2v-3h-2v3H3V5h7V3H3C1.9,3,1,3.9,1,5v14C1,20.1,1.9,21,3,21z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CE93D8" d="M21,3H3C1.9,3,1,3.9,1,5v3h2V5h18v14h-7v2h7c1.1,0,2-0.9,2-2V5C23,3.9,22.1,3,21,3z"/><path fill="#CE93D8" d="M21,3H3C1.9,3,1,3.9,1,5v3h2V5h18v14h-7v2h7c1.1,0,2-0.9,2-2V5C23,3.9,22.1,3,21,3z"/><path fill="#42A5F5" d="M1,18v3h3C4,19.34,2.66,18,1,18z"/><path fill="#42A5F5" d="M1,14v2c2.76,0,5,2.24,5,5h2C8,17.13,4.87,14,1,14z"/><path fill="#42A5F5" d="M1,10v2c4.97,0,9,4.03,9,9h2C12,14.92,7.07,10,1,10z"/></svg>			</div>
			<div class="icon-set__id">
				<span>cast</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#512DA8" d="M3,3v18h18V3H3z M11,19H5v-6h6V19z M11,11H5V5h6V11z M19,19h-6v-6h6V19z M19,11h-6V5h6V11z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_all</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1976D2" d="M17.702,5.71L11.991,0h-1v7.59L6.401,3l-1.41,1.41l5.59,5.59l-5.59,5.59L6.401,17l4.59-4.59V20h1l5.711-5.71L13.401,10L17.702,5.71z M12.991,3.83l1.881,1.88l-1.881,1.88V3.83z M14.872,14.29l-1.881,1.88v-3.76L14.872,14.29z"/><rect x="11.118" y="22" fill="#546E7A" width="2" height="2"/><rect x="7" y="22" fill="#455A64" width="2" height="2"/><rect x="15" y="22" fill="#455A64" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_bluetooth</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="8" y="21" fill="#455A64" width="8" height="2"/><path fill="#78909C" d="M16,1.01L8,1C6.9,1,6,1.9,6,3v14c0,1.1,0.9,2,2,2h8c1.1,0,2-0.9,2-2V3C18,1.9,17.1,1.01,16,1.01z"/><rect x="8" y="5" fill="#B3E5FC" width="8" height="10"/></svg>			</div>
			<div class="icon-set__id">
				<span>dock</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path d="M7,9v6h4l5,5V4l-5,5H7z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#64B5F6" d="M11,9H7v6h4l5,5V4L11,9z"/><path fill="none" d="M4,0h24v24H4V0z"/><polygon fill="#90A4AE" points="16,4 16,20 11,15 11,9 "/><rect x="7" y="9" fill="#78909C" width="4" height="6"/><path fill="#039BE5" d="M20.525,11.975c0-1.771-1.021-3.291-2.5-4.031v8.05C19.507,15.264,20.525,13.744,20.525,11.975z"/></svg>			</div>
			<div class="icon-set__id">
				<span>volume_down</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#BDBDBD" d="M18,8h-1V6c0-2.76-2.24-5-5-5S7,3.24,7,6v2H6c-1.1,0-2,0.9-2,2v10c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V10C20,8.9,19.1,8,18,8z M9,6c0-0.86,0.32-1.63,0.86-2.19S11.14,2.9,12,2.9c1.71,0,3.1,1.39,3.1,3.1v2H9V6z M18,20H6V10h12V20z"/><path fill="#607D8B" d="M12,17c1.1,0,2-0.9,2-2s-0.9-2-2-2c-1.1,0-2,0.9-2,2S10.9,17,12,17z"/><path fill="#90A4AE" d="M18,8H6c-1.1,0-2,0.9-2,2v10c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V10C20,8.9,19.1,8,18,8z M18,20H6V10h12V20z"/></svg>			</div>
			<div class="icon-set__id">
				<span>lock_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M12,8c1.1,0,2-0.9,2-2s-0.9-2-2-2c-1.1,0-2,0.9-2,2S10.9,8,12,8z"/><path fill="#455A64" d="M12,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C14,10.9,13.1,10,12,10z"/><path fill="#37474F" d="M12,16c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S13.1,16,12,16z"/></svg>			</div>
			<div class="icon-set__id">
				<span>more_vert</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><g><path fill="#FFD54F" d="M9.95,9.639c0,0-3.174-5.414,2.134-8.854c0,0,4.986,2.934,1.947,8.961"/><path fill="#FFCA28" d="M13.045,9.059c0,0,3.111-5.45,8.738-2.565c0,0-0.057,5.786-6.797,6.154"/><path fill="#FFCA28" d="M10.995,9.032c0,0-3.11-5.45-8.738-2.565c0,0,0.056,5.785,6.796,6.155"/><path fill="#FFD54F" d="M13.004,15.016c0,0,3.109,5.448,8.738,2.563c0,0-0.057-5.784-6.797-6.154"/><path fill="#FFD54F" d="M10.953,15.042c0,0-3.11,5.449-8.738,2.563c0,0,0.056-5.783,6.796-6.153"/><path fill="#FFCA28" d="M9.935,14.357c0,0-3.174,5.414,2.134,8.855c0,0,4.988-2.934,1.947-8.963"/><circle fill="#5D4037" cx="12.019" cy="12.062" r="4"/></g><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>filter_vintage</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#1976D2" cx="15" cy="8.13" r="4"/><polygon fill="#8BC34A" points="6,10 6,7 4,7 4,10 1,10 1,12 4,12 4,15 6,15 6,12 9,12 9,10 "/><path fill="#1565C0" d="M23,18v2H7v-2c0-2.66,5.33-4,8-4S23,15.34,23,18z"/><path fill="none" d="M15,12.13v-8c2.21,0,4,1.79,4,4S17.21,12.13,15,12.13z"/><path fill="none" d="M23,18v2h-8v-6C17.67,14,23,15.34,23,18z"/><polygon fill="none" points="39,-2 39,22 29.88,22 29.88,0.08 15,0.08 15,-2 "/></svg>			</div>
			<div class="icon-set__id">
				<span>person_add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#43A047" d="M12,6c0-0.55-0.45-1-1-1H5.82l0.66-3.18L6.5,1.59c0-0.31-0.13-0.59-0.33-0.8L5.38,0L0.44,4.94C0.17,5.21,0,5.59,0,6v6.5C0,13.33,0.67,14,1.5,14h6.75c0.62,0,1.15-0.38,1.38-0.91l2.26-5.29C11.96,7.63,12,7.44,12,7.25V6z"/><path fill="#D32F2F" d="M22.5,10h-6.75c-0.62,0-1.15,0.38-1.38,0.91l-2.26,5.29c-0.07,0.17-0.11,0.36-0.11,0.55V18c0,0.55,0.45,1,1,1h5.18l-0.66,3.18l-0.02,0.24c0,0.311,0.13,0.59,0.33,0.8L18.62,24l4.94-4.939C23.83,18.79,24,18.41,24,18v-6.5C24,10.67,23.33,10,22.5,10z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>thumbs_up_down</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M12,11L2.315,4.947C2.667,4.381,3.287,4,4,4h16c0.712,0,1.335,0.38,1.69,0.944L12,11z M12,13L2.009,6.756L2,18c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6.75L12,13z"/><path fill="#90A4AE" d="M22,6.75V6c0-0.388-0.116-0.749-0.31-1.056L12,11L2.315,4.947C2.125,5.254,2.01,5.613,2.01,6l0,0.756L12,13L22,6.75z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>email</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#C5CAE9" d="M23,5v14c0,1.1-0.9,2-2,2H3c-1.1,0-2-0.9-2-2V5c0-1.1,0.9-2,2-2h18C22.1,3,23,3.9,23,5z"/><polygon fill="#C5CAE9" points="21,19 3,19 3,5 13,5 13,9 21,9 "/><path fill="#0097A7" d="M23,5v3.96H13V3h8C22.1,3,23,3.9,23,5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>tab</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><polygon fill="#CFD8DC" points="15,15 17,15 17,5 15,5 15,9 13,9 13,5 11,5 11,11 15,11 "/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_4</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M18,4H6C4.9,4,4,4.9,4,6v12c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V6C20,4.9,19.1,4,18,4z M18,18H6V6h12V18z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_square</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M-618-568H782v3600H-618V-568z M0,0h24v24H0V0z"/><path fill="#66BB6A" d="M12,4c-5,0-9,4-9,9s4,9,9,9s9-4,9-9S17,4,12,4z"/><path fill="#C8E6C9" d="M12,20c-3.9,0-7-3.1-7-7c0-3.9,3.1-7,7-7c3.9,0,7,3.1,7,7C19,16.9,15.9,20,12,20z"/><polygon fill="#43A047" points="22,5.7 17.4,1.8 16.1,3.3 20.7,7.2 "/><polygon fill="#43A047" points="7.9,3.4 6.6,1.9 2,5.7 3.3,7.2 "/><polygon fill="#43A047" points="12.5,8 11,8 11,14 15.7,16.9 16.5,15.7 12.5,13.3 "/></svg>			</div>
			<div class="icon-set__id">
				<span>access_alarms</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FF9800" d="M15,21l5.641-1.9C20.85,19.03,21,18.85,21,18.62V3.5C21,3.22,20.779,3,20.5,3l-0.16,0.03L15,5.1V21z M9,3L3.36,4.9C3.15,4.97,3,5.15,3,5.38V20.5C3,20.78,3.22,21,3.5,21l0.16-0.03L9,18.9V3z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#FFC107" points="15,21 9,18.891 9,2.99 15,5.1 "/></svg>			</div>
			<div class="icon-set__id">
				<span>map</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFD54F" d="M12.65,10C11.83,7.67,9.61,6,7,6c-3.31,0-6,2.69-6,6v0.04C1.02,15.33,3.7,18,7,18c2.61,0,4.83-1.67,5.65-4H17v4h4v-4h2v-4H12.65z M7,14c-1.09,0-1.98-0.88-2-1.96V12c0-1.1,0.9-2,2-2s2,0.9,2,2v0.04C8.98,13.12,8.09,14,7,14z"/></g><g id="Capa_2"><path fill="#FFB300" d="M23,10v2.04H9V12c0-1.1-0.9-2-2-2s-2,0.9-2,2v0.04H1V12c0-3.31,2.69-6,6-6c2.61,0,4.83,1.67,5.65,4H23z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>vpn_key</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#43A047" d="M6.999,5.999v6h-6v-6h2v-4c0-0.55,0.45-1,1-1s1,0.45,1,1v4H6.999z"/><path fill="#455A64" d="M14.999,13.999v2c0,1.3-0.83,2.4-1.98,2.811c-0.01,0.01-0.01,0.01-0.02,0.01v4.18h-2v-4.18c-0.01,0-0.01-0.012-0.02-0.012c-1.15-0.418-1.98-1.519-1.98-2.809v-2H14.999z"/><path fill="#455A64" d="M6.999,13.999v2c0,1.29-0.83,2.391-1.98,2.811c-0.01,0-0.01,0.01-0.02,0.01v4.18h-2v-4.18c-0.01,0-0.01-0.012-0.02-0.012c-1.15-0.418-1.98-1.519-1.98-2.809v-2H6.999z"/><path fill="#E53935" d="M22.999,5.999v6h-6v-6h2v-4c0-0.55,0.45-1,1-1s1,0.45,1,1v4H22.999z"/><path fill="#039BE5" d="M14.999,5.999v6h-6v-6h2v-4c0-0.55,0.45-1,1-1s1,0.45,1,1v4H14.999z"/><path fill="#455A64" d="M22.999,13.999v2c0,1.3-0.83,2.4-1.98,2.811c-0.01,0.01-0.01,0.01-0.02,0.01v4.18h-2v-4.18c-0.01,0-0.01-0.012-0.02-0.012c-1.15-0.418-1.98-1.519-1.98-2.809v-2H22.999z"/><path fill="#90A4AE" d="M4.999,2.008v3.988h-2V2.008c0-0.554,0.45-1.009,1-1.009S4.999,1.454,4.999,2.008z"/><path fill="#90A4AE" d="M12.999,2.008v3.988h-2V2.008c0-0.554,0.45-1.009,1-1.009S12.999,1.454,12.999,2.008z"/><path fill="#90A4AE" d="M20.999,2.008v3.988h-2V2.008c0-0.554,0.45-1.009,1-1.009S20.999,1.454,20.999,2.008z"/><path fill="#388E3C" d="M0.999,13.999h6v2c0,1.29-0.83,2.391-1.98,2.811H2.979c-1.15-0.42-1.98-1.521-1.98-2.811V13.999z"/><path fill="#0288D1" d="M8.999,13.999h6v2c0,1.3-0.83,2.4-1.98,2.811h-2.039c-1.15-0.42-1.98-1.521-1.98-2.811V13.999z"/><path fill="#C62828" d="M22.999,13.999v2c0,1.3-0.83,2.4-1.98,2.811h-2.039c-1.15-0.42-1.98-1.521-1.98-2.811v-2H22.999z"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_input_component</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><g><path fill="#5C6BC0" d="M13.98,6.007L4.991,17.991l18-0.031C22.991,17.96,14.063,6.007,13.98,6.007z"/><polygon fill="#9FA8DA" points="7.007,10.007 13.022,17.976 1.007,17.991 		"/></g></g></svg>			</div>
			<div class="icon-set__id">
				<span>filter_hdr</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#80CBC4" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.11,4,6.6,5.64,5.35,8.04C2.34,8.36,0,10.91,0,14c0,3.311,2.69,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z"/><polygon fill="#009688" points="17,13 12,18 7,13 10,13 10,9 14,9 14,13 "/></svg>			</div>
			<div class="icon-set__id">
				<span>cloud_download</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M18,2.01L6,2C4.89,2,4,2.89,4,4v16c0,1.109,0.89,2,2,2h12c1.109,0,2-0.891,2-2V4C20,2.89,19.109,2.01,18,2.01z"/><path fill="#B3E5FC" d="M12,20c-3.31,0-6-2.689-6-6c0-3.31,2.69-6,6-6c3.311,0,6,2.69,6,6C18,17.311,15.311,20,12,20z"/><path fill="#E1F5FE" d="M9.17,16.736c1.56,1.561,4.1,1.561,5.66,0c1.561-1.562,1.561-4.101,0-5.66L9.17,16.736z"/><circle fill="#EF5350" cx="10" cy="5" r="1"/><circle fill="#66BB6A" cx="7" cy="5" r="1"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_laundry_service</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#00BCD4" d="M11.991,3C7.023,3,3,7.032,3,12c0,4.967,4.023,9,8.991,9C16.967,21,21,16.967,21,12C21,7.032,16.967,3,11.991,3z"/><path fill="#0097A7" d="M11.99,2C6.47,2,2,6.48,2,12c0,5.52,4.47,10,9.99,10C17.52,22,22,17.52,22,12C22,6.48,17.52,2,11.99,2zM18.92,8h-2.949c-0.32-1.25-0.781-2.45-1.381-3.56C16.43,5.07,17.96,6.35,18.92,8z M12,4.04c0.83,1.2,1.48,2.53,1.91,3.96h-3.82C10.52,6.57,11.17,5.24,12,4.04z M4.26,14C4.1,13.359,4,12.689,4,12c0-0.69,0.1-1.36,0.26-2h3.38c-0.08,0.66-0.14,1.32-0.14,2c0,0.68,0.06,1.34,0.14,2H4.26z M5.08,16h2.95c0.32,1.25,0.78,2.45,1.38,3.561C7.57,18.93,6.04,17.66,5.08,16z M8.03,8H5.08c0.96-1.66,2.49-2.93,4.33-3.56C8.81,5.55,8.35,6.75,8.03,8z M12,19.96c-0.83-1.2-1.48-2.53-1.91-3.96h3.82C13.48,17.43,12.83,18.76,12,19.96z M14.34,14H9.66c-0.09-0.66-0.16-1.32-0.16-2c0-0.68,0.07-1.35,0.16-2h4.68c0.09,0.65,0.16,1.32,0.16,2C14.5,12.68,14.43,13.34,14.34,14z M14.59,19.561c0.6-1.11,1.061-2.311,1.381-3.561h2.949C17.96,17.65,16.43,18.93,14.59,19.561z M16.359,14c0.08-0.66,0.141-1.32,0.141-2c0-0.68-0.061-1.34-0.141-2h3.381C19.9,10.64,20,11.31,20,12c0,0.689-0.1,1.359-0.26,2H16.359z"/></svg>			</div>
			<div class="icon-set__id">
				<span>language</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#F06292" points="3.515,7.758 3.508,13.407 5.629,11.286 10.585,16.242 12,14.828 7.044,9.872 9.165,7.75 "/><polygon fill="#5C6BC0" points="18.371,12.714 13.415,7.757 12,9.172 16.956,14.129 14.835,16.25 20.485,16.242 20.492,10.593 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>import_export</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#37474F" d="M21,6.5v11l-4-4V17c0,0.55-0.45,1-1,1H4c-0.55,0-1-0.45-1-1V7c0-0.55,0.45-1,1-1h12c0.55,0,1,0.45,1,1v3.5l0.02-0.02L21,6.5z"/></g><g id="Capa_2"><polygon fill="#263238" points="21,6.5 21,17.5 17,13.5 17,10.5 17.02,10.48 	"/></g><g id="Capa_3"><path fill="#455A64" d="M17.02,10.48v1.49H3V7c0-0.55,0.45-1,1-1h12c0.55,0,1,0.45,1,1v3.5L17.02,10.48z"/><polygon fill="#37474F" points="21,6.5 21,11.93 17,11.93 17,10.5 17.02,10.48 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>videocam</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#DB4437" d="M24,0v24H0V0H24 M25-1H-1v26h26V-1z"/><path fill="#6D4C41" d="M20,6h-4V4c0-1.11-0.89-2-2-2h-4C8.89,2,8,2.89,8,4v2H4C2.89,6,2.01,6.89,2.01,8v5.5L2,19c0,1.11,0.89,2,2,2h16c1.11,0,2-0.89,2-2V8C22,6.89,21.11,6,20,6z M14,6h-4V4h4V6z"/><path fill="#795548" d="M10,4v2h2v15H4c-1.11,0-2-0.89-2-2l0.01-5.5V8c0-1.11,0.88-2,1.99-2h4V4c0-1.11,0.89-2,2-2h2v2H10z"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>work</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36">
<g data-iconmelon="Iconmelon:cf9e86cbfb7713a1ebee5168bbe803f5">
  <g>
	<g id="_x23_ffffffff">
	  <path fill="#3FB8AF" d="M14.236,26.392c-0.53-2.039-0.833-5.134-0.716-7.112c-2.607-0.077-5.164,0.449-6.09-2.97
c2.658-1.592,4.27,0.46,6.202,1.616c0.132-1.256,0.415-3.201,0.56-4.451c-2.722-0.998-5.639-2.261-7.097-4.932
C5.499,5.973,5.909,2.829,6.401,0c2.021,0.404,4.218,0.733,5.765,2.229c2.084,1.818,3.684,5.583,3.404,8.7
c0.319-2.091,2.013-4.47,3.591-5.278c1.648-0.922,3.599-0.783,5.424-0.89c-0.404,1.755-0.676,3.662-1.9,5.063
c-1.534,1.9-4.446,3.146-6.902,3.64c-1.218,4.223-1.093,8.59,0,12.807"></path>
	</g>
	<path fill="#3B3C3B" d="M13.456,30.383c0,0-2.221-0.154-2.767-1.292c-0.559-1.171,1.028-2.084,1.721-0.893
c-0.465-1.304,1.217-1.952,1.775-0.781C14.731,28.556,13.456,30.383,13.456,30.383z"></path>
	<g>
	  <g>
		<path fill="#3B3C3B" d="M16.938,28.471l-0.004-0.091c-0.015-0.321,0.116-0.796,0.296-1.062l0.238-0.352
	c0.388-0.568,0.142-1.414-0.547-1.88c-0.691-0.473-1.561-0.384-1.948,0.185c-0.004,0.004-0.238,0.357-0.238,0.357
	c-0.181,0.263-0.575,0.565-0.875,0.666l-0.093,0.029c-0.298,0.106-0.333,0.34-0.063,0.516l2.78,1.885
	C16.75,28.905,16.953,28.787,16.938,28.471z"></path>
		<path fill="#3B3C3B" d="M14.674,28.408c0.314,0.209,0.689,0.203,0.896,0.002l-1.233-0.833
	C14.225,27.841,14.362,28.196,14.674,28.408z"></path>
	  </g>
	</g>
	<path fill="#3B3C3B" d="M12.391,31.798c-0.325,0.006-0.495-0.115-0.661-0.266c-0.141,0.01-0.321,0.091-0.43,0.146
c-0.034,0.019-0.066,0.039-0.104,0.057c0.059-0.166,0.145-0.298,0.271-0.396c0.028-0.021,0.056-0.053,0.091-0.064c0,0,0,0,0-0.003
c-0.18,0.003-0.33,0.084-0.477,0.13l0,0c0.076-0.12,0.178-0.24,0.285-0.327c0.045-0.037,0.086-0.07,0.132-0.107
c-0.002-0.192,0.002-0.378,0.041-0.541c0.209-0.949,0.764-1.593,1.642-1.868c0.317-0.103,0.825-0.141,1.186-0.053
c0.18,0.045,0.343,0.096,0.498,0.161c0.083,0.036,0.16,0.078,0.233,0.125c0.025,0.015,0.051,0.03,0.074,0.047
c-0.083-0.003-0.176-0.026-0.266-0.01c-0.083,0.012-0.163,0.01-0.239,0.024c-0.19,0.042-0.36,0.098-0.504,0.181
c-0.07,0.044-0.176,0.092-0.225,0.151c0.095-0.002,0.178,0.021,0.249,0.046c0.269,0.097,0.426,0.273,0.53,0.542
c-0.082-0.008-0.322-0.033-0.375,0.015c0.104,0.005,0.206,0.065,0.277,0.111c0.217,0.138,0.396,0.368,0.393,0.724
c-0.029-0.013-0.057-0.027-0.086-0.041c-0.051-0.022-0.11-0.038-0.174-0.051c-0.028-0.005-0.083-0.021-0.113-0.01
c0,0,0.002,0,0.005,0c0.04,0.051,0.108,0.084,0.152,0.135c0.14,0.178,0.271,0.447,0.187,0.767c-0.02,0.083-0.054,0.154-0.087,0.221
c0,0-0.002,0-0.002-0.003c-0.018-0.033-0.055-0.06-0.079-0.089c-0.069-0.088-0.16-0.167-0.249-0.238
c-0.304-0.241-0.582-0.39-1.025-0.497c-0.11-0.028-0.241-0.052-0.376-0.052c0.039,0.109,0.026,0.292-0.002,0.399
c-0.077,0.269-0.236,0.461-0.475,0.567c-0.056,0.023-0.119,0.042-0.185,0.057C12.46,31.789,12.427,31.791,12.391,31.798z"></path>
	<path fill="#3B3C3B" d="M16.101,26.797c-1.006,0-1.828,0.662-1.828,1.482c0,0.44,0.239,0.835,0.614,1.107
c0.039,0.111,0.021,0.228-0.071,0.385c-0.096,0.175-0.284,0.323-0.543,0.44c0.65-0.047,1.207-0.213,1.526-0.474
c0.096,0.012,0.197,0.023,0.301,0.023c1.01,0,1.825-0.663,1.825-1.482C17.926,27.459,17.111,26.797,16.101,26.797"></path>
	<path fill="#3B3C3B" d="M9.913,28.865c0,0-1.462-1.679-1.043-2.868c0.433-1.224,2.201-0.748,1.848,0.585
c0.593-1.251,2.241-0.521,1.808,0.702C12.105,28.476,9.913,28.865,9.913,28.865z"></path>
	<path fill="#3B3C3B" d="M10.549,30.617c0.711-0.71,0.824-1.76,0.244-2.341c-0.311-0.311-0.759-0.42-1.217-0.349
c-0.106-0.051-0.175-0.146-0.222-0.322c-0.056-0.19-0.028-0.429,0.073-0.695C9,27.403,8.724,27.914,8.683,28.325
c-0.077,0.059-0.156,0.123-0.229,0.195c-0.714,0.716-0.822,1.76-0.242,2.34C8.791,31.439,9.835,31.332,10.549,30.617"></path>
	<path fill="#3B3C3B" d="M5.516,31.311l0.203-0.204c0.013-0.012,0.025-0.019,0.035-0.026c0.048,0.023,0.091,0.038,0.138,0.055
c0.002,0.013,0.004,0.028,0.004,0.044l0.001,0.288c0,0.136,0.11,0.247,0.247,0.247h0.492c0.133-0.002,0.246-0.114,0.246-0.247
l-0.001-0.285c0.001-0.016,0.001-0.034,0.005-0.044C6.93,31.12,6.976,31.1,7.021,31.076c0.009,0.009,0.021,0.019,0.036,0.032
l0.201,0.2c0.097,0.098,0.253,0.099,0.349,0.004l0.348-0.348c0.099-0.099,0.096-0.254-0.001-0.352l-0.201-0.201
c-0.014-0.013-0.025-0.024-0.031-0.036c0.02-0.045,0.04-0.091,0.059-0.135c0.008-0.001,0.026-0.001,0.041-0.001l0.289-0.002
c0.135-0.002,0.244-0.111,0.243-0.244L8.356,29.5c0-0.136-0.111-0.247-0.247-0.247l-0.29,0.002
c-0.017,0.001-0.031-0.003-0.044-0.003c-0.019-0.047-0.033-0.089-0.056-0.138c0.009-0.009,0.016-0.023,0.026-0.033l0.204-0.204
c0.095-0.095,0.097-0.251,0.002-0.346l-0.349-0.349c-0.095-0.095-0.252-0.099-0.35-0.002l-0.204,0.205
c-0.01,0.01-0.021,0.02-0.033,0.025c-0.045-0.02-0.09-0.036-0.138-0.057c-0.002-0.011-0.003-0.026-0.002-0.045v-0.291
c0.003-0.132-0.108-0.242-0.245-0.244l-0.494,0.004c-0.136,0-0.245,0.108-0.243,0.243L5.892,28.31c0.002,0.018,0,0.034-0.001,0.041
c-0.048,0.018-0.094,0.041-0.135,0.061c-0.013-0.009-0.024-0.02-0.035-0.029l-0.204-0.205c-0.095-0.095-0.251-0.099-0.35,0
l-0.348,0.348c-0.095,0.097-0.094,0.253,0.001,0.348l0.205,0.205c0.011,0.01,0.02,0.023,0.03,0.033
c-0.023,0.046-0.042,0.094-0.061,0.137c-0.011,0.004-0.024,0.004-0.044,0.005l-0.285-0.001c-0.137,0-0.247,0.109-0.246,0.245
l0,0.492c0,0.137,0.111,0.248,0.244,0.244l0.289,0.003c0.017,0.001,0.031,0.001,0.043,0.003c0.021,0.049,0.037,0.093,0.056,0.138
c-0.005,0.012-0.015,0.021-0.027,0.034l-0.204,0.203c-0.096,0.097-0.094,0.257,0,0.352l0.349,0.348
C5.263,31.407,5.42,31.406,5.516,31.311z M5.862,29.219c0.288-0.287,0.753-0.286,1.044,0.004c0.288,0.287,0.29,0.755,0.001,1.042
c-0.288,0.288-0.756,0.287-1.043,0C5.574,29.974,5.572,29.509,5.862,29.219z"></path>
	<path fill="#3B3C3B" d="M19.026,26.213c0,0,1.679-1.463,2.869-1.044c1.223,0.433,0.746,2.2-0.586,1.848
c1.251,0.593,0.521,2.24-0.702,1.808C19.416,28.405,19.026,26.213,19.026,26.213z"></path>
	<path fill="#3B3C3B" d="M21.862,31.289c0.712-0.71,0.824-1.76,0.244-2.34c-0.312-0.312-0.76-0.421-1.218-0.35
c-0.106-0.051-0.176-0.146-0.223-0.321c-0.056-0.191-0.027-0.43,0.073-0.696c-0.427,0.494-0.702,1.004-0.744,1.415
c-0.076,0.059-0.155,0.123-0.229,0.196c-0.714,0.715-0.822,1.76-0.242,2.339C20.103,32.112,21.147,32.005,21.862,31.289"></path>
	<path fill="#3B3C3B" d="M15.889,30.679l0.203-0.203c0.013-0.013,0.025-0.019,0.034-0.027c0.049,0.023,0.092,0.038,0.138,0.056
c0.002,0.013,0.004,0.028,0.004,0.043l0.001,0.289c0,0.135,0.11,0.246,0.247,0.246h0.492c0.134-0.001,0.246-0.113,0.246-0.246
l-0.002-0.285c0.002-0.017,0.002-0.034,0.005-0.045c0.045-0.017,0.092-0.038,0.137-0.062c0.009,0.009,0.022,0.019,0.035,0.032
l0.201,0.201c0.098,0.098,0.254,0.099,0.35,0.004l0.348-0.348c0.099-0.099,0.096-0.254-0.001-0.352l-0.201-0.201
c-0.014-0.014-0.025-0.024-0.031-0.037c0.02-0.044,0.04-0.09,0.06-0.135c0.009-0.001,0.026-0.001,0.04-0.001l0.29-0.001
c0.135-0.003,0.244-0.112,0.243-0.244l0.002-0.494c0-0.136-0.11-0.247-0.246-0.247l-0.29,0.001
c-0.016,0.001-0.03-0.002-0.044-0.002c-0.019-0.047-0.033-0.089-0.057-0.139c0.01-0.009,0.016-0.023,0.026-0.033l0.204-0.203
c0.095-0.095,0.096-0.252,0.001-0.347l-0.348-0.348c-0.095-0.095-0.253-0.099-0.351-0.003l-0.203,0.205
c-0.01,0.01-0.021,0.02-0.033,0.025c-0.045-0.02-0.09-0.035-0.138-0.057c-0.003-0.011-0.003-0.025-0.003-0.044v-0.291
c0.004-0.132-0.107-0.243-0.244-0.244l-0.494,0.004c-0.137,0-0.245,0.108-0.243,0.242l-0.002,0.289
c0.002,0.018,0,0.033-0.001,0.041c-0.048,0.017-0.094,0.04-0.135,0.06c-0.014-0.008-0.024-0.02-0.035-0.029l-0.204-0.204
c-0.095-0.096-0.251-0.099-0.35,0l-0.348,0.348c-0.095,0.096-0.093,0.253,0.002,0.348l0.205,0.204
c0.011,0.01,0.02,0.023,0.03,0.033c-0.023,0.046-0.042,0.094-0.061,0.138c-0.011,0.003-0.024,0.003-0.044,0.005l-0.285-0.002
c-0.137,0-0.247,0.11-0.246,0.246l0,0.492c0,0.137,0.111,0.247,0.244,0.244l0.289,0.002c0.016,0.001,0.031,0.001,0.043,0.004
c0.02,0.048,0.037,0.093,0.056,0.137c-0.005,0.013-0.015,0.022-0.028,0.034l-0.203,0.204c-0.096,0.096-0.094,0.256,0,0.352
l0.348,0.348C15.636,30.776,15.793,30.775,15.889,30.679z M16.235,28.588c0.288-0.288,0.754-0.286,1.044,0.003
c0.287,0.288,0.289,0.755,0.001,1.042c-0.288,0.289-0.755,0.288-1.042,0C15.947,29.342,15.945,28.877,16.235,28.588z"></path>
	<path fill="#3B3C3B" d="M24.767,32c0,0-1.345-1.775-0.844-2.934c0.515-1.191,2.246-0.595,1.803,0.71
c0.677-1.206,2.271-0.365,1.756,0.825C26.981,31.762,24.767,32,24.767,32z"></path>
	<path fill="#3B3C3B" d="M19.507,24.079c-0.009-0.005-0.019-0.011-0.025-0.015c-0.007-0.005-0.019-0.009-0.025-0.013
c-0.752-0.423-1.714-0.151-2.134,0.597c-0.418,0.748,0.075,3.126,0.075,3.126s2.286-0.811,2.708-1.559
C20.524,25.47,20.263,24.506,19.507,24.079z M19.397,25.847c-0.217,0.384-0.704,0.52-1.088,0.303
c-0.387-0.215-0.522-0.7-0.305-1.087c0.214-0.387,0.699-0.522,1.086-0.305C19.474,24.975,19.61,25.462,19.397,25.847z"></path>
	<path fill="#3B3C3B" d="M17.162,29.262c0,0,0.153-2.222,1.291-2.767c1.17-0.56,2.082,1.027,0.893,1.72
c1.302-0.465,1.95,1.216,0.78,1.775C18.988,30.536,17.162,29.262,17.162,29.262z"></path>
	<g id="_x23_ffffffff_1_">
	  <path fill="#FF3D7F" d="M15.783,21.595c1.335-0.798,2.451-2.217,4.288-1.116c-0.641,2.363-2.34,2-4.142,2.053L15.783,21.595z"></path>
	</g>
  </g>
</g>
</svg>			</div>
			<div class="icon-set__id">
				<span>sprout</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M18.311,6l-2.761,5L18.311,6z"/><polygon fill="#4CAF50" points="11,9 13,9 13,6 16,6 16,4 13,4 13,1 11,1 11,4 8,4 8,6 11,6 "/><path fill="#455A64" d="M7,18c-1.1,0-1.99,0.9-1.99,2S5.9,22,7,22s2-0.9,2-2S8.1,18,7,18z M17,18c-1.1,0-1.99,0.9-1.99,2S15.9,22,17,22s2-0.9,2-2S18.1,18,17,18z"/><path fill="#607D8B" d="M7.17,14.75l0.03-0.12L8.1,13h7.45c0.75,0,1.41-0.41,1.75-1.03l3.86-7.01L19.42,4h-0.01l-1.1,2l-2.761,5H8.53L8.4,10.73L6.16,6L5.21,4L4.27,2H1v2h2l3.6,7.59l-1.35,2.45C5.09,14.32,5,14.65,5,15c0,1.1,0.9,2,2,2h12v-2H7.42C7.29,15,7.17,14.891,7.17,14.75z"/></svg>			</div>
			<div class="icon-set__id">
				<span>add_shopping_cart</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#BBDEFB" points="15.344,2.984 5,2.984 3.766,3.891 2.969,5.063 2.969,13.938 15.344,13.938 "/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#BBDEFB" d="M19,3h-7.51c0.24,0.62,0.39,1.29,0.42,1.99H12c0,0.75-0.12,1.47-0.33,2.14c-0.9,2.79-3.49,4.82-6.56,4.86C5.08,12,5.05,12,5.03,12H5v-0.01c-0.7-0.021-1.37-0.15-2-0.39V19c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M5,18l3.5-4.5l2.5,3.01L14.5,12l4.5,6H5z"/><polygon fill="#43A047" points="19,18 5,18 8.5,13.5 11,16.51 14.5,12 "/><path fill="#29B6F6" d="M5,8V4.99h3C8,6.65,6.66,8,5,8z"/><path fill="#29B6F6" d="M11.92,5.33c0,0.63-0.09,1.23-0.25,1.8C10.89,9.94,8.31,12,5.25,12c-0.05,0-0.09,0-0.14-0.01C5.08,12,5.05,12,5.03,12C5.02,12,5.01,12,5,11.99V10c2.76,0,5-2.25,5-5.01h1.91C11.92,5.1,11.92,5.22,11.92,5.33z"/><path fill="#29B6F6" d="M8.35,4.18c0,2.3-1.87,4.17-4.17,4.17C3.77,8.35,3.37,8.29,3,8.18V5c0-1.1,0.9-2,2-2h3.18C8.29,3.37,8.35,3.77,8.35,4.18z"/><path fill="#29B6F6" d="M9.984,5.438c0,0,0.109-1.047-0.25-2.469l1.813,0.016c0,0,0.438,1.156,0.359,2.406L9.984,5.438z"/><path fill="#29B6F6" d="M5.156,10c0,0-1.391,0.094-2.203-0.422C2.922,9.625,3,11.375,3,11.375S3.778,12.031,5.03,12L5.156,10z"/><polygon fill="#66BB6A" points="5,18 8.5,13.5 12.234,18 "/></svg>			</div>
			<div class="icon-set__id">
				<span>satellite</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFAB91" d="M21,11c0,2.76-2.24,5-5,5h-1.77c-0.83,0-1.5,0.67-1.5,1.5c0,0.38,0.149,0.73,0.38,0.99c0.239,0.27,0.39,0.62,0.39,1.01c0,0.83-0.67,1.5-1.5,1.5c-4.97,0-9-4.03-9-9c0-4.97,4.03-9,9-9C16.97,3,21,6.58,21,11z"/><path fill="none" d="M0,0h24v24H0V0z"/><circle fill="#E53935" cx="6.875" cy="10.938" r="2.125"/><circle fill="#43A047" cx="11.953" cy="6.625" r="2.125"/><circle fill="#1E88E5" cx="17.031" cy="10.188" r="2.125"/></svg>			</div>
			<div class="icon-set__id">
				<span>palette</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3.938" y="4.938" fill="#29B6F6" width="3" height="13"/><rect x="18" y="5" fill="#29B6F6" width="3" height="13"/><rect x="8" y="5" fill="#0288D1" width="9" height="13"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="8" y="5" fill="#039BE5" width="4.47" height="13"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_array</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="20" y="14" fill="#E53935" width="2" height="2"/><rect x="20" y="7" fill="#E53935" width="2" height="5"/><circle fill="#B0BEC5" cx="10" cy="12" r="8"/><path fill="#FFFFFF" d="M10,14c-1.1,0-2-0.9-2-2c0-1.1,0.9-2,2-2s2,0.9,2,2C12,13.1,11.1,14,10,14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>disc_full</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M21.012,16.499v3.5c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.569,3.57c0.109,0.35,0.029,0.74-0.25,1l-2.199,2.209c0.271,0.521,0.568,1.041,0.92,1.561c0.21,0.318,0.439,0.63,0.682,0.939c0.129,0.158,0.25,0.301,0.369,0.449c0.289,0.34,0.59,0.658,0.899,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.119,0.109,0.239,0.221,0.369,0.312c0.133,0.102,0.25,0.2,0.383,0.3c0.131,0.109,0.26,0.2,0.397,0.29c0.144,0.103,0.276,0.188,0.42,0.29c0.276,0.2,0.58,0.381,0.88,0.54c0.188,0.108,0.393,0.221,0.58,0.319l2.2-2.199c0.276-0.271,0.67-0.352,1.021-0.24c1.118,0.37,2.318,0.57,3.568,0.57C20.562,15.499,21.012,15.948,21.012,16.499z"/><g id="Capa_2"><path fill="#455A64" d="M21.012,19.679v0.32c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1H4.7l0.012,0.06c0,0-0.131,3.77,1.92,7.72c0.271,0.521,0.568,1.041,0.92,1.561c0.21,0.318,0.439,0.63,0.682,0.939c0.129,0.158,0.25,0.301,0.369,0.449c0.289,0.34,0.59,0.658,0.899,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.119,0.102,0.239,0.199,0.369,0.312c0.133,0.102,0.25,0.2,0.383,0.3c0.131,0.104,0.271,0.189,0.397,0.29c0.144,0.103,0.276,0.188,0.42,0.29c0.276,0.184,0.58,0.37,0.88,0.54c0.188,0.108,0.393,0.221,0.58,0.319c2.051,1.068,4.604,1.869,7.764,2.211L21.012,19.679z"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#66BB6A" d="M19,12h2c0-4.97-4.029-9-9-9v2C15.87,5,19,8.13,19,12z"/><path fill="#43A047" d="M15,12h2c0-2.76-2.24-5-5-5v2C13.66,9,15,10.34,15,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>phone_in_talk</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M17,1.01L7,1C5.9,1,5,1.9,5,3v18c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V3C19,1.9,18.1,1.01,17,1.01z"/><rect x="7" y="5" fill="#BBDEFB" width="10" height="14"/></svg>			</div>
			<div class="icon-set__id">
				<span>smartphone</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#81D4FA" d="M21,3H3C1.9,3,1,3.9,1,5v14c0,1.1,0.9,1.98,2,1.98h18c1.1,0,2-0.881,2-1.98V5C23,3.9,22.1,3,21,3z"/><rect x="3" y="4.98" fill="#81D4FA" width="18" height="14.03"/><rect x="11" y="7" fill="#26A69A" width="8" height="6"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>picture_in_picture</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><rect x="5" y="11" fill="#78909C" width="14" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><rect x="5" y="11" fill="#78909C" width="14" height="0.97"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>remove</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M22,3H2C0.9,3,0,3.9,0,5v14c0,1.1,0.9,2,2,2h20c1.1,0,1.99-0.9,1.99-2L24,5C24,3.9,23.1,3,22,3z"/><rect x="14" y="6" fill="#FFEE58" width="8" height="6"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#FBC02D" points="22.016,7.063 22,6 18,9 14,6 13.984,7 18,10 "/><path fill="#C8E6C9" d="M8,6c1.66,0,3,1.34,3,3s-1.34,3-3,3s-3-1.34-3-3S6.34,6,8,6z"/><path fill="#C8E6C9" d="M13.902,17.968h-12v-1c0-2,4-3.1,6-3.1s6,1.1,6,3.1V17.968z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>quick_contacts_mail</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#0097A7" d="M18,18v2h2l-3,3l-3-3h2v-2H8c-1.1,0-2-0.9-2-2V8H2V6h4V4H4l3-3l3,3H8v12h14v2H18z"/><path fill="#0097A7" d="M7.922,8H16l0.031,8.125l1.953,0.016L18,8c0-1.1-0.9-2-2-2L7.938,6.016L7.922,8z"/><path fill="#43A047" d="M22.12,16v2H8c-1.1,0-2-0.9-2-2h0.01V4.44H6V4H4l3-3l3,3H8v12H22.12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>transform</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#607D8B" d="M17.3,12c0,3-2.54,5.1-5.3,5.1S6.7,15,6.7,12H5c0,3.42,2.72,6.23,6,6.72V22h2v-3.28c3.279-0.479,6-3.3,6-6.72H17.3z"/><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="#1976D2" d="M15,6c0-1.66-1.34-3-3-3S9,4.34,9,6v3h5.995L15,6z"/><path fill="#0D47A1" d="M12,15c1.66,0,2.99-1.34,2.99-3l0.005-3H9v3C9,13.66,10.34,15,12,15z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_voice</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M21,3h-8.25v2H21V3z M21,11h-8.25v2H21V11z M21,21v-2h-8.25v2H21z"/><path fill="#607D8B" d="M15,7H3v2h12V7z M15,15H3v2h12V15z M15,19H3v2h12V19z M15,11H3v2h12V11z M15,3H3v2h12V3z"/><path fill="none" d="M24,24H0V0h24V24z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_align_left</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path d="M21,12v8c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-1H6v1c0,0.55-0.45,1-1,1H4c-0.55,0-1-0.45-1-1v-8l2.08-5.99C5.29,5.42,5.84,5,6.5,5h11c0.66,0,1.22,0.42,1.42,1.01L21,12z"/><path fill="#455A64" d="M21,14.72V20c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-1H6v1c0,0.55-0.45,1-1,1H4c-0.55,0-1-0.45-1-1v-5.28H21z"/><path fill="#E53935" d="M21,12v2.72H3V12l2.08-5.99C5.29,5.42,5.84,5,6.5,5h11c0.66,0,1.22,0.42,1.42,1.01L21,12z"/><circle fill="#FFFFFF" cx="6.5" cy="14.5" r="1.5"/><circle fill="#FFFFFF" cx="17.5" cy="14.5" r="1.5"/><polygon fill="#BBDEFB" points="5,11 6.5,6.5 17.5,6.5 19,11 "/></svg>			</div>
			<div class="icon-set__id">
				<span>directions_car</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="5" fill="#AB47BC" width="5" height="6"/><rect x="4" y="12" fill="#9CCC65" width="5" height="6"/><rect x="10" y="12" fill="#26A69A" width="5" height="6"/><rect x="16" y="12" fill="#42A5F5" width="5" height="6"/><rect x="10" y="5" fill="#EC407A" width="5" height="6"/><rect x="16" y="5" fill="#EF5350" width="5" height="6"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_module</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CFD8DC" d="M17,13v-1.5c0-0.83-0.67-1.5-1.5-1.5c0.83,0,1.5-0.67,1.5-1.5V7c0-1.11-0.9-2-2-2h-4v2h4v2h-2v2h2v2h-4v2h4C16.1,15,17,14.109,17,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_3</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" enable-background="new    " d="M18.04,16v-5.5c0-2.7-1.64-5.01-4-5.98c-0.1-0.05-0.2-0.09-0.31-0.13c-0.221-0.09-0.45-0.15-0.69-0.21V3.5c0-0.74-0.53-1.35-1.24-1.47C11.72,2.01,11.63,2,11.54,2c-0.1,0-0.2,0.01-0.3,0.03c-0.69,0.14-1.2,0.74-1.2,1.47v0.68c-2.87,0.68-5,3.25-5,6.32V16l-2,2v1h17v-1L18.04,16z M9,13.2l2.8-3.4H9V8h5v1.8l-2.8,3.4H14V15H9V13.2z"/><path fill="#546E7A" enable-background="new    " d="M11.541,22c0.141,0,0.27-0.01,0.4-0.04c0.649-0.13,1.189-0.58,1.439-1.18c0.1-0.24,0.16-0.5,0.16-0.78h-4C9.541,21.1,10.441,22,11.541,22z"/><polygon fill="#90A4AE" enable-background="new    " points="20.041,18 20.041,19 3.041,19 3.041,18 5.001,16.04 18.081,16.04 "/><path fill="#90A4AE" enable-background="new    " d="M13.731,4.39c-0.07-0.01-0.15-0.03-0.24-0.06c-1.601-0.5-2.931-0.28-3.45-0.16V3.5c0-0.83,0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5v0.68C13.281,4.24,13.511,4.3,13.731,4.39z"/><path fill="none" d="M0,0v24h24V0H0z M14,9.8l-2.8,3.4H14V15H9v-1.8l2.8-3.4H9V8h5V9.8z"/></svg>			</div>
			<div class="icon-set__id">
				<span>notifications_paused</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.5" fill="#F06292" d="M16.112,3.791c-1.591,0-3.116,0.74-4.112,1.91c-0.996-1.169-2.522-1.91-4.112-1.91c-2.814,0-5.026,2.211-5.026,5.026c0,3.454,3.107,6.268,7.813,10.545L12,20.559l1.325-1.205c4.705-4.269,7.813-7.083,7.813-10.537C21.138,6.002,18.927,3.791,16.112,3.791z"/><path fill="#F06292" d="M16.5,3c-1.74,0-3.41,0.81-4.5,2.09C10.91,3.81,9.24,3,7.5,3C4.42,3,2,5.42,2,8.5c0,3.78,3.4,6.859,8.55,11.54L12,21.35l1.45-1.319C18.6,15.359,22,12.28,22,8.5C22,5.42,19.58,3,16.5,3z M12.1,18.55L12,18.65l-0.1-0.101C7.14,14.24,4,11.39,4,8.5C4,6.5,5.5,5,7.5,5c1.54,0,3.04,0.99,3.57,2.36h1.87C13.46,5.99,14.96,5,16.5,5c2,0,3.5,1.5,3.5,3.5C20,11.39,16.859,14.24,12.1,18.55z"/></svg>			</div>
			<div class="icon-set__id">
				<span>favorite_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFC107" fill-opacity="0.3" d="M17,5.33C17,4.6,16.4,4,15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33V15h10V5.33z"/><path fill="#FFC107" d="M7,15v5.67C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V15H7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_30</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#1E88E5" points="23,12 2.01,21 2,14 17,12 2,10 2.01,3 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>send</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#66BB6A" d="M22,22l-4-4H4c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h16c1.1,0,1.99,0.9,1.99,2v6.96L22,22z"/><rect x="6" y="12" fill="#CFD8DC" width="12" height="2"/><path opacity="0.25" fill="#43A047" enable-background="new    " d="M21.99,4v6.96H2V4c0-1.1,0.9-2,2-2h16C21.1,2,21.99,2.9,21.99,4z"/><rect x="6" y="9" fill="#CFD8DC" width="12" height="2"/><rect x="6" y="6" fill="#CFD8DC" width="12" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>comment</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#0097A7" d="M19,2H5C3.89,2,3,2.9,3,4v14c0,1.1,0.89,2,2,2h4l3,3l3-3h4c1.1,0,2-0.9,2-2V4C21,2.9,20.1,2,19,2z M12,5.3c1.49,0,2.7,1.21,2.7,2.7s-1.21,2.7-2.7,2.7S9.3,9.49,9.3,8S10.51,5.3,12,5.3z M18,16H6v-0.9c0-2,4-3.1,6-3.1s6,1.1,6,3.1V16z"/><path opacity="0.5" fill="#00BCD4" enable-background="new    " d="M18,15.1V16H6v-0.9c0-2,4-3.1,6-3.1S18,13.1,18,15.1z"/><circle opacity="0.5" fill="#00BCD4" enable-background="new    " cx="12" cy="8" r="2.7"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>location_history</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M16.53,2.52C19.8,4.07,22.14,7.24,22.5,11H24C23.489,4.84,18.34,0,12.05,0l-0.66,0.03L15.2,3.84L16.53,2.52z"/><path fill="#78909C" d="M10.28,1.75c-0.59-0.59-1.54-0.59-2.12,0L1.8,8.11c-0.59,0.59-0.59,1.54,0,2.12l12.02,12.02c0.59,0.59,1.541,0.59,2.121,0l6.359-6.359c0.59-0.591,0.59-1.541,0-2.121L10.28,1.75z"/><rect x="3.551" y="7.503" transform="matrix(0.7071 0.7071 -0.7071 0.7071 12.0141 -5.0059)" fill="#B2EBF2" width="16.999" height="8.994"/><path fill="#9575CD" d="M7.57,21.48C4.3,19.939,1.96,16.76,1.6,13H0.1c0.51,6.16,5.66,11,11.95,11l0.66-0.03L8.9,20.16L7.57,21.48z"/></svg>			</div>
			<div class="icon-set__id">
				<span>screen_rotation</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M17,3H7C5.9,3,5,3.9,5,5v14c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V5C19,3.9,18.1,3,17,3z M17,19H7V5h10V19z"/><path fill="none" d="M24,0h24v24H24V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_portrait</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFB300" fill-opacity="0.3" d="M17,5.33C17,4.6,16.4,4,15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33V13h10V5.33z"/><path fill="#FFD54F" d="M7,13v7.67C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V13H7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_50</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M11.98,14l-0.02,0.02l-4.55,4.57L8.83,20l3.13-3.15l0.02-0.02L15.141,20l1.409-1.41L11.98,14z M15.141,4L11.98,7.17L8.83,4L7.41,5.41L11.98,10l4.581-4.59L15.141,4z"/><polygon fill="#78909C" points="16.561,5.41 11.98,10 7.41,5.41 8.83,4 11.98,7.17 15.141,4 "/></svg>			</div>
			<div class="icon-set__id">
				<span>unfold_less</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#37474F" d="M20,4v16c0,1.1-0.9,2-2,2H6c-1.1,0-2-0.9-2-2L4.02,8L10,2h8C19.1,2,20,2.9,20,4z"/><path opacity="0.4" fill="#455A64" d="M12,2v20H6c-1.1,0-2-0.9-2-2L4.02,8L10,2H12z"/><rect x="10" y="3.99" fill="#FDD835" width="2" height="4"/><rect x="13" y="3.99" fill="#FDD835" width="2" height="4"/><rect x="16" y="3.99" fill="#FDD835" width="2" height="4"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>sd_card</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><polygon fill="#43A047" points="19,17.979 5,17.979 8.5,13.479 11,16.489 14.5,11.979 	"/><polygon fill="#66BB6A" points="5,17.979 8.5,13.479 12.234,17.979 	"/></g><rect x="3.906" y="4.844" fill="#CFD8DC" width="15.094" height="11.188"/><path fill="#D1C4E9" d="M21,19V5c0-1.1-0.9-2-2-2H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14C20.1,21,21,20.1,21,19z M8.5,13.5l2.5,3.01L14.5,12l4.5,6H5L8.5,13.5z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>insert_photo</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36">
<g data-iconmelon="Smallicons:7348d307519be212c93cc26177661982">
<path fill="#EFC75E" d="M9.627 22.319c1.495 1.489 1.945 3.938.451 5.428-1.495 1.489-6.001 3.148-7.496 1.659-1.494-1.489.172-5.979 1.666-7.468 1.495-1.489 3.885-1.109 5.379.381z"></path>
<path fill="#E2574C" d="M23.432 19.008c2.251 4.667-3.242 15.53-5.782 12.456-1.03-1.246-.962-3.234-3.651-5.794-.004.016.979.174 2.13-.868 2.656-2.046 7.303-5.777 7.303-5.794z"></path>
<path fill="#E2574C" d="M13.973 8.556c-5.045-2.286-16.73 3.171-13.4 5.738 1.35 1.041 3.492.984 6.267 3.699-.017.004-.192-.984.926-2.135 2.189-2.658 6.188-7.302 6.207-7.302z"></path>
<path fill="#CF5349" d="M11.751 8.038l-.218-.011c-1.107 1.352-3.812 4.685-5.461 6.794-.676.734-.859 1.401-.898 1.795.323.222.656.478 1 .772.084-.398.305-.953.858-1.554 2.105-2.694 5.948-7.404 5.966-7.403-.377-.18-.801-.303-1.247-.393z"></path>
<path fill="#CF5349" d="M23.463 19.082c-.658.554-4.828 3.973-7.306 5.924-.819.758-1.546.9-1.909.914.356.358.664.705.926 1.04.326.005 1.111-.098 1.997-.917 2.074-1.634 5.322-4.285 6.715-5.436-.075-.552-.213-1.067-.423-1.525z"></path>
<path fill="#D7B354" d="M6.089 21.038c.208.932.847 2.043 1.842 3.036.99.988 2.097 1.624 3.028 1.836.158-1.252-.36-2.623-1.332-3.591-.967-.964-2.307-1.452-3.538-1.281z"></path>
<path fill="#EBEBEB" d="M31.814.16c.752.711-.355 11.267-7.57 18.434-4.467 4.438-7.43 6.156-8.139 6.771-1.961.889-3.262 1.367-7.148-2.495-3.877-3.852-3.131-4.92-2.271-6.765.638-.716 2.405-3.691 6.873-8.13 7.212-7.166 17.468-8.513 18.255-7.815z"></path>
<path fill="#324D5B" d="M22.795 9.163c1.555 1.558 1.555 4.084 0 5.643-1.556 1.558-4.077 1.558-5.633 0-1.555-1.559-1.555-4.085 0-5.643 1.556-1.559 4.077-1.559 5.633 0z"></path>
<path fill="#CCD0D2" d="M21.4 10.577c.784.779.784 2.042 0 2.821-.783.779-2.055.779-2.84 0-.783-.779-.783-2.042 0-2.821.786-.779 2.057-.779 2.84 0z"></path>
</g>
</svg>			</div>
			<div class="icon-set__id">
				<span>rocket</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#6D4C41" d="M21,18v1c0,1.1-0.9,2-2,2H5c-1.11,0-2-0.9-2-2V5c0-1.1,0.89-2,2-2h14c1.1,0,2,0.9,2,2v1h-9c-1.11,0-2,0.9-2,2v8c0,1.1,0.89,2,2,2H21z"/><rect x="12" y="8" fill="#4CAF50" width="10" height="8"/><circle fill="#FFFFFF" cx="16" cy="12" r="1.5"/></svg>			</div>
			<div class="icon-set__id">
				<span>account_balance_wallet</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M12,2C6.49,2,2,6.49,2,12s4.49,10,10,10s10-4.49,10-10S17.51,2,12,2z M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/><path fill="#546E7A" d="M15,12c0,1.66-1.34,3-3,3s-3-1.34-3-3s1.34-3,3-3S15,10.34,15,12z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>adjust</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><rect x="19" y="19" fill="#7E57C2" width="2" height="2"/><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="11" y="19" fill="#7E57C2" width="2" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><rect x="19" y="11" fill="#7E57C2" width="2" height="2"/><polygon fill="#512DA8" points="3,3 3,21 5,21 5,5 21,5 21,3 "/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_style</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19,5H5C3.9,5,3,5.9,3,7v10c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V7C21,5.9,20.1,5,19,5z M19,17H5V7h14V17z"/><path fill="none" d="M20.404-1.526h24v24h-24V-1.526z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_landscape</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#D1C4E9" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M16.5,16H8c-1.66,0-3-1.34-3-3s1.34-3,3-3l0.14,0.01C8.58,8.28,10.13,7,12,7c2.21,0,4,1.79,4,4h0.5c1.38,0,2.5,1.12,2.5,2.5S17.88,16,16.5,16z"/><path fill="#82B1FF" d="M19,13.5c0,1.38-1.12,2.5-2.5,2.5H8c-1.66,0-3-1.34-3-3s1.34-3,3-3l0.14,0.01C8.58,8.28,10.13,7,12,7c2.21,0,4,1.79,4,4h0.5C17.88,11,19,12.12,19,13.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>cloud_circle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#546E7A" points="7.77,6.76 6.23,5.48 0.82,12 6.23,18.52 7.77,17.24 3.42,12 "/><rect x="7" y="11" fill="#455A64" width="2" height="2"/><rect x="15" y="11" fill="#455A64" width="2" height="2"/><rect x="11" y="11" fill="#546E7A" width="2" height="2"/><polygon fill="#546E7A" points="17.77,5.48 16.23,6.76 20.58,12 16.23,17.24 17.77,18.52 23.18,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_ethernet</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="5.776" y="5.038" fill="#FFEB3B" width="12.945" height="13.452"/><path fill="none" d="M0,0h24v24H0V0z"/><circle opacity="0.8" fill="#FFCC80" enable-background="new    " cx="12.053" cy="11.904" r="6.865"/><path fill="#FFEB3B" d="M20,15.311L23.311,12L20,8.69V4h-4.689L12,0.69L8.69,4H4v4.69L0.69,12L4,15.311V20h4.69L12,23.311L15.311,20H20V15.311z M12,18c-3.31,0-6-2.689-6-6c0-3.31,2.69-6,6-6c3.311,0,6,2.69,6,6C18,15.311,15.311,18,12,18z"/></svg>			</div>
			<div class="icon-set__id">
				<span>brightness_low</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#0288D1" d="M20.92,19c0,1.609-1.311,2.92-2.92,2.92s-2.92-1.311-2.92-2.92c0-0.22,0.029-0.439,0.08-0.65l-7.12-4.16C7.5,14.689,6.79,15,6,15c-1.66,0-3-1.34-3-3v-0.03C3.02,10.32,4.35,9,6,9c0.79,0,1.5,0.31,2.04,0.81l7.05-4.11C15.04,5.47,15,5.24,15,5c0-1.66,1.34-3,3-3s3,1.34,3,3s-1.34,3-3,3c-0.79,0-1.5-0.31-2.04-0.81L8.91,11.3C8.96,11.52,9,11.74,9,11.97V12c0,0.24-0.04,0.47-0.09,0.7l7.13,4.149c0.521-0.471,1.2-0.771,1.96-0.771C19.609,16.08,20.92,17.391,20.92,19z"/><circle fill="#42A5F5" cx="18.02" cy="5.003" r="3.006"/><circle fill="#42A5F5" cx="5.979" cy="11.982" r="3.006"/><circle fill="#42A5F5" cx="17.947" cy="18.951" r="3.006"/></svg>			</div>
			<div class="icon-set__id">
				<span>share</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M21,4v14c0,1.1-0.9,2-2,2h-4l-3,3l-3-3H5c-1.11,0-2-0.9-2-2V4c0-1.1,0.89-2,2-2h14C20.1,2,21,2.9,21,4z"/><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.5" fill="#90A4AE" d="M21,4v14c0,1.1-0.9,2-2,2h-4l-3,3V2h7C20.1,2,21,2.9,21,4z"/><rect x="11" y="16" fill="#37474F" width="2" height="2"/><path fill="#37474F" d="M15.07,10.25l-0.9,0.92C13.45,11.9,13,12.5,13,14h-2v-0.5c0-1.1,0.45-2.1,1.17-2.83l1.24-1.26C13.779,9.05,14,8.55,14,8c0-1.1-0.9-2-2-2c-1.1,0-2,0.9-2,2H8c0-2.21,1.79-4,4-4s4,1.79,4,4C16,8.88,15.641,9.68,15.07,10.25z"/></svg>			</div>
			<div class="icon-set__id">
				<span>live_help</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#BA68C8" d="M19,4h-4L7.11,16.63L4.5,12L9,4H5l-4.5,8L5,20h4l7.891-12.63L19.5,12L15,20h4l4.5-8L19,4z"/></g><g id="Capa_2"><polygon fill="#AB47BC" points="14.969,3.984 18.984,3.984 8.984,20 4.984,20 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>polymer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M20,6h-8l-2-2H4C2.9,4,2.01,4.9,2.01,6L2,18c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V8C22,6.9,21.1,6,20,6z"/><path fill="#673AB7" d="M15,9c1.1,0,2,0.9,2,2s-0.9,2-2,2s-2-0.9-2-2S13.9,9,15,9z M19,17h-8v-1c0-1.33,2.67-2,4-2s4,0.67,4,2V17z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>folder_shared</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#7E57C2" d="M9,2C7.95,2,6.95,2.16,6,2.46c4.06,1.27,7,5.06,7,9.54c0,4.48-2.94,8.27-7,9.54C6.95,21.84,7.95,22,9,22c5.52,0,10-4.48,10-10C19,6.48,14.52,2,9,2z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>brightness_3</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#D7CCC8" d="M2.993,8.067l-0.008,11c0,1.1,0.891,2,2,2h14c1.1,0,2-0.9,2-2v-11H2.993z"/><path fill="#C2185B" d="M20.984,5.067c0-1.1-0.9-2-2-2h-14c-1.109,0-1.99,0.9-1.99,2l-0.002,3h17.992V5.067z"/><path fill="#A1887F" d="M7.985,5.067h-2v-4h2V5.067z M17.984,1.067h-2v4h2V1.067z"/></g><rect x="7" y="10" fill="#455A64" width="10" height="2"/><rect x="7" y="14" fill="#455A64" width="7" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>event_note</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><rect x="9.375" y="13.292" fill="#795548" width="5.333" height="6.708"/><polygon fill="#D7CCC8" points="5,12 5,20 10,20 10,14 14,14 14,20 19,20 19,12 19.021,12 12.01,5.691 "/><polygon fill="#F44336" points="12,3 2,12 5,12 12.01,5.691 19.021,12 22,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>home</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="11" fill="#546E7A" width="16" height="2"/><polygon fill="#78909C" points="13,5 13,1 11,1 11,5 8,5 12,9 16,5 "/><polygon fill="#78909C" points="12,15 11.97,15.03 8,19 11,19 11,23 13,23 13,19 16,19 "/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#78909C" points="11.97,1 11.97,8.97 8,5 11,5 11,1 "/><polygon fill="#78909C" points="11.97,15.03 11.97,23 11,23 11,19 8,19 "/><rect x="4" y="11" fill="#546E7A" width="16" height="1.01"/></svg>			</div>
			<div class="icon-set__id">
				<span>vertical_align_center</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#039BE5" points="22,12 21.98,12.02 18,16 18,13 3,13 3,11 18,11 18,8 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>trending_neutral</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#90A4AE" cx="6" cy="14" r="1"/><circle fill="#90A4AE" cx="6" cy="18" r="1"/><circle fill="#90A4AE" cx="6" cy="10" r="1"/><path fill="#B0BEC5" d="M3,9.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S3.279,9.5,3,9.5z"/><circle fill="#90A4AE" cx="6" cy="6" r="1"/><path fill="#B0BEC5" d="M21,10.5c0.279,0,0.5-0.22,0.5-0.5S21.279,9.5,21,9.5s-0.5,0.22-0.5,0.5S20.721,10.5,21,10.5z"/><circle fill="#90A4AE" cx="14" cy="6" r="1"/><path fill="#B0BEC5" d="M14,3.5c0.279,0,0.5-0.22,0.5-0.5S14.279,2.5,14,2.5S13.5,2.72,13.5,3S13.721,3.5,14,3.5z"/><path fill="#B0BEC5" d="M3,13.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S3.279,13.5,3,13.5z"/><path fill="#B0BEC5" d="M10,20.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S10.279,20.5,10,20.5z"/><path fill="#B0BEC5" d="M10,3.5c0.279,0,0.5-0.22,0.5-0.5S10.279,2.5,10,2.5S9.5,2.72,9.5,3S9.721,3.5,10,3.5z"/><circle fill="#90A4AE" cx="10" cy="6" r="1"/><circle fill="#78909C" cx="10" cy="14" r="1.5"/><circle fill="#90A4AE" cx="18" cy="14" r="1"/><circle fill="#90A4AE" cx="18" cy="18" r="1"/><circle fill="#90A4AE" cx="18" cy="10" r="1"/><circle fill="#90A4AE" cx="18" cy="6" r="1"/><path fill="#B0BEC5" d="M21,13.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S21.279,13.5,21,13.5z"/><circle fill="#90A4AE" cx="14" cy="18" r="1"/><path fill="#B0BEC5" d="M14,20.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S14.279,20.5,14,20.5z"/><circle fill="#78909C" cx="10" cy="10" r="1.5"/><circle fill="#90A4AE" cx="10" cy="18" r="1"/><circle fill="#78909C" cx="14" cy="14" r="1.5"/><circle fill="#78909C" cx="14" cy="10" r="1.5"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#9FA8DA" points="2.5,5.27 3.77,4 20,20.23 18.721,21.5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>blur_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#3F51B5" d="M11.25,4.828c0.109,0.109,3.828,3.844,3.828,3.844l-5.953,5.703l-0.922-0.047l-2.828-3.109l5.797-6.406"/><path fill="none" d="M0,0h24v24H0V0z M15.35,6.41l-1.77-1.77c-0.2-0.2-0.51-0.2-0.71,0L6,11.53V14h2.47l6.879-6.88C15.55,6.93,15.55,6.61,15.35,6.41z"/><path fill="#E57373" d="M11.672,5.297l3.016,3c0,0,1.688-1.297,1.641-1.281s0.031-2.406,0.031-2.406s-3.141-1.156-3.188-1.141S11.672,5.297,11.672,5.297z"/><path fill="#C5CAE9" d="M11.813,5.406c0,0,2.734,2.719,2.781,2.719s-0.453,0.453-0.453,0.453l-2.781-2.766L11.813,5.406z"/><polygon fill="#C5CAE9" points="5.75,11.281 8.719,14.25 5.5,14.672 "/><path fill="#FFE082" d="M20,2H4C2.9,2,2.01,2.9,2.01,4v6.91L2,22l4-4h14c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2z M8.47,14H6v-2.47l0.62-0.62l6.26-6.26c0.2-0.2,0.51-0.2,0.71,0l1.771,1.77c0.199,0.2,0.199,0.51,0,0.71l-3.791,3.78L8.47,14z"/><path opacity="0.5" fill="#FFECB3" d="M22,4v6.91H11.57l3.791-3.78c0.199-0.2,0.199-0.51,0-0.71L13.59,4.65c-0.2-0.2-0.51-0.2-0.71,0l-6.26,6.26H2.01V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><polygon fill="#E57373" points="18,14 8.47,14 10.5,11.984 18,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>rate_review</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3.586" y="10.586" transform="matrix(0.7071 0.7071 -0.7071 0.7071 9.9497 -0.0208)" fill="#81D4FA" width="2.828" height="2.828"/><rect x="17.586" y="10.586" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -2.9203 16.9497)" fill="#4FC3F7" width="2.828" height="2.828"/><path fill="#1976D2" d="M17.72,7.7l-5.71-5.71h-1v7.59L6.42,4.99L5.01,6.4l5.59,5.59l-5.59,5.59l1.41,1.41l4.59-4.59v7.59h1l5.71-5.71l-4.3-4.29L17.72,7.7z M13.01,5.82l1.88,1.88l-1.88,1.88V5.82z M14.89,16.28l-1.88,1.88V14.4L14.89,16.28z"/></svg>			</div>
			<div class="icon-set__id">
				<span>bluetooth_connected</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#558B2F" d="M21,13.72V19c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-1H6v1c0,0.55-0.45,1-1,1H4c-0.55,0-1-0.45-1-1v-5.28H21z"/><path fill="#AED581" d="M21,11v2.72H3V11l2.08-5.99C5.29,4.42,5.84,4,6.5,4h11c0.66,0,1.221,0.42,1.42,1.01L21,11z"/><circle fill="#FFFFFF" cx="6.5" cy="13.5" r="1.5"/><circle fill="#FFFFFF" cx="17.5" cy="13.5" r="1.5"/><polygon fill="#66BB6A" points="5,10 6.5,5.5 17.5,5.5 19,10 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>drive_eta</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4.604" y="3.979" fill="#FFFFFF" width="15.792" height="4.146"/><path fill="#CFD8DC" d="M22,7.97V18c0,1.1-0.9,2-2,2H4c-1.1,0-2-0.9-2-2L2.01,7.97H22z"/><path fill="#455A64" d="M22,4v4.906L2.016,8.859L2.01,6C2.01,4.9,2.9,4,4,4h1l1.99,3.97L7,8h3L9.99,7.97L8,4h2l1.99,3.97L12,8h3l-0.01-0.03L13,4h2l1.99,3.97L17,8h3l-0.01-0.03L18,4H22z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>movie</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#42A5F5" d="M16.5,12c1.38,0,2.49-1.12,2.49-2.5S17.88,7,16.5,7S14,8.12,14,9.5S15.12,12,16.5,12z"/><path fill="#1565C0" d="M9,11c1.66,0,2.99-1.34,2.99-3S10.66,5,9,5S6,6.34,6,8S7.34,11,9,11z"/><path fill="#42A5F5" d="M16.5,14c-1.83,0-5.5,0.92-5.5,2.75V19h11v-2.25C22,14.92,18.33,14,16.5,14z"/><path fill="#1565C0" d="M9,13c-2.33,0-7,1.17-7,3.5V19h7v-2.25c0-0.85,0.33-2.34,2.37-3.47C10.5,13.1,9.66,13,9,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>account_child</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M12,1C5.93,1,1,5.93,1,12c0,6.07,4.93,11,11,11c6.07,0,11-4.93,11-11C23,5.93,18.07,1,12,1z"/><path fill="#90A4AE" d="M12,21c-4.96,0-9-4.04-9-9s4.04-9,9-9s9,4.04,9,9S16.96,21,12,21z"/><path fill="none" d="M0,0h24v24H0V0z"/><circle fill="#37474F" cx="6.5" cy="11.5" r="1.5"/><path fill="#37474F" d="M15,6.5C15,5.67,14.33,5,13.5,5h-3C9.67,5,9,5.67,9,6.5S9.67,8,10.5,8h3C14.33,8,15,7.33,15,6.5z"/><circle fill="#37474F" cx="8.5" cy="16.5" r="1.5"/><circle fill="#37474F" cx="17.5" cy="11.5" r="1.5"/><circle fill="#37474F" cx="15.5" cy="16.5" r="1.5"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_input_svideo</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#C5CAE9" d="M22.999,4.999V19c0,1.1-0.9,2-2,2h-18c-1.101,0-2-0.9-2-2V4.999c0-1.1,0.899-2,2-2h18C22.099,2.999,22.999,3.899,22.999,4.999z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#42A5F5" points="11,16 15,12 11,8 11,11 1,11 1,13 11,13 "/><path fill="none" d="M30.204-4.178h24v24h-24V-4.178z"/></svg>			</div>
			<div class="icon-set__id">
				<span>input</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#00897B" d="M21,16v-2l-8-5V3.5C13,2.67,12.33,2,11.5,2S10,2.67,10,3.5V9l-8,5v2l8-2.5V19l-2,1.5V22l3.5-1l3.5,1v-1.5L13,19v-5.5L21,16z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_airport</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B3E5FC" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z"/><rect x="5" y="5" fill="#B3E5FC" width="14" height="14"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#9575CD" points="10,8 10,16 15,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>slideshow</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M14,3.46c0,0-0.729,9.353,4.92,8.54c0,0,0.914,1.854,0.914,1.938s-2.751,4.521-2.751,4.521l-1.125,0.875l-1.916-1.125c0,0-3.958-2.834-3.979-2.896s-2.542-1.271-2.542-1.271s-1.438-1.5-1.438-1.563S6.021,8.708,6.021,8.708S10,6.063,10.063,6.021s2.688-1.917,2.792-1.979S14,3.46,14,3.46z"/><path fill="#FFA726" d="M22,4V3.5C22,2.12,20.88,1,19.5,1S17,2.12,17,3.5V4c-0.55,0-1,0.45-1,1v4c0,0.55,0.45,1,1,1h5c0.55,0,1-0.45,1-1V5C23,4.45,22.55,4,22,4z M17.8,4V3.5c0-0.94,0.76-1.7,1.7-1.7s1.7,0.76,1.7,1.7V4H17.8z"/><path fill="#546E7A" d="M18.92,12c0.04,0.33,0.08,0.66,0.08,1c0,2.08-0.8,3.97-2.1,5.391C16.641,17.58,15.9,17,15,17h-1v-3c0-0.55-0.45-1-1-1H7v-2h2c0.55,0,1-0.45,1-1V8h2c1.1,0,2-0.9,2-2V3.46C13.05,3.16,12.05,3,11,3C5.48,3,1,7.48,1,13c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10c0-0.34-0.02-0.67-0.05-1H18.92z"/><path fill="#CFD8DC" d="M10,20.93c-3.95-0.49-7-3.85-7-7.93c0-0.62,0.08-1.21,0.21-1.79L8,16v1c0,1.1,0.9,2,2,2V20.93z"/><path fill="#FFCC80" d="M22,3.5V4h-0.8V3.5c0-0.94-0.76-1.7-1.7-1.7s-1.7,0.76-1.7,1.7V4H17V3.5C17,2.12,18.12,1,19.5,1S22,2.12,22,3.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>vpn_lock</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#00ACC1" points="14,14 14,16 6.99,16 6.99,19 3,15 3.05,14.95 6.99,11 6.99,14 "/><polygon fill="#43A047" points="17.01,13 17.01,10 10,10 10,8 17.01,8 17.01,5 21,9 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>swap_horiz</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E53935" d="M12,4c-4.97,0-9,4.03-9,9s4.02,9,9,9c4.971,0,9-4.03,9-9S16.971,4,12,4z"/><path fill="#FFCDD2" d="M12.125,20.042c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.995,20.042,12.125,20.042z"/><polygon fill="#D32F2F" points="22,5.72 17.4,1.86 16.109,3.39 20.71,7.25 "/><polygon fill="#D32F2F" points="7.88,3.39 6.6,1.86 2,5.71 3.29,7.24 "/><polygon fill="#D32F2F" points="12.5,8 11,8 11,14 15.75,16.85 16.5,15.62 12.5,13.25 "/></svg>			</div>
			<div class="icon-set__id">
				<span>access_alarm</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M17,1.01L7,1C5.9,1,5.01,1.9,5.01,3v18c0,1.1,0.89,2,1.99,2h10c1.1,0,2-0.9,2-2V3C19,1.9,18.1,1.01,17,1.01z"/><rect x="7" y="5" fill="#B2EBF2" width="10" height="14"/></svg>			</div>
			<div class="icon-set__id">
				<span>stay_current_portrait</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M21.005,16.488v3.5c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.569,3.57c0.109,0.35,0.029,0.74-0.25,1l-2.199,2.21c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.318,0.439,0.63,0.682,0.939c0.129,0.158,0.25,0.301,0.368,0.449c0.29,0.34,0.591,0.658,0.9,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.12,0.109,0.24,0.221,0.37,0.312c0.132,0.102,0.25,0.2,0.382,0.3c0.131,0.109,0.26,0.2,0.398,0.29c0.143,0.103,0.277,0.188,0.42,0.29c0.277,0.2,0.58,0.381,0.88,0.54c0.188,0.108,0.393,0.221,0.58,0.319l2.2-2.199c0.276-0.271,0.67-0.352,1.021-0.24c1.118,0.37,2.318,0.57,3.568,0.57C20.555,15.488,21.005,15.938,21.005,16.488z"/><g id="Capa_2"><path fill="#455A64" d="M21.005,19.668v0.32c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h0.688l0.012,0.06c0,0-0.131,3.77,1.92,7.72c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.318,0.439,0.63,0.682,0.939c0.129,0.158,0.25,0.301,0.368,0.449c0.29,0.34,0.591,0.658,0.9,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.12,0.102,0.24,0.199,0.37,0.312c0.132,0.102,0.25,0.2,0.382,0.3c0.131,0.104,0.271,0.189,0.398,0.29c0.143,0.103,0.277,0.188,0.42,0.29c0.277,0.184,0.58,0.37,0.88,0.54c0.188,0.108,0.393,0.221,0.58,0.319c2.051,1.068,4.604,1.869,7.764,2.211L21.005,19.668z"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>phone</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#BBDEFB" d="M19,3H5C3.89,3,3,3.9,3,5v14c0,1.1,0.89,2,2,2h14c1.109,0,2-0.9,2-2V5C21,3.9,20.109,3,19,3z"/><polyline fill="#90CAF9" points="19,5 19,19 5,19 5,5 19,5 "/><polygon fill="#FFFFFF" points="10,17 5,12 6.41,10.59 10,14.17 17.59,6.58 19,8 "/></svg>			</div>
			<div class="icon-set__id">
				<span>check_box</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="15" y="19" fill="#26C6DA" width="2" height="2"/><rect x="19" y="7" fill="#26C6DA" width="2" height="2"/><path fill="#0097A7" d="M3,5v14c0,1.1,0.9,2,2,2h4v-2H5V5h4V3H5C3.9,3,3,3.9,3,5z"/><path fill="#26C6DA" d="M19,3v2h2C21,3.9,20.1,3,19,3z"/><rect x="11" y="1" fill="#0097A7" width="2" height="22"/><rect x="19" y="15" fill="#26C6DA" width="2" height="2"/><rect x="15" y="3" fill="#26C6DA" width="2" height="2"/><rect x="19" y="11" fill="#26C6DA" width="2" height="2"/><path fill="#26C6DA" d="M19,21c1.1,0,2-0.9,2-2h-2V21z"/></svg>			</div>
			<div class="icon-set__id">
				<span>flip</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#673AB7" points="13,11 5.83,11 9.41,7.41 8,6 2,12 8,18 9.41,16.59 5.83,13 15,13 "/><polygon fill="#311B92" points="21,13 21,7 19,7 19,11 13,11 15,13 "/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_return</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#039BE5" cx="5" cy="16" r="1.5"/><circle opacity="0.8" fill="#039BE5" cx="9" cy="12" r="1"/><circle opacity="0.8" fill="#039BE5" cx="9" cy="8" r="1"/><rect x="3" y="19" fill="#039BE5" width="18" height="2"/><circle fill="#039BE5" cx="5" cy="8" r="1.5"/><circle fill="#039BE5" cx="5" cy="12" r="1.5"/><circle opacity="0.8" fill="#039BE5" cx="9" cy="16" r="1"/><path fill="#81D4FA" d="M17,16.5c0.279,0,0.5-0.22,0.5-0.5s-0.221-0.5-0.5-0.5s-0.5,0.22-0.5,0.5S16.721,16.5,17,16.5z"/><rect x="3" y="3" fill="#039BE5" width="18" height="2"/><path fill="#81D4FA" d="M17,8.5c0.279,0,0.5-0.22,0.5-0.5S17.279,7.5,17,7.5S16.5,7.72,16.5,8S16.721,8.5,17,8.5z"/><path fill="#81D4FA" d="M17,12.5c0.279,0,0.5-0.22,0.5-0.5s-0.221-0.5-0.5-0.5s-0.5,0.22-0.5,0.5S16.721,12.5,17,12.5z"/><circle fill="#4FC3F7" cx="13" cy="8" r="1"/><circle fill="#4FC3F7" cx="13" cy="12" r="1"/><circle fill="#4FC3F7" cx="13" cy="16" r="1"/></svg>			</div>
			<div class="icon-set__id">
				<span>blur_linear</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path d="M20,6h-8l-2-2H4C2.9,4,2.01,4.9,2.01,6L2,18c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V8C22,6.9,21.1,6,20,6z"/><g><path fill="#FFC107" d="M2,18c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2v-2H2.001L2,18z"/><path fill="#FF9800" d="M20,6h-8l-2-2H4C2.9,4,2.01,4.9,2.01,6L2.001,16H22V8C22,6.9,21.1,6,20,6z"/><rect x="4" y="8" fill="#D7CCC8" width="16" height="8"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>folder_open</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#29B6F6" d="M21,16.5V20c0,0.55-0.45,1-1,1C10.609,21,3,13.391,3,4c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.57,3.57c0.109,0.35,0.029,0.74-0.25,1l-2.2,2.21c0.271,0.52,0.569,1.04,0.92,1.56c0.21,0.32,0.44,0.63,0.681,0.94c0.129,0.159,0.25,0.3,0.369,0.45c0.29,0.34,0.59,0.658,0.9,0.959c0.22,0.211,0.449,0.42,0.689,0.631c0.12,0.109,0.24,0.22,0.37,0.31c0.13,0.101,0.25,0.2,0.38,0.3c0.131,0.11,0.26,0.2,0.4,0.29c0.141,0.101,0.279,0.19,0.42,0.29c0.279,0.2,0.58,0.381,0.88,0.54c0.19,0.11,0.39,0.22,0.58,0.32l2.2-2.2c0.279-0.27,0.67-0.35,1.02-0.24c1.12,0.37,2.32,0.57,3.57,0.57C20.55,15.5,21,15.95,21,16.5z"/><path fill="#1976D2" d="M20.371,3.821l-2.885-2.883H16.98v3.833l-2.317-2.318l-0.712,0.713l2.823,2.823L13.951,8.81l0.712,0.712l2.317-2.317v3.833h0.506l2.885-2.884l-2.174-2.166L20.371,3.821z M17.991,2.872l0.95,0.949l-0.95,0.95V2.872z M18.941,8.153l-0.95,0.95V7.205L18.941,8.153z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="none" d="M0,0h24v24H0V0z"/><g id="Capa_2"><path fill="#1976D2" d="M21,19.68V20c0,0.55-0.45,1-1,1C10.609,21,3,13.391,3,4c0-0.55,0.45-1,1-1h0.689L4.7,3.06c0,0-0.13,3.77,1.92,7.72c0.271,0.52,0.569,1.04,0.92,1.56c0.21,0.32,0.44,0.63,0.681,0.94c0.129,0.159,0.25,0.3,0.369,0.45c0.29,0.34,0.59,0.658,0.9,0.959c0.22,0.211,0.449,0.42,0.689,0.631c0.12,0.1,0.24,0.199,0.37,0.31c0.13,0.101,0.25,0.2,0.38,0.3c0.131,0.102,0.271,0.19,0.4,0.29c0.141,0.101,0.279,0.19,0.42,0.29c0.279,0.182,0.58,0.37,0.88,0.54c0.19,0.11,0.39,0.22,0.58,0.32c2.05,1.069,4.601,1.87,7.761,2.21L21,19.68z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>phone_bluetooth_speaker</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="5.094" y="2.875" fill="#FFB74D" width="7.906" height="10.031"/><rect x="8.438" y="3.469" opacity="0.3" fill="#FFA726" width="5.313" height="9.156"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#BBDEFB" d="M18,2H6C4.9,2,4,2.9,4,4v16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V4C20,2.9,19.1,2,18,2z M6,4h5v8l-2.5-1.5L6,12V4z M6,19l3-3.859l2.14,2.579l3-3.86L18,19H6z"/><g><polygon fill="#43A047" points="18.169,18.979 5.938,18.979 8.995,15.047 11.179,17.678 14.237,13.736 	"/><polygon fill="#66BB6A" points="5.938,18.979 8.995,15.047 12.258,18.979 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>photo_album</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><polygon fill="#E53935" points="22,12 22,18 16,18 18.29,15.71 13.41,10.83 9.41,14.83 2,7.41 3.41,6 9.41,12 13.41,8 19.71,14.29"/><path fill="none" d="M0,0h24v24H0V0z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>trending_down</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#546E7A" points="15,11 15,5 12,2 9,5 9,7 3,7 3,21 21,21 21,11 "/><rect x="5" y="17" fill="#BBDEFB" width="2" height="2"/><rect x="5" y="13" fill="#BBDEFB" width="2" height="2"/><rect x="5" y="9" fill="#BBDEFB" width="2" height="2"/><rect x="11" y="17" fill="#BBDEFB" width="2" height="2"/><rect x="11" y="13" fill="#BBDEFB" width="2" height="2"/><rect x="11" y="9" fill="#BBDEFB" width="2" height="2"/><rect x="11" y="5" fill="#BBDEFB" width="2" height="2"/><rect x="17" y="17" fill="#BBDEFB" width="2" height="2"/><rect x="17" y="13" fill="#BBDEFB" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>location_city</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path opacity="0.8" fill="#90A4AE" d="M20.969,16v-2l-8-5V3.5c0-0.83-0.67-1.5-1.5-1.5s-1.5,0.67-1.5,1.5V9l-8,5v2l8-2.5V19l-2,1.5V22l3.5-1l3.5,1v-1.5l-2-1.5v-5.5L20.969,16z"/><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z"/></g><g id="Capa_2"><polygon fill="#9FA8DA" points="3.016,5.281 4.25,3.984 19.984,19.719 18.703,20.969 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>airplanemode_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M20,4H4C2.89,4,2.01,4.89,2.01,6L2,18c0,1.109,0.89,2,2,2h16c1.109,0,2-0.891,2-2V6C22,4.89,21.109,4,20,4z"/><rect x="4" y="6" fill="#78909C" width="16" height="12"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFD54F" d="M11,10v1h3c0.55,0,1,0.45,1,1v3c0,0.55-0.45,1-1,1h-1v1h-2v-1H9v-2h4v-1h-3c-0.55,0-1-0.45-1-1V9c0-0.55,0.45-1,1-1h1V7h2v1h2v2H11z"/><rect x="11" y="7" fill="#FFCA28" width="2" height="1"/><rect x="11" y="16" fill="#FFCA28" width="2" height="1"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_atm</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#66BB6A" d="M6.58,3.58L5.15,2.15C2.76,3.97,1.18,6.8,1.03,10h2C3.18,7.35,4.54,5.03,6.58,3.58z"/><path fill="#66BB6A" d="M19.971,10h2c-0.15-3.2-1.729-6.03-4.131-7.85l-1.43,1.43C18.46,5.03,19.82,7.35,19.971,10z"/><path fill="#DCE775" d="M20,18v1H3v-1l2-2v-5.5c0-3.07,2.13-5.64,5-6.32V3.5C10,2.67,10.67,2,11.5,2S13,2.67,13,3.5v0.68c0.24,0.06,0.471,0.12,0.689,0.21C16.21,5.29,18,7.68,18,10.5V16L20,18z"/><path fill="#DCE775" d="M11.5,22c0.14,0,0.27-0.01,0.4-0.04c0.65-0.13,1.19-0.58,1.44-1.18c0.1-0.24,0.16-0.5,0.16-0.78h-4C9.5,21.1,10.4,22,11.5,22z"/><polygon fill="#FDD835" points="20,18 20,19 3,19 3,18 4.96,16.04 18.04,16.04 	"/></g><g id="Capa_2"><path fill="#FDD835" d="M13.689,4.39C13.62,4.38,13.54,4.36,13.45,4.33c-1.6-0.5-2.93-0.28-3.45-0.16V3.5C10,2.67,10.67,2,11.5,2S13,2.67,13,3.5v0.68C13.24,4.24,13.471,4.3,13.689,4.39z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>notifications_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#78909C" d="M21,20c-2.5-3.5-6-5.1-11-5.1V19l-7-7l7-7v4c6.971,1,9.98,5.96,10.99,10.939C20.99,19.96,21,19.98,21,20z"/><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><path fill="#78909C" d="M20.99,19.939l-0.01,0.011c-4.24-9.7-17.01-8.11-17.952-7.98L10,5v4C16.971,10,19.98,14.96,20.99,19.939z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>reply</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#B0BEC5" d="M12,1.999c-5.52,0-10,4.48-10,10c0,5.52,4.48,10,10,10c5.519,0,10-4.48,10-10C21.999,6.479,17.519,1.999,12,1.999z M10,16.499v-9l6,4.5L10,16.499z"/><path opacity="0.3" fill="#90A4AE" enable-background="new    " d="M21.999,11.999c0,5.52-4.48,10-10,10v-20C17.519,1.999,21.999,6.479,21.999,11.999z"/><polygon fill="#29B6F6" points="15.999,11.999 10,16.499 10,7.499 	"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>play_circle_fill</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#80DEEA" cx="5" cy="12" r="4"/><path fill="#00ACC1" d="M17,6c-3.31,0-6,2.69-6,6c0,3.31,2.69,6,6,6s6-2.69,6-6C23,8.69,20.31,6,17,6z M17,16c-2.21,0-4-1.79-4-4s1.79-4,4-4s4,1.79,4,4S19.21,16,17,16z"/></svg>			</div>
			<div class="icon-set__id">
				<span>hdr_weak</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_2_"><circle fill="#90A4AE" cx="12" cy="11.984" r="3.016"/><path fill="#90A4AE" d="M19.561,16.751c0,0-0.981,0.812-1.545,1.015l0.844-1.483C18.859,16.281,19.684,16.174,19.561,16.751z"/><g id="Capa_1_1_" display="none"><path display="inline" fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path display="inline" d="M15,11.92c-0.01-0.32-0.07-0.63-0.17-0.91c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c-0.03,0-0.07-0.01-0.11-0.02c-0.03,0-0.06-0.01-0.09-0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.15,3.15l0.02-0.16C15.01,11.98,15,11.95,15,11.92z M18.08,17.811l-2.41-2.41l-1.42-1.42l-4.23-4.229L3.27,3L2,4.27l2.28,2.28l0.46,0.46c-0.25,0.18-0.48,0.38-0.71,0.6C3.87,7.75,3.72,7.9,3.56,8.06C3.38,8.24,3.2,8.44,3.03,8.63C2.7,9.01,2.4,9.41,2.12,9.84c-0.16,0.23-0.3,0.48-0.44,0.73C1.42,11.03,1.2,11.5,1,12c0.24,0.6,0.52,1.18,0.85,1.72c0.11,0.21,0.24,0.41,0.38,0.601c0.15,0.222,0.3,0.439,0.46,0.649c0.17,0.2,0.33,0.4,0.51,0.591C3.31,15.68,3.42,15.8,3.54,15.92c0.26,0.28,0.54,0.54,0.83,0.78c0.2,0.17,0.4,0.33,0.61,0.479c0,0,0,0.01,0.01,0.021c0.04,0.03,0.07,0.05,0.11,0.069c0.16,0.131,0.34,0.238,0.51,0.34c0.08,0.07,0.16,0.108,0.24,0.16c0.1,0.068,0.22,0.141,0.33,0.188c0.2,0.12,0.39,0.229,0.6,0.319c0.24,0.119,0.49,0.238,0.74,0.33c0.09,0.051,0.19,0.08,0.29,0.108C8,18.811,8.2,18.88,8.41,18.939c0,0,0,0,0.01,0c0.24,0.08,0.48,0.148,0.73,0.211c0.3,0.08,0.61,0.14,0.93,0.188c0.09,0.021,0.19,0.03,0.29,0.04c0.22,0.04,0.44,0.062,0.66,0.08h0.04c0.31,0.03,0.62,0.04,0.93,0.04c0.16,0,0.33,0,0.49-0.01c0.1,0,0.21-0.012,0.31-0.021c0.24-0.01,0.47-0.04,0.7-0.068c0.141-0.01,0.279-0.039,0.42-0.062c0.68-0.108,1.33-0.271,1.95-0.489c0.17-0.061,0.34-0.119,0.51-0.188l0.42,0.42L19.73,22L21,20.73L18.08,17.811z M12.58,16.96C12.39,16.99,12.2,17,12,17c-2.76,0-5-2.24-5-5c0-0.2,0.01-0.39,0.04-0.58c0.02-0.15,0.04-0.3,0.07-0.45c0.09-0.41,0.24-0.8,0.42-1.17l1.55,1.55c-0.02,0.09-0.04,0.18-0.05,0.27C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.181-0.028,0.271-0.05l1.552,1.55c-0.37,0.181-0.761,0.33-1.17,0.421C12.88,16.92,12.73,16.939,12.58,16.96z M21.76,9.68c-0.33-0.49-0.689-0.94-1.08-1.37c-0.199-0.21-0.4-0.42-0.609-0.61c-0.182-0.17-0.359-0.33-0.541-0.47c-0.118-0.11-0.25-0.21-0.379-0.31c-0.012,0-0.012-0.01-0.012-0.01c-0.05-0.04-0.1-0.07-0.147-0.11c-0.142-0.1-0.28-0.2-0.421-0.3s-0.301-0.2-0.449-0.28c-0.102-0.07-0.2-0.13-0.313-0.18c-0.18-0.11-0.357-0.21-0.539-0.29C17.23,5.73,17.2,5.71,17.16,5.7c-0.24-0.13-0.48-0.24-0.73-0.33c-0.029-0.02-0.06-0.03-0.1-0.04c-0.25-0.1-0.5-0.19-0.75-0.27c-0.57-0.18-1.15-0.32-1.75-0.42c-0.301-0.04-0.6-0.08-0.91-0.1C12.62,4.51,12.31,4.5,12,4.5c-0.21,0-0.43,0.01-0.64,0.02c-0.14,0-0.27,0.01-0.41,0.03c-0.34,0.03-0.68,0.07-1.01,0.13c-0.17,0.03-0.33,0.06-0.49,0.1C9.28,4.82,9.12,4.86,8.96,4.9S8.64,4.99,8.48,5.04S8.16,5.14,8.01,5.2l2.15,2.15l0.01,0.01c0.29-0.12,0.58-0.2,0.89-0.27c0.14-0.03,0.28-0.05,0.43-0.06C11.66,7.01,11.83,7,12,7c2.76,0,5,2.24,5,5c0,0.17-0.01,0.34-0.029,0.51c-0.011,0.15-0.029,0.29-0.061,0.432c-0.07,0.311-0.15,0.601-0.271,0.891l0.012,0.01l2.771,2.771l0.142,0.141c0.188-0.16,0.369-0.32,0.551-0.49c0.18-0.17,0.35-0.34,0.52-0.52c0.17-0.182,0.33-0.37,0.49-0.561c0.148-0.189,0.3-0.382,0.449-0.58c0.141-0.199,0.279-0.399,0.41-0.609s0.261-0.42,0.381-0.641c0.238-0.432,0.449-0.892,0.63-1.352C22.66,11.18,22.25,10.4,21.76,9.68z"/></g><g id="Capa_2"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#90A4AE" d="M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M21.76,9.68c-0.31-0.49-0.68-0.94-1.08-1.37c-0.199-0.21-0.4-0.42-0.609-0.61c-0.182-0.17-0.359-0.33-0.541-0.47c-0.118-0.11-0.25-0.21-0.379-0.31c-0.012,0-0.012-0.01-0.012-0.01c-0.05-0.04-0.1-0.07-0.147-0.11c-0.142-0.1-0.28-0.2-0.421-0.3s-0.301-0.2-0.449-0.28c-0.102-0.07-0.2-0.13-0.313-0.18c-0.18-0.11-0.357-0.21-0.539-0.29C17.23,5.73,17.2,5.71,17.16,5.7c-0.24-0.13-0.48-0.24-0.73-0.33c-0.029-0.02-0.06-0.03-0.1-0.04c-0.25-0.1-0.5-0.19-0.75-0.27c-0.57-0.19-1.15-0.33-1.75-0.42c-0.301-0.04-0.6-0.08-0.91-0.1C12.62,4.51,12.31,4.5,12,4.5c-0.21,0-0.43,0.01-0.64,0.02c-0.14,0-0.27,0.01-0.41,0.03c-0.34,0.03-0.68,0.07-1.01,0.13c-0.17,0.03-0.33,0.06-0.49,0.1C9.28,4.82,9.12,4.86,8.96,4.9S8.64,4.99,8.48,5.04S8.16,5.14,8.01,5.2l2.15,2.15c0.29-0.12,0.59-0.2,0.9-0.26c0.14-0.03,0.28-0.05,0.43-0.06C11.66,7.01,11.83,7,12,7c2.76,0,5,2.24,5,5c0,0.17-0.01,0.34-0.029,0.51c-0.011,0.15-0.029,0.29-0.061,0.432c-0.061,0.311-0.141,0.608-0.26,0.898l2.77,2.771l0.141,0.14c0.189-0.16,0.369-0.319,0.551-0.49c0.181-0.17,0.351-0.34,0.521-0.52c0.17-0.181,0.33-0.37,0.49-0.562c0.147-0.188,0.3-0.381,0.448-0.58c0.142-0.199,0.279-0.398,0.41-0.608s0.262-0.421,0.381-0.642C22.609,12.92,22.82,12.47,23,12C22.68,11.18,22.27,10.4,21.76,9.68z M13.029,16.891c-0.149,0.029-0.299,0.051-0.449,0.069C12.39,16.99,12.2,17,12,17c-2.76,0-5-2.24-5-5c0-0.2,0.01-0.39,0.04-0.58c0.02-0.15,0.04-0.3,0.07-0.45C7.32,9.94,7.86,9.02,8.6,8.33L6.27,6C5.78,6.26,5.31,6.57,4.86,6.91c-0.04,0.03-0.08,0.06-0.12,0.1c-0.25,0.18-0.48,0.38-0.71,0.6C3.87,7.75,3.72,7.9,3.56,8.06C3.38,8.24,3.2,8.44,3.03,8.63C2.7,9.01,2.4,9.41,2.12,9.84c-0.16,0.23-0.3,0.48-0.44,0.73C1.42,11.03,1.19,11.5,1,12c0.24,0.6,0.52,1.18,0.85,1.72c0.11,0.21,0.24,0.41,0.38,0.601c0.15,0.222,0.3,0.439,0.46,0.649c0.17,0.2,0.33,0.4,0.51,0.591C3.31,15.68,3.42,15.8,3.54,15.92c0.26,0.28,0.54,0.54,0.83,0.78c0.2,0.17,0.4,0.33,0.61,0.479c0,0,0,0.01,0.01,0.021c0.04,0.03,0.07,0.05,0.11,0.069c0.16,0.131,0.34,0.238,0.51,0.34c0.08,0.07,0.16,0.108,0.24,0.16c0.1,0.068,0.22,0.141,0.33,0.188c0.2,0.12,0.39,0.229,0.6,0.319c0.24,0.119,0.49,0.238,0.74,0.33c0.09,0.051,0.19,0.08,0.29,0.108C8,18.811,8.2,18.88,8.41,18.939c0,0,0,0,0.01,0c0.24,0.08,0.48,0.148,0.73,0.211c0.3,0.08,0.61,0.14,0.93,0.188c0.09,0.021,0.19,0.03,0.29,0.04c0.22,0.04,0.44,0.062,0.66,0.08h0.04c0.31,0.03,0.62,0.04,0.93,0.04c0.16,0,0.33,0,0.49-0.01c0.1,0,0.21-0.012,0.31-0.021c0.24-0.01,0.47-0.04,0.7-0.068c0.141-0.01,0.279-0.039,0.42-0.062c0.68-0.108,1.33-0.271,1.95-0.489c0.17-0.061,0.34-0.119,0.51-0.188c0.591-0.24,1.16-0.521,1.7-0.851L15.67,15.4C14.98,16.141,14.061,16.68,13.029,16.891z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z M14.83,11.01c-0.01-0.04-0.021-0.07-0.03-0.1c-0.01-0.03-0.021-0.06-0.04-0.08c-0.05-0.14-0.119-0.27-0.199-0.4c-0.051-0.08-0.101-0.15-0.148-0.22c-0.17-0.23-0.37-0.43-0.592-0.59c-0.09-0.07-0.188-0.14-0.288-0.2c-0.11-0.06-0.222-0.12-0.341-0.17c-0.012-0.01-0.021-0.01-0.029-0.01c-0.092-0.04-0.182-0.08-0.271-0.1c-0.021-0.01-0.041-0.02-0.07-0.02c-0.061-0.02-0.119-0.03-0.18-0.05c-0.011,0-0.011,0-0.021,0c0,0-0.011,0-0.011-0.01c-0.039-0.01-0.069-0.01-0.102-0.01c-0.02-0.01-0.03-0.01-0.04-0.01c-0.01,0-0.03-0.01-0.05-0.01v0.01c-0.13-0.02-0.27-0.03-0.41-0.03l-0.17,0.01l3.148,3.15C15,12.11,15,12.06,15,12v-0.08C14.99,11.6,14.93,11.29,14.83,11.01zM9.89,9.88c-0.04,0.03-0.07,0.07-0.1,0.11c-0.07,0.06-0.12,0.12-0.17,0.19C9.61,10.19,9.6,10.2,9.6,10.21c-0.06,0.06-0.1,0.13-0.14,0.2c-0.18,0.28-0.31,0.6-0.39,0.93c-0.02,0.09-0.03,0.19-0.04,0.28C9.01,11.75,9,11.87,9,12c0,1.66,1.34,3,3,3c0.13,0,0.25-0.01,0.38-0.03c0.09-0.01,0.19-0.02,0.28-0.04c0.33-0.08,0.648-0.21,0.93-0.39c0.07-0.04,0.141-0.08,0.2-0.14c0.17-0.12,0.33-0.262,0.46-0.42l-4.23-4.23C9.97,9.79,9.93,9.83,9.89,9.88z"/><path fill="#66BB6A" d="M10.02,9.75l4.23,4.23c-0.13,0.158-0.29,0.3-0.46,0.42c-0.06,0.06-0.13,0.1-0.2,0.14c-0.279,0.18-0.6,0.31-0.93,0.39c-0.09,0.021-0.19,0.03-0.28,0.04C12.25,14.99,12.13,15,12,15c-1.66,0-3-1.34-3-3c0-0.13,0.01-0.25,0.03-0.38c0.01-0.09,0.02-0.19,0.04-0.28c0.08-0.33,0.21-0.65,0.39-0.93c0.04-0.07,0.08-0.14,0.14-0.2c0-0.01,0.01-0.02,0.02-0.03c0.05-0.07,0.1-0.13,0.17-0.19c0.03-0.04,0.06-0.08,0.1-0.11C9.93,9.83,9.97,9.79,10.02,9.75z"/><path fill="#66BB6A" d="M15,11.92V12c0,0.06,0,0.11-0.01,0.17l-3.15-3.15l0.17-0.01c0.14,0,0.28,0.01,0.41,0.03c0.01-0.01,0.03-0.01,0.05,0c0.01,0,0.02,0,0.04,0.01c0.03,0,0.061,0,0.102,0.01c0.011,0,0.021,0.01,0.029,0.01c0.061,0.02,0.119,0.03,0.18,0.05c0.029,0,0.051,0.01,0.068,0.02c0.092,0.02,0.182,0.06,0.271,0.1c0.01,0,0.02,0,0.029,0.01c0.119,0.05,0.229,0.11,0.34,0.17c0.101,0.06,0.201,0.13,0.291,0.2c0.22,0.16,0.42,0.36,0.59,0.59c0.05,0.07,0.1,0.14,0.148,0.22c0.08,0.13,0.149,0.26,0.199,0.4c0.03,0.06,0.051,0.12,0.07,0.18C14.93,11.29,14.99,11.6,15,11.92z"/><path fill="#90A4AE" d="M10.02,9.75l4.23,4.23c-0.13,0.158-0.29,0.3-0.46,0.42c-0.06,0.06-0.13,0.1-0.2,0.14c-0.279,0.18-0.6,0.31-0.93,0.39c-0.09,0.021-0.19,0.03-0.28,0.04C12.25,14.99,12.13,15,12,15c-1.66,0-3-1.34-3-3c0-0.13,0.01-0.25,0.03-0.38c0.01-0.09,0.02-0.19,0.04-0.28c0.08-0.33,0.21-0.65,0.39-0.93c0.04-0.07,0.08-0.14,0.14-0.2c0-0.01,0.01-0.02,0.02-0.03c0.05-0.07,0.1-0.13,0.17-0.19c0.03-0.04,0.06-0.08,0.1-0.11C9.93,9.83,9.97,9.79,10.02,9.75z"/><path fill="#90A4AE" d="M15,11.92V12c0,0.06,0,0.11-0.01,0.17l-3.15-3.15l0.17-0.01c0.14,0,0.28,0.01,0.41,0.03c0.01-0.01,0.03-0.01,0.05,0c0.01,0,0.02,0,0.04,0.01c0.03,0,0.061,0,0.102,0.01c0.011,0,0.021,0.01,0.029,0.01c0.061,0.02,0.119,0.03,0.18,0.05c0.029,0,0.051,0.01,0.068,0.02c0.092,0.02,0.182,0.06,0.271,0.1c0.01,0,0.02,0,0.029,0.01c0.119,0.05,0.229,0.11,0.34,0.17c0.101,0.06,0.201,0.13,0.291,0.2c0.22,0.16,0.42,0.36,0.59,0.59c0.05,0.07,0.1,0.14,0.148,0.22c0.08,0.13,0.149,0.26,0.199,0.4c0.03,0.06,0.051,0.12,0.07,0.18C14.93,11.29,14.99,11.6,15,11.92z"/><path fill="#B0BEC5" d="M19.15,6.92c-0.012,0-0.012-0.01-0.012-0.01c-0.051-0.04-0.101-0.07-0.148-0.11c-0.141-0.1-0.28-0.2-0.42-0.3c-0.141-0.1-0.301-0.2-0.45-0.28c-0.101-0.07-0.2-0.13-0.312-0.18c-0.181-0.11-0.358-0.21-0.539-0.29C17.23,5.73,17.2,5.71,17.16,5.7c-0.24-0.13-0.48-0.24-0.73-0.33c-0.029-0.02-0.06-0.03-0.1-0.04c-0.25-0.1-0.5-0.19-0.75-0.27c-0.57-0.19-1.15-0.33-1.75-0.42c-0.301-0.04-0.6-0.08-0.91-0.1C12.62,4.51,12.31,4.5,12,4.5c-0.21,0-0.43,0.01-0.64,0.02c-0.14,0-0.27,0.01-0.41,0.03c-0.34,0.03-0.68,0.07-1.01,0.13c-0.17,0.03-0.33,0.06-0.49,0.1C9.28,4.82,9.12,4.86,8.96,4.9S8.64,4.99,8.48,5.04S8.16,5.14,8.01,5.2l2.15,2.15c0.29-0.12,0.59-0.2,0.9-0.26c0.14-0.03,0.28-0.05,0.43-0.06C11.66,7.01,11.83,7,12,7c2.76,0,5,2.24,5,5c0,0.17-0.01,0.34-0.029,0.51c-0.011,0.15-0.029,0.29-0.061,0.432c-0.061,0.311-0.141,0.608-0.26,0.898l2.77,2.771c0.84-1.353,1.33-2.94,1.33-4.65C20.75,10.08,20.16,8.35,19.15,6.92z M13.029,16.891c-0.149,0.029-0.299,0.051-0.449,0.069C12.39,16.99,12.2,17,12,17c-2.76,0-5-2.24-5-5c0-0.2,0.01-0.39,0.04-0.58c0.02-0.15,0.04-0.3,0.07-0.45C7.32,9.94,7.86,9.02,8.6,8.33L6.27,6C5.78,6.26,5.31,6.57,4.86,6.91c-1.02,1.43-1.61,3.17-1.61,5.05c0,1.96,0.64,3.76,1.73,5.22c0,0,0,0.012,0.01,0.021c0.04,0.029,0.07,0.05,0.11,0.068c0.16,0.131,0.34,0.239,0.51,0.342c0.08,0.068,0.16,0.107,0.24,0.158c0.1,0.069,0.22,0.142,0.33,0.189c0.2,0.12,0.39,0.229,0.6,0.32c0.24,0.119,0.49,0.238,0.74,0.328c0.09,0.052,0.19,0.08,0.29,0.109C8,18.811,8.2,18.88,8.41,18.939c0,0,0,0,0.01,0c0.24,0.08,0.48,0.148,0.73,0.211c0.3,0.08,0.61,0.14,0.93,0.188c0.09,0.021,0.19,0.03,0.29,0.04c0.22,0.04,0.44,0.062,0.66,0.08h0.04c0.31,0.03,0.62,0.04,0.93,0.04c0.16,0,0.33,0,0.49-0.01c0.1,0,0.21-0.012,0.31-0.021c0.24-0.01,0.47-0.04,0.7-0.068c0.141-0.01,0.279-0.039,0.42-0.062c0.68-0.108,1.33-0.271,1.95-0.489c0.17-0.061,0.34-0.119,0.51-0.188c0.591-0.24,1.16-0.521,1.7-0.851L15.67,15.4C14.98,16.141,14.061,16.68,13.029,16.891z"/></g><path fill="#B0BEC5" d="M7.344,10.094c0,0,0.406-2.063,3.266-2.891L8.48,5.04c0,0-2.808,0.788-4.074,2.569C3.14,9.39,7.344,10.094,7.344,10.094z"/><path fill="#B0BEC5" d="M16.65,13.84c0,0-0.588,1.723-2.433,2.645l2.161,2.176c0,0,2.262-0.878,3.229-2.348L16.65,13.84z"/><g id="Capa_3"><polygon fill="#9FA8DA" points="2,4.281 3.266,2.969 21,20.703 19.734,21.984 		"/></g></g><g id="Capa_2_1_"></g><path fill="none" d="M25.583,0.667h24v24h-24V0.667z"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M47.583,6.387l-4.6-3.86l-1.291,1.53l4.601,3.86L47.583,6.387zM33.603,3.947l-1.419-1.42l-4.73,3.97l1.42,1.42L33.603,3.947z"/><path fill="#90A4AE" d="M37.583,4.667c-4.97,0-9,4.03-9,9s4.021,9,9,9c4.971,0,9-4.03,9-9S42.554,4.667,37.583,4.667z"/><path fill="#E0E0E0" d="M37.583,20.667c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S41.453,20.667,37.583,20.667z"/><polygon fill="#9FA8DA" points="45.673,22.667 27.232,4.237 28.503,2.957 46.942,21.397 "/></svg>			</div>
			<div class="icon-set__id">
				<span>visibility_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><polygon fill="#43A047" points="22,6 22,12 19.71,9.71 13.41,16 9.41,12 3.41,18 2,16.59 9.41,9.17 13.41,13.17 18.29,8.29 16,6"/><path fill="none" d="M0,0h24v24H0V0z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>trending_up</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#D32F2F" points="6.41,5 5,6.41 10.59,12 5,17.59 6.41,19 12,13.41 12,10.59 "/><polygon fill="#F44336" points="19,17.59 13.41,12 19,6.41 17.59,5 12,10.59 12,13.41 17.59,19 "/></svg>			</div>
			<div class="icon-set__id">
				<span>clear</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFCCBC" d="M21,9v9l-2.188,2.531c0,0-2.125,0.594-2.188,0.594s-2.938-3.594-2.938-3.594l-2.875-3.719c0,0-2.281-4.375-2.313-4.563C8.466,9.061,9.311,6.155,9.311,6.155l1.531-1.531c0,0,2.969-1.375,3.063-1.406C13.999,3.187,18,4,18.094,4.063s2.375,3.687,2.375,3.687L21,9z"/><path fill="#FFAB91" d="M17,20c-0.29,0-0.561-0.061-0.76-0.15c-0.711-0.369-1.211-0.88-1.711-2.38c-0.51-1.56-1.469-2.29-2.389-3c-0.79-0.61-1.61-1.239-2.32-2.53C9.29,10.98,9,9.93,9,9c0-2.8,2.2-5,5-5s3.859-0.141,6.953,4.938L21,9c0-3.93-3.07-7-7-7c-3.93,0-7,3.07-7,7c0,1.26,0.38,2.65,1.07,3.9c0.91,1.647,1.98,2.479,2.85,3.147c0.81,0.62,1.39,1.07,1.71,2.052c0.601,1.818,1.37,2.84,2.729,3.551C15.87,21.88,16.43,22,17,22c2.21,0,4-1.79,4-4h-2C19,19.1,18.1,20,17,20z"/><path fill="#43A047" d="M7.64,2.64L6.22,1.22C4.23,3.21,3,5.96,3,9s1.23,5.79,3.22,7.78l1.41-1.41C6.01,13.74,5,11.49,5,9S6.01,4.26,7.64,2.64z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFAB91" d="M19.688,19.184c-0.783,0-1.719-0.168-2.506-0.795c-1.062-0.846-1.527-2.248-1.388-4.17c0.243-3.333-0.237-6.921-1.39-7.25c-0.575,0.166-1.919,0.852-2.303,2.086l-1.018-1.034c1.042-1.813,1.313-2.417,2.787-2.596c2.563,0.733,4.183,5.325,3.918,8.94c-0.062,0.841-0.001,1.949,0.638,2.457c0.801,0.638,2.854,0.242,2.573,0.209V18C20.902,18.07,20.514,19.184,19.688,19.184z"/></svg>			</div>
			<div class="icon-set__id">
				<span>hearing</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4DD0E1" d="M22.016,3.984v12c0,1.1-0.9,2-2,2h-14l-4,4l0.01-11v-7c0-1.1,0.891-2,1.99-2h16C21.115,1.984,22.016,2.884,22.016,3.984z"/><path opacity="0.3" fill="#26C6DA" enable-background="new    " d="M22.016,3.984v7H2.025v-7c0-1.1,0.891-2,1.99-2h16C21.115,1.984,22.016,2.884,22.016,3.984z"/><rect x="15" y="9" fill="#ECEFF1" width="2" height="2"/><rect x="11" y="9" fill="#ECEFF1" width="2" height="2"/><rect x="7" y="9" fill="#ECEFF1" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>sms</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FDD835" d="M11.99,2C6.47,2,2,6.48,2,12c0,5.52,4.47,10,9.99,10C17.52,22,22,17.52,22,12C22,6.48,17.52,2,11.99,2z"/><circle fill="#FDD835" cx="12" cy="12" r="8"/><circle fill="#546E7A" cx="15.5" cy="9.5" r="1.5"/><circle fill="#546E7A" cx="8.5" cy="9.5" r="1.5"/><path fill="#546E7A" d="M12,17.5c2.33,0,4.311-1.46,5.109-3.5H6.89C7.69,16.04,9.67,17.5,12,17.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>insert_emoticon</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#7986CB" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><path fill="#3F51B5" d="M12,5c1.66,0,3,1.34,3,3s-1.34,3-3,3S9,9.66,9,8S10.34,5,12,5z M12,19.2c-2.5,0-4.71-1.28-6-3.22c0.03-1.99,4-3.08,6-3.08c1.99,0,5.971,1.09,6,3.08C16.71,17.92,14.5,19.2,12,19.2z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>account_circle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><polygon fill="#C5CAE9" points="18.878,5.873 17.744,7.023 13.996,3.271 15.128,2.123 	"/><path fill="#E57373" d="M20.708,4.042c0.39-0.39,0.39-1.02,0-1.41l-2.34-2.34c-0.39-0.39-1.021-0.39-1.41,0l-1.83,1.83l3.75,3.75L20.708,4.042z"/><g><polygon fill="#7986CB" points="3.998,16.99 7.748,16.99 3.998,13.24 		"/><polygon fill="#3F51B5" points="17.75,7 7.748,16.99 3.998,13.24 14.047,3.266 		"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#B71C1C" fill-opacity="0.36" d="M0,20h24v4H0V20z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_color</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M18.5,0h-14C3.12,0,2,1.12,2,2.5v19C2,22.88,3.12,24,4.5,24h14c1.38,0,2.5-1.12,2.5-2.5v-19C21,1.12,19.88,0,18.5,0z"/><circle fill="#B0BEC5" cx="11.5" cy="21.5" r="1.5"/><rect x="4" y="3" fill="#78909C" width="15" height="16"/></svg>			</div>
			<div class="icon-set__id">
				<span>tablet_mac</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.11,4,6.61,5.64,5.36,8.04C2.35,8.36,0,10.9,0,14c0,3.311,2.69,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z M19,18H6c-2.21,0-4-1.79-4-4s1.79-4,4-4s4,1.79,4,4h2c0-2.76-1.86-5.08-4.4-5.78C8.61,6.88,10.2,6,12,6c3.029,0,5.5,2.47,5.5,5.5V12H19c1.65,0,3,1.35,3,3S20.65,18,19,18z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_drama</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#EC407A" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><polygon fill="#CFD8DC" points="12,14 8,10 16,10 "/></svg>			</div>
			<div class="icon-set__id">
				<span>arrow_drop_down_circle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#E57373" d="M12,17.41l9-9L19.59,7L12,14.59V17.41z"/><path fill="#F44336" d="M12,14.59L6.41,9H11V7H3v8h2v-4.59l7,7V14.59z"/></svg>			</div>
			<div class="icon-set__id">
				<span>call_missed</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFA726" d="M20,15.5c-1.25,0-2.45-0.2-3.57-0.57c-0.35-0.109-0.739-0.029-1.02,0.24l-2.2,2.2c-2.83-1.44-5.15-3.75-6.59-6.59l2.2-2.21c0.28-0.26,0.36-0.65,0.25-1C8.7,6.45,8.5,5.25,8.5,4c0-0.55-0.45-1-1-1H4C3.45,3,3,3.45,3,4c0,9.39,7.61,17,17,17c0.55,0,1-0.45,1-1v-3.5C21,15.95,20.55,15.5,20,15.5z M20,4V3.5C20,2.12,18.88,1,17.5,1S15,2.12,15,3.5V4c-0.55,0-1,0.45-1,1v4c0,0.55,0.45,1,1,1h5c0.55,0,1-0.45,1-1V5C21,4.45,20.55,4,20,4z M19.2,4h-3.4V3.5c0-0.94,0.76-1.7,1.7-1.7s1.7,0.76,1.7,1.7V4z"/><g><path fill="#78909C" d="M21.014,16.484v3.5c0,0.55-0.45,1-1,1c-9.391,0-17-7.609-17-17c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.57,3.57c0.109,0.35,0.029,0.74-0.25,1l-2.2,2.21c0.271,0.52,0.569,1.04,0.92,1.56c0.21,0.319,0.44,0.63,0.681,0.94c0.129,0.158,0.25,0.3,0.369,0.449c0.29,0.34,0.59,0.658,0.9,0.959c0.22,0.211,0.449,0.42,0.689,0.631c0.12,0.109,0.24,0.221,0.37,0.311c0.131,0.102,0.25,0.2,0.381,0.3c0.131,0.11,0.26,0.2,0.399,0.29c0.142,0.102,0.278,0.189,0.42,0.29c0.278,0.2,0.58,0.381,0.88,0.54c0.188,0.109,0.392,0.221,0.58,0.32l2.2-2.2c0.277-0.271,0.67-0.351,1.021-0.24c1.118,0.37,2.318,0.57,3.568,0.57C20.563,15.484,21.014,15.935,21.014,16.484z"/><g id="Capa_2"><path fill="#455A64" d="M21.014,19.664v0.32c0,0.55-0.45,1-1,1c-9.391,0-17-7.609-17-17c0-0.55,0.45-1,1-1h0.689l0.011,0.06c0,0-0.13,3.77,1.92,7.72c0.271,0.52,0.569,1.04,0.92,1.56c0.21,0.319,0.44,0.63,0.681,0.94c0.129,0.158,0.25,0.3,0.369,0.449c0.29,0.34,0.59,0.658,0.9,0.959c0.22,0.211,0.449,0.42,0.689,0.631c0.12,0.101,0.24,0.199,0.37,0.311c0.131,0.102,0.25,0.2,0.381,0.3c0.131,0.103,0.271,0.19,0.399,0.29c0.142,0.102,0.278,0.189,0.42,0.29c0.278,0.183,0.58,0.37,0.88,0.54c0.188,0.109,0.392,0.221,0.58,0.32c2.05,1.068,4.603,1.869,7.763,2.21L21.014,19.664z"/></g></g><path fill="#FFCC80" d="M20,3.5V4h-0.8V3.5c0-0.94-0.76-1.7-1.7-1.7s-1.7,0.76-1.7,1.7V4H15V3.5C15,2.12,16.12,1,17.5,1S20,2.12,20,3.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>phone_locked</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#263238" d="M10.564,13.436L4.93,19.07C3.12,17.26,2,14.76,2,12C2,6.48,6.48,2,12,2v7.967c-1.118,0-2.033,0.915-2.033,2.033C9.967,12.559,10.196,13.067,10.564,13.436z M19.07,4.93l-5.635,5.634c0.369,0.369,0.598,0.877,0.598,1.436c0,1.118-0.915,2.033-2.033,2.033V22c5.52,0,10-4.48,10-10C22,9.24,20.88,6.74,19.07,4.93z"/><path fill="#546E7A" d="M12,9.967V2c2.76,0,5.26,1.12,7.07,2.93l-5.635,5.634C13.067,10.196,12.559,9.967,12,9.967z M4.93,19.07C6.74,20.88,9.24,22,12,22v-7.967c-0.559,0-1.067-0.229-1.436-0.598L4.93,19.07z"/><path fill="#64B5F6" d="M12,16.5c-2.49,0-4.5-2.01-4.5-4.5S9.51,7.5,12,7.5s4.5,2.01,4.5,4.5S14.49,16.5,12,16.5z M12,11c-0.55,0-1,0.45-1,1s0.45,1,1,1s1-0.45,1-1S12.55,11,12,11z"/></svg>			</div>
			<div class="icon-set__id">
				<span>album</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="none" points="24,0 24,14 23,14 23,20 21,20 21,17 3,17 3,20 1,20 1,5 3,5 3,14 3.51,14 3.51,4.19 0,4.19 0,0 "/><path fill="#546E7A" d="M7,13c1.66,0,3-1.34,3-3S8.66,7,7,7s-3,1.34-3,3S5.34,13,7,13z"/><path fill="#7E57C2" d="M23,11v9h-2v-3H3v3H1V5h2v9h8V7h8C21.21,7,23,8.79,23,11z"/><polygon fill="#B39DDB" points="23,14 23,20 21,20 21,17 3,17 3,20 1,20 1,5 3,5 3,14 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_hotel</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFA726" d="M12,6v3l4-4l-4-4v3c-4.42,0-8,3.58-8,8c0,1.57,0.46,3.03,1.24,4.26L6.7,14.8C6.25,13.97,6,13.01,6,12C6,8.69,8.69,6,12,6z"/><path fill="#FB8C00" d="M18.76,7.74L17.3,9.2c0.44,0.84,0.7,1.79,0.7,2.8c0,3.311-2.689,6-6,6v-3l-4,4l4,4v-3c4.42,0,8-3.58,8-8C20,10.43,19.54,8.97,18.76,7.74z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>autorenew</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><rect x="10" y="12" fill="#D4E157" width="5" height="6"/><rect x="4" y="5" fill="#EF5350" width="5" height="13"/><rect x="16" y="12" fill="#9CCC65" width="5" height="6"/><rect x="10" y="5" fill="#AB47BC" width="11" height="6"/><path fill="none" d="M0,0h24v24H0V0z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>view_quilt</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#EF5350" d="M15.41,7.41L14,6l-6,6l6,6l1.41-1.41L10.83,12L15.41,7.41z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>navigate_before</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#0097A7" points="16,12 10,18 8.59,16.59 13.17,12 13.16,11.99 8.59,7.41 10,6 "/><path fill="none" d="M0,0h24v24H0V0z"/><polygon opacity="0.3" fill="#00838F" points="16,12 10,18 8.59,16.59 13.17,12 13.16,11.99 15.99,11.99 "/></svg>			</div>
			<div class="icon-set__id">
				<span>chevron_right</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CFD8DC" d="M17,13h-4v-2h2c1.1,0,2-0.89,2-2V7c0-1.11-0.9-2-2-2h-4v2h4v2h-2c-1.1,0-2,0.89-2,2v4h6V13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_2</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M14,12c0-1.1-0.9-2-2-2c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2C13.1,14,14,13.1,14,12z"/><path fill="#FB8C00" d="M12,3c-4.97,0-9,4.03-9,9H0l4,4l4-4H5c0-3.87,3.13-7,7-7s7,3.13,7,7s-3.13,7-7,7c-1.51,0-2.91-0.49-4.06-1.3l-1.42,1.44C8.04,20.3,9.94,21,12,21c4.971,0,9-4.03,9-9S16.971,3,12,3z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_backup_restore</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><rect x="15" y="3" fill="#455A64" width="2" height="7"/><path fill="#78909C" d="M21,16.5V20c0,0.55-0.45,1-1,1C10.61,21,3,13.39,3,4c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.57,3.57c0.11,0.35,0.03,0.74-0.25,1l-2.2,2.21c0.27,0.52,0.57,1.04,0.92,1.56c0.21,0.32,0.44,0.63,0.68,0.94c0.13,0.16,0.25,0.3,0.37,0.45c0.29,0.34,0.59,0.659,0.9,0.96c0.22,0.21,0.45,0.42,0.69,0.63c0.12,0.109,0.24,0.22,0.37,0.31c0.13,0.101,0.25,0.2,0.38,0.3c0.13,0.11,0.26,0.2,0.4,0.29c0.14,0.101,0.28,0.19,0.42,0.29c0.28,0.2,0.58,0.38,0.88,0.54c0.19,0.11,0.39,0.22,0.58,0.32l2.2-2.2c0.28-0.27,0.67-0.35,1.02-0.24c1.12,0.37,2.32,0.57,3.57,0.57C20.55,15.5,21,15.95,21,16.5z"/><rect x="19" y="3" fill="#455A64" width="2" height="7"/></g><g id="Capa_2"><path fill="#455A64" d="M21,19.68V20c0,0.55-0.45,1-1,1C10.61,21,3,13.39,3,4c0-0.55,0.45-1,1-1h0.69L4.7,3.06c0,0-0.13,3.77,1.92,7.72c0.27,0.52,0.57,1.04,0.92,1.56c0.21,0.32,0.44,0.63,0.68,0.94c0.13,0.16,0.25,0.3,0.37,0.45c0.29,0.34,0.59,0.659,0.9,0.96c0.22,0.21,0.45,0.42,0.69,0.63c0.12,0.1,0.24,0.199,0.37,0.31c0.13,0.101,0.25,0.2,0.38,0.3c0.13,0.101,0.27,0.19,0.4,0.29c0.14,0.101,0.28,0.19,0.42,0.29c0.28,0.181,0.58,0.37,0.88,0.54c0.19,0.11,0.39,0.22,0.58,0.32c2.05,1.07,4.6,1.87,7.76,2.21L21,19.68z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>phone_paused</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M6,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2s2-0.9,2-2C8,10.9,7.1,10,6,10z"/><path fill="#455A64" d="M18,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2s2-0.9,2-2C20,10.9,19.1,10,18,10z"/><path fill="#546E7A" d="M12,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C14,10.9,13.1,10,12,10z"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_control</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><rect x="10" y="5" fill="#0288D1" width="5" height="13"/><rect x="4" y="5" fill="#0288D1" width="5" height="13"/><rect x="16" y="5" fill="#0288D1" width="5" height="13"/><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><rect x="4" y="5" fill="#039BE5" width="2.53" height="13"/><rect x="10" y="5" fill="#039BE5" width="2.53" height="13"/><rect x="16" y="5" fill="#039BE5" width="2.5" height="13"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>view_column</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E57373" d="M15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33v15.33C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V5.33C17,4.6,16.4,4,15.67,4z"/><rect x="11" y="16" fill="#CFD8DC" width="2" height="2"/><rect x="11" y="9" fill="#CFD8DC" width="2" height="5"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_alert</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect y="9" fill="#263238" width="2" height="6"/><rect x="3" y="7" fill="#37474F" width="2" height="10"/><rect x="22" y="9" fill="#263238" width="2" height="6"/><rect x="19" y="7" fill="#37474F" width="2" height="10"/><path fill="#546E7A" d="M16.5,3h-9C6.67,3,6,3.67,6,4.5v15C6,20.33,6.67,21,7.5,21h9c0.83,0,1.5-0.67,1.5-1.5v-15C18,3.67,17.33,3,16.5,3z"/><rect x="8.166" y="4.958" fill="#BBDEFB" width="8" height="14"/></svg>			</div>
			<div class="icon-set__id">
				<span>vibration</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon opacity="0.9" fill="#43A047" points="18,7 6,19 0.41,13.41 1.83,12 6.02,16.2 16.59,5.59 "/><polygon fill="#66BB6A" points="22.24,5.59 11.66,16.17 7.48,12 6.07,13.41 11.66,19 23.66,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>done_all</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#78909C" d="M21,6l-4-4v3H5v6h2V7h10v3L21,6z M17,13v4H7v-3l-4,4l0.03,0.03L7,22v-3h12v-6H17z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>repeat</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#311B92" d="M19,8l-4,4h3c0,3.311-2.689,6-6,6c-1.01,0-1.97-0.25-2.8-0.7l-1.46,1.46C8.97,19.54,10.43,20,12,20c4.42,0,8-3.58,8-8h3L19,8z"/><path fill="#0D47A1" d="M6,12c0-3.31,2.69-6,6-6c1.01,0,1.971,0.25,2.8,0.7l1.46-1.46C15.029,4.46,13.57,4,12,4c-4.42,0-8,3.58-8,8H1l4,4l4-4H6z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>cached</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><polygon fill="#E53935" points="22.999,21 1,21 11.959,2.07 11.999,2 	"/><polygon fill="#EF5350" points="11.959,2.07 11.959,21 1,21 	"/><rect x="11" y="16" fill="#EEEEEE" width="2" height="2"/><rect x="11" y="10" fill="#EEEEEE" width="2" height="4"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>warning</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="11" y="7" fill="#7E57C2" width="2" height="2"/><rect x="11" y="11" fill="#7E57C2" width="2" height="2"/><rect x="15" y="11" fill="#7E57C2" width="2" height="2"/><path fill="#512DA8" d="M3,3v18h18V3H3z M19,19H5V5h14V19z"/><rect x="11" y="15" fill="#7E57C2" width="2" height="2"/><rect x="7" y="11" fill="#7E57C2" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_outer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#26C6DA" points="22,7 22,21 2,21 2,3 12,3 12,7 "/><polygon fill="#42A5F5" points="14.54,10.42 14.54,17.58 12,17.58 12,21 2,21 2,3 12,3 12,10.42 "/><rect x="4" y="17" fill="#FDD835" width="2" height="2"/><rect x="4" y="13" fill="#FDD835" width="2" height="2"/><rect x="4" y="9" fill="#FDD835" width="2" height="2"/><rect x="4" y="5" fill="#FDD835" width="2" height="2"/><rect x="8" y="17" fill="#FDD835" width="2" height="2"/><rect x="8" y="13" fill="#FDD835" width="2" height="2"/><rect x="8" y="9" fill="#FDD835" width="2" height="2"/><rect x="8" y="5" fill="#FDD835" width="2" height="2"/><polygon fill="#26C6DA" points="20,19 12,19 12,17 14,17 14,15 12,15 12,13 14,13 14,11 12,11 12,9 20,9 "/><rect x="16" y="11" fill="#42A5F5" width="2" height="2"/><rect x="16" y="15" fill="#42A5F5" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>domain</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="#607D8B" d="M10.585,9.415L12,10.83l-4.59,4.58L6,14L10.585,9.415z M13.415,9.415L12,10.83l4.59,4.58L18,14L13.415,9.415z"/><rect x="10.999" y="8.414" transform="matrix(-0.707 0.7072 -0.7072 -0.707 27.1412 7.5851)" fill="#90A4AE" width="2.001" height="2.001"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_arrow_up</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4E342E" d="M18.993,3.019h-4.181c-0.42-1.16-1.521-2-2.819-2c-1.299,0-2.4,0.84-2.82,2h-4.18c-1.1,0-2,0.9-2,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2v-14C20.993,3.918,20.093,3.019,18.993,3.019z"/><path fill="#D7CCC8" d="M19.701,6.556c0-1.355-1.102-2.454-2.455-2.454H6.781c-1.356,0-2.455,1.099-2.455,2.454v10.676c0,1.354,1.099,2.453,2.455,2.453h10.465c1.354,0,2.455-1.099,2.455-2.453V6.556z"/><circle fill="#F1F8E9" cx="11.993" cy="4.019" r="1"/></g><polygon fill="#4CAF50" points="10,17 6,13 7.41,11.59 10,14.17 16.59,7.58 18,9 "/><path fill="#FFA000" d="M7,6.234l9.984,0.031l0.002-1.236v-2h-2.18c-0.42-1.16-1.521-2-2.82-2s-2.4,0.84-2.82,2h-2.18v2L7,6.234zM11.986,3.029c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S11.437,3.029,11.986,3.029z"/></svg>			</div>
			<div class="icon-set__id">
				<span>assignment_turned_in</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#3E2723" cx="12" cy="12" r="12"/><path fill="#FFCDD2" d="M19.96,14.82c-1.09,3.74-4.271,6.46-8.04,6.46c-3.78,0-6.96-2.72-8.04-6.47C2.69,14.7,1.75,13.63,1.75,12.29c0-1.27,0.85-2.31,1.97-2.5c2.09-1.46,3.8-3.49,4.09-5.05V4.73c1.35,2.63,6.299,5.19,11.831,5.06l0.299-0.03c1.281,0,2.311,1.14,2.311,2.54C22.25,13.68,21.23,14.811,19.96,14.82z"/><path fill="#AD1457" d="M14.689,17.1C13.95,17.68,12.99,18,12,18s-1.95-0.32-2.69-0.9c-0.22-0.17-0.53-0.13-0.7,0.09c-0.17,0.221-0.13,0.53,0.09,0.701C9.61,18.609,10.79,19,12,19s2.391-0.391,3.311-1.1c0.219-0.17,0.26-0.48,0.09-0.7C15.23,16.97,14.91,16.939,14.689,17.1z"/><circle fill="#263238" cx="8.5" cy="12.5" r="1"/><circle fill="#263238" cx="15.5" cy="12.5" r="1"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>face_unlock</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#2196F3" d="M15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33v15.33C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V5.33C17,4.6,16.4,4,15.67,4z"/><polygon fill="#BBDEFB" points="11,20 11,14.5 9,14.5 13,7 13,12.5 15,12.5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_charging_full</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#D32F2F" d="M12.216,12.083L13,16h7V6h-5.6L12.216,12.083z"/><path fill="#5D4037" d="M5,14v7h2v-8L5,14z"/><polygon fill="#F44336" points="16.002,14 5,14 5,4 14,4 "/></svg>			</div>
			<div class="icon-set__id">
				<span>assistant_photo</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#0288D1" d="M22,12c0,5.52-4.48,10-10,10C6.48,22,2,17.52,2,12c0-5.18,3.95-9.45,9-9.95C11.33,2.02,11.66,2,12,2h1v8.28c0.06,0.03,0.11,0.07,0.17,0.11C13.67,10.74,14,11.33,14,12c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2c0-0.74,0.4-1.37,1-1.72V8.14c-1.72,0.45-3,2-3,3.86c0,2.21,1.79,4,4,4s4-1.79,4-4c0-1.1-0.45-2.1-1.17-2.83l1.41-1.41C17.33,8.84,18,10.34,18,12c0,3.31-2.69,6-6,6c-3.31,0-6-2.69-6-6c0-2.97,2.16-5.43,5-5.91V4.07C7.05,4.56,4,7.92,4,12c0,4.42,3.58,8,8,8s8-3.58,8-8c0-2.21-0.9-4.21-2.34-5.66l1.41-1.41C20.88,6.74,22,9.24,22,12z"/><path fill="#01579B" d="M14,12c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2c0-0.74,0.4-1.37,1-1.72V9.23c0.29-0.12,0.61-0.18,0.94-0.18c0.38,0,0.74,0.08,1.06,0.22v1.01c0.06,0.03,0.11,0.07,0.17,0.11C13.67,10.74,14,11.33,14,12z"/><path fill="#01579B" d="M13.17,10.39H11V2.05C11.33,2.02,11.66,2,12,2h1v8.28C13.06,10.31,13.11,10.35,13.17,10.39z"/></svg>			</div>
			<div class="icon-set__id">
				<span>track_changes</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFCC80" d="M20.99,6l-0.95,1.68L12,22L3.74,7.3L3.01,6C5.23,3.54,8.43,2,12,2C15.57,2,18.78,3.55,20.99,6z"/><path fill="#43A047" d="M7,7c0-1.1,0.9-2,2-2s2,0.9,2,2s-0.9,2-2,2S7,8.1,7,7z"/><path fill="#D32F2F" d="M12,15c-1.1,0-2-0.9-2-2c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,14.1,13.1,15,12,15z"/></g><g id="Capa_2"><path fill="#FFB74D" d="M20.99,6l-0.95,1.68c-0.48-1.19-2.32-4.42-8.06-4.39C6.51,3.32,4.41,6.08,3.74,7.3L3.01,6C5.23,3.54,8.43,2,12,2C15.57,2,18.78,3.55,20.99,6z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>local_pizza</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#B39DDB" points="22,5.72 17.4,1.86 16.109,3.39 20.71,7.25 "/><polygon fill="#B39DDB" points="7.88,3.39 6.6,1.86 2,5.71 3.29,7.24 "/><path fill="#7E57C2" d="M12,4c-4.97,0-9,4.03-9,9s4.02,9,9,9c4.971,0,9-4.03,9-9S16.971,4,12,4z"/><path fill="#E0E0E0" d="M12,20c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,20,12,20z"/><polygon fill="#303F9F" points="12.5,8 11,8 11,14 15.75,16.85 16.5,15.62 12.5,13.25 "/></svg>			</div>
			<div class="icon-set__id">
				<span>alarm</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_2"><path fill="#CFD8DC" d="M6.108,7.281l11.625,11.521c0,0,4.619-3.859-0.809-11.877c0,0-8.165-3.921-10.904,0.377"/></g><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path fill="#78909C" d="M19.04,4.55l-1.42,1.42C16.07,4.74,14.12,4,12,4c-1.83,0-3.53,0.55-4.95,1.48l1.46,1.46C9.53,6.35,10.73,6,12,6c3.87,0,7,3.13,7,7c0,1.27-0.35,2.47-0.939,3.49l1.449,1.449C20.45,16.53,21,14.83,21,13c0-2.12-0.74-4.07-1.971-5.61l1.421-1.42L19.04,4.55z"/><rect x="9" y="1" fill="#CFD8DC" width="6" height="2"/><polygon fill="#78909C" points="11,9.44 13,11.44 13,8 11,8 	"/><path fill="#78909C" d="M3.02,4L1.75,5.27L4.5,8.03C3.55,9.45,3,11.16,3,13c0,4.97,4.02,9,9,9c1.84,0,3.55-0.55,4.98-1.5l2.5,2.5l1.27-1.27l-7.71-7.711L3.02,4z"/><path fill="#CFD8DC" d="M12,20c-3.87,0-7-3.13-7-7c0-1.28,0.35-2.48,0.95-3.52l9.56,9.56C14.48,19.65,13.279,20,12,20z"/><polygon fill="#78909C" points="11,11.439 13,11.439 13,8 11,9.313 	"/></g><path fill="#78909C" d="M18.061,16.49c0,0-1.076,1.963-2.701,2.619c0,0,1.664,1.469,1.621,1.391s1.556-0.98,2.529-2.561c-0.01-0.033-0.354-1.189-0.322-1.205C19.221,16.718,18.061,16.49,18.061,16.49z"/><path fill="#78909C" d="M8.51,6.94c0,0-1.963,1.294-2.713,2.763l-1.578-1.25c0,0,1.375-2.297,3.438-3.344L8.51,6.94z"/><g id="Capa_3"><polygon fill="#9FA8DA" points="1.734,5.281 3,4 20.734,21.719 19.484,22.969 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>timer_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#1976D2" cx="12" cy="8" r="4"/><path fill="#1565C0" d="M12,13c-2.67,0-8,1.34-8,4v3h16v-3C20,14.34,14.67,13,12,13z"/><path fill="#FFFFFF" d="M12,5.9c1.16,0,2.1,0.94,2.1,2.1s-0.938,2.1-2.1,2.1S9.9,9.16,9.9,8S10.84,5.9,12,5.9"/><path fill="#FFFFFF" d="M12,14.9c2.971,0,6.1,1.459,6.1,2.1v1.1H5.9V17C5.9,16.359,9.03,14.9,12,14.9"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>person_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="11" y="19" fill="#7E57C2" width="2" height="2"/><rect x="11" y="15" fill="#7E57C2" width="2" height="2"/><rect x="11" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="7" fill="#7E57C2" width="2" height="2"/><rect x="11" y="11" fill="#7E57C2" width="2" height="2"/><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="7" y="3" fill="#7E57C2" width="2" height="2"/><rect x="7" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#512DA8" width="2" height="18"/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><rect x="19" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="11" fill="#7E57C2" width="2" height="2"/><rect x="19" y="19" fill="#7E57C2" width="2" height="2"/><rect x="15" y="11" fill="#7E57C2" width="2" height="2"/><rect x="15" y="3" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_left</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="17" fill="#37474F" width="6" height="2"/><rect x="4" y="5" fill="#37474F" width="16" height="2"/><path fill="#546E7A" d="M17,11H4v2h13.25c1.1,0,2,0.9,2,2s-0.9,2-2,2H15v-2l-3,3l3,3v-2h2c2.21,0,4-1.79,4-4S19.21,11,17,11z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>wrap_text</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#FFD54F" points="21,8.27 21,15.73 15.73,21 8.27,21 3,15.73 3,8.27 8.27,3 15.73,3 "/><polygon opacity="0.25" fill="#FFCA28" points="21,8.27 21,15.73 15.73,21 12,21 12,3 15.73,3 "/><path fill="#37474F" d="M12,17.3c-0.72,0-1.3-0.58-1.3-1.3s0.58-1.3,1.3-1.3c0.721,0,1.3,0.58,1.3,1.3S12.721,17.3,12,17.3z"/><rect x="11" y="7" fill="#37474F" width="2" height="6"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>report</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.5" fill="#607D8B" d="M9,8v8h2V8H9z M13,8v8h2V8H13z"/><path fill="#607D8B" d="M3.9,12c0-1.71,1.39-3.1,3.1-3.1h4V7H7c-2.76,0-5,2.24-5,5s2.24,5,5,5h4v-1.9H7C5.29,15.1,3.9,13.71,3.9,12zM8,13h8v-2H8V13z M17,7h-4v1.9h4c1.71,0,3.1,1.39,3.1,3.1s-1.39,3.1-3.1,3.1h-4V17h4c2.76,0,5-2.24,5-5S19.76,7,17,7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>insert_link</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#EF5350" d="M20.939,11C20.48,6.83,17.17,3.52,13,3.06V1h-2v2.06C6.83,3.52,3.52,6.83,3.06,11H1v2h2.06c0.46,4.17,3.77,7.48,7.94,7.939V23h2v-2.061c4.17-0.459,7.48-3.77,7.939-7.939H23v-2H20.939z M12,19c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,19,12,19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>gps_not_fixed</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><circle fill="#1A237E" cx="12" cy="12" r="6"/><path fill="#4FC3F7" d="M21.538,9c1.08,3.418,0.229,7.152-2.169,9.762l-5.64-9.759L21.538,9z"/><path fill="#03A9F4" d="M19.369,18.761c-2.421,2.645-6.081,3.774-9.54,3.004L15.462,12L19.369,18.761z"/><path fill="#4FC3F7" d="M9.828,21.765c-3.5-0.774-6.308-3.379-7.37-6.76L13.73,15L9.828,21.765z"/><path fill="#03A9F4" d="M2.459,15.003C1.38,11.584,2.231,7.85,4.628,5.24L10.269,15L2.459,15.003z"/><path fill="#4FC3F7" d="M4.631,5.239c2.42-2.645,6.08-3.773,9.539-3.002L8.538,12L4.631,5.239z"/><path fill="#03A9F4" d="M14.17,2.24c3.5,0.774,6.309,3.379,7.37,6.76L10.268,9.005L14.17,2.24z"/></svg>			</div>
			<div class="icon-set__id">
				<span>camera</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4E342E" d="M18.993,3.019h-4.181c-0.42-1.16-1.521-2-2.819-2c-1.299,0-2.4,0.84-2.82,2h-4.18c-1.1,0-2,0.9-2,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2v-14C20.993,3.918,20.093,3.019,18.993,3.019z"/><path fill="#D7CCC8" d="M19.701,6.556c0-1.355-1.102-2.454-2.455-2.454H6.781c-1.356,0-2.455,1.099-2.455,2.454v10.676c0,1.354,1.099,2.453,2.455,2.453h10.465c1.354,0,2.455-1.099,2.455-2.453V6.556z"/><circle fill="#F1F8E9" cx="11.993" cy="4.019" r="1"/></g><polygon fill="#FF7043" points="12,18 7,13 10,13 10,9 14,9 14,13 17,13 "/><path fill="#FFA000" d="M7.031,6.234l9.984,0.031l0.002-1.236v-2h-2.18c-0.42-1.16-1.521-2-2.82-2s-2.4,0.84-2.82,2h-2.18v2L7.031,6.234z M12.018,3.029c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S11.468,3.029,12.018,3.029z"/></svg>			</div>
			<div class="icon-set__id">
				<span>assignment_returned</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#4CAF50" points="12.291,13.119 17,8.41 17,15 19,15 19,5 9,5 9,7 15.59,7 10.881,11.709 11.031,12.969 "/><rect x="7.148" y="10.989" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 2.6944 32.8251)" fill="#81C784" width="1.994" height="9.731"/></svg>			</div>
			<div class="icon-set__id">
				<span>call_made</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#7986CB" d="M11,8c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S12.66,8,11,8z M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M17.59,19l-3.83-3.83C12.96,15.69,12.02,16,11,16c-2.76,0-5-2.24-5-5s2.24-5,5-5s5,2.24,5,5c0,1.02-0.31,1.96-0.83,2.75L19,17.59L17.59,19z M11,8c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S12.66,8,11,8z M11,8c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S12.66,8,11,8z"/><path fill="#E0E0E0" d="M15.17,13.75C15.69,12.96,16,12.02,16,11c0-2.76-2.24-5-5-5s-5,2.24-5,5s2.24,5,5,5c1.02,0,1.96-0.31,2.76-0.83L17.59,19L19,17.59L15.17,13.75z M11,14c-1.66,0-3-1.34-3-3s1.34-3,3-3s3,1.34,3,3S12.66,14,11,14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>pageview</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9FA8DA" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M9,17H7v-7h2V17zM13,17h-2V7h2V17z M17,17h-2v-4h2V17z"/><rect x="15" y="13" fill="#BA68C8" width="2" height="4"/><rect x="11" y="7" fill="#F06292" width="2" height="10"/><rect x="7" y="10" fill="#4FC3F7" width="2" height="7"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>assessment</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M24,18v2H0v-2h4v-1c-0.09,0-0.18-0.01-0.27-0.02C2.75,16.85,2,16.01,2,15V5c0-1.1,0.9-2,2-2h16c1.1,0,2,0.9,2,2l-0.01,10c0,1.01-0.75,1.85-1.721,1.98C20.18,16.99,20.09,17,20,17v1H24z"/><rect x="4" y="5" fill="#E3F2FD" width="16" height="10"/><rect y="18.014" fill="#455A64" width="24.008" height="1.972"/><path fill="#263238" d="M20.27,16.98C20.18,16.99,20.09,17,20,17v1H4v-1c-0.09,0-0.18-0.01-0.27-0.02H20.27z"/></svg>			</div>
			<div class="icon-set__id">
				<span>laptop_windows</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#8BC34A" fill-opacity="0.3" d="M17,5.33V9h-4V7l-1.07,2H7V5.33C7,4.6,7.6,4,8.33,4H10V2h4v2h1.67C16.4,4,17,4.6,17,5.33z"/><polygon fill="#FFFFFF" points="11,20 11,14.5 9,14.5 11.93,9 13,7 13,12.5 15,12.5 "/><path fill="#8BC34A" d="M17,9v11.67C17,21.4,16.4,22,15.66,22H8.33C7.6,22,7,21.4,7,20.67V9h4.93L9,14.5h2V20l4-7.5h-2V9H17z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_charging_80</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><path fill="#D4E157" d="M12,20c-4.42,0-8-3.58-8-8c0-1.85,0.63-3.55,1.69-4.9l11.21,11.21C15.55,19.37,13.85,20,12,20z"/><path fill="#D4E157" d="M18.311,16.9L7.1,5.69C8.45,4.63,10.15,4,12,4c4.42,0,8,3.58,8,8C20,13.85,19.37,15.55,18.311,16.9z"/></svg>			</div>
			<div class="icon-set__id">
				<span>do_not_disturb</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M14.12,10H19V8.2h-3.62l-1.66-2.76l-0.34-0.57c-0.3-0.5-0.84-0.83-1.46-0.83c-0.17,0-0.34,0.03-0.49,0.07L6,5.8V11h1.8V7.33l2.11-0.66L6,22h1.8l2.87-8.11L13,17v5h1.8v-6.41l-2.49-4.54l0.73-2.87L14.12,10z M14,0.2c-1,0-1.8,0.81-1.8,1.8S13,3.8,14,3.8c0.99,0,1.8-0.81,1.8-1.8C15.8,1,14.99,0.2,14,0.2z"/><polygon fill="#37474F" points="10.67,13.89 12.31,11.05 14.8,15.59 14.8,22 13,22 13,17 "/><polygon fill="#37474F" points="13.04,8.18 12.31,11.05 13.72,5.44 15.38,8.2 19,8.2 19,10 14.12,10 "/></svg>			</div>
			<div class="icon-set__id">
				<span>directions_walk</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#CFD8DC" cx="11.844" cy="11.903" r="9.359"/><path fill="#546E7A" fill-opacity="0.9" d="M11.99,2C6.47,2,2,6.48,2,12c0,5.52,4.47,10,9.99,10C17.52,22,22,17.52,22,12C22,6.48,17.52,2,11.99,2z M12,20c-4.42,0-8-3.58-8-8s3.58-8,8-8s8,3.58,8,8S16.42,20,12,20z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#455A64" fill-opacity="0.9" d="M12.5,7H11v6l5.25,3.15L17,14.92l-4.5-2.67V7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>access_time</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M22.09,17l-0.399,3.66l-1.91,1.45L8.77,22.84l-4.71-0.68l-0.9-1.351L1.81,19.03l-0.72-7.31c0,0,1.41-9.16,1.29-9.1c0.15-0.15,1.68-1.34,1.68-1.34l5.72-0.16l7.69,0.22l3.811,1.38l0.8,2.25l0.2,6.61L22.09,17z M19,3H5C3.9,3,3,3.9,3,5v14c0,0.54,0.21,1.03,0.58,1.39C3.93,20.77,4.44,21,5,21h14c1.1,0,2-0.9,2-2V5c0-0.55-0.22-1.05-0.59-1.41C20.05,3.22,19.55,3,19,3z M0,0v24h24V0H0z M22.09,17l-0.399,3.66l-1.91,1.45L8.77,22.84l-4.71-0.68l-0.9-1.351L1.81,19.03l-0.72-7.31c0,0,1.41-9.16,1.29-9.1c0.15-0.15,1.68-1.34,1.68-1.34l5.72-0.16l7.69,0.22l3.811,1.38l0.8,2.25l0.2,6.61L22.09,17zM19,3H5C3.9,3,3,3.9,3,5v14c0,0.54,0.21,1.03,0.58,1.39C3.93,20.77,4.44,21,5,21h14c1.1,0,2-0.9,2-2V5c0-0.55-0.22-1.05-0.59-1.41C20.05,3.22,19.55,3,19,3z"/><path d="M20.41,3.59C20.05,3.22,19.55,3,19,3H5C3.9,3,3,3.9,3,5v14c0,0.54,0.21,1.03,0.58,1.39C3.93,20.77,4.44,21,5,21h14c1.1,0,2-0.9,2-2V5C21,4.45,20.78,3.95,20.41,3.59z M5.5,7.5h2v-2H9v2h2V9H9v2H7.5V9h-2V7.5z M19,19H5L19,5V19z"/><path fill="#DCE775" d="M21,5v14c0,1.1-0.9,2-2,2H5c-0.56,0-1.07-0.23-1.42-0.61L19,5l1.41-1.41C20.78,3.95,21,4.45,21,5z"/><path fill="#66BB6A" d="M20.41,3.59L19,5L3.58,20.39C3.21,20.03,3,19.54,3,19V5c0-1.1,0.9-2,2-2h14C19.55,3,20.05,3.22,20.41,3.59z"/><rect x="12" y="15.5" fill="#66BB6A" width="5" height="1.5"/><polygon fill="#DCE775" points="11,7.5 11,9 9,9 9,11 7.5,11 7.5,9 5.5,9 5.5,7.5 7.5,7.5 7.5,5.5 9,5.5 9,7.5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>iso</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M20.938,10.999c-0.459-4.17-3.77-7.48-7.939-7.94v-2.06h-2v2.06c-4.17,0.46-7.48,3.77-7.939,7.94H0.999v2H3.06c0.459,4.17,3.77,7.48,7.939,7.939v2.061h2v-2.061c4.17-0.459,7.48-3.77,7.939-7.939h2.061v-2H20.938z M11.999,18.999c-3.87,0-7-3.131-7-7c0-3.87,3.13-7,7-7s7,3.13,7,7C18.999,15.868,15.869,18.999,11.999,18.999z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#9FA8DA" d="M3,4.27L4.27,3c0,0,16.699,16.773,16.73,16.73S19.73,21,19.73,21L3,4.27z"/></svg>			</div>
			<div class="icon-set__id">
				<span>gps_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFC107" fill-opacity="0.3" d="M17,5.33v9.17h-3.07l1.07-2h-2V7l-4,7.5H7V5.33C7,4.6,7.6,4,8.33,4H10V2h4v2h1.67C16.4,4,17,4.6,17,5.33z"/><polygon fill="#FFFFFF" points="15,12.5 13.93,14.5 11,20 11,14.5 9,14.5 13,7 13,12.5 "/><path fill="#FFC107" d="M17,14.5v6.17C17,21.4,16.4,22,15.66,22H8.33C7.6,22,7,21.4,7,20.67V14.5h4V20l2.93-5.5H17z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_charging_30</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#29B6F6" d="M24,7.25l-4.811,5.92l-1.569,1.92L12,22l-5.57-6.85l-1.52-1.87L0,7.23C3.15,4.85,6.95,3,12,3S20.85,4.87,24,7.25z"/><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.2" fill="#81D4FA" enable-background="new    " d="M17.62,15.09L12,22l-5.57-6.85c0.73-2.4,2.96-4.15,5.6-4.15C14.65,11,16.87,12.72,17.62,15.09z"/><rect x="11" y="10" fill="#CFD8DC" width="2" height="6"/><rect x="11" y="6" fill="#CFD8DC" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>perm_scan_wifi</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FF5722" d="M7,17v3.67C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V17H7z"/><path fill="#FF5722" fill-opacity="0.3" d="M17,5.33C17,4.6,16.4,4,15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33V17h10V5.33z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_20</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#BBDEFB" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><polygon fill="#64B5F6" points="14.189,14.189 6,18 9.81,9.81 "/><polygon fill="#F44336" points="9.81,9.81 18,6 14.189,14.189 "/><circle fill="#455A64" cx="12" cy="12" r="1.1"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>explore</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#558B2F" fill-opacity="0.3" d="M17,5.33C17,4.6,16.4,4,15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33V9h10V5.33z"/><path fill="#9CCC65" d="M7,9v11.67C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V9H7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_80</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#FFEE58" d="M12.427,2.55c-0.367-0.369-0.861-0.582-1.414-0.583l-7-0.023C3.447,1.943,2.937,2.182,2.576,2.555c-0.348,0.36-0.569,0.846-0.571,1.384l-0.008,7c-0.001,0.551,0.216,1.053,0.582,1.422l8.98,9.023c0.353,0.354,0.854,0.575,1.405,0.577c0.544,0.008,1.048-0.209,1.409-0.582l7.021-6.979c0.369-0.366,0.59-0.868,0.584-1.414c0.01-0.544-0.225-1.063-0.582-1.423L12.427,2.55z M7.007,5.454c-0.002,0.439-0.179,0.812-0.47,1.087c-0.271,0.254-0.63,0.416-1.034,0.408C4.668,6.946,3.999,6.272,4.008,5.445C4.003,5.042,4.159,4.688,4.422,4.414C4.69,4.132,5.074,3.949,5.513,3.951C6.34,3.946,7.009,4.62,7.007,5.454z"/><path opacity="0.2" fill="#8D6E63" enable-background="new    " d="M21.396,11.566c0.358,0.362,0.594,0.879,0.582,1.422c0.006,0.545-0.215,1.048-0.584,1.414l-3.545,3.521L6.537,6.541c0.291-0.275,0.469-0.648,0.47-1.087C7.009,4.62,6.34,3.946,5.513,3.951c-0.438-0.002-0.82,0.181-1.09,0.463L2.576,2.555c0.361-0.373,0.871-0.612,1.437-0.611l7,0.023c0.551,0.001,1.045,0.215,1.414,0.583L21.396,11.566z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>local_offer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#757575" d="M7,17.5c0,3.04,2.46,5.5,5.5,5.5s5.5-2.46,5.5-5.5V6h-1.5v11.5c0,2.21-1.79,4-4,4s-4-1.79-4-4V6H7V17.5z"/><path fill="#616161" d="M7,5v1h1.5V5c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5v10.5c0,0.55-0.45,1-1,1s-1-0.45-1-1V6H10v9.5c0,1.38,1.12,2.5,2.5,2.5s2.5-1.12,2.5-2.5V5c0-2.21-1.79-4-4-4S7,2.79,7,5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>attach_file</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#0D47A1" points="13,7 21,7 21,9 15,9 15,22 13,22 13,16 12,10.625 "/><polygon fill="#1976D2" points="13,16 11,16 11,22 9,22 9,9 3,9 3,7 13,7 "/><path fill="#1976D2" d="M12,2c1.1,0,2,0.9,2,2s-0.9,2-2,2c-1.1,0-2-0.9-2-2S10.9,2,12,2z"/></svg>			</div>
			<div class="icon-set__id">
				<span>accessibility</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M14,1.88V4h8c0.97,0,1.78,0.7,1.96,1.62C23.99,5.74,24,5.87,24,6v10c0,1.1-0.9,2-2,2h-8v0.41h10V24H0V0h24v1.88H14z"/><path fill="#FFCA28" d="M2,6H0v5h0.01L0,20c0,1.1,0.9,2,2,2h18v-2H2V6z"/><path fill="#FFD54F" d="M24,6v10c0,1.1-0.9,2-2,2H6c-1.1,0-2-0.9-2-2L4.01,5.62V4C4.01,2.9,4.9,2,6,2h6l2,2h8c0.97,0,1.78,0.7,1.96,1.62C23.99,5.74,24,5.87,24,6z"/><path fill="#FFCA28" d="M24,6v10c0,1.1-0.9,2-2,2h-8V4h8c0.97,0,1.78,0.7,1.96,1.62C23.99,5.74,24,5.87,24,6z"/><polygon fill="#66BB6A" points="7,15 11.5,9 15,13.51 17.5,10.5 21,15 "/></svg>			</div>
			<div class="icon-set__id">
				<span>perm_media</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><rect x="3" y="6" fill="#546E7A" width="12" height="2"/><rect x="3" y="10" fill="#78909C" width="10.66" height="2"/><rect x="3" y="14" fill="#546E7A" width="8" height="2"/><path fill="#0277BD" d="M22,6v2h-3v9c0,1.311-0.83,2.42-2,2.82C16.689,19.939,16.35,20,16,20c-1.66,0-3-1.34-3-3s1.34-3,3-3c0.35,0,0.689,0.07,1,0.18V6H22z"/><path fill="#01579B" d="M22,6v2h-3v9c0,1.311-0.83,2.42-2,2.82V6H22z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>queue_mus24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M3,21h8.25v-2H3V21z M3,13h8.25v-2H3V13z M3,3v2h8.25V3H3z"/><path fill="#607D8B" d="M9,17h12v-2H9V17z M9,9h12V7H9V9z M9,5h12V3H9V5z M9,13h12v-2H9V13z M9,21h12v-2H9V21z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_align_right</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36">
<g data-iconmelon="Smallicons:4867ef0e7ff418d32d87ec799608ed0c">
  <path fill="#3CAED6" d="M12 0s12 13.373 12 20-5.372 12-12 12c-6.627 0-12-5.373-12-12l12-20z"></path>
  <path fill="#63BFDE" d="M16.994 16c-1.108 0-2.006.898-2.006 2.007 0 1.107.897 2.005 2.006 2.005 1.107 0 2.006-.897 2.006-2.005 0-1.109-.898-2.007-2.006-2.007z"></path>
  <path fill="#369DC0" d="M13 31c-6.627 0-12-5.373-12-12 0-5.497 8.25-15.627 11.066-18.926l-.066-.074s-12 13.373-12 20 5.373 12 12 12c3.568 0 6.764-1.566 8.961-4.039-2.119 1.885-4.901 3.039-7.961 3.039z"></path>
</g>
</svg>			</div>
			<div class="icon-set__id">
				<span>drop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><circle fill="#039BE5" cx="8" cy="15" r="2.5"/><circle fill="#039BE5" cx="12" cy="8" r="2.5"/><circle fill="#039BE5" cx="16" cy="15" r="2.5"/></svg>			</div>
			<div class="icon-set__id">
				<span>group_work</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#78909C" d="M16.961,8.53c0.529,0.75,0.879,1.6,1.02,2.47H20c-0.17-1.39-0.729-2.73-1.63-3.89l-0.601,0.6L16.961,8.53z"/><path fill="#78909C" d="M16.971,15.47l0.479,0.48l0.931,0.938C19.279,15.73,19.83,14.391,20,13h-2.02C17.84,13.88,17.49,14.72,16.971,15.47z"/><path fill="#78909C" d="M13.07,17.9v2.029c1.391-0.171,2.74-0.711,3.9-1.609L16.18,17.52l-0.65-0.649C14.779,17.41,13.939,17.75,13.07,17.9z"/><path fill="#78909C" d="M4.07,12c0,4.08,3.05,7.439,7,7.93v-2.02c-2.84-0.48-5-2.94-5-5.91s2.16-5.43,5-5.91V10l4.55-4.45l-0.04-0.03L11.07,1v3.07C7.12,4.56,4.07,7.92,4.07,12z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>rotate_right</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M22,8v12c0,1.1-0.891,2-2,2H4c-1.11,0-2-0.9-2-2V8c0-0.83,0.51-1.57,1.24-1.85L3.61,6l0.05-0.02L15.88,1l0.681,1.66L8.35,5.98L8.3,6H20C21.109,6,22,6.89,22,8z"/><path fill="#546E7A" d="M11.96,6v16H4c-1.11,0-2-0.9-2-2V8c0-0.83,0.51-1.57,1.24-1.85L3.61,6H11.96z"/><path fill="#CFD8DC" d="M10,17c0,1.66-1.34,3-3,3c-0.01,0-0.01,0-0.02,0C5.33,19.99,4,18.65,4,17s1.33-2.99,2.98-3c0.01,0,0.01,0,0.02,0C8.66,14,10,15.34,10,17z"/><polygon fill="#CFD8DC" points="20,12 18,12 18,10 16,10 16,12 4,12 4,8 20,8 "/><path fill="none" d="M0,0v24h24V0H0z M11.96,22.96H1.08V6h2.53L3.24,6.15C2.51,6.43,2,7.17,2,8v12c0,1.1,0.89,2,2,2h7.96V22.96z"/><rect x="16" y="10" fill="#E53935" width="2" height="2"/><polygon fill="#90A4AE" points="16.561,2.66 8.35,5.98 3.66,5.98 15.88,1 "/></svg>			</div>
			<div class="icon-set__id">
				<span>radio</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E57373" d="M19,5v16l-7-3l-0.03,0.01L5,21L5.01,5C5.01,3.9,5.9,3,7,3h10C18.1,3,19,3.9,19,5z"/><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.4" fill="#EF5350" enable-background="new    " d="M19,5v16l-7-3l-0.03,0.01V3H17C18.1,3,19,3.9,19,5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>bookmark_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M21,3l-0.01,12.93c0,0.69-0.351,1.301-0.88,1.66l-8.061,5.38V24H0V0h12.05v1H19C20.1,1,21,1.9,21,3z"/><path fill="#81D4FA" d="M21,3l-0.01,12.93c0,0.69-0.351,1.301-0.88,1.66l-8.061,5.38L12,23l-8.12-5.41C3.35,17.23,3,16.62,3,15.93L3.01,3C3.01,1.9,3.9,1,5,1h14C20.1,1,21,1.9,21,3z"/><path opacity="0.5" fill="#4FC3F7" enable-background="new    " d="M20.11,17.59l-8.061,5.38V1H19c1.1,0,2,0.9,2,2l-0.01,12.93C20.99,16.62,20.64,17.23,20.11,17.59z"/><polygon fill="#FDD835" points="10,16 5,11 6.41,9.59 10,13.17 17.59,5.58 19,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>beenhere</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#D7CCC8" d="M3.008,8L3,19c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V8H3.008z"/><rect x="12" y="12" fill="#F57C00" width="5" height="5"/><path fill="#C2185B" d="M21,5c0-1.1-0.9-2-2-2H5C3.89,3,3.01,3.9,3.01,5L3.008,8H21V5z"/><path fill="#A1887F" d="M8,5H6V1h2V5z M18,1h-2v4h2V1z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>event</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path d="M10.18,9"/><path fill="#90A4AE" d="M21,16v-2l-8-5V3.5C13,2.67,12.33,2,11.5,2S10,2.67,10,3.5V9l-8,5v2l8-2.5V19l-2,1.5V22l3.5-1l3.5,1v-1.5L13,19v-5.5L21,16z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>flight</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="2" y="15.5" fill="#D32F2F" width="20" height="2"/><rect x="2" y="10.5" fill="#E53935" width="20" height="2"/><rect x="2" y="5.5" fill="#D32F2F" width="20" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>dehaze</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="#5D4037" d="M19,2H5C3.9,2,3,2.9,3,4v16c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V4C21,2.9,20.1,2,19,2z"/><rect x="5" y="4" fill="#D7CCC8" width="14" height="16"/><path fill="#FFA000" d="M7,7h10V4V2h-2.18C14.4,0.84,13.3,0,12,0S9.6,0.84,9.18,2H7v2V7z M12,2c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S11.45,2,12,2z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>content_paste</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#455A64" d="M12,17c3.311,0,6-2.689,6-6V3h-2.5v8c0,1.93-1.57,3.5-3.5,3.5c-1.93,0-3.5-1.57-3.5-3.5V3H6v8C6,14.311,8.69,17,12,17z"/><rect x="5" y="19" fill="#607D8B" width="14" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_underline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><rect x="3" y="3" fill="#F44336" width="8" height="10"/><rect x="3" y="15" fill="#FBC02D" width="8" height="6"/><rect x="13" y="11" fill="#F06292" width="8" height="10"/><rect x="13" y="3" fill="#FF9800" width="8" height="6"/></svg>			</div>
			<div class="icon-set__id">
				<span>dashboard</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#455A64" points="10,9 14,9 14,6 17,6 12,1 7,6 10,6 "/><polygon fill="#90A4AE" points="9,10 6,10 6,7 1,12 6,17 6,14 9,14 "/><polygon fill="#90A4AE" points="23,12 18,7 18,10 15,10 15,14 18,14 18,17 "/><polygon fill="#455A64" points="14,15 10,15 10,18 7,18 12,23 17,18 14,18 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>open_with</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4.604" y="3.979" fill="#FFFFFF" width="15.792" height="4.146"/><path fill="#CFD8DC" d="M22,7.97V18c0,1.1-0.9,2-2,2H4c-1.1,0-2-0.9-2-2L2.01,7.97H22z"/><path fill="#455A64" d="M22,4v4.906L2.016,8.859L2.01,6C2.01,4.9,2.9,4,4,4h1l1.99,3.97L7,8h3L9.99,7.97L8,4h2l1.99,3.97L12,8h3l-0.01-0.03L13,4h2l1.99,3.97L17,8h3l-0.01-0.03L18,4H22z"/><path fill="none" d="M0,0h24v24H0V0z"/><g><rect x="13.73" y="10.54" transform="matrix(0.7077 -0.7065 0.7065 0.7077 -4.2919 13.461)" fill="#C5CAE9" width="0.787" height="2.756"/><path fill="#E57373" d="M16.326,11.664c0.203-0.203,0.203-0.53,0-0.733l-1.217-1.215c-0.203-0.203-0.529-0.203-0.73,0l-0.953,0.951l1.949,1.949L16.326,11.664z"/><g><polygon fill="#7986CB" points="7.125,18.917 9.073,18.917 7.125,16.968 		"/><rect x="6.909" y="13.691" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -7.4423 12.1725)" fill="#3F51B5" width="8.126" height="2.756"/></g></g></svg>			</div>
			<div class="icon-set__id">
				<span>movie_creation</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M1.01,7L1,17c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2V7c0-1.1-0.9-2-2-2H3C1.9,5,1.01,5.9,1.01,7z"/><rect x="5" y="7" fill="#B2EBF2" width="14" height="10"/></svg>			</div>
			<div class="icon-set__id">
				<span>stay_current_landscape</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path opacity="0.8" fill="#D7CCC8" d="M3.008,8L3,19c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V8H3.008z"/><rect x="6.939" y="9.976" opacity="0.8" fill="#42A5F5" width="5" height="5"/><path opacity="0.8" fill="#C2185B" d="M21,5c0-1.1-0.9-2-2-2H5C3.89,3,3.01,3.9,3.01,5L3.008,8H21V5z"/><path opacity="0.8" fill="#A1887F" d="M8,5H6V1h2V5z M18,1h-2v4h2V1z"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>today</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="16" fill="#455A64" width="18" height="2"/><rect x="3" y="11" fill="#546E7A" width="18" height="2"/><rect x="3" y="6" fill="#455A64" width="18" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>menu</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M14,0.54V2h6c1.1,0,2,0.9,2,2v12c0,1.1-0.9,2-2,2h-6v0.78h10V24H0V0h24v0.54H14z"/><path fill="#0288D1" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path fill="#01579B" d="M22,4v12c0,1.1-0.9,2-2,2h-6V2h6C21.1,2,22,2.9,22,4z"/><path fill="#FFFFFF" d="M18,7h-3v5.5c0,1.38-1.12,2.5-2.5,2.5S10,13.88,10,12.5s1.12-2.5,2.5-2.5c0.57,0,1.08,0.19,1.5,0.51V5h4V7z"/><path fill="#01579B" d="M4,6H2v14c0,1.1,0.9,2,2,2h14v-2H4V6z"/></svg>			</div>
			<div class="icon-set__id">
				<span>my_library_mus24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#D32F2F" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M4,12c0-4.42,3.58-8,8-8c1.85,0,3.55,0.63,4.9,1.69L5.69,16.9C4.63,15.55,4,13.85,4,12z M12,20c-1.85,0-3.55-0.63-4.9-1.689L18.311,7.1C19.37,8.45,20,10.15,20,12C20,16.42,16.42,20,12,20z"/></svg>			</div>
			<div class="icon-set__id">
				<span>block</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#3F51B5" d="M16.59,8.59L12,13.17L7.41,8.59L6,10l6,6l6-6L16.59,8.59z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>expand_more</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><g><path fill="#CFD8DC" d="M18.956,4.867v14.331c0,0.982-0.786,1.789-1.747,1.789H6.731c-0.961,0-1.747-0.807-1.747-1.789L5.002,8.45l5.222-5.374h6.984C18.17,3.076,18.956,3.882,18.956,4.867z"/><path opacity="0.2" fill="#90A4AE" enable-background="new    " d="M18.956,4.867v14.331c0,0.982-0.786,1.789-1.747,1.789H11.97V3.076h5.239C18.17,3.076,18.956,3.882,18.956,4.867z"/></g><rect x="14.64" y="16.512" fill="#B0BEC5" width="1.746" height="1.792"/><rect x="7.654" y="16.512" fill="#B0BEC5" width="1.746" height="1.792"/><rect x="11.147" y="14.72" fill="#B0BEC5" width="1.746" height="3.584"/><rect x="14.64" y="11.138" fill="#B0BEC5" width="1.746" height="3.582"/><rect x="11.147" y="11.138" fill="#B0BEC5" width="1.746" height="1.792"/><rect x="7.654" y="11.138" fill="#B0BEC5" width="1.746" height="3.582"/></g><path fill="none" d="M0,0h24v24H0V0z"/><rect x="10.852" y="0.897" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 29.4275 14.3102)" fill="#9FA8DA" width="1.796" height="24.706"/></svg>			</div>
			<div class="icon-set__id">
				<span>no_sim</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#66BB6A" d="M10,6L8.59,7.41L13.17,12l-4.58,4.59L10,18l6-6L10,6z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>navigate_next</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#455A64" d="M16.492,4H18v3h-3.79L16.492,4z M14,15h-3.21l-0.282,3H14V15z"/><polygon fill="#607D8B" points="11.79,15 13.538,10.911 16.492,4 10,4 10,7 12.21,7 8.79,15 6,15 6,18 10.508,18 11.79,15 "/></svg>			</div>
			<div class="icon-set__id">
				<span>format_ital24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#039BE5" enable-background="new    " d="M6,19h4V5H6V19z M14,5v14h4V5H14z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>pause</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M18,0H6C4.34,0,3,1.34,3,3v18c0,1.66,1.34,3,3,3h12c1.66,0,3-1.34,3-3V3C21,1.34,19.66,0,18,0z"/><rect x="10" y="21" fill="#FAFAFA" width="4" height="1"/><rect x="4.75" y="3" fill="#BBDEFB" width="14.5" height="16"/></svg>			</div>
			<div class="icon-set__id">
				<span>tablet_android</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#009688" d="M18,20V8c0-1.1-0.9-2-2-2H4C2.9,6,2,6.9,2,8v12c0,1.1,0.9,2,2,2h12C17.1,22,18,21.1,18,20z"/><path fill="#8BC34A" d="M22,16V4c0-1.1-0.9-2-2-2H8C6.9,2,6,2.9,6,4v12c0,1.1,0.9,2,2,2h12C21.1,18,22,17.1,22,16z"/><polygon fill="#388E3C" points="11,12 13.029,14.71 16,11 20,16 8,16 "/></svg>			</div>
			<div class="icon-set__id">
				<span>collections</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="none" points="24,0 24,14 23,14 23,20 21,20 21,17 3,17 3,20 1,20 1,5 3,5 3,14 3.51,14 3.51,4.19 0,4.19 0,0 "/><polygon fill="none" points="1.41,22.59 24,22.59 24,24 0,24 0,20.58 1.41,20.58 "/><path fill="#546E7A" d="M7,13c1.66,0,3-1.34,3-3S8.66,7,7,7s-3,1.34-3,3S5.34,13,7,13z"/><path fill="#64B5F6" d="M23,11v9h-2v-3H3v3H1V5h2v9h8V7h8C21.21,7,23,8.79,23,11z"/><polygon fill="#6D4C41" points="23,14 23,20 21,20 21,17 3,17 3,20 1,20 1,5 3,5 3,14 "/></svg>			</div>
			<div class="icon-set__id">
				<span>hotel</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="none" points="24,0 24,24 4.91,24 4.91,5.91 0,5.91 0,0 "/><g><path fill="#6D4C41" d="M12.99,16.16v3.88h3v2H4.91v-2h6.08V16.1c-3.31-0.55-5.83-3.42-5.83-6.89c0-3.87,3.13-7,7-7c3.869,0,7,3.13,7,7c0,3.37-2.37,6.17-5.53,6.851h-0.01c-0.14,0.029-0.29,0.051-0.44,0.069C13.12,16.141,13.061,16.15,12.99,16.16z"/><rect x="4.913" y="20.021" fill="#43A047" width="14.118" height="2.021"/><path fill="#43A047" d="M19.162,9.213c0,3.37-2.371,6.17-5.531,6.85h-0.01c-0.139,0.03-0.289,0.052-0.439,0.07c-0.061,0.01-0.119,0.021-0.188,0.03c-0.271,0.03-0.55,0.04-0.831,0.04c-0.4,0-0.79-0.03-1.17-0.09v-0.012c-3.311-0.55-5.83-3.42-5.83-6.89c0-3.87,3.13-7,7-7C16.031,2.213,19.162,5.343,19.162,9.213z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>nature</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26A69A" d="M12,6c2.45,0,4.71,0.2,7.29,0.64C19.76,8.42,20,10.22,20,12s-0.24,3.58-0.71,5.359C16.71,17.8,14.45,18,12,18s-4.71-0.2-7.29-0.641C4.24,15.58,4,13.78,4,12s0.24-3.58,0.71-5.36C7.29,6.2,9.55,6,12,6 M12,4C9.27,4,6.78,4.24,4.05,4.72L3.12,4.88l-0.25,0.9C2.29,7.85,2,9.93,2,12c0,2.07,0.29,4.15,0.87,6.22l0.25,0.89l0.93,0.16C6.78,19.76,9.27,20,12,20c2.73,0,5.221-0.24,7.95-0.72l0.93-0.16l0.25-0.89C21.71,16.15,22,14.07,22,12c0-2.07-0.29-4.15-0.87-6.22l-0.25-0.89l-0.93-0.16C17.221,4.24,14.73,4,12,4z"/></svg>			</div>
			<div class="icon-set__id">
				<span>panorama_wide_angle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#7986CB" cx="12" cy="12" r="10"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>brightness_1</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E53935" d="M23,10l-0.03,0.27l-0.34,1.25l-2.2,8.02C20.19,20.38,19.42,21,18.5,21h-13c-0.92,0-1.69-0.62-1.92-1.46l-2.2-8.02l-0.34-1.25C1.01,10.18,1,10.09,1,10c0-0.55,0.45-1,1-1h4.79L6.8,8.98l15.44,0.05C22.68,9.14,23,9.53,23,10z"/><path fill="#DCE775" d="M12,17c-1.1,0-2-0.9-2-2s0.9-2,2-2c1.1,0,2,0.9,2,2S13.1,17,12,17z"/><path fill="#DCE775" d="M17.21,9l-4.38-6.56C12.641,2.16,12.32,2.02,12,2.02s-0.64,0.14-0.83,0.43L6.8,8.98L6.79,9H4.58v0.02H19.52V9H17.21z M9,9l3-4.4L15,9H9z"/><path fill="none" d="M22.63,11.52H24V24H0V11.52h1.38l-0.34-1.25C1.01,10.18,1,10.09,1,10c0-0.55,0.45-1,1-1h4.79L6.8,8.98l15.44,0.05C22.68,9.14,23,9.53,23,10l-0.03,0.27L22.63,11.52z"/><rect fill="none" width="24" height="8.96"/><path fill="#D32F2F" d="M23,10l-0.03,0.27l-0.34,1.25H1.38l-0.34-1.25C1.01,10.18,1,10.09,1,10c0-0.55,0.45-1,1-1h4.79L6.8,8.98l15.44,0.05C22.68,9.14,23,9.53,23,10z"/></svg>			</div>
			<div class="icon-set__id">
				<span>shopping_basket</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#43A047" d="M17,4v2h-2.93c-0.73,2.36-1.97,4.59-3.71,6.53l-0.03,0.028l2.54,2.512l-0.76,2.039L9,14l-5,5l-1.42-1.42l5.09-5.021C6.42,11.17,5.42,9.63,4.69,8h2C7.3,9.19,8.07,10.32,9,11.35c1.44-1.6,2.5-3.43,3.17-5.36H1V4h7V2h2v2H17z"/><polygon fill="#37474F" points="18.5,10 16.5,10 12,22 14,22 15.12,19 19.87,19 21,22 23,22 	"/><polygon fill="#FFFFFF" points="15.88,17 17.5,12.67 19.12,17 	"/></g><g id="Capa_2"><polygon fill="#43A047" points="1,4 1,5.99 17,6 17,4 	"/><rect x="8" y="2" fill="#43A047" width="2" height="2"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>translate</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><rect x="6" y="6" fill="#546E7A" width="2" height="12"/><polygon fill="#78909C" points="18,6 18,18 9.5,12 	"/><path fill="none" d="M9.5,12l8.5,6V6L9.5,12z M6,6v12h2V6H6z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>skip_previous</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M19,3H5C3.89,3,3,3.9,3,5v14c0,1.1,0.89,2,2,2h14c1.11,0,2-0.9,2-2V5C21,3.9,20.11,3,19,3z M17.25,12c0,0.23-0.02,0.46-0.05,0.68l1.479,1.16c0.13,0.11,0.17,0.3,0.08,0.45l-1.399,2.42c-0.091,0.15-0.271,0.21-0.431,0.15l-1.739-0.7c-0.36,0.28-0.761,0.51-1.181,0.689L13.75,18.7c-0.03,0.17-0.18,0.3-0.35,0.3h-2.8c-0.17,0-0.32-0.13-0.35-0.29l-0.26-1.85c-0.43-0.181-0.82-0.41-1.18-0.69l-1.74,0.7c-0.16,0.06-0.34,0-0.43-0.15l-1.4-2.42c-0.09-0.149-0.05-0.34,0.08-0.45L6.8,12.69C6.77,12.46,6.75,12.23,6.75,12s0.02-0.46,0.05-0.68l-1.48-1.16c-0.13-0.11-0.17-0.3-0.08-0.45l1.4-2.42c0.09-0.15,0.27-0.21,0.43-0.15l1.74,0.7c0.36-0.28,0.76-0.51,1.18-0.69l0.26-1.85C10.28,5.13,10.43,5,10.6,5h2.8c0.17,0,0.319,0.13,0.35,0.29l0.26,1.85c0.431,0.18,0.82,0.41,1.181,0.69l1.739-0.7c0.16-0.06,0.34,0,0.431,0.15L18.76,9.7c0.09,0.15,0.05,0.34-0.08,0.45L17.2,11.31C17.23,11.54,17.25,11.77,17.25,12z"/><path fill="#CFD8DC" d="M18.68,13.84c0.13,0.11,0.17,0.3,0.08,0.45l-1.399,2.42c-0.091,0.15-0.271,0.21-0.431,0.15l-1.739-0.7c-0.36,0.28-0.761,0.51-1.181,0.689L13.75,18.7c-0.03,0.17-0.18,0.3-0.35,0.3h-2.8c-0.17,0-0.32-0.13-0.35-0.29l-0.26-1.85c-0.43-0.181-0.82-0.41-1.18-0.69l-1.74,0.7c-0.16,0.06-0.34,0-0.43-0.15l-1.4-2.42c-0.09-0.149-0.05-0.34,0.08-0.45L6.8,12.69C6.77,12.46,6.75,12.23,6.75,12s0.02-0.46,0.05-0.68l-1.48-1.16c-0.13-0.11-0.17-0.3-0.08-0.45l1.4-2.42c0.09-0.15,0.27-0.21,0.43-0.15l1.74,0.7c0.36-0.28,0.76-0.51,1.18-0.69l0.26-1.85C10.28,5.13,10.43,5,10.6,5h2.8c0.17,0,0.319,0.13,0.35,0.29l0.26,1.85c0.431,0.18,0.82,0.41,1.181,0.69l1.739-0.7c0.16-0.06,0.34,0,0.431,0.15L18.76,9.7c0.09,0.15,0.05,0.34-0.08,0.45L17.2,11.31c0.03,0.23,0.05,0.46,0.05,0.69s-0.02,0.46-0.05,0.68L18.68,13.84z"/><path fill="#78909C" d="M21,5v14c0,1.1-0.89,2-2,2h-7.06v-2h1.46c0.17,0,0.319-0.13,0.35-0.3l0.26-1.851c0.42-0.18,0.82-0.409,1.181-0.689l1.739,0.7c0.16,0.06,0.34,0,0.431-0.15l1.399-2.42c0.09-0.15,0.05-0.34-0.08-0.45L17.2,12.68c0.03-0.22,0.05-0.45,0.05-0.68s-0.02-0.46-0.05-0.69l1.479-1.16c0.13-0.11,0.17-0.3,0.08-0.45L17.36,7.28C17.27,7.13,17.09,7.07,16.93,7.13l-1.739,0.7c-0.36-0.28-0.75-0.51-1.181-0.69l-0.26-1.85C13.72,5.13,13.57,5,13.4,5h-1.46V3H19C20.11,3,21,3.9,21,5z"/><path fill="#78909C" d="M12,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C14,10.9,13.1,10,12,10z"/><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>settings_applications</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#757575" d="M7.5,7C4.46,7,2,9.46,2,12.5S4.46,18,7.5,18H17v-1.5H7.5c-2.21,0-4-1.79-4-4s1.79-4,4-4H17V7H7.5z"/><path fill="#616161" d="M18,7h-1v1.5h1c1.38,0,2.5,1.12,2.5,2.5s-1.12,2.5-2.5,2.5H9.5c-0.55,0-1-0.45-1-1s0.45-1,1-1H17V10H9.5C8.12,10,7,11.12,7,12.5S8.12,15,9.5,15H18c2.21,0,4-1.79,4-4S20.21,7,18,7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>attachment</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#D32F2F" d="M9,4.26v2.09C6.67,7.17,5,9.39,5,12c0,1.66,0.68,3.15,1.76,4.24L9,14v6H3l2.36-2.359C3.91,16.2,3,14.21,3,12C3,8.27,5.55,5.15,9,4.26z"/><rect x="11" y="15" fill="#B71C1C" width="2" height="2"/><path fill="#D32F2F" d="M18.641,6.36C20.09,7.8,21,9.79,21,12c0,3.73-2.55,6.85-6,7.74v-2.09c2.33-0.82,4-3.041,4-5.65c0-1.66-0.68-3.15-1.76-4.24L15,10V4h6L18.641,6.36z"/><rect x="11" y="7" fill="#B71C1C" width="2" height="6"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>sync_problem</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#8BC34A" points="8,10 5,10 5,7 3,7 3,10 0,10 0,12 3,12 3,15 5,15 5,12 8,12 "/><path fill="#00BCD4" d="M18,11c1.66,0,2.99-1.34,2.99-3S19.66,5,18,5s-3,1.34-3,3S16.34,11,18,11z M18,13c-2,0-6,1-6,3v2h12v-2C24,14,20,13,18,13z"/><path fill="#1976D2" d="M13,11c1.66,0,2.99-1.34,2.99-3S14.66,5,13,5s-3,1.34-3,3S11.34,11,13,11z M13,13c-2,0-6,1-6,3v2h12v-2C19,14,15,13,13,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>group_add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path opacity="0.8" fill="#43A047" d="M19,11v2h2v2h-2v2c0,1.1-0.9,2-2,2h-2v2h-2v-2h-2v2H9v-2H7c-1.1,0-2-0.9-2-2v-2H3v-2h2v-2H3V9h2V7c0-1.1,0.9-2,2-2h2V3h2v2h2V3h2v2h2c1.1,0,2,0.9,2,2v2h2v2H19z"/><rect x="7" y="7" fill="#B0BEC5" width="10" height="10"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="9" y="9" opacity="0.8" fill="#43A047" width="6" height="6"/><rect x="11" y="11" fill="#B0BEC5" width="2" height="2"/><rect x="9" y="3" fill="#B0BEC5" width="2" height="2"/><rect x="13" y="3" fill="#B0BEC5" width="2" height="2"/><polygon fill="#B0BEC5" points="8.98,18.98 11,18.98 11,21 9,21 9,19 8.98,19 "/><polygon fill="#B0BEC5" points="15.02,18.98 15.02,19 15,19 15,21 13,21 13,18.98 "/><rect x="3" y="9" fill="#B0BEC5" width="2" height="2"/><rect x="3" y="13" fill="#B0BEC5" width="2" height="2"/><rect x="19" y="9" fill="#B0BEC5" width="2" height="2"/><rect x="19" y="13" fill="#B0BEC5" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>memory</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#607D8B" points="10,18 8,21 8,22 16,22 16,21 14,18 "/><path fill="#90A4AE" d="M3,18c1.167,0,16.5,0,18,0c1.1,0,2-0.9,2-2H1C1,17.1,1.9,18,3,18z"/><path fill="#263238" d="M21,2H3C1.9,2,1,2.9,1,4v12h22V4C23,2.9,22.1,2,21,2z"/><rect x="3" y="4" fill="#5C6BC0" width="18" height="10"/></svg>			</div>
			<div class="icon-set__id">
				<span>desktop_mac</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M11.99,2C6.47,2,2,6.48,2,12c0,5.52,4.47,10,9.99,10C17.52,22,22,17.52,22,12C22,6.48,17.52,2,11.99,2z"/><circle fill="#C5CAE9" cx="12" cy="12" r="8"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#0D47A1" points="17,14.92 16.25,16.15 11,13 11,7 12.5,7 12.5,12.25 "/><rect x="11" y="7" fill="#1976D2" width="1.5" height="6"/></svg>			</div>
			<div class="icon-set__id">
				<span>query_builder</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#29B6F6" d="M12.04,2H12C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10h0.04c5.5-0.02,9.96-4.49,9.96-10S17.54,2.02,12.04,2zM12.04,20H12c-4.41,0-8-3.59-8-8s3.59-8,8-8h0.04C16.43,4.02,20,7.6,20,12C20,16.4,16.43,19.98,12.04,20z"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="9" y="8" fill="#039BE5" width="2" height="8"/><rect x="13" y="8" fill="#039BE5" width="2" height="8"/><path opacity="0.4" fill="#039BE5" d="M22,12c0,5.51-4.46,9.98-9.96,10v-2c4.39-0.02,7.96-3.6,7.96-8c0-4.4-3.57-7.98-7.96-8V2C17.54,2.02,22,6.49,22,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>pause_circle_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#B0BEC5" d="M16.56,8.94L7.62,0L6.21,1.41l2.38,2.38L3.44,8.94c-0.59,0.59-0.59,1.54,0,2.12l5.5,5.5C9.23,16.85,9.62,17,10,17s0.77-0.15,1.06-0.44l5.5-5.5C17.15,10.48,17.15,9.53,16.56,8.94z M5.21,10L10,5.21L14.79,10H5.21z"/><path fill="#26A69A" d="M19,11.5c0,0-2,2.17-2,3.5c0,1.1,0.9,2,2,2s2-0.9,2-2C21,13.67,19,11.5,19,11.5z"/><path fill="#26A69A" fill-opacity="0.36" d="M0,20h24v4H0V20z"/></g><g id="Capa_2"><polygon fill="#26A69A" points="14.854,10.021 9.938,14.542 5.146,9.979 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>format_color_fill</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><polygon fill="#B0BEC5" points="12.018,21.07 21.018,14.07 12.018,7.07 3.018,14.07 	"/><polygon fill="#CFD8DC" points="12.018,16 21.018,9 12.018,2 3.018,9 	"/></g><rect x="11.102" y="-2.244" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 28.2636 10.2933)" fill="#9FA8DA" width="1.796" height="26.489"/></svg>			</div>
			<div class="icon-set__id">
				<span>layers_clear</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="5" y="11" fill="#00796B" width="14" height="2"/><rect x="3" y="15" fill="#00796B" width="14" height="2"/><rect x="7" y="7" fill="#00796B" width="14" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>clear_all</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M17.5,11.45c-0.24-0.06-0.48-0.15-0.69-0.25C17.02,11.31,17.25,11.4,17.5,11.45z M0,0v24h24V0H0z M22.88,21.75H13.98V21H14v-7.5h1.5v5c0,1.38,1.12,2.5,2.5,2.5s2.5-1.12,2.5-2.5V9c0-0.69-0.28-1.32-0.73-1.77l0.011-0.01L16.06,3.5L15,4.56l2.11,2.11C16.17,7.03,15.5,7.93,15.5,9c0,0.69,0.28,1.32,0.73,1.77c0.18,0.17,0.369,0.31,0.579,0.43c0.21,0.11,0.44,0.2,0.69,0.25c0.01-0.01,0.01-0.01,0.02,0c0.091,0.01,0.181,0.03,0.271,0.04c0.09,0.01,0.18,0.01,0.27,0.01c0.07,0,0.141,0,0.2-0.01c0.26-0.03,0.51-0.1,0.74-0.2v7.21c0,0.55-0.45,1-1,1s-1-0.45-1-1V14c0-1.1-0.9-2-2-2h-1V5c0-0.09-0.01-0.18-0.02-0.27V2.71h8.899V21.75z"/><path fill="#E53935" d="M20.5,9v9.5c0,1.38-1.12,2.5-2.5,2.5s-2.5-1.12-2.5-2.5v-5H14V21H4V5c0-1.1,0.9-2,2-2h6c1.01,0,1.85,0.75,1.98,1.73C13.99,4.82,14,4.91,14,5v7h1c1.1,0,2,0.9,2,2v4.5c0,0.55,0.45,1,1,1s1-0.45,1-1v-7.21c-0.23,0.1-0.48,0.17-0.74,0.2c-0.06,0.01-0.13,0.01-0.2,0.01c-0.09,0-0.18,0-0.27-0.01c-0.09-0.01-0.18-0.03-0.271-0.04c-0.01-0.01-0.01-0.01-0.02,0c-0.25-0.05-0.48-0.14-0.69-0.25c-0.21-0.12-0.399-0.26-0.579-0.43C15.78,10.32,15.5,9.69,15.5,9c0-1.07,0.67-1.97,1.61-2.33L15,4.56l1.06-1.06l3.721,3.72L19.77,7.23C20.22,7.68,20.5,8.31,20.5,9z"/><path fill="#90A4AE" d="M20.5,9v9.5c0,1.38-1.12,2.5-2.5,2.5s-2.5-1.12-2.5-2.5v-5H14V21h-0.02V4.73C13.99,4.82,14,4.91,14,5v7h1c1.1,0,2,0.9,2,2v4.5c0,0.55,0.45,1,1,1s1-0.45,1-1v-7.21c-0.23,0.1-0.48,0.17-0.74,0.2c-0.06,0.01-0.13,0.01-0.2,0.01c-0.09,0-0.18,0-0.27-0.01c-0.09-0.01-0.18-0.03-0.271-0.04c-0.01-0.01-0.01-0.01-0.02,0c-0.25-0.05-0.48-0.14-0.69-0.25c-0.21-0.12-0.399-0.26-0.579-0.43C15.78,10.32,15.5,9.69,15.5,9c0-1.07,0.67-1.97,1.61-2.33L15,4.56l1.06-1.06l3.721,3.72L19.77,7.23C20.22,7.68,20.5,8.31,20.5,9z"/><path fill="#455A64" d="M20.5,9v0.95c-0.3,0.64-0.84,1.14-1.5,1.38c-0.23,0.09-0.48,0.14-0.74,0.16c-0.06,0.01-0.13,0.01-0.2,0.01c-0.09,0-0.18,0-0.27-0.01c-0.09-0.01-0.18-0.03-0.271-0.04c-0.01-0.01-0.01-0.01-0.02,0c-0.24-0.06-0.48-0.15-0.69-0.25c-0.21-0.12-0.399-0.26-0.579-0.43C15.78,10.32,15.5,9.69,15.5,9c0-1.07,0.67-1.97,1.61-2.33L15,4.56l1.06-1.06l3.721,3.72L19.77,7.23C20.22,7.68,20.5,8.31,20.5,9z"/><rect x="6" y="5" fill="#E3F2FD" width="6" height="5"/><circle fill="#90A4AE" cx="18" cy="9" r="1"/><rect x="4.004" y="11.184" fill="#D32F2F" width="9.963" height="6.126"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_gas_station</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><rect x="4" y="11" fill="#E53935" width="8" height="2"/><polygon fill="#546E7A" points="19,18 17,18 17,7.38 14,8.4 14,6.7 18.7,5 19,5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>exposure_minus_1</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4CAF50" enable-background="new    " d="M4,6H2v14c0,1.1,0.9,2,2,2h14v-2H4V6z"/><path fill="#66BB6A" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path opacity="0.7" fill="#4CAF50" enable-background="new    " d="M22,4v12c0,1.1-0.9,2-2,2h-6.04V2H20C21.1,2,22,2.9,22,4z"/><polygon fill="#FFFFFF" points="19,11 15,11 15,15 13,15 13,11 9,11 9,9 13,9 13,5 15,5 15,9 19,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>add_to_photos</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#03A9F4" d="M12,19c-3.865,0-7-3.138-7-7c0-3.865,3.135-7,7-7V2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c3.668,0,6.868-1.985,8.608-4.933l-2.583-1.521C16.808,17.61,14.566,19,12,19z"/><path fill="#9575CD" d="M12,2v3c3.862,0,7,3.135,7,7c0,1.296-0.359,2.506-0.975,3.547l2.583,1.521C21.487,15.58,22,13.852,22,12C22,6.48,17.52,2,12,2z"/></svg>			</div>
			<div class="icon-set__id">
				<span>data_usage</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#D32F2F" d="M12,2C8.13,2,5,5.13,5,9c0,5.25,7,13,7,13s7-7.75,7-13C19,5.13,15.87,2,12,2z M11.997,18.878C9.884,16.188,7,11.876,7,9c0-2.757,2.243-5,5-5s5,2.243,5,5C17,11.852,14.081,16.212,11.997,18.878z"/><path fill="#F44336" d="M12,4C9.243,4,7,6.243,7,9c0,2.876,2.884,7.188,4.997,9.878C14.081,16.212,17,11.852,17,9C17,6.243,14.757,4,12,4z M12,11.5c-1.38,0-2.5-1.12-2.5-2.5s1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5S13.38,11.5,12,11.5z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>location_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M13,3c-4.97,0-9,4.03-9,9H1l3.89,3.891l0.07,0.14L9,12H6c0-3.87,3.13-7,7-7s7,3.13,7,7s-3.13,7-7,7c-1.93,0-3.68-0.79-4.94-2.061l-1.42,1.42C8.27,19.99,10.51,21,13,21c4.971,0,9-4.03,9-9S17.971,3,13,3z"/><polygon fill="#1976D2" points="12,8 12,13 16.279,15.54 17,14.33 13.5,12.25 13.5,8 "/></svg>			</div>
			<div class="icon-set__id">
				<span>restore</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="5" y="4" fill="#039BE5" width="14" height="2"/><polygon fill="#29B6F6" points="5,14 9,14 9,20 15,20 15,14 19,14 12,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>publish</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#B0BEC5" d="M12,1.999c-5.52,0-10,4.48-10,10c0,3.7,2.01,6.92,4.99,8.65l1-1.73C5.609,17.529,4,14.959,4,11.999c0-4.42,3.58-8,8-8s8,3.58,8,8c0,2.96-1.609,5.53-4,6.92l1,1.73c2.99-1.73,5-4.95,5-8.65C22,6.479,17.52,1.999,12,1.999z M12,5.999c-3.311,0-6,2.69-6,6c0,2.22,1.21,4.15,3,5.189l1-1.739c-1.189-0.7-2-1.97-2-3.45c0-2.21,1.79-4,4-4s4,1.79,4,4c0,1.48-0.811,2.75-2,3.45l1,1.739c1.79-1.039,3-2.972,3-5.189C18,8.689,15.311,5.999,12,5.999z M12.089,9.999c-1.09,0-1.989,0.89-2,1.98c0,0.01,0,0.01,0,0.02c0,1.1,0.899,2,2,2s2-0.9,2-2c0-0.01,0-0.01,0-0.02C14.079,10.889,13.18,9.999,12.089,9.999z"/></g><g id="Capa_2"><path fill="#9FA8DA" d="M1.984,3.797L3.25,2.5c0,0,17.766,17.672,17.734,17.719c-0.027,0.041-1.266,1.25-1.266,1.25L1.984,3.797z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>portable_wifi_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M20,8v12c0,1.1-0.9,2-2,2H5.99C4.89,22,4,21.1,4,20L4.01,4C4.01,2.9,4.9,2,6,2h8L20,8z"/><rect x="8" y="16" fill="#455A64" width="8" height="2"/><rect x="8" y="12" fill="#455A64" width="8" height="2"/><polygon opacity="0.5" fill="#90A4AE" points="20,8 20,9 14,9 14,2 "/></svg>			</div>
			<div class="icon-set__id">
				<span>description</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#CFD8DC" d="M19.999,8v12c0,1.1-0.9,2-2,2H5.991C4.89,22,4,21.1,4,20L4.011,4c0-1.101,0.89-2,1.989-2h8L19.999,8z"/><polygon opacity="0.5" fill="#90A4AE" enable-background="new    " points="19.999,8.095 19.999,9.095 13.999,9.095 13.999,2.095"/></g><path fill="none" d="M0,0h24v24H0V0z"/><polygon opacity="0.8" fill="#1976D2" points="14,15.5 14,19.5 10,19.5 10,15.5 7,15.5 12,10.5 17,15.5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>insert_drive_file</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="22" fill="#455A64" width="2" height="2"/><rect x="11" y="22" fill="#546E7A" width="2" height="2"/><rect x="15" y="22" fill="#455A64" width="2" height="2"/><path fill="#78909C" d="M16,0.01L8,0C6.9,0,6,0.9,6,2v16c0,1.1,0.9,2,2,2h8c1.1,0,2-0.9,2-2V2C18,0.9,17.1,0.01,16,0.01z"/><rect x="8" y="4" fill="#BBDEFB" width="8" height="12"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_cell</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path d="M23,12v5h-2c0,0.79-0.3,1.5-0.8,2.03C19.66,19.63,18.87,20,18,20s-1.66-0.37-2.2-0.97C15.3,18.5,15,17.79,15,17H9c0,0.79-0.3,1.5-0.8,2.03C7.66,19.63,6.87,20,6,20s-1.66-0.37-2.2-0.97C3.3,18.5,3,17.79,3,17H1V6c0-1.1,0.9-2,2-2h14v4h3L23,12z"/><path fill="#B0BEC5" d="M17,4v11.03H1V6c0-1.1,0.9-2,2-2H17z"/><path fill="#78909C" d="M23,15.03V17h-2c0,0.79-0.3,1.5-0.8,2.03h-4.4C15.3,18.5,15,17.79,15,17H9c0,0.79-0.3,1.5-0.8,2.03H3.8C3.3,18.5,3,17.79,3,17H1v-1.97H23z"/><circle fill="#455A64" cx="5.984" cy="16.984" r="3.016"/><polygon fill="#29B6F6" points="23,12 23,15.03 17,15.03 17,8 20,8 "/><circle fill="#455A64" cx="18" cy="16.969" r="3.016"/><circle fill="#90A4AE" cx="6" cy="17" r="1.5"/><polygon fill="#B2EBF2" points="20.04,9.5 22,12 17.54,12 17.54,9.5 "/><circle fill="#90A4AE" cx="18" cy="17" r="1.5"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_shipping</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_din</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#1976D2" d="M17.71,7.71L12,2h-1v7.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L11,14.41V22h1l5.71-5.71L13.41,12L17.71,7.71z M13,5.83l1.88,1.88L13,9.59V5.83z M14.88,16.29L13,18.17v-3.76L14.88,16.29z"/></svg>			</div>
			<div class="icon-set__id">
				<span>bluetooth</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#43A047" d="M12,4V1L8,5l4,4V6c3.311,0,6,2.69,6,6c0,1.01-0.25,1.97-0.7,2.8l1.46,1.46C19.54,15.03,20,13.57,20,12C20,7.58,16.42,4,12,4z"/><path fill="#66BB6A" d="M12,18c-3.31,0-6-2.689-6-6c0-1.01,0.25-1.97,0.7-2.8L5.24,7.74C4.46,8.97,4,10.43,4,12c0,4.42,3.58,8,8,8v3l4-4l-4-4V18z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>loop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M20,12c0-2.54-1.189-4.81-3.04-6.27L16,0H8L7.05,5.73C5.19,7.19,4,9.45,4,12s1.19,4.811,3.05,6.27L8,24h8l0.96-5.73C18.811,16.811,20,14.54,20,12z"/><circle fill="#FF7043" cx="12.01" cy="11.969" r="8.011"/><path fill="#CFD8DC" d="M6,12c0-3.31,2.69-6,6-6c3.311,0,6,2.69,6,6c0,3.311-2.689,6-6,6C8.69,18,6,15.311,6,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>watch</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#43A047" d="M24,17.37c0,0.28-0.109,0.53-0.29,0.71l-2.479,2.48c-0.182,0.181-0.432,0.289-0.711,0.289c-0.271,0-0.521-0.1-0.699-0.279c-0.791-0.729-1.682-1.359-2.66-1.851c-0.33-0.159-0.561-0.51-0.561-0.899V14.72c-0.91-0.29-1.851-0.5-2.82-0.62c-0.348-0.045-0.691-0.057-1.037-0.076C12.496,14.011,12.249,14,12,14c-1.4,0-2.76,0.189-4.05,0.561c-0.17,0.051-0.33,0.101-0.49,0.149C7.44,14.72,7.42,14.73,7.4,14.73v3.1c0,0.4-0.23,0.74-0.56,0.9c-0.97,0.488-1.87,1.108-2.66,1.85c-0.18,0.17-0.43,0.279-0.7,0.279c-0.28,0-0.53-0.109-0.71-0.289l-1.54-1.54l-0.94-0.94C0.11,17.91,0,17.66,0,17.38s0.11-0.53,0.29-0.71C3.34,13.78,7.46,12,12,12s8.66,1.78,11.71,4.67C23.891,16.85,24,17.1,24,17.37z"/><path fill="#039BE5" d="M21.16,6.26l-1.41-1.41L16.189,8.4l1.41,1.41C17.6,9.81,21.05,6.29,21.16,6.26z"/><rect x="11" y="2" fill="#039BE5" width="2" height="5"/><path fill="#039BE5" d="M6.4,9.81L7.81,8.4L4.26,4.84L2.84,6.26C2.95,6.29,6.4,9.81,6.4,9.81z"/></g><g id="Capa_2"><path fill="#388E3C" d="M24,17.37c0,0.28-0.109,0.53-0.29,0.71l-0.56,0.561c-3.23-2.979-6.381-4.199-9.371-4.541c-0.539-0.068-1.079-0.108-1.609-0.119c-1.58-0.021-2.98,0.211-4.22,0.58c-0.17,0.051-0.33,0.101-0.49,0.149C7.44,14.72,7.42,14.73,7.4,14.73c-3.49,1.209-5.54,3.51-6.17,4.3l-0.94-0.94C0.11,17.91,0,17.66,0,17.38s0.11-0.53,0.29-0.71C3.34,13.78,7.46,12,12,12s8.66,1.78,11.71,4.67C23.891,16.85,24,17.1,24,17.37z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>ring_volume</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#5C6BC0" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M12,20c-4.42,0-8-3.58-8-8c0-1.85,0.63-3.55,1.69-4.9l11.21,11.21C15.55,19.37,13.85,20,12,20z M18.311,16.9L7.1,5.69C8.45,4.63,10.15,4,12,4c4.42,0,8,3.58,8,8C20,13.85,19.37,15.55,18.311,16.9z"/><polygon fill="#EF5350" points="1.766,3.156 3.078,1.656 22.063,20.563 20.875,22.25 "/></svg>			</div>
			<div class="icon-set__id">
				<span>not_interested</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><line display="none" fill="none" x1="0.29" y1="11.67" x2="17.834" y2="30.166"/><line display="none" fill="none" x1="12" y1="7" x2="12" y2="30.5"/><line display="none" fill="none" x1="7.4" y1="9.72" x2="12" y2="24.016"/><g><path fill="#F44336" d="M3.48,15.85c0.27,0,0.52-0.1,0.7-0.279c0.79-0.73,1.68-1.36,2.66-1.851c0.33-0.159,0.56-0.51,0.56-0.899V9.72c-2.474,0.796-4.66,2.224-6.388,4.082l1.758,1.759C2.95,15.74,3.2,15.85,3.48,15.85z"/><path fill="#D32F2F" d="M12,9c1.601,0,3.15,0.25,4.601,0.72c2.474,0.796,4.659,2.224,6.388,4.082l0.722-0.722c0.181-0.17,0.29-0.42,0.29-0.7c0-0.28-0.109-0.53-0.29-0.71C20.66,8.78,16.54,7,12,7S3.34,8.78,0.29,11.67C0.11,11.85,0,12.1,0,12.38c0,0.28,0.11,0.53,0.29,0.7l0.722,0.722C2.74,11.943,4.926,10.516,7.4,9.72C8.85,9.25,10.399,9,12,9z"/><path fill="#F44336" d="M20.521,15.85c-0.271,0-0.521-0.1-0.7-0.279c-0.79-0.73-1.68-1.36-2.66-1.851c-0.33-0.159-0.56-0.51-0.56-0.899V9.72c2.474,0.796,4.659,2.224,6.388,4.082l-1.758,1.759C21.05,15.74,20.8,15.85,20.521,15.85z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>call_end</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#3949AB" fill-opacity="0.3" d="M2,22h20V2L2,22z"/><path fill="#9FA8DA" d="M17,7L2,22h15V7z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>network_cell</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CDDC39" fill-opacity="0.3" d="M17,5.33C17,4.6,16.4,4,15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33V11h10V5.33z"/><path fill="#CDDC39" d="M7,11v9.67C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V11H7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_60</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M21,4H3C1.9,4,1,4.9,1,6v12c0,1.1,0.9,2,2,2h18c1.1,0,1.99-0.9,1.99-2L23,6C23,4.9,22.1,4,21,4z"/><rect x="5" y="6" fill="#BBDEFB" width="14" height="12"/></svg>			</div>
			<div class="icon-set__id">
				<span>tablet</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFD600" d="M7,2v11h3v9l7-12h-4l4-8H7z"/><path fill="#FFC107" d="M7,13l5.5-11H7V13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>flash_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#78909C" d="M20,4v7h-7l3.221-3.22C15.141,6.69,13.66,6,12,6c-3.31,0-6,2.69-6,6c0,3.311,2.69,6,6,6c2.609,0,4.83-1.67,5.65-4h2.08c-0.892,3.45-4,6-7.73,6c-4.42,0-7.99-3.58-7.99-8S7.58,4,12,4c2.21,0,4.2,0.9,5.65,2.35L20,4z"/><path fill="none" d="M0,0h24v24H0V0z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>refresh</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M18,14.5c0-0.83,0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5V16h-3V14.5z M0,0v24h24V0H0z M18,14.5c0-0.83,0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5V16h-3V14.5z"/><path fill="#90A4AE" d="M19.5,10c0.17,0,0.33,0.03,0.5,0.05V1L1,20h13v-3c0-0.891,0.391-1.68,1-2.23V14.5C15,12.02,17.02,10,19.5,10z"/><path fill="#FFA726" d="M22.12,16.01H22.1C22.07,16,22.03,16,22,16v-1.5c0-1.38-1.12-2.5-2.5-2.5S17,13.12,17,14.5V16c-0.03,0-0.07,0-0.1,0.01C16.4,16.06,16,16.48,16,17v4c0,0.55,0.45,1,1,1h5c0.55,0,1-0.45,1-1v-4C23,16.49,22.61,16.07,22.12,16.01zM18,14.5c0-0.83,0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5V16h-3V14.5z"/><path fill="#FFCC80" d="M22,16v-1.5c0-1.38-1.12-2.5-2.5-2.5S17,13.12,17,14.5V16c-0.03,0-0.07,0-0.1,0.01H22.1C22.07,16,22.03,16,22,16z M21,16h-3v-1.5c0-0.83,0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5V16z"/></svg>			</div>
			<div class="icon-set__id">
				<span>network_locked</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="9.375" y="9.958" fill="#9FA8DA" width="10.167" height="8.958"/><path fill="#C5CAE9" d="M4.609,17.984l3.938-4.469c0,0,3.922,4.797,3.969,4.766"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#5C6BC0" d="M21,19V5c0-1.1-0.9-2-2-2H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14C20.1,21,21,20.1,21,19z M8.5,13.5l2.5,3.01L14.5,12l4.5,6H5L8.5,13.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>image</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M21.005,16.488v3.5c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.569,3.57c0.109,0.35,0.029,0.74-0.25,1l-2.199,2.21c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.319,0.439,0.63,0.682,0.94c0.129,0.158,0.25,0.3,0.368,0.449c0.29,0.34,0.591,0.658,0.9,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.12,0.109,0.24,0.221,0.37,0.311c0.132,0.102,0.25,0.2,0.382,0.3c0.131,0.11,0.26,0.2,0.398,0.29c0.143,0.102,0.277,0.189,0.42,0.29c0.277,0.2,0.58,0.381,0.88,0.54c0.188,0.109,0.392,0.221,0.58,0.32l2.2-2.2c0.277-0.271,0.67-0.351,1.021-0.24c1.118,0.37,2.318,0.57,3.568,0.57C20.555,15.488,21.005,15.938,21.005,16.488z"/><g id="Capa_2"><path fill="#455A64" d="M21.005,19.668v0.32c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h0.688l0.012,0.06c0,0-0.131,3.77,1.92,7.72c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.319,0.439,0.63,0.682,0.94c0.129,0.158,0.25,0.3,0.368,0.449c0.29,0.34,0.591,0.658,0.9,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.12,0.101,0.24,0.199,0.37,0.311c0.132,0.102,0.25,0.2,0.382,0.3c0.131,0.103,0.271,0.19,0.398,0.29c0.143,0.102,0.277,0.189,0.42,0.29c0.277,0.183,0.58,0.37,0.88,0.54c0.188,0.109,0.392,0.221,0.58,0.32c2.05,1.068,4.603,1.869,7.763,2.21L21.005,19.668z"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/><rect x="11" y="9" fill="#455A64" width="2" height="2"/><rect x="15" y="9" fill="#546E7A" width="2" height="2"/><rect x="19" y="9" fill="#455A64" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_phone</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#D7CCC8" d="M3.007,8.001l-0.008,11c0,1.1,0.891,2,2,2h14c1.1,0,2-0.9,2-2v-11H3.007z"/><path fill="#C2185B" d="M20.999,5.001c0-1.1-0.9-2-2-2h-14c-1.109,0-1.99,0.9-1.99,2l-0.002,3h17.992V5.001z"/><path fill="#A1887F" d="M7.999,5.001h-2v-4h2V5.001z M17.999,1.001h-2v4h2V1.001z"/></g><polygon fill="#4CAF50" points="16.529,11.06 15.471,10 10.59,14.88 8.47,12.76 7.41,13.82 10.59,17 "/></svg>			</div>
			<div class="icon-set__id">
				<span>event_available</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M21,5H3C1.9,5,1,5.9,1,7v10c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2V7C23,5.9,22.1,5,21,5z"/><rect x="5" y="7" fill="#B2EBF2" width="14" height="10"/><path fill="#FFA726" d="M14,11v-1c0-1.11-0.9-2-2-2c-1.11,0-2,0.9-2,2v1c-0.55,0-1,0.45-1,1v3c0,0.55,0.45,1,1,1h4c0.55,0,1-0.45,1-1v-3C15,11.45,14.55,11,14,11z M10.8,11v-1c0-0.66,0.54-1.2,1.2-1.2s1.2,0.54,1.2,1.2v1H10.8z"/><path fill="#FFCC80" d="M14,10v1h-0.8v-1c0-0.66-0.54-1.2-1.2-1.2s-1.2,0.54-1.2,1.2v1H10v-1c0-1.1,0.89-2,2-2C13.1,8,14,8.89,14,10z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>screen_lock_landscape</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="2.979" y="5" opacity="0.7" fill="#C5CAE9" width="18.063" height="14.011"/><path opacity="0.6" fill="#BA68C8" d="M3,21h18c1.1,0,2-0.9,2-2v-3h-2v3H3V5h7V3H3C1.9,3,1,3.9,1,5v14C1,20.1,1.9,21,3,21z"/><rect x="5" y="7" fill="#CE93D8" width="14" height="10"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#42A5F5" d="M1,18v3h3C4,19.34,2.66,18,1,18z"/><path fill="#42A5F5" d="M1,14v2c2.76,0,5,2.24,5,5h2C8,17.13,4.87,14,1,14z"/><path fill="#CE93D8" d="M19,7H5v1.63c3.96,1.28,7.09,4.41,8.37,8.37H19V7z"/><path fill="#42A5F5" d="M1,10v2c4.97,0,9,4.03,9,9h2C12,14.92,7.07,10,1,10z"/><path fill="#CE93D8" d="M21,3H3C1.9,3,1,3.9,1,5v3h2V5h18v14h-7v2h7c1.1,0,2-0.9,2-2V5C23,3.9,22.1,3,21,3z"/></svg>			</div>
			<div class="icon-set__id">
				<span>cast_connected</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#6D4C41" d="M22,4v11c0,1.109-0.891,2-2,2h-4v5l-4-2l-0.02,0.01L8,22v-5H4c-1.11,0-2-0.891-2-2V4c0-1.11,0.89-2,2-2h16C21.109,2,22,2.89,22,4z"/><rect x="4" y="13" fill="#6D4C41" width="16" height="2"/><rect x="4" y="4" fill="#6D4C41" width="16" height="6"/><rect x="1.953" y="9.969" fill="#B39DDB" width="20.009" height="3.031"/><polygon fill="#FFEE58" points="16,17 16,22 12,20 11.98,20.01 8,22 8,17 "/><polygon opacity="0.6" fill="#FFCA28" enable-background="new    " points="16,16.98 16,22 12,20 11.98,20.01 11.98,16.98 "/></svg>			</div>
			<div class="icon-set__id">
				<span>wallet_membership</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M7,3h14v14H7V3z"/><path fill="#5C6BC0" d="M3,5H1v16c0,1.1,0.9,2,2,2h16v-2H3V5z"/><path fill="#9FA8DA" d="M21,1H7C5.9,1,5,1.9,5,3v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V3C23,1.9,22.1,1,21,1z M21,17H7V3h14V17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_none</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#01579B" d="M12,3v9.28v8.45c1.585-0.566,2.758-1.995,2.95-3.73H15V6h4V3H12z"/><path fill="#0288D1" d="M10.5,12C8.01,12,6,14.01,6,16.5S8.01,21,10.5,21c0.528,0,1.03-0.102,1.5-0.27v-8.45C11.53,12.11,11.03,12,10.5,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>audiotrack</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#EF9A9A" points="22,5.72 17.4,1.86 16.109,3.39 20.71,7.25 "/><polygon fill="#EF9A9A" points="7.88,3.39 6.6,1.86 2,5.71 3.29,7.24 "/><path fill="#F44336" d="M12,4c-4.97,0-9,4.03-9,9s4.02,9,9,9c4.971,0,9-4.03,9-9S16.971,4,12,4z"/><path fill="#E0E0E0" d="M12,20c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,20,12,20z"/><polygon fill="#FFA000" points="10.54,14.53 8.41,12.4 7.35,13.46 10.53,16.641 16.529,10.64 15.471,9.58 "/></svg>			</div>
			<div class="icon-set__id">
				<span>alarm_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26A69A" d="M20,6.54v10.91c-2.6-0.771-5.279-1.16-8-1.16c-2.72,0-5.4,0.39-8,1.16V6.54C6.6,7.31,9.28,7.7,12,7.7C14.721,7.71,17.4,7.32,20,6.54 M21.43,4c-0.1,0-0.199,0.02-0.31,0.06C18.18,5.16,15.09,5.7,12,5.7S5.82,5.15,2.88,4.06C2.77,4.02,2.66,4,2.57,4C2.23,4,2,4.23,2,4.63v14.75C2,19.77,2.23,20,2.57,20c0.1,0,0.2-0.02,0.31-0.061c2.94-1.1,6.03-1.64,9.12-1.64s6.18,0.55,9.12,1.64C21.23,19.98,21.33,20,21.43,20c0.33,0,0.57-0.23,0.57-0.63V4.63C22,4.23,21.76,4,21.43,4z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>panorama_horizontal</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19,4H5C3.9,4,3,4.9,3,6v12c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V6C21,4.9,20.1,4,19,4z M19,18H5V6h14V18z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_3_2</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><rect x="1" y="9" fill="#2E7D32" width="4" height="12"/><path fill="#43A047" d="M22.99,10.08L23,10.09V12c0,0.26-0.05,0.5-0.141,0.73l-3.021,7.05C19.54,20.5,18.83,21,18,21H9c-1.1,0-2-0.9-2-2V9c0-0.55,0.22-1.05,0.59-1.41L14.17,1l1.021,1.01l0.039,0.04C15.5,2.32,15.67,2.7,15.67,3.11l-0.029,0.32L14.689,8H21c1.01,0,1.859,0.76,1.98,1.75C22.99,9.83,23,9.91,23,10L22.99,10.08z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>thumb_up</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="10" y="6.01" opacity="0.5" fill="#689F38" width="3" height="12"/><path fill="#689F38" d="M11.8,10.9c-2.27-0.59-3-1.2-3-2.15c0-1.09,1.01-1.85,2.7-1.85c1.779,0,2.439,0.85,2.5,2.1h2.21C16.141,7.28,15.09,5.7,13,5.19V3h-3v2.16C8.06,5.58,6.5,6.84,6.5,8.77c0,2.31,1.91,3.46,4.7,4.13c2.5,0.6,3,1.479,3,2.41c0,0.689-0.49,1.789-2.7,1.789c-2.06,0-2.87-0.92-2.98-2.1h-2.2c0.12,2.189,1.76,3.42,3.68,3.83V21h3v-2.15c1.95-0.369,3.5-1.5,3.5-3.55C16.5,12.46,14.07,11.49,11.8,10.9z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>attach_money</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_" display="none"><path display="inline" fill="none" d="M17.65,17.67l-0.801-0.811l-0.609-0.601L7.77,7.77L7.12,7.13L6.36,6.36L4.14,4.14L2.86,5.41l2.36,2.36C5.14,7.9,5.06,8.03,5,8.16C4.78,8.54,4.6,8.94,4.46,9.35C4.16,10.18,4,11.07,4,12c0,0.73,0.1,1.43,0.29,2.1c0.06,0.23,0.13,0.44,0.21,0.66c0.41,1.09,1.05,2.07,1.86,2.881L4,20h0.02l-0.01,0.01h6v-6L10,14.02V14l-2.24,2.24c-0.67-0.682-1.19-1.51-1.48-2.45c-0.17-0.56-0.27-1.16-0.27-1.78c0-1,0.25-1.94,0.68-2.77l8.062,8.062c-0.126,0.063-0.24,0.144-0.372,0.198c-0.12,0.05-0.26,0.1-0.39,0.15v2.09c0.01,0,0.01,0,0.02-0.01v0.02c0.801-0.21,1.54-0.53,2.221-0.96l2.352,2.351l1.271-1.271L17.65,17.67z M9.23,6.68C9.48,6.56,9.73,6.44,10,6.35V5.2c0-0.01,0.01-0.01,0.01-0.01V4.27H10V4.26c-0.21,0.06-0.42,0.12-0.63,0.2C9.09,4.56,8.81,4.68,8.54,4.8C8.28,4.92,8.02,5.06,7.77,5.22L9.23,6.68z M19.51,9.25c-0.398-1.09-1.05-2.07-1.859-2.88l2.359-2.36h-0.02L20,4h-6v6l0.01-0.01v0.02l2.24-2.24c0.41,0.41,0.75,0.87,1.029,1.38c0.181,0.34,0.341,0.69,0.451,1.07C17.9,10.78,18,11.38,18,12c0,0.38-0.04,0.74-0.1,1.1c-0.11,0.592-0.313,1.15-0.58,1.67l0.01,0.013l1.449,1.448c0.479-0.761,0.841-1.608,1.041-2.528c0.039-0.183,0.068-0.37,0.1-0.552c0.061-0.368,0.09-0.75,0.09-1.141c0-0.729-0.1-1.44-0.289-2.1C19.66,9.68,19.59,9.46,19.51,9.25z M17.65,17.67l-0.801-0.811l-0.609-0.601L7.77,7.77L7.12,7.13L6.36,6.36L4.14,4.14L2.86,5.41l2.36,2.36C5.14,7.9,5.06,8.03,5,8.16C4.78,8.54,4.6,8.94,4.46,9.35C4.16,10.18,4,11.07,4,12c0,0.73,0.1,1.43,0.29,2.1c0.06,0.23,0.13,0.44,0.21,0.66c0.41,1.09,1.05,2.07,1.86,2.881L4,20h0.02l-0.01,0.01h6v-6L10,14.02V14l-2.24,2.24c-0.67-0.682-1.19-1.51-1.48-2.45c-0.17-0.56-0.27-1.16-0.27-1.78c0-1,0.25-1.94,0.68-2.77l8.062,8.062c-0.126,0.063-0.24,0.144-0.372,0.198c-0.12,0.05-0.26,0.1-0.39,0.15v2.09c0.01,0,0.01,0,0.02-0.01v0.02c0.801-0.21,1.54-0.53,2.221-0.96l2.352,2.351l1.271-1.271L17.65,17.67z M9.23,6.68C9.48,6.56,9.73,6.44,10,6.35V5.2c0-0.01,0.01-0.01,0.01-0.01V4.27H10V4.26c-0.21,0.06-0.42,0.12-0.63,0.2C9.09,4.56,8.81,4.68,8.54,4.8C8.28,4.92,8.02,5.06,7.77,5.22L9.23,6.68z M19.51,9.25c-0.398-1.09-1.05-2.07-1.859-2.88l2.359-2.36h-0.02L20,4h-6v6l0.01-0.01v0.02l2.24-2.24c0.41,0.41,0.75,0.87,1.029,1.38c0.181,0.34,0.341,0.69,0.451,1.07C17.9,10.78,18,11.38,18,12c0,0.38-0.04,0.74-0.1,1.1c-0.11,0.592-0.313,1.15-0.58,1.67l0.01,0.013l1.449,1.448c0.479-0.761,0.841-1.608,1.041-2.528c0.039-0.183,0.068-0.37,0.1-0.552c0.061-0.368,0.09-0.75,0.09-1.141c0-0.729-0.1-1.44-0.289-2.1C19.66,9.68,19.59,9.46,19.51,9.25z"/><path display="inline" d="M9.37,4.46C9.09,4.56,8.81,4.68,8.54,4.8C8.28,4.92,8.02,5.06,7.77,5.22l1.46,1.46C9.48,6.56,9.73,6.44,10,6.35V4.26C9.79,4.32,9.58,4.38,9.37,4.46z M17.65,17.67l-0.801-0.811l-0.609-0.601L7.77,7.77L7.12,7.13L6.36,6.36L4.14,4.14L2.86,5.41l2.36,2.36C5.14,7.9,5.06,8.03,5,8.16C4.78,8.54,4.6,8.94,4.46,9.35C4.16,10.18,4,11.07,4,12c0,0.73,0.1,1.43,0.29,2.1c0.06,0.23,0.13,0.44,0.21,0.66c0.41,1.09,1.05,2.07,1.86,2.881L4,20h6v-6l-2.24,2.24c-0.67-0.682-1.19-1.51-1.48-2.45C6.1,13.23,6,12.63,6,12c0-1,0.25-1.94,0.68-2.77l0.01,0.01l8.062,8.062c-0.126,0.063-0.24,0.144-0.372,0.198c-0.12,0.05-0.26,0.1-0.39,0.15v2.09c0.01,0,0.01,0,0.02-0.01c0.79-0.211,1.54-0.53,2.211-0.95l0.01,0.01l2.352,2.351l1.271-1.271L17.65,17.67z M19.51,9.25c-0.398-1.09-1.05-2.07-1.859-2.88l-0.01-0.01l2.352-2.35L20,4h-6v6l2.24-2.24l0.01,0.01c0.41,0.41,0.75,0.87,1.029,1.38c0.181,0.34,0.341,0.69,0.451,1.07C17.9,10.78,18,11.38,18,12c0,0.38-0.04,0.74-0.1,1.1c-0.11,0.592-0.313,1.15-0.58,1.67l0.01,0.013l1.449,1.448c0.479-0.761,0.841-1.608,1.041-2.528c0.039-0.183,0.068-0.37,0.1-0.552C19.971,12.77,20,12.39,20,12c0-0.72-0.1-1.42-0.279-2.09C19.66,9.68,19.59,9.46,19.51,9.25z"/></g><g id="Capa_2"><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M10,14.02l0.01-0.01v6h-6L4.02,20l2.35-2.35l-0.01-0.01C5.55,16.83,4.91,15.85,4.5,14.76c-0.08-0.22-0.15-0.43-0.21-0.66c-0.18-0.658-0.28-1.357-0.28-2.09c0-0.93,0.16-1.83,0.45-2.66C4.61,8.94,4.79,8.54,5,8.16c0.37-0.67,0.82-1.27,1.36-1.8l0.76,0.77l0.65,0.64C7.34,8.2,6.97,8.7,6.69,9.24c-0.43,0.83-0.68,1.77-0.68,2.77c0,0.62,0.1,1.221,0.27,1.78c0.29,0.94,0.81,1.771,1.48,2.45l0.01,0.01L10,14.02z"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M10.01,4.27v0.92c0,0-0.01,0-0.01,0.01v1.15C9.73,6.44,9.48,6.56,9.23,6.68L7.78,5.23c0.25-0.15,0.5-0.29,0.76-0.43c0.27-0.12,0.55-0.24,0.83-0.34C9.58,4.39,9.79,4.33,10,4.27H10.01z"/><path fill="#90A4AE" d="M16.85,16.859l0.801,0.811c-0.42,0.43-0.898,0.811-1.42,1.12c-0.681,0.43-1.42,0.75-2.221,0.96v-2.09c0.852-0.301,1.61-0.78,2.23-1.4L16.85,16.859z"/><path fill="#90A4AE" d="M19.51,9.25c0.32,0.86,0.5,1.78,0.5,2.76c0,0.39-0.029,0.771-0.09,1.141c-0.029,0.182-0.061,0.369-0.1,0.552c-0.2,0.92-0.563,1.771-1.041,2.528l-1.449-1.448C17.6,14.26,17.8,13.7,17.9,13.1c0.08-0.35,0.107-0.72,0.107-1.09c0-0.63-0.1-1.229-0.277-1.79c-0.11-0.38-0.271-0.729-0.451-1.07C17,8.64,16.66,8.18,16.25,7.77l-2.24,2.24v-6h6L17.65,6.37C18.46,7.18,19.109,8.16,19.51,9.25z"/></g><path fill="#90A4AE" d="M17.33,14.782c0,0-0.533,1.702-2.924,2.702c0.031,0.078,0.391,0.875,0.531,0.891s1.293,0.415,1.293,0.415s1.516-1.032,2.549-2.56l-0.342-1.34L17.33,14.782z"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M9.23,6.68c0,0-1.48,0.367-2.542,2.523L5.234,7.797c0,0,0.528-1.368,2.545-2.567"/><g id="Capa_3"><polygon fill="#9FA8DA" points="2.859,5.422 4.125,4.125 19.828,19.844 18.594,21.156 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>sync_disabled</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#4CAF50" points="19,9 15,9 15,3 9,3 9,9 5,9 12,16 "/><rect x="5" y="18" fill="#388E3C" width="14" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>file_download</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#26A69A" points="21,4 12,20 3,4 "/><polygon opacity="0.3" fill="#00897B" points="21,4 12,20 12,4 "/></svg>			</div>
			<div class="icon-set__id">
				<span>details</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M12,5V1L7,6l5,5V7c3.311,0,6,2.69,6,6c0,3.311-2.689,6-6,6c-3.31,0-6-2.689-6-6H4c0,4.42,3.58,8,8,8s8-3.58,8-8S16.42,5,12,5z"/><path fill="#90A4AE" d="M20,13c0,4.42-3.58,8-8,8s-8-3.58-8-8h2c0,3.311,2.69,6,6,6c3.311,0,6-2.689,6-6c0-3.31-2.689-6-6-6v4L7,6l5-5v4C16.42,5,20,8.58,20,13z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>replay</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FF9800" d="M12,3c-4.97,0-9,4.03-9,9s4.03,9,9,9c0.83,0,1.5-0.67,1.5-1.5c0-0.391-0.15-0.74-0.391-1.01c-0.229-0.26-0.379-0.61-0.379-0.99c0-0.83,0.67-1.5,1.5-1.5H16c2.76,0,5-2.24,5-5C21,6.58,16.971,3,12,3z"/><circle fill="#FFC107" cx="6.5" cy="10.5" r="1.5"/><circle fill="#FF5722" cx="9.5" cy="6.5" r="1.5"/><circle fill="#D32F2F" cx="14.5" cy="6.5" r="1.5"/><circle fill="#7B1FA2" cx="17.5" cy="10.5" r="1.5"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>color_lens</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><g><path fill="#B0BEC5" d="M4,5.966h18v-2H4c-1.1,0-2,0.9-2,2v11H0v3h14v-3H4V5.966z"/><path fill="#CFD8DC" d="M23,7.966h-6c-0.55,0-1,0.45-1,1v10c0,0.55,0.45,1,1,1h6c0.55,0,1-0.45,1-1v-10C24,8.416,23.55,7.966,23,7.966z"/><rect x="18" y="9.966" fill="#B0BEC5" width="4" height="7"/></g><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path d="M37.753,0.608l-1.27,1.27l1.82,1.82c-0.291,0.34-0.471,0.78-0.471,1.26v11h-2v3h17.73l2.35,2.35l1.27-1.27L39.724,2.578L37.753,0.608z"/></g><g id="Capa_2"><polygon fill="#9FA8DA" points="0.66,2.953 1.957,1.625 21.394,21.078 20.112,22.375 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>phonelink_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="19" fill="#7E57C2" width="2" height="2"/><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="7" fill="#7E57C2" width="2" height="2"/><rect x="3" y="15" fill="#7E57C2" width="2" height="2"/><rect x="7" y="3" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#7E57C2" width="2" height="2"/><rect x="15" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><rect x="19" y="3" fill="#7E57C2" width="2" height="2"/><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><polygon fill="#512DA8" points="13,3 11,3 11,11 3,11 3,13 11,13 11,21 13,21 13,13 21,13 21,11 13,11 "/><rect x="19" y="19" fill="#7E57C2" width="2" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_inner</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#F48FB1" d="M11.99,2C6.47,2,2,6.48,2,12c0,5.52,4.47,10,9.99,10C17.52,22,22,17.52,22,12C22,6.48,17.52,2,11.99,2z"/><circle fill="#F48FB1" cx="12" cy="12" r="8"/><circle fill="#CFD8DC" cx="15.5" cy="9.5" r="1.5"/><circle fill="#CFD8DC" cx="8.5" cy="9.5" r="1.5"/><path fill="#CFD8DC" d="M12,17.5c2.33,0,4.311-1.46,5.109-3.5H6.89C7.69,16.04,9.67,17.5,12,17.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>mood</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#00897B" d="M3,5h2V3C3.9,3,3,3.9,3,5z"/><rect x="3" y="11" fill="#00897B" width="2" height="2"/><rect x="7" y="19" fill="#00897B" width="2" height="2"/><rect x="3" y="7" fill="#00897B" width="2" height="2"/><rect x="11" y="3" fill="#00897B" width="2" height="2"/><path fill="#00897B" d="M19,3v2h2C21,3.9,20.1,3,19,3z"/><path fill="#00897B" d="M5,21v-2H3C3,20.1,3.9,21,5,21z"/><rect x="3" y="15" fill="#00897B" width="2" height="2"/><rect x="7" y="3" fill="#00897B" width="2" height="2"/><rect x="11" y="19" fill="#00897B" width="2" height="2"/><rect x="19" y="11" fill="#00897B" width="2" height="2"/><path fill="#00897B" d="M19,21c1.1,0,2-0.9,2-2h-2V21z"/><rect x="19" y="7" fill="#00897B" width="2" height="2"/><rect x="19" y="15" fill="#00897B" width="2" height="2"/><rect x="15" y="19" fill="#00897B" width="2" height="2"/><rect x="15" y="3" fill="#00897B" width="2" height="2"/><path fill="#00796B" d="M7,7v10h10V7H7z M15,15H9V9h6V15z"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>select_all</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#43A047" points="19,9 15,9 15,3 9,3 9,9 5,9 12,16 "/><rect x="5" y="18" fill="#388E3C" width="14" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>get_app</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect fill="none" width="24" height="24"/><rect x="3" y="13" fill="#546E7A" width="18" height="2"/><rect x="3" y="17" opacity="0.8" fill="#455A64" width="18" height="2"/><rect x="3" y="9" opacity="0.8" fill="#455A64" width="18" height="2"/><rect x="3" y="5" fill="#B71C1C" width="18" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>reorder</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#D32F2F" d="M4,6H2v14c0,1.1,0.9,2,2,2h14v-2H4V6z"/><path fill="#D32F2F" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path fill="#E53935" d="M14,2v16H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2H14z"/><polygon fill="#FFFFFF" points="12,14.5 12,5.5 18,10 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>video_collection</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M10,2C8.18,2,6.47,2.5,5,3.35C7.99,5.08,10,8.3,10,12s-2.01,6.92-5,8.65C6.47,21.5,8.18,22,10,22c5.52,0,10-4.48,10-10C20,6.48,15.52,2,10,2z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>brightness_2</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CFD8DC" d="M17,8.5V7c0-1.11-0.9-2-2-2h-2c-1.1,0-2,0.89-2,2v1.5c0,0.83,0.67,1.5,1.5,1.5c-0.83,0-1.5,0.67-1.5,1.5V13c0,1.11,0.9,2,2,2h2c1.1,0,2-0.89,2-2v-1.5c0-0.83-0.67-1.5-1.5-1.5C16.33,10,17,9.33,17,8.5z M15,13h-2v-2h2V13z M15,9h-2V7h2V9z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_8</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#66BB6A" d="M21.917,22l-4-4h-14c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h16c1.1,0,1.99,0.9,1.99,2v6.96L21.917,22z"/><path opacity="0.25" fill="#43A047" enable-background="new    " d="M21.907,4v6.96H1.917V4c0-1.1,0.9-2,2-2h16C21.017,2,21.907,2.9,21.907,4z"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>mode_comment</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#7B1FA2" fill-opacity="0.9" d="M11.99,2C6.47,2,2,6.48,2,12c0,5.52,4.47,10,9.99,10C17.52,22,22,17.52,22,12C22,6.48,17.52,2,11.99,2z"/><circle fill="#C5CAE9" fill-opacity="0.9" cx="12" cy="12" r="8"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#6A1B9A" fill-opacity="0.9" d="M12.5,7H11v6l5.25,3.15L17,14.92l-4.5-2.67V7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>schedule</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M11.984,13.989c1.661,0,2.991-1.34,2.991-3l0.01-6c0-1.66-1.34-3-3-3c-1.66,0-3,1.34-3,3v6C8.984,12.649,10.324,13.989,11.984,13.989z"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M18.984,10.989c0,3.42-2.721,6.24-6,6.72v3.28h-2v-3.28c-3.279-0.489-6-3.31-6-6.72h1.7c0,2.99,2.521,5.08,5.26,5.1h0.04c2.76,0,5.299-2.1,5.299-5.1H18.984z"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M18.984,10.989c0,3.42-2.721,6.24-6,6.72v3.28h-1.041v-4.9h0.04c2.76,0,5.299-2.1,5.299-5.1H18.984z"/><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/></g><g id="Capa_2"><polygon fill="#9FA8DA" points="2.969,4.281 4.266,2.984 20.984,19.719 19.703,20.984 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>moff</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.3" d="M11,17c0,0.55,0.45,1,1,1s1-0.45,1-1s-0.45-1-1-1S11,16.45,11,17z M18,12c0-0.55-0.45-1-1-1s-1,0.45-1,1s0.45,1,1,1S18,12.55,18,12z M6,12c0,0.55,0.45,1,1,1s1-0.45,1-1s-0.45-1-1-1S6,11.45,6,12z"/><path fill="#43A047" d="M12,13l1.41-1.41l-6.8-6.8v0.02C6.079,5.208,5.596,5.664,5.165,6.165L6.58,7.58L12,13z"/><path fill="#AED581" d="M5,12c0-1.68,0.59-3.22,1.58-4.42L5.165,6.165C3.817,7.733,3,9.765,3,12c0,2.74,1.227,5.19,3.158,6.842l1.417-1.417C6.003,16.142,5,14.19,5,12z"/><path fill="#FFB74D" d="M19.201,17.381l-1.426-1.426C16.516,17.794,14.401,19,12,19c-1.68,0-3.22-0.591-4.425-1.575l-1.417,1.417C7.729,20.185,9.766,21,12,21C14.95,21,17.561,19.574,19.201,17.381z"/><path fill="#FF8A65" d="M21,12c0-4.97-4.029-9-9-9h-1v4h2V5.08c3.391,0.49,6,3.39,6,6.92c0,1.469-0.453,2.83-1.225,3.955l1.426,1.426C20.326,15.879,21,14.021,21,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>av_timer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#8E24AA" d="M11,6c1.38,0,2.63,0.56,3.54,1.46L12,10h6V4l-2.05,2.05C14.68,4.78,12.93,4,11,4c-3.53,0-6.43,2.61-6.92,6H6.1C6.56,7.72,8.58,6,11,6z"/><path fill="#BA68C8" d="M21.496,19.984l-1.487,1.493l-4.869-4.842c-0.02,0.021-0.05,0.03-0.069,0.04c-0.181,0.13-0.391,0.262-0.6,0.381c-1.019,0.603-2.209,0.944-3.469,0.946c-1.93,0.004-3.681-0.773-4.954-2.041l-2.046,2.054l-0.01-6l6-0.011L7.456,14.55c0.912,0.898,2.163,1.456,3.542,1.453c2.42-0.004,4.438-1.728,4.893-4.008l2.02-0.004c-0.128,0.941-0.446,1.822-0.915,2.591c0,0-0.08,0.15-0.239,0.371c-0.04,0.06-0.08,0.12-0.12,0.18L21.496,19.984z"/><path fill="#8E24AA" d="M21.496,19.984l-1.487,1.493l-4.869-4.842c-0.02,0.021-0.05,0.03-0.069,0.04c0.839-0.611,1.397-1.303,1.687-1.723c-0.04,0.06-0.08,0.12-0.12,0.18L21.496,19.984z"/></svg>			</div>
			<div class="icon-set__id">
				<span>find_replace</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#DCE775" d="M18,16v-5.5c0-0.18-0.01-0.35-0.02-0.52C17.97,9.89,17.96,9.8,17.95,9.72c-0.01-0.12-0.03-0.23-0.05-0.35C17.88,9.23,17.85,9.09,17.81,8.95c-0.029-0.12-0.06-0.24-0.1-0.36c-0.03-0.11-0.06-0.21-0.11-0.32c-0.04-0.14-0.1-0.28-0.159-0.41c-0.061-0.13-0.12-0.26-0.19-0.39c-0.05-0.1-0.11-0.21-0.17-0.31c-0.01-0.02-0.03-0.04-0.04-0.06c-0.07-0.12-0.15-0.24-0.24-0.36c-0.38-0.53-0.84-1.01-1.37-1.41c-0.25-0.19-0.52-0.37-0.8-0.52C14.62,4.8,14.6,4.8,14.59,4.79C14.3,4.64,14,4.5,13.69,4.39c-0.07-0.03-0.141-0.05-0.21-0.07c-0.07-0.03-0.15-0.05-0.221-0.07C13.18,4.22,13.09,4.2,13,4.18V3.5C13,2.67,12.33,2,11.5,2S10,2.67,10,3.5v0.68C7.13,4.86,5,7.43,5,10.5V16l-2,2v1h17v-1L18,16z M16,17H7v-6.5C7,8.01,9.01,6,11.5,6S16,8.01,16,10.5V17z"/><path fill="#DCE775" d="M11.5,22c0.141,0,0.27-0.01,0.4-0.04c0.649-0.13,1.189-0.58,1.439-1.18c0.1-0.24,0.16-0.5,0.16-0.78h-4C9.5,21.1,10.4,22,11.5,22z"/><polygon fill="#FDD835" points="20,18 20,19 3,19 3,18 4.96,16.04 7,16.04 7,17 16,17 16,16.04 18.04,16.04 	"/><path fill="#FDD835" d="M13.69,4.39c-0.07-0.01-0.15-0.03-0.24-0.06C11.85,3.83,10.52,4.05,10,4.17V3.5C10,2.67,10.67,2,11.5,2S13,2.67,13,3.5v0.68C13.24,4.24,13.47,4.3,13.69,4.39z"/></g><path fill="none" d="M0,0v24h24V0H0z M7,10.5C7,8.01,9.01,6,11.5,6S16,8.01,16,10.5V17H7V10.5z"/><path fill="none" d="M0,0v24h24V0H0z M7,10.5C7,8.01,9.01,6,11.5,6S16,8.01,16,10.5V17H7V10.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>notifications_none</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#B0BEC5" d="M17.657,7.716l-5.71-5.71h-1v7.59l-4.59-4.59l-1.41,1.41l5.59,5.59l-5.59,5.59l1.41,1.41l4.59-4.59v7.59h1l5.71-5.71l-4.3-4.29L17.657,7.716z M12.947,5.836l1.88,1.88l-1.88,1.88V5.836z M14.827,16.296l-1.88,1.88v-3.76L14.827,16.296z"/></g><g id="Capa_2"><polygon fill="#9FA8DA" points="3.953,5.406 5.406,3.984 19.984,18.578 18.578,20.016 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>bluetooth_disabled</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#0D47A1" points="20,12 12,4 12,8 11.999,8 15.998,12 12,16 12,16 12,20 "/><polygon fill="#1976D2" points="11.999,8 4,8 4,16 12,16 15.998,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>forward</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#EF5350" d="M22,4v12c0,1.1-0.9,2-2,2H6l-4,4l0.01-11.09V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><rect x="11" y="12" fill="#CFD8DC" width="2" height="2"/><path opacity="0.5" fill="#E53935" d="M22,4v6.91H2.01V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><rect x="11" y="6" fill="#CFD8DC" width="2" height="4"/></svg>			</div>
			<div class="icon-set__id">
				<span>sms_failed</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#7986CB" d="M3,5v14c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2H5C3.89,3,3,3.9,3,5z"/><path fill="#3F51B5" d="M15,9c0,1.66-1.34,3-3,3s-3-1.34-3-3s1.34-3,3-3S15,7.34,15,9z"/><path fill="#3F51B5" d="M6,17c0-2,4-3.1,6-3.1s6,1.1,6,3.1v1H6V17z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>account_box</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#CFD8DC" d="M21.999,5.999v12c0,1.1-0.9,2-2,2h-16c-1.1,0-2-0.9-2-2l0.01-12c0-1.1,0.89-2,1.99-2h16C21.099,3.999,21.999,4.899,21.999,5.999z"/><polygon fill="#E53935" points="21.999,5.999 21.978,7.937 11.999,12.999 1.999,7.916 1.999,6.041 11.999,10.999 	"/><circle fill="#C62828" cx="11.981" cy="11.437" r="2.278"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_post_office</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#78909C" d="M20.96,16.536v3.5c0,0.55-0.45,1-1,1c-9.391,0-17-7.609-17-17c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.57,3.57c0.109,0.35,0.029,0.74-0.25,1l-2.2,2.21c0.271,0.52,0.569,1.04,0.92,1.56c0.21,0.32,0.44,0.63,0.681,0.94c0.129,0.159,0.25,0.3,0.369,0.45c0.29,0.34,0.59,0.658,0.9,0.959c0.22,0.211,0.449,0.42,0.689,0.631c0.12,0.109,0.24,0.22,0.37,0.31c0.131,0.101,0.25,0.2,0.381,0.3c0.131,0.11,0.26,0.2,0.399,0.29c0.142,0.101,0.278,0.19,0.42,0.29c0.278,0.2,0.58,0.381,0.88,0.54c0.189,0.11,0.391,0.22,0.58,0.32l2.2-2.2c0.278-0.27,0.67-0.35,1.021-0.24c1.119,0.37,2.319,0.57,3.569,0.57C20.51,15.536,20.96,15.986,20.96,16.536z"/><g id="Capa_2"><path fill="#455A64" d="M20.96,19.716v0.32c0,0.55-0.45,1-1,1c-9.391,0-17-7.609-17-17c0-0.55,0.45-1,1-1h0.689l0.011,0.06c0,0-0.13,3.77,1.92,7.72c0.271,0.52,0.569,1.04,0.92,1.56c0.21,0.32,0.44,0.63,0.681,0.94c0.129,0.159,0.25,0.3,0.369,0.45c0.29,0.34,0.59,0.658,0.9,0.959c0.22,0.211,0.449,0.42,0.689,0.631c0.12,0.1,0.24,0.199,0.37,0.31c0.131,0.101,0.25,0.2,0.381,0.3c0.131,0.103,0.271,0.19,0.399,0.29c0.142,0.101,0.278,0.19,0.42,0.29c0.278,0.182,0.58,0.37,0.88,0.54c0.189,0.11,0.391,0.22,0.58,0.32c2.05,1.069,4.602,1.87,7.762,2.21L20.96,19.716z"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#FFF176" points="12,3 12,13 15,10 21,10 21,3 "/></svg>			</div>
			<div class="icon-set__id">
				<span>perm_phone_msg</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#00ACC1" d="M17,6c-3.311,0-6,2.69-6,6c0,3.311,2.689,6,6,6s6-2.689,6-6C23,8.69,20.311,6,17,6z"/><path fill="#4DD0E1" d="M5,8c-2.21,0-4,1.79-4,4s1.79,4,4,4s4-1.79,4-4S7.21,8,5,8z M5,14c-1.1,0-2-0.9-2-2c0-1.1,0.9-2,2-2s2,0.9,2,2C7,13.1,6.1,14,5,14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>hdr_strong</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><path fill="#CFD8DC" d="M15,5h-2c-1.1,0-2,0.89-2,2v2c0,1.11,0.9,2,2,2h2v2h-4v2h4c1.1,0,2-0.89,2-2V7C17,5.89,16.1,5,15,5z M15,9h-2V7h2V9z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_9</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36">
<g data-iconmelon="Iconmelon:20c70f6ef8ccbfbdbf65f8b190fc10b1">
  <g>
	<circle fill="#40B7AE" cx="13.477" cy="13.695" r="13.477"></circle>
	<path opacity="0.2" d="M30.297,6.936L30.297,6.936L27.953,1.46L16.745,6.256l0.156,0.365l0,0l-0.156-0.365l-5.889,2.52
c-0.363,0.156-0.597,0.486-0.649,0.852c-0.122,0.187-0.185,0.404-0.181,0.625l-4.476,1.916c-0.557,0.238-0.84,0.825-0.631,1.312
c0.207,0.486,0.827,0.687,1.384,0.449l4.121-1.764l0.224,0.522L5.756,14.78c-0.557,0.238-0.839,0.825-0.631,1.312
c0.208,0.486,0.827,0.687,1.384,0.449l4.893-2.094l0.182,0.425l-4.139,1.772c-0.557,0.238-0.84,0.825-0.632,1.312
c0.209,0.486,0.828,0.687,1.385,0.449l4.14-1.772l0.224,0.522l-3.436,1.471c-0.557,0.238-0.839,0.824-0.631,1.311
s0.827,0.688,1.384,0.449l3.436-1.471l2.452-1.049l5.026-2.151l-2.427-5.672l0,0l2.427,5.672L32,10.917L30.297,6.936z"></path>
	<g>
	  <rect x="18.067" y="2.951" transform="matrix(0.9194 -0.3934 0.3934 0.9194 -1.236 10.1589)" fill="#E9437F" width="12.191" height="10.287"></rect>
	  <path fill="#FFFFFF" d="M11.254,9.216l-0.026-0.061l-5.888,2.52C4.783,11.913,4.5,12.5,4.709,12.986
c0.208,0.486,0.827,0.687,1.384,0.449l4.121-1.764l0.224,0.522l-4.893,2.094c-0.557,0.238-0.839,0.825-0.631,1.312
c0.208,0.486,0.827,0.687,1.385,0.449l4.892-2.094l0.183,0.425l-4.141,1.771c-0.556,0.238-0.838,0.825-0.631,1.312
c0.208,0.485,0.828,0.687,1.385,0.448l4.14-1.771l0.224,0.522l-3.436,1.47c-0.557,0.238-0.839,0.825-0.631,1.312
s0.827,0.688,1.384,0.449l3.436-1.471l2.453-1.05l5.026-2.151l-3.617-8.449L11.254,9.216z"></path>
	  <g opacity="0.3">
		<path d="M16.689,6.128l1.465,3.42c-0.486-1.135,0.04-2.449,1.175-2.934c1.136-0.486,2.449,0.041,2.935,1.175
	s-0.041,2.449-1.176,2.934l9.153-3.917l-2.343-5.475L16.689,6.128z"></path>
		<circle cx="20.209" cy="8.669" r="2.235"></circle>
	  </g>
	  <g>
		<path fill="#E9437F" d="M16.534,5.764l1.464,3.42c-0.485-1.135,0.041-2.448,1.175-2.934c1.136-0.486,2.449,0.041,2.935,1.175
	c0.486,1.135-0.04,2.449-1.176,2.934l9.154-3.917l-2.344-5.476L16.534,5.764z"></path>
		<circle fill="#E9437F" cx="20.053" cy="8.305" r="2.235"></circle>
	  </g>
	  <circle fill="#FFFFFF" cx="19.122" cy="6.937" r="0.564"></circle>
	  <path opacity="0.1" d="M10.518,8.697c-0.58,0.249-0.858,0.901-0.62,1.458s0.902,0.806,1.482,0.558l2.357-1.009
c0.278,0.997,1.012,1.85,2.078,2.251c1.094,0.411,2.283,0.251,3.229-0.326L17.63,8.325l-0.104-0.243l-0.862-2.015L10.518,8.697z"></path>
	  <path fill="#FFFFFF" d="M10.646,8.284c-0.558,0.238-0.815,0.883-0.577,1.439c0.238,0.557,0.883,0.815,1.439,0.577l2.258-0.967
c0.283,0.995,1.004,1.854,2.038,2.268c1.062,0.425,2.207,0.284,3.109-0.275L17.5,8.022l-0.104-0.243l-0.862-2.015L10.646,8.284z"></path>
	</g>
	<path fill="#3C3C3B" d="M16.693,19.562c0,0-0.451-0.598-0.282-0.986c0.172-0.4,0.755-0.2,0.605,0.238
c0.228-0.406,0.764-0.123,0.591,0.278C17.438,19.482,16.693,19.562,16.693,19.562z"></path>
	<g>
	  <path fill="#3C3C3B" d="M15.378,18.688l-0.024-0.016c-0.087-0.051-0.191-0.164-0.23-0.257l-0.053-0.122
c-0.086-0.199-0.351-0.277-0.59-0.174c-0.242,0.104-0.365,0.349-0.28,0.547c0,0.002,0.055,0.123,0.055,0.123
c0.038,0.093,0.052,0.248,0.027,0.346l-0.007,0.029c-0.022,0.097,0.033,0.145,0.125,0.104l0.968-0.418
C15.462,18.811,15.465,18.736,15.378,18.688z"></path>
	  <path fill="#3C3C3B" d="M14.979,19.275c0.108-0.047,0.171-0.148,0.152-0.236l-0.429,0.186
C14.754,19.299,14.871,19.322,14.979,19.275z"></path>
	</g>
	<path fill="#3C3C3B" d="M16.409,15.778h-0.093c-0.005,0-0.011-0.001-0.014-0.001c-0.007-0.016-0.012-0.029-0.019-0.044
c0.002-0.004,0.006-0.008,0.009-0.012l0.065-0.065c0.031-0.031,0.031-0.082,0-0.113l-0.112-0.112c-0.03-0.03-0.082-0.03-0.111,0
l-0.065,0.065c-0.003,0.003-0.008,0.007-0.012,0.009c-0.014-0.007-0.029-0.012-0.045-0.017c0-0.004-0.001-0.009-0.001-0.016v-0.091
c0-0.044-0.034-0.081-0.078-0.081h-0.158c-0.045,0-0.08,0.036-0.08,0.081v0.091c0,0.006,0,0.011-0.001,0.016
c-0.016,0.006-0.03,0.012-0.044,0.017c-0.002-0.001-0.008-0.005-0.011-0.009l-0.065-0.065c-0.031-0.031-0.082-0.031-0.111,0
l-0.113,0.112c-0.031,0.031-0.031,0.082,0,0.113l0.066,0.065c0.004,0.003,0.006,0.007,0.01,0.012
c-0.007,0.015-0.013,0.028-0.019,0.044c-0.005,0-0.01,0.001-0.015,0.001h-0.092c-0.043,0-0.08,0.036-0.08,0.079v0.159
c0,0.043,0.035,0.08,0.08,0.08h0.092c0.005,0,0.01,0,0.015,0.001c0.006,0.015,0.012,0.029,0.019,0.044
c-0.003,0.003-0.006,0.007-0.01,0.01l-0.066,0.066c-0.031,0.029-0.031,0.081,0,0.112l0.113,0.112c0.031,0.031,0.08,0.031,0.111,0
l0.065-0.065c0.003-0.004,0.009-0.007,0.011-0.009c0.015,0.007,0.03,0.012,0.044,0.017c0.001,0.005,0.001,0.01,0.001,0.015v0.093
c0,0.043,0.035,0.08,0.08,0.08h0.158c0.044,0,0.078-0.036,0.078-0.08V16.4c0-0.005,0.001-0.01,0.001-0.015
c0.016-0.005,0.031-0.011,0.045-0.017c0.004,0.001,0.007,0.005,0.012,0.009l0.065,0.065c0.03,0.031,0.081,0.031,0.111,0
l0.112-0.112c0.031-0.031,0.031-0.083,0-0.112l-0.065-0.066c-0.003-0.003-0.007-0.007-0.009-0.01
c0.007-0.016,0.012-0.029,0.019-0.044c0.003-0.001,0.009-0.001,0.014-0.001h0.093c0.044,0,0.08-0.037,0.08-0.08v-0.159
C16.489,15.814,16.453,15.778,16.409,15.778z M15.854,16.176c-0.131,0-0.236-0.106-0.236-0.238c0-0.131,0.105-0.238,0.236-0.238
c0.133,0,0.239,0.107,0.239,0.238C16.093,16.07,15.986,16.176,15.854,16.176z"></path>
	<path fill="#3C3C3B" d="M14.457,20.433c-0.004-0.001-0.008-0.002-0.011-0.003s-0.009-0.001-0.013-0.002
c-0.333-0.088-0.678,0.115-0.764,0.447s0.346,1.201,0.346,1.201s0.804-0.547,0.891-0.878
C14.991,20.867,14.793,20.521,14.457,20.433z M14.205,21.355c-0.173-0.043-0.274-0.218-0.229-0.389
c0.044-0.172,0.218-0.273,0.39-0.229c0.17,0.044,0.273,0.219,0.229,0.389C14.55,21.299,14.375,21.4,14.205,21.355z"></path>
	<path fill="#3C3C3B" d="M15.449,23.479c0.101-0.002,0.153,0.036,0.205,0.082c0.044-0.003,0.1-0.027,0.134-0.045
c0.01-0.006,0.021-0.012,0.031-0.018c-0.018,0.052-0.044,0.093-0.083,0.123c-0.009,0.006-0.018,0.016-0.028,0.021l0,0
c0.055,0,0.102-0.026,0.147-0.04l0,0c-0.023,0.037-0.055,0.075-0.088,0.102c-0.015,0.012-0.027,0.021-0.041,0.033
c0.001,0.061-0.001,0.117-0.013,0.168c-0.065,0.295-0.237,0.494-0.509,0.578c-0.099,0.031-0.256,0.045-0.368,0.018
c-0.056-0.016-0.106-0.029-0.153-0.051c-0.026-0.01-0.051-0.023-0.073-0.039c-0.007-0.004-0.015-0.008-0.022-0.014
c0.025,0,0.055,0.008,0.083,0.004c0.025-0.004,0.051-0.004,0.073-0.008c0.06-0.014,0.112-0.031,0.156-0.057
c0.021-0.014,0.055-0.029,0.07-0.047c-0.029,0-0.056-0.006-0.077-0.014c-0.083-0.031-0.133-0.086-0.165-0.168
c0.025,0.002,0.1,0.01,0.117-0.006c-0.033,0-0.064-0.02-0.086-0.033c-0.067-0.043-0.123-0.115-0.122-0.225
c0.009,0.004,0.018,0.008,0.027,0.012c0.015,0.008,0.032,0.012,0.053,0.016c0.009,0.002,0.026,0.008,0.035,0.004c0,0,0,0-0.002,0
c-0.013-0.016-0.033-0.025-0.047-0.043c-0.043-0.055-0.084-0.139-0.058-0.236c0.006-0.025,0.017-0.049,0.027-0.068l0,0
c0.006,0.011,0.018,0.02,0.025,0.027c0.021,0.027,0.05,0.053,0.077,0.074c0.094,0.074,0.18,0.121,0.316,0.154
c0.035,0.01,0.076,0.016,0.117,0.016c-0.012-0.033-0.008-0.09,0.002-0.123c0.022-0.084,0.072-0.143,0.146-0.176
c0.018-0.008,0.036-0.014,0.057-0.018C15.428,23.481,15.438,23.48,15.449,23.479z"></path>
	<path fill="#3C3C3B" d="M15.333,13.115c-0.349,0-0.634,0.229-0.634,0.514c0,0.153,0.082,0.291,0.213,0.385
c0.013,0.038,0.007,0.079-0.025,0.133c-0.033,0.062-0.099,0.113-0.188,0.153c0.225-0.017,0.418-0.074,0.529-0.164
c0.033,0.004,0.068,0.008,0.104,0.008c0.352,0,0.634-0.23,0.634-0.515S15.685,13.115,15.333,13.115"></path>
	<path fill="#3C3C3B" d="M14.567,27.014h-0.092c-0.006,0-0.011,0-0.015,0c-0.006-0.018-0.012-0.031-0.019-0.045
c0.002-0.004,0.006-0.008,0.009-0.012l0.065-0.064c0.031-0.031,0.031-0.082,0-0.113l-0.111-0.111c-0.031-0.031-0.083-0.031-0.112,0
l-0.065,0.064c-0.003,0.004-0.008,0.008-0.011,0.01c-0.015-0.008-0.03-0.014-0.046-0.018c0-0.004,0-0.01,0-0.016v-0.092
c0-0.045-0.035-0.08-0.079-0.08h-0.158c-0.045,0-0.08,0.035-0.08,0.08v0.092c0,0.006,0,0.012-0.001,0.016
c-0.016,0.006-0.03,0.012-0.044,0.018c-0.003-0.002-0.008-0.006-0.011-0.01l-0.065-0.064c-0.031-0.031-0.081-0.031-0.111,0
l-0.113,0.111c-0.031,0.031-0.031,0.082,0,0.113l0.066,0.064c0.004,0.004,0.007,0.008,0.01,0.012
c-0.007,0.014-0.014,0.027-0.019,0.045c-0.004,0-0.009,0-0.015,0h-0.092c-0.043,0-0.079,0.035-0.079,0.08v0.158
c0,0.043,0.034,0.08,0.079,0.08h0.092c0.006,0,0.011,0,0.015,0c0.005,0.016,0.012,0.029,0.019,0.045
c-0.003,0.004-0.006,0.006-0.01,0.012l-0.066,0.064c-0.031,0.029-0.031,0.082,0,0.113l0.113,0.111c0.031,0.031,0.08,0.031,0.111,0
l0.065-0.064c0.003-0.006,0.008-0.008,0.011-0.01c0.015,0.008,0.03,0.012,0.044,0.018c0.001,0.004,0.001,0.01,0.001,0.014v0.094
c0,0.043,0.035,0.078,0.08,0.078h0.158c0.044,0,0.079-0.035,0.079-0.078v-0.094c0-0.004,0-0.01,0-0.014
c0.016-0.006,0.031-0.012,0.046-0.018c0.003,0.002,0.006,0.004,0.011,0.01l0.065,0.064c0.031,0.031,0.081,0.031,0.112,0
l0.111-0.111c0.031-0.031,0.031-0.084,0-0.113l-0.065-0.064c-0.003-0.006-0.007-0.008-0.009-0.012
c0.007-0.016,0.013-0.029,0.019-0.045c0.004,0,0.009,0,0.015,0h0.092c0.044,0,0.08-0.037,0.08-0.08v-0.158
C14.647,27.049,14.611,27.014,14.567,27.014z M14.012,27.412c-0.131,0-0.236-0.107-0.236-0.24c0-0.131,0.105-0.236,0.236-0.236
c0.133,0,0.238,0.105,0.238,0.236C14.25,27.305,14.145,27.412,14.012,27.412z"></path>
	<path fill="#3C3C3B" d="M28.309,30.783c-0.348,0-0.654,0.178-0.833,0.447c-0.178-0.568-0.626-1.02-1.194-1.201
c-0.043-0.85-0.743-1.525-1.603-1.525c-0.383,0-0.733,0.135-1.009,0.357c-0.106-0.049-0.225-0.076-0.349-0.076
c-0.125,0-0.243,0.029-0.35,0.078c-0.153-0.305-0.468-0.514-0.832-0.514c-0.507,0-0.917,0.402-0.931,0.906
c-0.036,0.012-0.07,0.027-0.105,0.041c-0.035-0.01-0.071-0.016-0.109-0.016c-0.091,0-0.175,0.031-0.243,0.08
c-0.188-0.107-0.407-0.17-0.641-0.17c-0.563,0-1.042,0.361-1.22,0.863c-0.17-0.053-0.35-0.084-0.537-0.084
c-0.682,0-1.275,0.379-1.583,0.936c-0.073-0.02-0.148-0.029-0.227-0.029c-0.5,0-0.905,0.404-0.905,0.904h13.669
C29.308,31.23,28.86,30.783,28.309,30.783z"></path>
	<path fill="#3C3C3B" d="M12.67,30.783c-0.348,0-0.654,0.178-0.833,0.447c-0.233-0.748-0.932-1.291-1.757-1.291
c-0.269,0-0.521,0.061-0.751,0.164c0.007-0.045,0.012-0.09,0.012-0.137c0-0.381-0.243-0.705-0.583-0.824
c-0.15-0.762-0.822-1.336-1.627-1.336c-0.572,0-1.076,0.291-1.375,0.73c-0.058-0.02-0.119-0.033-0.184-0.033
c-0.142,0-0.27,0.053-0.37,0.137c-0.243-0.182-0.543-0.291-0.87-0.291c-0.801,0-1.451,0.648-1.451,1.451
c0,0.061,0.006,0.121,0.013,0.18c-0.059-0.004-0.118-0.01-0.179-0.01c-0.682,0-1.275,0.379-1.584,0.936
c-0.072-0.02-0.147-0.029-0.226-0.029c-0.5,0-0.905,0.404-0.905,0.904h13.669C13.669,31.23,13.222,30.783,12.67,30.783z"></path>
  </g>
</g>
</svg>			</div>
			<div class="icon-set__id">
				<span>invest</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M21,3H3C1.9,3,1,3.9,1,5v14c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2V5C23,3.9,22.1,3,21,3z"/><rect x="3" y="4.99" fill="#C5CAE9" width="18" height="14.02"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#37474F" points="12.01,5.5 10,8 14,8 "/><polygon fill="#546E7A" points="18,10 18,14 20.5,12.01 "/><polygon fill="#546E7A" points="6,10 3.5,12.01 6,14 "/><polygon fill="#37474F" points="14,16 10,16 12.01,18.5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_overscan</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M16,1H8C6.34,1,5,2.34,5,4v16c0,1.66,1.34,3,3,3h8c1.66,0,3-1.34,3-3V4C19,2.34,17.66,1,16,1z"/><rect x="10" y="20" fill="#FAFAFA" width="4" height="1"/><rect x="6.75" y="4" fill="#BBDEFB" width="10.5" height="14"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>phone_android</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#43A047" cx="12" cy="12" r="4"/><path fill="#66BB6A" d="M20.94,11C20.48,6.83,17.17,3.52,13,3.06V1h-2v2.06C6.83,3.52,3.52,6.83,3.06,11H1v2h2.06c0.46,4.17,3.77,7.48,7.94,7.94V23h2v-2.06c4.17-0.46,7.48-3.771,7.94-7.94H23v-2H20.94z M12,19c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,19,12,19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>gps_fixed</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#42A5F5" d="M21,5v14c0,1.1-0.9,2-2,2H5c-1.1,0-2-0.9-2-2L3.01,5C3.01,3.9,3.9,3,5,3h14C20.1,3,21,3.9,21,5z"/><path opacity="0.5" fill="#1E88E5" d="M21,5v14c0,1.1-0.9,2-2,2h-7.04V3H19C20.1,3,21,3.9,21,5z"/><polygon fill="#FFFFFF" points="18,14 14,14 14,18 10,18 10,14 6,14 6,10 10,10 10,6 14,6 14,10 18,10 "/></svg>			</div>
			<div class="icon-set__id">
				<span>local_hospital</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#FFB74D" d="M13.51,0.68L13.5,0.67c0,0,0.74,2.65,0.74,4.8c0,2.06-1.351,3.73-3.41,3.73C8.76,9.2,7.2,7.53,7.2,5.47l0.03-0.36C5.21,7.51,4,10.62,4,14c0,2.41,1.06,4.57,2.75,6.03C8.15,21.26,9.99,22,12,22c4.42,0,8-3.58,8-8C20,8.61,17.41,3.81,13.51,0.68z M11.71,19c-0.9,0-1.7-0.35-2.29-0.93c-0.57-0.561-0.93-1.34-0.93-2.21c0-1.62,1.05-2.761,2.81-3.12c1.771-0.36,3.601-1.21,4.62-2.58c0.39,1.29,0.59,2.649,0.59,4.04C16.51,16.85,14.36,19,11.71,19z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#E53935" d="M20,14c0,4.42-3.58,8-8,8c-2.01,0-3.881-0.786-5.281-2.016L6.75,20.02c0,0,1.328-0.301,2.67-1.949c0.59,0.58,1.39,0.93,2.29,0.93c2.65,0,4.8-2.15,4.8-4.8c0-1.391-0.2-2.75-0.59-4.04c0.85-4.3-2.24-9.21-2.41-9.48C17.41,3.81,20,8.61,20,14z"/></g><g id="Capa_3"></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>whatshot</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#43A047" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M12,20c-4.42,0-8-3.58-8-8c0-1.85,0.63-3.55,1.69-4.9l11.21,11.21C15.55,19.37,13.85,20,12,20z M18.311,16.9L7.1,5.69C8.45,4.63,10.15,4,12,4c4.42,0,8,3.58,8,8C20,13.85,19.37,15.55,18.311,16.9z"/></svg>			</div>
			<div class="icon-set__id">
				<span>dnd_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="10" fill="#AB47BC" width="3" height="7"/><rect x="10" y="10" fill="#42A5F5" width="3" height="7"/><rect x="2" y="19" fill="#7B1FA2" width="19" height="3"/><rect x="16" y="10" fill="#FF7043" width="3" height="7"/><polygon fill="#7B1FA2" points="11.5,1 2,6 2,8 21,8 21,6 "/></svg>			</div>
			<div class="icon-set__id">
				<span>account_balance</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0.01,0h24v24h-24V0z"/><g><path fill="#42A5F5" d="M21.006,5.019v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C20.105,3.019,21.006,3.919,21.006,5.019z"/><path opacity="0.5" fill="#1E88E5" enable-background="new    " d="M21.006,5.019v14c0,1.1-0.9,2-2,2h-7.009v-18h7.009C20.105,3.019,21.006,3.919,21.006,5.019z"/></g><path fill="#CFD8DC" d="M13.51,12c0.83,0,1.5,0.67,1.5,1.5V15c0,1.11-0.899,2-2,2h-4v-2h4v-2h-2v-2h2V9h-4V7h4c1.101,0,2,0.89,2,2v1.5C15.01,11.33,14.34,12,13.51,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>looks_3</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#37474F" d="M21.959,5.593v12.824L18,14.5l-0.031,3.359C17.938,19.078,17.024,19,16.415,19H3.109C2.499,19,2,18.476,2,17.835V6.176C2,5.535,2.499,5.01,3.109,5.01h13.306c0.609,0,1.108,0.525,1.108,1.166v4.08l0.021-0.023L21.959,5.593z"/><g id="Capa_2"><polygon fill="#263238" points="21.959,5.593 21.959,18.417 18,14.5 17.984,10.256 18.004,10.233 		"/></g><g id="Capa_3"><path fill="#455A64" d="M17.984,10.25v1.688L2,11.97V6.176C2,5.535,2.499,5.01,3.109,5.01h13.306c1.351,0.005,1.507,0.333,1.569,1.146V10.25L17.984,10.25z"/><polygon fill="#37474F" points="21.959,5.593 21.959,11.923 17.984,11.938 17.984,10.25 18,9.5 		"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#CFD8DC" points="13,15.5 13,13 7,13 7,15.5 3.5,12 7,8.5 7,11 13,11 13,8.5 16.5,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>switch_video</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#81C784" d="M12,1l-0.04,0.02L3,5v6c0,5.54,3.82,10.71,8.96,11.99c0.01,0,0.03,0.01,0.04,0.01c5.16-1.26,9-6.45,9-12V5L12,1z M10,17l-4-4l1.41-1.41L10,14.17l6.59-6.59L18,9L10,17z"/><path fill="#66BB6A" d="M21,5v6c0,5.55-3.84,10.74-9,12c-0.01,0-0.03-0.01-0.04-0.01v-7.95L18,9l-1.41-1.42l-4.63,4.63V1.02L12,1L21,5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>verified_user</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#00ACC1" d="M6,11H4V7H2v10h2v-4h2v4h2V7H6V11z M13,7H9v10h4c1.1,0,2-0.9,2-2V9C15,7.9,14.1,7,13,7z M13,15h-2V9h2V15zM22,11V9c0-1.1-0.9-2-2-2h-4v10h2v-4h1l1,4h2l-1.189-4.17C21.51,12.52,22,11.82,22,11z M20,11h-2V9h2V11z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>hdr_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M18,2H6C4.9,2,4,2.9,4,4v16c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V4C20,2.9,19.1,2,18,2z"/><polygon fill="#E57373" points="10.91,4 10.91,12 8.41,10.5 5.91,12 5.91,4 "/><polygon opacity="0.4" fill="#EF5350" enable-background="new    " points="10.91,4 10.91,12 8.41,10.5 8.41,4 "/></svg>			</div>
			<div class="icon-set__id">
				<span>book</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M20,18v1H3v-1l2-2v-5.5c0-3.07,2.13-5.64,5-6.32V3.5C10,2.67,10.67,2,11.5,2S13,2.67,13,3.5v0.68c0.24,0.06,0.47,0.12,0.69,0.21C16.21,5.29,18,7.68,18,10.5V16L20,18z"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M11.5,22c0.141,0,0.27-0.01,0.4-0.04c0.649-0.13,1.189-0.58,1.439-1.18c0.1-0.24,0.16-0.5,0.16-0.78h-4C9.5,21.1,10.4,22,11.5,22z"/><polygon opacity="0.6" fill="#90A4AE" enable-background="new    " points="20,18 20,19 3,19 3,18 4.96,16.04 18.04,16.04 	"/><path opacity="0.6" fill="#90A4AE" enable-background="new    " d="M13.69,4.39c-0.07-0.01-0.15-0.03-0.24-0.06C11.85,3.83,10.52,4.05,10,4.17V3.5C10,2.67,10.67,2,11.5,2S13,2.67,13,3.5v0.68C13.24,4.24,13.47,4.3,13.69,4.39z"/><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><polygon fill="#9FA8DA" points="2.984,4.266 4.266,2.984 20.984,19.734 19.719,20.984 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>notifications_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="5" y="8.5" fill="#00E676" width="14" height="6"/><rect x="11" y="0.55" opacity="0.6" fill="#00E676" width="2" height="2.95"/><polygon opacity="0.6" fill="#00E676" points="19.04,3.05 17.25,4.84 18.66,6.25 20.46,4.46 "/><rect x="11" y="19.5" opacity="0.6" fill="#00E676" width="2" height="2.95"/><polygon opacity="0.6" fill="#00E676" points="20.45,18.54 18.65,16.75 17.24,18.16 19.029,19.96 "/><rect x="3.884" y="3.653" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 5.5035 11.5794)" opacity="0.6" fill="#00E676" width="2.531" height="1.994"/><polygon opacity="0.6" fill="#00E676" points="4.96,19.95 6.75,18.15 5.34,16.74 3.55,18.53 "/></svg>			</div>
			<div class="icon-set__id">
				<span>wb_irradescent</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M20,18c1.1,0,1.99-0.9,1.99-2L22,6c0-1.1-0.9-2-2-2H4C2.9,4,2,4.9,2,6v10c0,1.1,0.9,2,2,2H0v2h24v-2H20z"/><rect x="4" y="6" fill="#9FA8DA" width="16" height="10"/><rect fill="none" width="24" height="20"/><path fill="none" d="M-591.667-1721h1400v3600h-1400V-1721z M26.333-1h24v24h-24V-1z"/><rect y="18" fill="#37474F" width="24" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>computer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="15.713" y="4.878" transform="matrix(-0.7076 0.7066 -0.7066 -0.7076 33.4439 1.2191)" fill="#C5CAE9" width="1.513" height="5.303"/><path fill="#E57373" d="M20.71,7.04c0.39-0.39,0.39-1.02,0-1.41l-2.34-2.34c-0.39-0.39-1.021-0.39-1.41,0l-1.83,1.83l3.75,3.75L20.71,7.04z"/><path fill="none" d="M0,0h24v24H0V0z"/><g><polygon fill="#7986CB" points="3,21 6.75,21 3,17.25 	"/><rect x="2.583" y="10.943" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 27.3744 15.8505)" fill="#3F51B5" width="15.642" height="5.302"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>create</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B3E5FC" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path fill="#81D4FA" d="M22,4v12c0,1.1-0.9,2-2,2h-6.03V2H20C21.1,2,22,2.9,22,4z"/><polygon fill="#66BB6A" points="11,12 13.029,14.71 16,11 20,16 8,16 "/><path fill="#81D4FA" d="M2,6v14c0,1.1,0.9,2,2,2h14v-2H4V6H2z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#00838F" d="M18,20v2H4c-1.1,0-2-0.9-2-2V6h2v14H18z"/><path fill="#00838F" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path fill="#0097A7" d="M14,2v16H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2H14z"/><polygon fill="#FFFFFF" points="19,11 15,11 15,15 13,15 13,11 9,11 9,9 13,9 13,5 15,5 15,9 19,9 "/><path fill="#00838F" d="M18,22H4c-1.1,0-2-0.9-2-2V6h0.93l0.01,0.02l0.01,14.169c0,0,0.32,0.779,1.17,0.779c0.86,0,13.86,0,13.86,0L18,22z"/></svg>			</div>
			<div class="icon-set__id">
				<span>queue</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="17" fill="#BA68C8" width="10" height="2"/><rect x="4" y="9" fill="#4DB6AC" width="16" height="2"/><rect x="4" y="13" fill="#DCE775" width="16" height="2"/><rect x="4" y="5" fill="#4FC3F7" width="16" height="2"/><path fill="none" d="M0,0v24h24V0H0z M22.49,19.42H13.97V19H14v-2h-0.03v-2H20v-2h-6.03v-2H20V9h-6.03V7H20V5h-6.03V3.28h8.521V19.42z"/><rect x="13.97" y="17" opacity="0.2" fill="#546E7A" width="0.03" height="2"/><rect x="13.97" y="5" opacity="0.2" fill="#546E7A" width="6.03" height="2"/><rect x="13.97" y="9" opacity="0.2" fill="#546E7A" width="6.03" height="2"/><rect x="13.97" y="13" opacity="0.2" fill="#546E7A" width="6.03" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>subject</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path fill="#546E7A" d="M16.05,16.29l2.86-3.07c0.38-0.39,0.72-0.79,1.04-1.18c0.319-0.39,0.59-0.78,0.819-1.17c0.23-0.39,0.41-0.78,0.541-1.17C21.439,9.31,21.5,8.91,21.5,8.52c0-0.53-0.09-1.02-0.27-1.46c-0.181-0.44-0.44-0.81-0.78-1.11c-0.341-0.31-0.771-0.54-1.261-0.71C18.68,5.08,18.109,5,17.471,5c-0.691,0-1.311,0.11-1.851,0.32c-0.54,0.21-1,0.51-1.36,0.88c-0.369,0.37-0.65,0.8-0.84,1.3c-0.18,0.47-0.27,0.97-0.279,1.5h2.139c0.011-0.31,0.051-0.6,0.131-0.87c0.09-0.29,0.23-0.54,0.4-0.75c0.18-0.21,0.41-0.37,0.68-0.49c0.27-0.12,0.6-0.18,0.96-0.18c0.31,0,0.579,0.05,0.81,0.15s0.43,0.25,0.59,0.43c0.16,0.18,0.28,0.4,0.371,0.65C19.3,8.19,19.35,8.46,19.35,8.75c0,0.22-0.029,0.43-0.08,0.65c-0.06,0.22-0.149,0.45-0.289,0.7c-0.141,0.25-0.32,0.53-0.561,0.83c-0.23,0.3-0.52,0.65-0.88,1.03l-4.17,4.55V18H22v-1.71H16.05z"/><polygon fill="#66BB6A" points="8,7 6,7 6,11 2,11 2,13 6,13 6,17 8,17 8,13 12,13 12,11 8,11 "/></svg>			</div>
			<div class="icon-set__id">
				<span>exposure_plus_2</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><polygon fill="#CFD8DC" points="13,15 17,7 17,5 11,5 11,7 15,7 11,15 "/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_7</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#EF5350" d="M18.012,2.784v4.4c-0.091,0.46-0.498,0.8-0.978,0.8H5.075c-0.548,0-0.997-0.45-0.997-1v-4c0-0.55,0.449-1,0.997-1h11.959C17.514,1.984,17.921,2.324,18.012,2.784z"/><polygon fill="#78909C" points="21,4 21,12 13,12 13,14.061 12.98,14.061 12.98,10 19,10 19,6 18,6 18,4 	"/><path fill="#FDD835" d="M12.98,10v11.2c-0.09,0.46-0.5,0.8-0.98,0.8h-2c-0.55,0-1-0.45-1-1V10H12.98z"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_paint</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFC107" d="M19,3H4.99c-1.1,0-1.98,0.9-1.98,2L3,19c0,1.1,0.89,2,1.99,2H19c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z"/><path fill="#FF9800" d="M19,15h-4c0,1.66-1.34,3-3,3s-3-1.34-3-3H4.99V5H19V15z"/><polygon fill="#F5F5F5" points="16,10 14,10 14,7 10,7 10,10 8,10 12,14 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>inbox</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#66BB6A" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/><path opacity="0.7" fill="#4CAF50" enable-background="new    " d="M12,2v2c4.41,0,8,3.59,8,8s-3.59,8-8,8v2c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><polygon fill="#4CAF50" enable-background="new    " points="13,7 11,7 11,11 7,11 7,13 11,13 11,17 13,17 13,13 17,13 17,11 13,11"/></svg>			</div>
			<div class="icon-set__id">
				<span>add_circle_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#A1887F" d="M12.429,2.628c-0.367-0.369-0.861-0.582-1.413-0.583l-7-0.023C3.45,2.021,2.94,2.26,2.579,2.633C2.231,2.993,2.01,3.479,2.008,4.017l-0.008,7c-0.001,0.552,0.216,1.054,0.582,1.423l8.981,9.023c0.352,0.354,0.853,0.575,1.405,0.577c0.544,0.008,1.048-0.209,1.409-0.582l7.021-6.978c0.369-0.367,0.59-0.869,0.584-1.414c0.011-0.545-0.223-1.062-0.582-1.423L12.429,2.628z M7.01,5.532C7.008,5.971,6.831,6.344,6.54,6.619C6.27,6.873,5.91,7.035,5.506,7.027C4.671,7.024,4.002,6.35,4.011,5.523C4.006,5.12,4.162,4.766,4.425,4.492c0.269-0.282,0.652-0.465,1.09-0.463C6.343,4.024,7.012,4.698,7.01,5.532z"/><path opacity="0.3" fill="#8D6E63" enable-background="new    " d="M21.4,11.644c0.359,0.363,0.593,0.879,0.582,1.422c0.006,0.545-0.215,1.048-0.584,1.414l-3.545,3.523L6.54,6.619c0.291-0.275,0.469-0.648,0.47-1.087c0.002-0.834-0.667-1.508-1.494-1.503C5.078,4.027,4.695,4.21,4.426,4.492L2.579,2.633C2.94,2.26,3.45,2.021,4.016,2.022l7,0.023c0.551,0.001,1.045,0.215,1.413,0.583L21.4,11.644z"/></g><path fill="#EC407A" d="M17.27,15.27L13,19.54L8.73,15.27C8.28,14.811,8,14.189,8,13.5c0-1.38,1.12-2.5,2.5-2.5c0.69,0,1.32,0.28,1.77,0.74L13,12.46l0.73-0.73C14.18,11.28,14.811,11,15.5,11c1.38,0,2.5,1.12,2.5,2.5C18,14.189,17.721,14.82,17.27,15.27z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>loyalty</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#0288D1" d="M20,13H4c-0.55,0-1,0.45-1,1v6c0,0.55,0.45,1,1,1h16c0.55,0,1-0.45,1-1v-6C21,13.45,20.55,13,20,13z"/><path fill="#80DEEA" d="M7,19c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2S8.1,19,7,19z"/><path fill="#0288D1" d="M20,3H4C3.45,3,3,3.45,3,4v6c0,0.55,0.45,1,1,1h16c0.55,0,1-0.45,1-1V4C21,3.45,20.55,3,20,3z"/><path fill="#80DEEA" d="M7,9C5.9,9,5,8.1,5,7s0.9-2,2-2s2,0.9,2,2S8.1,9,7,9z"/></svg>			</div>
			<div class="icon-set__id">
				<span>dns</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon opacity="0.8" fill="#0288D1" points="16.17,11 4,11 4,13 16.168,13 18.171,11.998 "/><polygon fill="#0288D1" points="12,4 10.59,5.41 17.171,11.998 16.168,13 16.17,13 10.59,18.59 12,20 20,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>arrow_forward</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="7" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#7E57C2" width="2" height="2"/><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="7" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="15" fill="#7E57C2" width="2" height="2"/><rect x="7" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><rect x="11" y="3" fill="#512DA8" width="2" height="18"/><rect x="19" y="19" fill="#7E57C2" width="2" height="2"/><rect x="19" y="11" fill="#7E57C2" width="2" height="2"/><rect x="19" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><rect x="15" y="3" fill="#7E57C2" width="2" height="2"/><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><rect x="15" y="11" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_vertical</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4E342E" d="M21,5.02v14c0,1.101-0.9,2-2,2H5c-1.1,0-2-0.899-2-2v-14c0-1.1,0.9-2,2-2h3.73v0.01h3.17c0.03-0.01,0.07-0.01,0.1-0.01s0.07,0,0.1,0.01h3.419V3.02H19C20.1,3.02,21,3.92,21,5.02z"/><path fill="#D7CCC8" d="M19.709,6.553c0-1.354-1.1-2.454-2.455-2.454H6.79c-1.356,0-2.455,1.099-2.455,2.454V17.23c0,1.354,1.099,2.453,2.455,2.453h10.464c1.355,0,2.455-1.1,2.455-2.453V6.553z"/><path fill="#F1F8E9" d="M13,4.02c0,0.03,0,0.05-0.01,0.08C12.95,4.61,12.52,5.02,12,5.02c-0.52,0-0.95-0.41-0.99-0.92C11,4.07,11,4.05,11,4.02c0-0.52,0.4-0.94,0.9-0.99c0.03-0.01,0.07-0.01,0.1-0.01s0.07,0,0.1,0.01C12.6,3.08,13,3.5,13,4.02z"/></g><polygon fill="#FF7043" points="16,15 12,15 12,18 7,13 12,8 12,11 16,11 "/><path fill="#FFA000" d="M7.019,6.229l9.985,0.031l0.002-1.236v-2h-2.18c-0.42-1.16-1.521-2-2.821-2c-1.3,0-2.4,0.84-2.82,2h-2.18v2L7.019,6.229z M12.005,3.024c0.549,0,1,0.45,1,1s-0.451,1-1,1c-0.55,0-1-0.45-1-1S11.456,3.024,12.005,3.024z"/><path fill="#4E342E" d="M12.1,3.03h-0.2c0.03-0.01,0.07-0.01,0.1-0.01S12.07,3.02,12.1,3.03z"/></svg>			</div>
			<div class="icon-set__id">
				<span>assignment_return</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E53935" d="M8,5v14l11-7L8,5z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>play_arrow</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" enable-background="new 0 0 32 32">  
  <g id="backpack" data-iconmelon="Smallicons:1546c758b463680dbd5499d6e8fe39db">
    <path fill="#BF392C" d="M2 17h28c1.104 0 2 .896 2 2v9c0 1.105-.896 2-2 2h-28c-1.104 0-2-.895-2-2v-9c0-1.104.896-2 2-2z"></path>
    <path fill="#324D5B" d="M16 2c1.305 0 2.402.838 2.816 2h2.184c0-2.209-2.238-4-5-4-2.761 0-5 1.791-5 4h2.184c.414-1.162 1.512-2 2.816-2z"></path>
    <path fill="#E2574C" d="M9 4h14c3.312 0 6 2.687 6 6v20c0 1.105-.896 2-2 2h-22c-1.104 0-2-.895-2-2v-20c0-3.313 2.686-6 6-6z"></path>
    <path fill="#BF392C" d="M6 4h20c2.209 0 4 1.791 4 4v4c0 2.209-1.791 4.001-4 4.001h-20c-2.209 0-4-1.792-4-4.001v-4c0-2.209 1.791-4 4-4z"></path>
    <path fill="#EFC75E" d="M8 25s.771 2 1.531 2c.739 0 1.469-2 1.469-2v-21h-3v21zm13-21v21s.771 2 1.531 2c.739 0 1.469-2 1.469-2v-21h-3z"></path>
    <path fill="#E4E7E7" d="M7 23h5v-3h-5v3zm13-3v3h5v-3h-5z"></path>
  </g>
</svg>  			</div>
			<div class="icon-set__id">
				<span>backpack</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M9,10v5h2V4h2v11h2V4h2V2H9C6.79,2,5,3.79,5,6S6.79,10,9,10z"/><polygon fill="#F57C00" points="21,18 17,14 17,17 5,17 5,19 17,19 17,22 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_textdirection_r_to_l</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="6" opacity="0.9" fill="#7E57C2" width="2" height="12"/><rect x="11" y="2" fill="#9575CD" width="2" height="20"/><rect x="3" y="10" fill="#9575CD" width="2" height="4"/><rect x="15" y="6" opacity="0.9" fill="#7E57C2" width="2" height="12"/><rect x="19" y="10" fill="#9575CD" width="2" height="4"/></svg>			</div>
			<div class="icon-set__id">
				<span>multitrack_audio</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26C6DA" d="M21.71,11.29l-9-9c-0.39-0.39-1.02-0.39-1.41,0l-9,9c-0.39,0.39-0.39,1.02,0,1.41l9,9c0.39,0.39,1.02,0.39,1.41,0l9-9C22.1,12.32,22.1,11.69,21.71,11.29z"/><path fill="#AB47BC" d="M14,14.5V12h-4v3H8v-4c0-0.55,0.45-1,1-1h5V7.5l3.5,3.5L14,14.5z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>directions</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#66BB6A" d="M22.023,21.973l-4-4h-14c-1.1,0-2-0.9-2-2v-12c0-1.1,0.9-2,2-2h16c1.1,0,1.99,0.9,1.99,2v6.96L22.023,21.973z"/><rect x="6.023" y="11.971" fill="#CFD8DC" width="12" height="2"/><path opacity="0.2" fill="#43A047" enable-background="new    " d="M22.014,3.971v6.96H2.023v-6.96c0-1.1,0.9-2,2-2h16C21.123,1.971,22.014,2.871,22.014,3.971z"/><rect x="6.02" y="8.97" fill="#455A64" width="12" height="2"/><rect x="6.023" y="5.971" fill="#455A64" width="12" height="2"/><rect x="12" y="8.97" fill="#CFD8DC" width="6.02" height="2"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>insert_comment</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="13" fill="#BA68C8" width="17" height="2"/><rect x="4" y="17" fill="#9575CD" width="17" height="2"/><rect x="4" y="9" fill="#F06292" width="17" height="2"/><rect x="4" y="5" fill="#E57373" width="17" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_headline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M17,13.86V15h3c0,1.86-1.28,3.41-3,3.86V20c0,0.55-0.45,1-1,1H8c-0.55,0-1-0.45-1-1v-1.14c-0.01,0-0.02-0.011-0.03-0.011C5.27,18.39,4,16.85,4,15h3v-1.14c-0.01,0-0.02-0.011-0.03-0.011C5.27,13.39,4,11.85,4,10h3V8.86c-0.01,0-0.02-0.01-0.03-0.01C5.27,8.39,4,6.85,4,5h3V4c0-0.55,0.45-1,1-1h8c0.55,0,1,0.45,1,1v1h3c0,1.86-1.28,3.41-3,3.86V10h3C20,11.86,18.72,13.41,17,13.86z"/><path fill="#546E7A" d="M17,4v16c0,0.55-0.45,1-1,1H8c-0.55,0-1-0.45-1-1v-1.14c-0.01,0-0.02-0.011-0.03-0.011V5H7V4c0-0.55,0.45-1,1-1h8C16.55,3,17,3.45,17,4z"/><path fill="#43A047" d="M12,19c-1.11,0-2-0.9-2-2s0.89-2,2-2c1.1,0,2,0.9,2,2S13.109,19,12,19z"/><path fill="#D4E157" d="M12,14c-1.11,0-2-0.9-2-2c0-1.1,0.89-2,2-2c1.1,0,2,0.9,2,2C14,13.1,13.109,14,12,14z"/><path fill="#EF5350" d="M12,9c-1.11,0-2-0.9-2-2c0-1.11,0.89-2,2-2c1.1,0,2,0.89,2,2C14,8.1,13.109,9,12,9z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>traff24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M22,8.04h2V24H0V8.04h2.01V6c0-1.11,0.88-2,1.99-2h16c1.11,0,2,0.89,2,2V8.04z"/><rect fill="none" width="24" height="1.21"/><path fill="#43A047" d="M22,6v12c0,1.11-0.89,2-2,2H4c-1.11,0-2-0.89-2-2l0.01-6.04V6c0-1.11,0.88-2,1.99-2h16C21.11,4,22,4.89,22,6z"/><rect x="4" y="12" fill="#E6EE9C" width="16" height="6"/><rect x="4" y="6" fill="#E6EE9C" width="16" height="2"/><path fill="#90CAF9" d="M22,11.96V18c0,1.11-0.89,2-2,2H4c-1.11,0-2-0.89-2-2l0.01-6.04H22z"/><path fill="#90CAF9" d="M22,6v2.04H2.01V6c0-1.11,0.88-2,1.99-2h16C21.11,4,22,4.89,22,6z"/></g><g id="Capa_2"><polygon fill="#66BB6A" points="11.938,8.047 2,8.063 2,11.953 9.125,11.969 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>payment</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><path fill="#CFD8DC" d="M17,13v-2c0-1.11-0.9-2-2-2h-2V7h4V5h-6v6h4v2h-4v2h4C16.1,15,17,14.109,17,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_5</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path d="M20.999,10.999v8c0,0.55-0.451,1-1,1h-1c-0.551,0-1-0.45-1-1v-1h-12v1c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-8l2.08-5.99c0.21-0.59,0.76-1.01,1.42-1.01h11c0.66,0,1.221,0.42,1.42,1.01L20.999,10.999z"/><path fill="#455A64" d="M20.999,13.719v5.28c0,0.55-0.451,1-1,1h-1c-0.551,0-1-0.45-1-1v-1h-12v1c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-5.28H20.999z"/><path fill="#90A4AE" d="M20.999,10.999v2.72h-18v-2.72l2.08-5.99c0.21-0.59,0.76-1.01,1.42-1.01h11c0.66,0,1.221,0.42,1.42,1.01L20.999,10.999z"/><circle fill="#FFFFFF" cx="6.499" cy="13.499" r="1.5"/><circle fill="#FFFFFF" cx="17.499" cy="13.499" r="1.5"/><polygon fill="#CFD8DC" points="4.999,9.999 6.499,5.499 17.499,5.499 18.999,9.999 	"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>time_to_leave</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="4" fill="#689F38" width="4" height="4"/><rect x="10" y="16" fill="#689F38" width="4" height="4"/><rect x="4" y="16" fill="#00796B" width="4" height="4"/><rect x="4" y="10" fill="#303F9F" width="4" height="4"/><rect x="10" y="10" fill="#0288D1" width="4" height="4"/><rect x="16" y="4" fill="#0288D1" width="4" height="4"/><rect x="10" y="4" fill="#303F9F" width="4" height="4"/><rect x="16" y="10" fill="#00796B" width="4" height="4"/><rect x="16" y="16" fill="#0288D1" width="4" height="4"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>apps</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#43A047" d="M12.153,6.797c-2.76,0-5,2.24-5,5s2.24,5,5,5c2.76,0,5-2.238,5-5C17.152,9.036,14.913,6.797,12.153,6.797zM12.153,1.797c-5.521,0-10,4.48-10,10c0,5.52,4.479,10,10,10s10-4.479,10-10C22.152,6.276,17.674,1.797,12.153,1.797zM12.153,19.797c-4.42,0-8-3.58-8-8s3.58-8,8-8c4.42,0,8,3.58,8,8S16.573,19.797,12.153,19.797z M12.153,6.797c-2.76,0-5,2.24-5,5s2.24,5,5,5c2.76,0,5-2.238,5-5C17.152,9.036,14.913,6.797,12.153,6.797z M12.153,6.797c-2.76,0-5,2.24-5,5s2.24,5,5,5c2.76,0,5-2.238,5-5C17.152,9.036,14.913,6.797,12.153,6.797z"/><circle fill="none" cx="12.153" cy="11.797" r="5"/><path fill="none" d="M0.153-0.203v24h24v-24H0.153z M12.153,19.797c-4.42,0-8-3.58-8-8s3.58-8,8-8c4.42,0,8,3.58,8,8S16.573,19.797,12.153,19.797z"/><circle fill="#388E3C" cx="12.153" cy="11.797" r="5"/><circle fill="#388E3C" cx="12.153" cy="11.797" r="4.177"/></svg>			</div>
			<div class="icon-set__id">
				<span>radio_button_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4DD0E1" d="M22.005,4.015v12c0,1.1-0.9,2-2,2h-14l-4,4l0.01-11v-7c0-1.1,0.891-2,1.99-2h16C21.104,2.015,22.005,2.915,22.005,4.015z"/><path opacity="0.3" fill="#26C6DA" enable-background="new    " d="M22.005,4.015v7H2.015v-7c0-1.1,0.891-2,1.99-2h16C21.104,2.015,22.005,2.915,22.005,4.015z"/><g><polygon fill="#43A047" points="19,14.001 5,14.001 8.5,9.5 11,12.511 14.5,8.001 		"/><polygon fill="#66BB6A" points="5,14.001 8.5,9.5 12.235,14.001 		"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>mms</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M20,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4z"/><path fill="#B0BEC5" d="M7.76,16.24l-1.41,1.41C4.78,16.1,4,14.05,4,12s0.78-4.1,2.34-5.66l1.41,1.41C6.59,8.93,6,10.46,6,12S6.59,15.07,7.76,16.24z"/><circle fill="#B0BEC5" cx="12" cy="12" r="4"/><path fill="#B0BEC5" d="M17.66,17.66l-1.41-1.41C17.41,15.07,18,13.54,18,12s-0.59-3.07-1.76-4.24l1.41-1.41C19.221,7.9,20,9.95,20,12S19.221,16.1,17.66,17.66z"/><path fill="#78909C" d="M12,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C14,10.9,13.1,10,12,10z"/></svg>			</div>
			<div class="icon-set__id">
				<span>surround_sound</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="7" fill="#26C6DA" width="2" height="2"/><rect x="7" y="11" fill="#26C6DA" width="2" height="2"/><path fill="#26C6DA" d="M9,3C7.89,3,7,3.9,7,5h2V3z"/><rect x="11" y="15" fill="#26C6DA" width="2" height="2"/><path fill="#26C6DA" d="M19,3v2h2C21,3.9,20.1,3,19,3z"/><rect x="11" y="3" fill="#26C6DA" width="2" height="2"/><path fill="#26C6DA" d="M9,17v-2H7C7,16.1,7.89,17,9,17z"/><rect x="19" y="11" fill="#26C6DA" width="2" height="2"/><rect x="19" y="7" fill="#26C6DA" width="2" height="2"/><path fill="#26C6DA" d="M19,17c1.1,0,2-0.9,2-2h-2V17z"/><path fill="#0097A7" d="M5,7H3v12c0,1.1,0.89,2,2,2h12v-2H5V7z"/><rect x="15" y="3" fill="#26C6DA" width="2" height="2"/><rect x="15" y="15" fill="#26C6DA" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>flip_to_back</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="7" fill="#7E57C2" width="2" height="2"/><rect x="3" y="15" fill="#7E57C2" width="2" height="2"/><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#7E57C2" width="2" height="2"/><rect x="7" y="3" fill="#7E57C2" width="2" height="2"/><rect x="15" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="7" fill="#7E57C2" width="2" height="2"/><rect x="11" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><rect x="11" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="11" fill="#512DA8" width="18" height="2"/><rect x="19" y="3" fill="#7E57C2" width="2" height="2"/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><rect x="11" y="15" fill="#7E57C2" width="2" height="2"/><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><rect x="19" y="19" fill="#7E57C2" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_horizontal</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="21" y="5" fill="#C8E6C9" width="2" height="14"/><rect x="17" y="5" fill="#C8E6C9" width="2" height="14"/><path fill="#546E7A" d="M14,5H2C1.45,5,1,5.45,1,6v12c0,0.55,0.45,1,1,1h12c0.55,0,1-0.45,1-1V6C15,5.45,14.55,5,14,5z"/><circle fill="#C8E6C9" cx="8" cy="10" r="2.25"/><path fill="#C8E6C9" d="M12.5,17h-9v-0.75c0-1.5,3-2.25,4.5-2.25s4.5,0.75,4.5,2.25V17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>recent_actors</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#9575CD" d="M22,5.72l-4.6-3.86l-1.291,1.53l4.601,3.86L22,5.72z M7.88,3.39L6.6,1.86L2,5.71l1.29,1.53L7.88,3.39z"/><path fill="#7E57C2" d="M12,4c-4.97,0-9,4.03-9,9s4.02,9,9,9c4.971,0,9-4.03,9-9S16.971,4,12,4z"/><path fill="#E0E0E0" d="M12,20c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,20,12,20z"/><polygon fill="#00BCD4" points="13,9 11,9 11,12 8,12 8,14 11,14 11,17 13,17 13,14 16,14 16,12 13,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>alarm_add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M12,3.06V4c3.641,0,6.67,2.59,7.359,6.04C21.95,10.22,24,12.36,24,15c0,2.76-2.24,5-5,5h-7v3.061h12V24H0V0h24v3.06H12z"/><path fill="#5C6BC0" d="M24,15c0,2.76-2.24,5-5,5H6c-3.31,0-6-2.689-6-6c0-3.09,2.34-5.64,5.35-5.96C6.6,5.64,9.11,4,12,4c3.641,0,6.67,2.59,7.359,6.04C21.95,10.22,24,12.36,24,15z"/><path opacity="0.1" fill="#9FA8DA" enable-background="new    " d="M24,15c0,2.76-2.24,5-5,5h-7V4c3.641,0,6.67,2.59,7.359,6.04C21.95,10.22,24,12.36,24,15z"/></svg>			</div>
			<div class="icon-set__id">
				<span>wb_cloudy</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#43A047" d="M22,8h-3v7c0,2.21-1.79,4-4,4s-4-1.79-4-4V8c0-1.1-0.9-2-2-2S7,6.9,7,8v7h3l-4,4l-4-4h3V8c0-2.21,1.79-4,4-4s4,1.79,4,4v7c0,1.1,0.9,2,2,2s2-0.9,2-2V8h-3l4-4L22,8z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#00ACC1" d="M13,8v3.46h-2V8c0-1.1-0.9-2-2-2S7,6.9,7,8v7h3l-4,4l-4-4h3V8c0-2.21,1.79-4,4-4S13,5.79,13,8z"/></svg>			</div>
			<div class="icon-set__id">
				<span>swap_calls</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M21,5v6H3V5c0-1.1,0.9-2,2-2h14C20.1,3,21,3.9,21,5z"/><rect x="3" y="11" opacity="0.8" fill="#B0BEC5" width="18" height="3.996"/><rect x="11" y="9" fill="#78909C" width="2" height="2"/><rect x="9" y="11" fill="#546E7A" width="2" height="2"/><rect x="13" y="11" fill="#546E7A" width="2" height="2"/><rect x="15" y="9" fill="#78909C" width="2" height="2"/><rect x="7" y="9" fill="#78909C" width="2" height="2"/><path opacity="0.7" fill="#90A4AE" d="M21,15v4c0,1.1-0.9,2-2,2H5c-1.1,0-2-0.9-2-2v-4H21z"/><rect x="7" y="16" fill="#263238" width="2" height="2"/><rect x="11" y="16" fill="#263238" width="2" height="2"/><rect x="15" y="16" fill="#263238" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="5" y="11" fill="#546E7A" width="2" height="2"/><rect x="17" y="11" fill="#546E7A" width="1.984" height="2"/><rect x="15" y="12.984" fill="#455A64" width="1.984" height="2"/><rect x="11.016" y="12.984" fill="#455A64" width="1.984" height="2"/><rect x="7" y="13" fill="#455A64" width="1.984" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>gradient</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#43A047" fill-opacity="0.3" d="M17,5.33V8h-4V7l-0.53,1H7V5.33C7,4.6,7.6,4,8.33,4H10V2h4v2h1.67C16.4,4,17,4.6,17,5.33z"/><polygon fill="#FFFFFF" points="11,20 11,14.5 9,14.5 12.47,8 13,7 13,12.5 15,12.5 "/><path fill="#43A047" d="M17,8v12.67C17,21.4,16.4,22,15.66,22H8.33C7.6,22,7,21.4,7,20.67V8h5.47L9,14.5h2V20l4-7.5h-2V8H17z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_charging_90</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M5,15H3v4c0,1.1,0.9,2,2,2h4v-2H5V15z"/><path fill="#546E7A" d="M5,5h4V3H5C3.9,3,3,3.9,3,5v4h2V5z"/><path fill="#546E7A" d="M19,3h-4v2h4v4h2V5C21,3.9,20.1,3,19,3z"/><path fill="#546E7A" d="M19,19h-4v2h4c1.1,0,2-0.9,2-2v-4h-2V19z"/><circle fill="#7E57C2" cx="12" cy="12" r="4"/><path fill="#FFFFFF" d="M12,14c-1.1,0-2-0.9-2-2c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,13.1,13.1,14,12,14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>center_focus_weak</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><g id="Capa_1_1_"><path fill="#78909C" d="M23.795,20.288c0.097,0.078,0.122,0.214,0.061,0.324l-1.006,1.741c-0.061,0.112-0.192,0.157-0.308,0.112l-1.253-0.504c-0.262,0.195-0.544,0.366-0.851,0.492l-0.192,1.335C20.231,23.909,20.125,24,20,24h-2.015c-0.125,0-0.23-0.091-0.246-0.211l-0.19-1.335c-0.308-0.126-0.59-0.292-0.852-0.492l-1.253,0.504c-0.11,0.04-0.248,0-0.308-0.112l-1.007-1.741c-0.061-0.11-0.034-0.246,0.061-0.324l1.063-0.828c-0.021-0.162-0.035-0.329-0.035-0.494c0-0.167,0.014-0.332,0.035-0.493l-1.063-0.831c-0.095-0.075-0.126-0.211-0.061-0.321l1.007-1.743c0.06-0.11,0.191-0.155,0.308-0.11l1.253,0.504c0.262-0.196,0.544-0.368,0.852-0.494l0.19-1.333c0.016-0.122,0.121-0.213,0.246-0.213H20c0.125,0,0.231,0.091,0.246,0.213l0.191,1.333c0.308,0.126,0.59,0.292,0.851,0.494l1.254-0.504c0.109-0.039,0.247,0,0.308,0.11l1.006,1.743c0.062,0.11,0.035,0.246-0.061,0.321l-1.062,0.831c0.021,0.161,0.034,0.322,0.034,0.493s-0.014,0.332-0.034,0.494L23.795,20.288z"/><path fill="#90A4AE" d="M18.992,13.931V24h-1.007c-0.125,0-0.23-0.091-0.246-0.211l-0.19-1.335c-0.308-0.126-0.59-0.292-0.852-0.492l-1.253,0.504c-0.11,0.04-0.248,0-0.308-0.112l-1.007-1.741c-0.061-0.11-0.034-0.246,0.061-0.324l1.063-0.828c-0.021-0.162-0.035-0.329-0.035-0.494c0-0.167,0.014-0.332,0.035-0.493l-1.063-0.831c-0.095-0.075-0.126-0.211-0.061-0.321l1.007-1.743c0.06-0.11,0.191-0.155,0.308-0.11l1.253,0.504c0.262-0.196,0.544-0.368,0.852-0.494l0.19-1.333c0.016-0.122,0.121-0.213,0.246-0.213H18.992z"/><circle fill="#546E7A" cx="18.984" cy="18.958" r="2.241"/><path fill="#FFFFFF" d="M18.992,20.728c-0.971,0-1.76-0.79-1.76-1.762s0.789-1.763,1.76-1.763c0.973,0,1.763,0.791,1.763,1.763S19.965,20.728,18.992,20.728z"/></g><g id="Capa_2"></g></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#90A4AE" d="M18.99,11.5c0.34,0,0.67,0.03,1,0.07L20,0L0,20h11.56c-0.04-0.33-0.07-0.66-0.07-1C11.49,14.859,14.85,11.5,18.99,11.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>perm_data_setting</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M24,0v24H0V0h2.38v15.72H4V6c0-3.5,3.58-4,8-4s8,0.5,8,4v9.72h1.5V0H24z"/><path d="M20,6v10c0,0.88-0.39,1.67-1,2.22V20c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-1H8v1c0,0.55-0.45,1-1,1H6c-0.55,0-1-0.45-1-1v-1.78C4.39,17.67,4,16.88,4,16V6c0-3.5,3.58-4,8-4S20,2.5,20,6z"/><path fill="#455A64" d="M20,15.75V16c0,0.88-0.39,1.67-1,2.22V20c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-1H8v1c0,0.55-0.45,1-1,1H6c-0.55,0-1-0.45-1-1v-1.78C4.39,17.67,4,16.88,4,16v-0.25H20z"/><path fill="#1E88E5" d="M20,6v9.72H4V6c0-3.5,3.58-4,8-4S20,2.5,20,6z"/><circle fill="#FFFFFF" cx="7.5" cy="15.5" r="1.5"/><circle fill="#FFFFFF" cx="16.5" cy="15.5" r="1.5"/><rect x="6" y="6" fill="#BBDEFB" width="12" height="5"/></svg>			</div>
			<div class="icon-set__id">
				<span>directions_bus</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#DCE775" d="M20,2H4C2.9,2,2.01,2.9,2.01,4L2,22l4-4h14c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2z"/><path fill="#689F38" d="M13,11h-2V5h2V11z M13,15h-2v-2h2V15z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>announcement</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C22.099,0.999,22.999,1.899,22.999,2.999z"/><path fill="#0097A7" d="M2.999,4.999h-2v16c0,1.1,0.9,2,2,2h16v-2h-16V4.999z"/><path opacity="0.5" fill="#0097A7" d="M22.999,2.999v14c0,1.1-0.9,2-2,2h-7.01v-18h7.01C22.099,0.999,22.999,1.899,22.999,2.999z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CFD8DC" d="M17,7V5h-4c-1.1,0-2,0.89-2,2v6c0,1.11,0.9,2,2,2h2c1.1,0,2-0.89,2-2v-2c0-1.11-0.9-2-2-2h-2V7H17z M15,11v2h-2v-2H15z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_6</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#039BE5" d="M19.943,3.998v16c0,1.1-0.9,2-2,2h-12c-1.1,0-2-0.9-2-2l0.021-12l5.979-6h8C19.043,1.998,19.943,2.898,19.943,3.998z"/><path opacity="0.5" fill="#0288D1" enable-background="new    " d="M19.943,3.998v16c0,1.1-0.9,2-2,2h-6v-20h6C19.043,1.998,19.943,2.898,19.943,3.998z"/></g><rect x="15" y="17" fill="#FFD54F" width="2" height="2"/><rect x="7" y="17" fill="#FFD54F" width="2" height="2"/><rect x="11" y="15" fill="#FFD54F" width="2" height="4"/><rect x="15" y="11" fill="#FFD54F" width="2" height="4"/><rect x="11" y="11" fill="#FFD54F" width="2" height="2"/><rect x="7" y="11" fill="#FFD54F" width="2" height="4"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>sim_card</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#263238" d="M6,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2s2-0.9,2-2C8,10.9,7.1,10,6,10z"/><path fill="#37474F" d="M18,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2s2-0.9,2-2C20,10.9,19.1,10,18,10z"/><path fill="#455A64" d="M12,10c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C14,10.9,13.1,10,12,10z"/></svg>			</div>
			<div class="icon-set__id">
				<span>more_horiz</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#29B6F6" d="M20,21c-1.391,0-2.779-0.47-4-1.32c-2.439,1.711-5.56,1.711-8,0C6.78,20.53,5.39,21,4,21H2v2h2c1.38,0,2.74-0.35,4-0.99c2.52,1.29,5.48,1.29,8,0c1.26,0.65,2.62,0.99,4,0.99h2v-2H20z"/><path fill="#546E7A" d="M21.94,12.32l-1.87,6.61L20.05,19H20c-0.22,0-0.44-0.02-0.65-0.05c-1.33-0.2-2.51-0.98-3.35-1.95c-0.98,1.12-2.4,2-4,2c-1.6,0-3.02-0.88-4-2c-0.98,1.12-2.4,2-4,2H3.95l-1.9-6.68c-0.09-0.26-0.06-0.54,0.06-0.78c0.12-0.24,0.34-0.42,0.6-0.5L4,10.62V6c0-1.1,0.9-2,2-2h3V1h6v3h3c1.1,0,2,0.9,2,2v4.62l1.28,0.42c0.26,0.08,0.479,0.26,0.6,0.5C22,11.78,22.02,12.06,21.94,12.32z"/><polygon fill="#BBDEFB" points="6,6 18,6 18,9.97 12,8 6,9.97 	"/><path fill="#EF5350" d="M21.94,12.32l-1.78,6.3l-0.09,0.31L19.35,18.95c-0.399-0.061-0.779-0.17-1.149-0.33c-0.86-0.36-1.61-0.94-2.2-1.62c-0.59,0.68-1.34,1.26-2.2,1.62C13.24,18.86,12.63,19,12,19c-1.6,0-3.02-0.88-4-2c-0.98,1.12-2.4,2-4,2H3.95l-1.9-6.68c-0.09-0.26-0.06-0.54,0.06-0.78c0.12-0.24,0.34-0.42,0.6-0.5L4,10.62l7.94-2.6L12,8l8,2.62l1.28,0.42c0.26,0.08,0.479,0.26,0.6,0.5C22,11.78,22.02,12.06,21.94,12.32z"/><path fill="#E53935" d="M21.94,12.32l-1.78,6.3H18.2c-0.86-0.36-1.61-0.94-2.2-1.62c-0.59,0.68-1.34,1.26-2.2,1.62h-1.86V8.02L12,8l8,2.62l1.28,0.42c0.26,0.08,0.479,0.26,0.6,0.5C22,11.78,22.02,12.06,21.94,12.32z"/><path fill="#64B5F6" d="M20.073,18.135c-1.391,0-2.837-0.506-4.058-1.229c-2.439,1.453-5.094,2.313-8.047,0.047c-1.221,0.723-2.505,1.182-3.896,1.182h-2v1.699h2c1.38,0,2.74-0.297,4-0.841c2.52,1.096,5.48,1.096,8,0c1.26,0.552,2.62,0.841,4,0.841h2v-1.699H20.073z"/><rect x="9" y="1" fill="#EF5350" width="6" height="2.99"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>directions_ferry</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4E342E" d="M19.001,3.016H14.82c-0.42-1.16-1.521-2-2.819-2s-2.4,0.84-2.82,2h-4.18c-1.1,0-2,0.9-2,2v14c0,1.101,0.9,2,2,2h14c1.101,0,2-0.899,2-2v-14C21.001,3.916,20.102,3.016,19.001,3.016z"/><path fill="#D7CCC8" d="M19.71,6.553c0-1.354-1.103-2.454-2.455-2.454H6.79c-1.356,0-2.455,1.099-2.455,2.454V17.23c0,1.354,1.099,2.453,2.455,2.453h10.465c1.354,0,2.455-1.101,2.455-2.453V6.553z"/><circle fill="#F1F8E9" cx="12.001" cy="4.016" r="1"/></g><path fill="none" d="M0,0h24v24H0V0z"/><rect x="7" y="16" fill="#455A64" width="7" height="2"/><rect x="7" y="12" fill="#455A64" width="10" height="2"/><rect x="7" y="8" fill="#455A64" width="10" height="2"/><path fill="#FFA000" d="M7.011,6.213l9.984,0.031l0.002-1.236v-2h-2.18c-0.42-1.16-1.521-2-2.82-2s-2.4,0.84-2.82,2h-2.18v2L7.011,6.213z M11.997,3.008c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S11.447,3.008,11.997,3.008z"/></svg>			</div>
			<div class="icon-set__id">
				<span>assignment</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M13,1.07V9h7C20,4.92,16.95,1.56,13,1.07z"/><path fill="#546E7A" d="M4,15c0,4.42,3.58,8,8,8s8-3.58,8-8v-4H4V15z"/><path fill="#78909C" d="M11,1.07C7.05,1.56,4,4.92,4,9h7V1.07z"/></svg>			</div>
			<div class="icon-set__id">
				<span>mouse</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1E88E5" d="M4,6H2v14c0,1.1,0.9,2,2,2h14v-2H4V6z"/><path fill="#42A5F5" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path fill="#1E88E5" d="M22,4v12c0,1.1-0.9,2-2,2h-6V2h6C21.1,2,22,2.9,22,4z"/><polygon fill="#FFFFFF" points="19,11 15,11 15,15 13,15 13,11 9,11 9,9 13,9 13,5 15,5 15,9 19,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>my_library_add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="6" y="6" fill="#E53935" width="12" height="12"/><rect x="6" y="6" fill="#EF5350" width="1.67" height="12"/><rect x="6" y="16.5" fill="#EF5350" width="12" height="1.5"/></svg>			</div>
			<div class="icon-set__id">
				<span>stop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#FFFFFF" points="19,5 19,19 12,11 12,5 "/><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="#ECEFF1" d="M21,5v14c0,1.1-0.911,2-2.024,2h-7.045V3h7.045C20.089,3,21,3.9,21,5z"/><path fill="#263238" d="M11.948,3L4.943,3.052c-1.089,0-1.98,0.9-1.98,2v14c0,1.1,0.775,1.995,1.865,1.995h7.156l-0.142-2.005l0.112-7.823l0,0L11.948,3z"/><polygon fill="#ECEFF1" points="12,11.047 11.984,21.047 4.828,21.047 	"/><polygon fill="#263238" points="11.953,11.047 11.969,21.047 19.125,21.047 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>filter_b_and_w</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1E88E5" d="M12,4C9.79,4,8,5.79,8,8s1.79,4,4,4s4-1.79,4-4S14.21,4,12,4z M12,14c-2.67,0-8,1.34-8,4v2h16v-2C20,15.33,14.67,14,12,14z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CFD8DC" fill-opacity="0.9" d="M12.489,5.583h-0.781v3.125l2.734,1.641l0.391-0.641l-2.344-1.39V5.583z"/></svg>			</div>
			<div class="icon-set__id">
				<span>timer_auto</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M19,4H5C3.89,4,3,4.9,3,6v12c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V6C21,4.9,20.11,4,19,4z M19,18H5V8h14V18z"/><path fill="#C5CAE9" d="M21,7.97V18c0,1.1-0.9,2-2,2H5c-1.11,0-2-0.9-2-2V7.97H21z"/><polygon fill="#42A5F5" points="12,10 8,14 11,14 11,20 13,20 13,14 16,14 "/></svg>			</div>
			<div class="icon-set__id">
				<span>open_in_browser</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#42A5F5" d="M20.98,4.947v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C20.08,2.947,20.98,3.847,20.98,4.947z"/><path opacity="0.5" fill="#1E88E5" enable-background="new    " d="M20.98,4.947v14c0,1.1-0.9,2-2,2h-7.009v-18h7.009C20.08,2.947,20.98,3.847,20.98,4.947z"/></g><path fill="#CFD8DC" d="M11,9v2h2c1.1,0,2,0.89,2,2v2c0,1.11-0.9,2-2,2H9v-2h4v-2H9V7h6v2H11z"/></svg>			</div>
			<div class="icon-set__id">
				<span>looks_5</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FF8A65" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.11,4,6.6,5.64,5.35,8.04C2.34,8.36,0,10.91,0,14c0,3.311,2.69,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z"/><path fill="#FF8A65" d="M19,18H6c-2.21,0-4-1.79-4-4s1.79-4,4-4h0.71C7.37,7.69,9.48,6,12,6c3.04,0,5.5,2.46,5.5,5.5V12H19c1.66,0,3,1.34,3,3S20.66,18,19,18z"/></svg>			</div>
			<div class="icon-set__id">
				<span>cloud_queue</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#388E3C" points="20,14.5 20,20 14.5,20 16.55,17.95 10.641,11.922 11.297,11.281 12,10.578 17.96,16.54 "/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#388E3C" points="12.1,10.651 11.5,11.267 10.818,12.133 4,5.41 5.41,4 "/><polygon fill="#78909C" points="20,4 20,9.5 17.96,7.46 5.41,20 4.69,19.28 4,18.59 16.54,6.04 14.5,4 "/></svg>			</div>
			<div class="icon-set__id">
				<span>shuffle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#DCE775" d="M12.461,21.679l5.067-11.925c0.317-0.747,0.135-1.59-0.392-2.153c-0.182-0.186-0.398-0.35-0.659-0.458l-7.34-3.122C8.39,3.702,7.554,3.879,6.991,4.404c-0.2,0.182-0.356,0.407-0.465,0.667L1.459,16.997c-0.325,0.754-0.134,1.604,0.405,2.168c0.183,0.186,0.399,0.336,0.646,0.444l7.332,3.113C10.857,23.156,12.036,22.687,12.461,21.679z"/><path fill="#43A047" d="M17.95,19.665L17.806,6.709c-0.009-0.812-0.515-1.511-1.222-1.815c-0.241-0.097-0.506-0.16-0.788-0.155L7.819,4.825C7.008,4.833,6.313,5.33,6.007,6.037c-0.11,0.247-0.164,0.516-0.159,0.798l0.146,12.956c0.005,0.821,0.52,1.523,1.241,1.823c0.241,0.097,0.501,0.147,0.771,0.147l7.965-0.09C17.074,21.662,17.965,20.759,17.95,19.665z"/><path fill="#EF5350" d="M22.029,15.95L17.07,3.98c-0.311-0.75-1.041-1.21-1.811-1.23c-0.26,0-0.529,0.04-0.789,0.15L7.1,5.95c-0.75,0.31-1.21,1.03-1.23,1.8c-0.01,0.27,0.04,0.54,0.15,0.8l4.96,11.97c0.31,0.761,1.05,1.221,1.831,1.23c0.26,0,0.52-0.05,0.77-0.15l7.359-3.05C21.96,18.13,22.45,16.96,22.029,15.95z"/><circle fill="#FFFFFF" cx="7.88" cy="7.75" r="1"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>style</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26A69A" d="M21,4H3C1.9,4,1,4.9,1,6v12c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2V6C23,4.9,22.1,4,21,4z M5,17l3.5-4.5l2.5,3.01L14.5,11l4.5,6H5z"/><polygon fill="#A5D6A7" points="19,17 5,17 8.5,12.5 11,15.51 14.5,11 "/><path fill="none" d="M24.875,0h24v24h-24V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>panorama</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#607D8B" d="M10,17H7v-3H5v5h5V17z M17,7v3h2V5h-5v2H17z"/><path fill="#455A64" d="M7,7h3V5H5v5h2V7z M19,14h-2v3h-3v2h5V14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>fullscreen</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#3F51B5" d="M12,1c-4.97,0-9,4.03-9,9v7c0,1.66,1.34,3,3,3h3v-8H5v-2c0-3.87,3.13-7,7-7s7,3.13,7,7v2h-4v8h4v1h-7v2h6c1.66,0,3-1.34,3-3V10C21,5.03,16.971,1,12,1z"/><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="#9575CD" d="M12,1c-4.971,0-9,4.03-9,9v7c0,1.309,0.836,2.405,2,2.816V12v-2c0-3.87,3.13-7,7-7s7,3.13,7,7v2v7.816c1.164-0.411,2-1.51,2-2.816v-7C21,5.03,16.971,1,12,1z"/><path fill="#3F51B5" d="M9,20v-8H5v7.816C5.314,19.93,5.646,20,6,20H9z"/><path fill="#3F51B5" d="M15,12v8h3c0.354,0,0.686-0.07,1-0.184V12H15z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>headset_m24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#29B6F6" d="M12,5c-3.87,0-7,3.13-7,7h2c0-2.76,2.24-5,5-5s5,2.24,5,5h2C19,8.13,15.87,5,12,5z"/><path fill="#78909C" d="M16.41,21L15,22.41l-3-3l-3,3L7.59,21L11,17.59v-3.3c-0.24-0.11-0.46-0.25-0.66-0.43C9.83,13.41,9.5,12.74,9.5,12c0-1.38,1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5c0,1.01-0.59,1.86-1.44,2.26c-0.01,0-0.01,0-0.01,0c-0.02,0.01-0.03,0.021-0.05,0.03v3.3L16.41,21z"/><path fill="#29B6F6" d="M12,1C5.93,1,1,5.93,1,12h2c0-4.97,4.03-9,9-9c4.971,0,9,4.03,9,9h2C23,5.93,18.07,1,12,1z"/><path fill="#90A4AE" d="M14.5,12c0,1.01-0.59,1.86-1.44,2.26c-0.01,0-0.01,0-0.01,0c-0.319,0.141-0.66,0.21-1.03,0.21c-0.64,0-1.22-0.229-1.68-0.609C9.83,13.41,9.5,12.74,9.5,12c0-1.38,1.12-2.5,2.5-2.5S14.5,10.62,14.5,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_input_antenna</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#8BC34A" fill-opacity="0.3" d="M17,5.33C17,4.6,16.4,4,15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33V8h10V5.33z"/><path fill="#8BC34A" d="M7,8v12.67C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V8H7z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_90</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M24,24H0V0h24V24z"/><path fill="#4FC3F7" d="M12,22c5.52,0,10-4.48,10-10S17.52,2,12,2S2,6.48,2,12S6.48,22,12,22z"/><path fill="#2196F3" d="M12,20c4.417,0,8-3.583,8-8c0-4.414-3.583-8-8-8c-4.414,0-8,3.586-8,8C4,16.417,7.586,20,12,20z"/><path fill="#CFD8DC" d="M11,7h2v2h-2V7z M11,11h2v6h-2V11z"/></svg>			</div>
			<div class="icon-set__id">
				<span>info</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#009688" d="M12,2C6.49,2,2,6.49,2,12s4.49,10,10,10h8c1.1,0,2-0.9,2-2v-8C22,6.49,17.51,2,12,2z"/><path fill="#B2DFDB" d="M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/><polygon fill="#004D40" points="13,7 11,7 11,11 7,11 7,13 11,13 11,17 13,17 13,13 17,13 17,11 13,11 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>loupe</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon points="18.41,19 17,20.41 12.02,15.39 12,15.37 11.9,15.27 12,15.12 12.98,13.6 13,13.62 "/><polygon fill="#26C6DA" points="16.5,8 13,8 13,14.41 12.02,15.39 12,15.41 7,20.41 5.59,19 11,13.59 11,8 7.5,8 12,3.5 "/><polygon fill="#66BB6A" points="18.41,19 17,20.41 12.02,15.39 12,15.41 12,3.5 16.5,8 13,8 13,13.62 "/></svg>			</div>
			<div class="icon-set__id">
				<span>call_merge</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M24,0H0v24h24V0z"/><path fill="#81C784" d="M17.66,19.24C16.1,20.8,14.05,21.58,12,21.58s-4.1-0.78-5.66-2.34c-3.12-3.12-3.12-8.19,0-11.31L12,2.27l5.66,5.66C20.78,11.05,20.78,16.12,17.66,19.24z"/><path fill="#4DD0E1" d="M17.66,19.24C16.1,20.8,14.05,21.58,12,21.58V2.27l5.66,5.66C20.78,11.05,20.78,16.12,17.66,19.24z"/></svg>			</div>
			<div class="icon-set__id">
				<span>invert_colors</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#0097A7" points="10.85,12.02 15.41,16.59 14,18 8,12 14,6 15.41,7.41 10.83,12 "/><path fill="none" d="M0,0h24v24H0V0z"/><polygon opacity="0.3" fill="#00838F" points="15.41,16.59 14,18 8.02,12.02 10.85,12.02 "/></svg>			</div>
			<div class="icon-set__id">
				<span>chevron_left</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><g><rect x="12.41" y="11" fill="#1976D2" width="8.59" height="2"/><polygon fill="#0D47A1" points="6.83,11 10.41,7.41 9,6 3,12 9,18 10.41,16.59 6.83,13 12.41,13 12.41,11 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_backspace</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="11" fill="#D1C4E9" width="2" height="2"/><rect x="3" y="15" fill="#B2EBF2" width="2" height="2"/><rect x="3" y="7" fill="#F8BBD0" width="2" height="2"/><rect x="7" y="11" fill="#9575CD" width="14" height="2"/><rect x="7" y="15" fill="#64B5F6" width="14" height="2"/><rect x="7" y="7" fill="#F06292" width="14" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>list</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#B0BEC5" d="M20,5H4v10c0,2.21,1.79,4,4,4h6c2.21,0,4-1.79,4-4v-1h2c1.109,0,2-0.891,2-2V7C22,5.89,21.109,5,20,5zM20,12h-2V7h2V12z"/><rect x="2" y="19" fill="#90A4AE" width="18" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_cafe</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FF9800" d="M21,12l-2.08-5.99C18.721,5.42,18.16,5,17.5,5h-11C5.84,5,5.29,5.42,5.08,6.01L3,12v3h18V12z"/><path fill="#455A64" d="M4,21h1c0.55,0,1-0.45,1-1v-1h12v1c0,0.55,0.45,1,1,1h1c0.55,0,1-0.45,1-1v-5H3v5C3,20.55,3.45,21,4,21z"/><circle fill="#E0E0E0" cx="6.5" cy="14.5" r="1.5"/><circle fill="#E0E0E0" cx="17.5" cy="14.5" r="1.5"/><polygon fill="#90A4AE" points="5,11 6.5,6.5 17.5,6.5 19,11 "/><rect x="9" y="3" fill="#90A4AE" width="6" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_taxi</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19,5H5C3.9,5,3,5.9,3,7v10c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V7C21,5.9,20.1,5,19,5z M19,17H5V7h14V17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_5_4</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><polygon fill="#66BB6A" points="10,7 8,7 8,11 4,11 4,13 8,13 8,17 10,17 10,13 14,13 14,11 10,11 "/><polygon fill="#546E7A" points="20,18 18,18 18,7.38 15,8.4 15,6.7 19.7,5 20,5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>plus_one</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="5" y="5.156" fill="#CFD8DC" width="13.125" height="13.313"/><path fill="#A5D6A7" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M9,17H7v-7h2V17zM13,17h-2V7h2V17z M17,17h-2v-4h2V17z"/><rect x="15" y="14.803" fill="#BA68C8" width="2" height="2.197"/><rect x="11" y="9.939" fill="#F06292" width="2" height="7.061"/><rect x="7" y="11.881" fill="#4FC3F7" width="2" height="5.119"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>insert_chart</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#607D8B" d="M15.6,10.79c0.971-0.67,1.65-1.77,1.65-2.79c0-2.26-1.75-4-4-4H7v14h7.04c2.09,0,3.71-1.7,3.71-3.79C17.75,12.689,16.891,11.39,15.6,10.79z M10,6.5h3c0.83,0,1.5,0.67,1.5,1.5S13.83,9.5,13,9.5h-3V6.5z M13.5,15.5H10v-3h3.5c0.83,0,1.5,0.67,1.5,1.5S14.33,15.5,13.5,15.5z"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="7" y="4" fill="#455A64" width="3" height="14"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_bold</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M21,6v12c0,1.1-0.9,2-2,2H5c-1.11,0-2-0.9-2-2V6c0-1.1,0.89-2,2-2h14C20.1,4,21,4.9,21,6z"/><polygon fill="#ECEFF1" points="11,15 9.5,15 9.5,13 7.5,13 7.5,15 6,15 6,9 7.5,9 7.5,11.5 9.5,11.5 9.5,9 11,9 "/><path opacity="0.2" fill="#546E7A" d="M20.875,5.958v12c0,1.1-0.9,2-2,2h-7v-16h7C19.975,3.958,20.875,4.858,20.875,5.958z"/><path fill="#ECEFF1" d="M17,9h-3c-0.55,0-1,0.45-1,1v4c0,0.55,0.45,1,1,1h0.75v1.5h1.5V15H17c0.55,0,1-0.45,1-1v-4C18,9.45,17.55,9,17,9z M16.5,13.5h-2v-3h2V13.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>high_quality</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26C6DA" d="M4,6h18V4H4C2.9,4,2,4.9,2,6v11H0v3h14v-3H4V6z"/><path fill="#78909C" d="M23,8h-6c-0.55,0-1,0.45-1,1v10c0,0.55,0.45,1,1,1h6c0.55,0,1-0.45,1-1V9C24,8.45,23.55,8,23,8z"/><rect x="18" y="10" fill="#BBDEFB" width="4" height="7"/></svg>			</div>
			<div class="icon-set__id">
				<span>devices</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M22,3H2C0.9,3,0,3.9,0,5v14c0,1.1,0.9,2,2,2h20c1.1,0,1.99-0.9,1.99-2L24,5C24,3.9,23.1,3,22,3z"/><path fill="#C8E6C9" d="M8,6c1.66,0,3,1.34,3,3s-1.34,3-3,3s-3-1.34-3-3S6.34,6,8,6z"/><path fill="#C8E6C9" d="M14,18H2v-1c0-2,4-3.1,6-3.1s6,1.1,6,3.1V18z"/><path fill="#66BB6A" d="M17.85,14h1.641L21,16l-1.99,1.99c-1.31-0.98-2.279-2.381-2.73-3.99C16.1,13.359,16,12.689,16,12c0-0.69,0.1-1.36,0.279-2c0.451-1.62,1.421-3.01,2.73-3.99L21,8l-1.51,2H17.85c-0.22,0.63-0.35,1.3-0.35,2S17.63,13.37,17.85,14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>quick_contacts_dialer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFEE58" d="M21.59,16.98H17C16.99,14.23,14.75,12,12,12s-4.99,2.23-5,4.98H1.69L5.66,9.07l6.46-2.1l7.66,2.6L21.59,16.98z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#29B6F6" d="M19,16.98c0,0.01,0,0.01,0,0.02h-2c0-0.01,0-0.01,0-0.02C16.99,14.23,14.75,12,12,12s-4.99,2.23-5,4.98c0,0.01,0,0.01,0,0.02H5c0-0.01,0-0.01,0-0.02C5.01,13.13,8.15,10,12,10C15.85,10,18.99,13.13,19,16.98z"/><path fill="#FF7043" d="M12,6C5.93,6,1,10.93,1,17h2c0-4.96,4.04-9,9-9s9,4.04,9,9h2C23,10.93,18.07,6,12,6z"/></svg>			</div>
			<div class="icon-set__id">
				<span>looks</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#66BB6A" d="M12,22c4.971,0,9-4.03,9-9C16.029,13,12,17.03,12,22z"/><path fill="#EF5350" d="M5.6,10.25c0,1.38,1.12,2.5,2.5,2.5c0.53,0,1.01-0.16,1.42-0.44L9.5,12.5c0,1.38,1.12,2.5,2.5,2.5s2.5-1.12,2.5-2.5l-0.02-0.19c0.399,0.28,0.89,0.44,1.42,0.44c1.379,0,2.5-1.12,2.5-2.5c0-1-0.59-1.85-1.43-2.25c0.84-0.4,1.43-1.25,1.43-2.25c0-1.38-1.121-2.5-2.5-2.5c-0.53,0-1.01,0.16-1.42,0.44L14.5,3.5C14.5,2.12,13.38,1,12,1S9.5,2.12,9.5,3.5l0.02,0.19C9.12,3.41,8.63,3.25,8.1,3.25c-1.38,0-2.5,1.12-2.5,2.5c0,1,0.59,1.85,1.43,2.25C6.19,8.4,5.6,9.25,5.6,10.25z"/><circle fill="#FFF176" cx="12" cy="8" r="2.5"/><path fill="#81C784" d="M3,13c0,4.97,4.03,9,9,9C12,17.03,7.97,13,3,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_florist</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M-618-3000H782V600H-618V-3000z M0,0h24v24H0V0z"/><path fill="#90A4AE" d="M20,6H10l0,0L8.016,6.047L8,4h6V0H6v6H4C2.9,6,2,6.9,2,8v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V8C22,6.9,21.1,6,20,6z"/><rect x="6" fill="#E53935" width="8.188" height="4"/><rect x="6.011" y="10.458" fill="#546E7A" width="12.531" height="1.531"/><rect x="6.011" y="3.74" fill="#E53935" width="2" height="8.25"/></svg>			</div>
			<div class="icon-set__id">
				<span>markunread_mailbox</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M20,6v9.5c0,1.93-1.57,3.5-3.5,3.5l1.5,1.5V21H6v-0.5L7.5,19C5.57,19,4,17.43,4,15.5V6c0-3.5,3.58-4,8-4S20,2.5,20,6z"/><circle fill="#FFFFFF" cx="7.5" cy="15.5" r="1.5"/><rect x="6" y="6" fill="#90CAF9" width="5" height="5"/><circle fill="#FFFFFF" cx="16.5" cy="15.5" r="1.5"/><rect x="13" y="6" fill="#90CAF9" width="5" height="5"/><polygon fill="#DCE775" points="18,20.5 18,21 6,21 6,20.5 7.45,19.05 16.55,19.05 "/></svg>			</div>
			<div class="icon-set__id">
				<span>directions_transit</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4CAF50" d="M6,18c0,0.55,0.45,1,1,1h1v3.5C8,23.33,8.67,24,9.5,24s1.5-0.67,1.5-1.5V19h2v3.5c0,0.83,0.67,1.5,1.5,1.5s1.5-0.67,1.5-1.5V19h1c0.55,0,1-0.45,1-1V8H6V18z"/><path fill="#4CAF50" d="M3.5,8C2.67,8,2,8.67,2,9.5v7C2,17.33,2.67,18,3.5,18S5,17.33,5,16.5v-7C5,8.67,4.33,8,3.5,8z"/><path fill="#4CAF50" d="M20.5,8C19.67,8,19,8.67,19,9.5v7c0,0.83,0.67,1.5,1.5,1.5s1.5-0.67,1.5-1.5v-7C22,8.67,21.33,8,20.5,8z"/><path fill="#4CAF50" d="M15.529,2.16l1.301-1.3c0.199-0.2,0.199-0.51,0-0.71c-0.2-0.2-0.51-0.2-0.71,0l-1.479,1.48C13.85,1.23,12.95,1,12,1c-0.96,0-1.86,0.23-2.66,0.63L7.85,0.15c-0.2-0.2-0.51-0.2-0.71,0c-0.2,0.2-0.2,0.51,0,0.71l1.31,1.31C6.97,3.26,6,5.01,6,7h12C18,5.01,17.029,3.25,15.529,2.16z"/><rect x="9" y="4" fill="#FFFFFF" width="1" height="1"/><rect x="14" y="4" fill="#FFFFFF" width="1" height="1"/></svg>			</div>
			<div class="icon-set__id">
				<span>android</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4DB6AC" d="M19.956,3.974v16c0,1.1-0.899,2-2,2h-12c-1.1,0-2-0.9-2-2l0.02-12l5.98-6h8C19.057,1.974,19.956,2.874,19.956,3.974z"/><path opacity="0.5" fill="#26A69A" enable-background="new    " d="M19.956,3.974v16c0,1.1-0.899,2-2,2h-6v-20h6C19.057,1.974,19.956,2.874,19.956,3.974z"/></g><rect x="11" y="15" fill="#FFFFFF" width="2" height="2"/><rect x="11" y="8" fill="#FFFFFF" width="2" height="5"/></svg>			</div>
			<div class="icon-set__id">
				<span>sim_card_alert</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E1BEE7" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.11,4,6.6,5.64,5.35,8.04C2.34,8.36,0,10.91,0,14c0,3.311,2.69,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z"/><polygon fill="#7986CB" points="14,13 14,17 10,17 10,13 7,13 12,8 17,13 "/></svg>			</div>
			<div class="icon-set__id">
				<span>backup</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#42A5F5" d="M20.99,4.996v14c0,1.1-0.9,2-2,2h-14c-1.101,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C20.09,2.996,20.99,3.896,20.99,4.996z"/><path opacity="0.5" fill="#1E88E5" enable-background="new    " d="M20.99,4.996v14c0,1.1-0.9,2-2,2h-7.009v-18h7.009C20.09,2.996,20.99,3.896,20.99,4.996z"/></g><polygon fill="#CFD8DC" points="15,7 15,17 13,17 13,13 9,13 9,7 11,7 11,11 13,11 13,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>looks_4</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#CFD8DC" points="17.266,10.453 19.094,8.656 15.344,4.891 13.703,6.547 "/><polygon fill="#78909C" points="17.811,9.94 6.75,21 3,21 3,17.25 14.061,6.19 "/><path fill="#B0BEC5" d="M20.71,7.04c0.39-0.39,0.39-1.02,0-1.41l-2.34-2.34c-0.39-0.39-1.021-0.39-1.41,0l-1.83,1.83l3.75,3.75L20.71,7.04z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#B0BEC5" points="6.75,21 3,21 3,17.25 "/></svg>			</div>
			<div class="icon-set__id">
				<span>edit</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E53935" d="M19,4H5C3.89,4,3,4.9,3,6v12c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V6C21,4.9,20.1,4,19,4z"/><path fill="#ECEFF1" d="M11,11H9.5v-0.5h-2v3h2V13H11v1c0,0.55-0.45,1-1,1H7c-0.55,0-1-0.45-1-1v-4c0-0.55,0.45-1,1-1h3c0.55,0,1,0.45,1,1V11z"/><path fill="#ECEFF1" d="M18,11h-1.5v-0.5h-2v3h2V13H18v1c0,0.55-0.45,1-1,1h-3c-0.55,0-1-0.45-1-1v-4c0-0.55,0.45-1,1-1h3c0.55,0,1,0.45,1,1V11z"/></svg>			</div>
			<div class="icon-set__id">
				<span>closed_caption</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M19,3v12h4V3H19z"/><path fill="#D32F2F" d="M17,5v10c0,0.55-0.221,1.05-0.58,1.41L9.83,23l-0.97-0.96l-0.09-0.09c-0.27-0.271-0.44-0.65-0.44-1.06l0.03-0.32L9.31,16H3c-0.99,0-1.82-0.73-1.97-1.689C1.01,14.21,1,14.109,1,14l0.01-0.08L1,13.91V12c0-0.26,0.05-0.5,0.14-0.73l3.02-7.05C4.46,3.5,5.17,3,6,3h9C16.1,3,17,3.9,17,5z"/><rect x="19" y="3" fill="#C62828" width="4" height="12"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>thumb_down</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="2" y="10" fill="#90A4AE" width="11.083" height="2"/><rect x="2" y="6" fill="#78909C" width="12" height="2"/><polygon fill="#42A5F5" points="22,14 22,16 18,16 18,20 16,20 16,16 12,16 12,14 16,14 16,10 18,10 18,14 "/><rect x="2" y="14" fill="#78909C" width="8" height="2"/><polygon fill="#1E88E5" points="22,14 22,16 18,16 18,20 16.94,20 16.94,10 18,10 18,14 "/></svg>			</div>
			<div class="icon-set__id">
				<span>playlist_add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="10" y="16" fill="#5C6BC0" width="4" height="2"/><rect x="3" y="6" fill="#5C6BC0" width="18" height="2"/><rect x="6" y="11" fill="#9FA8DA" width="12" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_list</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path opacity="0.6" fill="#B0BEC5" d="M12.98,2.05v19.9C12.66,21.98,12.33,22,12,22C6.48,22,2,17.52,2,12C2,6.48,6.48,2,12,2c0.33,0,0.65,0.02,0.97,0.05H12.98z"/><path fill="none" d="M0,0h24v24H0V0z"/><path d="M13,4.07C14.029,4.2,15,4.52,15.87,5H13V4.07z"/><path d="M13,7h5.24c0.25,0.31,0.48,0.65,0.68,1H13V7z"/><path d="M13,10h6.74c0.08,0.33,0.15,0.66,0.189,1H13V10z"/><path d="M13,19.93V19h2.87C15,19.48,14.029,19.8,13,19.93z"/><path d="M18.24,17H13v-1h5.92C18.721,16.35,18.49,16.689,18.24,17z"/><path d="M19.74,14H13v-1h6.93C19.891,13.34,19.82,13.67,19.74,14z"/><path fill="#E57373" d="M20.66,7h-7.69V2.05C16.26,2.36,19.08,4.28,20.66,7z"/><path fill="#BA68C8" d="M21.79,10.011H13V7h7.66C21.19,7.92,21.57,8.93,21.79,10.011z"/><path fill="#4FC3F7" d="M22,12c0,0.34-0.02,0.67-0.05,1H13v-3h8.79C21.93,10.64,22,11.32,22,12z"/><path fill="#81C784" d="M21.95,12.98c0,0.01,0,0.01,0,0.02c-0.101,1.05-0.37,2.06-0.79,3H13v-3.02H21.95z"/><path fill="#FFF176" d="M21.16,16c-0.49,1.12-1.18,2.14-2.03,3H13v-3H21.16z"/><path fill="#FFB74D" d="M19.16,18.97C19.15,18.98,19.14,18.99,19.13,19c-1.59,1.63-3.75,2.72-6.149,2.95v-2.98H19.16z"/></svg>			</div>
			<div class="icon-set__id">
				<span>tonality</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M9,14.5h2V20l3.471-6.5H9.53L9,14.5z M13,12.5V7l-2.93,5.5l-0.54,1h4.941l0.529-1H13z"/><path fill="#FFEE58" d="M17,13.5v7.17C17,21.4,16.4,22,15.66,22H8.33C7.6,22,7,21.4,7,20.67V13.5h2.53L9,14.5h2V20l3.47-6.5H17z"/><polygon fill="#FFFFFF" fill-opacity="0.3" points="15,12.5 14.47,13.5 11,20 11,14.5 9,14.5 9.53,13.5 13,7 13,12.5 "/><path fill="#FFEE58" fill-opacity="0.3" d="M17,5.33v8.17h-2.53l0.53-1h-2V7l-3.47,6.5H7V5.33C7,4.6,7.6,4,8.33,4H10V2h4v2h1.67C16.4,4,17,4.6,17,5.33z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_charging_50</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M22,4v12c0,1.1-0.9,2-2,2H6l-4,4v-0.02l0.01-11.94V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><path fill="none" d="M24,0v24H0V10.04h2.01V4C2.01,2.9,2.9,2,4,2h16c1.1,0,2,0.9,2,2v6.04h1.31V0H24z"/><path opacity="0.15" fill="#FFFFFF" d="M22,4v6.04H2.01V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><rect x="7" y="9" fill="#546E7A" width="2" height="2"/><rect x="11" y="9" fill="#546E7A" width="2" height="2"/><rect x="15" y="9" fill="#546E7A" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>textsms</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#FFEE58" cx="5.023" cy="6.612" r="2.936"/><g id="Capa_2_1_"><g><path fill="#81C784" d="M13.974,5.964L4.984,17.948l18-0.031C22.984,17.917,14.056,5.964,13.974,5.964z"/><polygon fill="#A5D6A7" points="7,9.964 13.015,17.934 1,17.948 		"/></g></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>landscape</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M20,2H4C2.9,2,2,2.9,2,4v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2z"/><rect x="4" y="16" fill="#BBDEFB" width="4" height="4"/><rect x="4" y="10" fill="#BBDEFB" width="4" height="4"/><rect x="4" y="4" fill="#BBDEFB" width="4" height="4"/><rect x="10" y="16" fill="#BBDEFB" width="4" height="4"/><rect x="10" y="10" fill="#BBDEFB" width="4" height="4"/><rect x="10" y="4" fill="#BBDEFB" width="4" height="4"/><rect x="16" y="16" fill="#BBDEFB" width="4" height="4"/><rect x="16" y="10" fill="#BBDEFB" width="4" height="4"/><rect x="16" y="4" fill="#BBDEFB" width="4" height="4"/></svg>			</div>
			<div class="icon-set__id">
				<span>grid_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#F44336" d="M10.5,13.5V18C9.12,18,8,19.12,8,20.5V22c0,0-2.958,0-4,0c-1.1,0-2-0.9-2-2s0-4,0-4h1.5C4.88,16,6,14.88,6,13.5H10.5z M20.5,11H19V7c0-1.1-0.9-2-2-2h-4V3.5C13,2.12,11.88,1,10.5,1v12.5H23C23,12.12,21.88,11,20.5,11z"/><path fill="#D32F2F" d="M10.5,13.5H6C6,12.12,4.88,11,3.5,11H2c0,0,0.01-3.208,0.01-4C2.01,5.9,2.9,5,4,5h4V3.5C8,2.12,9.12,1,10.5,1V13.5z M13,20.5V22c0,0,2.875,0,4,0c1.1,0,2-0.9,2-2v-4h1.5c1.38,0,2.5-1.12,2.5-2.5H10.5V18C11.88,18,13,19.12,13,20.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>extension</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#F06292" d="M21,6h-2v9H6v2c0,0.55,0.45,1,1,1h11l4,4V7C22,6.45,21.55,6,21,6z"/><path fill="#FFB74D" d="M17,12V3c0-0.55-0.45-1-1-1H3C2.45,2,2,2.45,2,3v14l4-4h10C16.55,13,17,12.55,17,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>forum</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#6D4C41" d="M20,6h-3V4c0-1.11-0.89-2-2-2H9C7.89,2,7,2.89,7,4v2H4C2.89,6,2,6.89,2,8v11c0,1.11,0.89,2,2,2h16c1.11,0,2-0.89,2-2V8C22,6.89,21.11,6,20,6z M15,6H9V4h6V6z"/><rect x="4" y="17" fill="#6D4C41" width="16" height="2"/><polygon fill="#6D4C41" points="20,14 4,14 4,8 7,8 7,10 9,10 9,8 15,8 15,10 17,10 17,8 20,8 "/><path fill="#90A4AE" d="M17,4v1.98h-2V4H9v1.98H7V4c0-1.11,0.89-2,2-2h6C16.11,2,17,2.89,17,4z"/><rect x="7" y="5.328" fill="#90A4AE" width="1.984" height="4.672"/><rect x="15.016" y="5.328" fill="#90A4AE" width="1.984" height="4.672"/><rect x="1.984" y="13.969" fill="#B39DDB" width="19.984" height="3.063"/><rect x="9" y="2" opacity="0.5" fill="#78909C" width="5.98" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>wallet_travel</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#4CAF50" d="M15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33v15.33C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V5.33C17,4.6,16.4,4,15.67,4z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_full</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#3F51B5" points="12,21.07 21,14.07 12,7.07 3,14.07 "/><polygon fill="#BA68C8" points="12,16 21,9 12,2 3,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>layers</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M5,15H3v4c0,1.1,0.9,2,2,2h4v-2H5V15z"/><path fill="#546E7A" d="M5,5h4V3H5C3.9,3,3,3.9,3,5v4h2V5z"/><path fill="#546E7A" d="M19,3h-4v2h4v4h2V5C21,3.9,20.1,3,19,3z"/><path fill="#546E7A" d="M19,19h-4v2h4c1.1,0,2-0.9,2-2v-4h-2V19z"/><path fill="#7E57C2" d="M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_center_focus</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#37474F" d="M21,7v12c0,1.1-0.9,2-2,2H5c-1.11,0-2-0.9-2-2V5c0-1.1,0.89-2,2-2h12L21,7z"/><path opacity="0.5" fill="#455A64" d="M11.96,3v18H5c-1.11,0-2-0.9-2-2V5c0-1.1,0.89-2,2-2H11.96z"/><path fill="#B0BEC5" d="M15,16c0,1.66-1.34,3-3,3h-0.05C10.31,18.97,9,17.641,9,16s1.31-2.97,2.95-3H12C13.66,13,15,14.34,15,16z"/><rect x="5" y="5" fill="#B0BEC5" width="10" height="4"/><path fill="#90A4AE" d="M15,16c0,1.66-1.34,3-3,3h-0.05v-6H12C13.66,13,15,14.34,15,16z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>save</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#546E7A" points="19.5,20.29 18.79,21 12,18 5.21,21 4.5,20.29 12,2 "/><polygon opacity="0.6" fill="#455A64" points="19.5,20.29 18.79,21 12,18 12,2 "/></svg>			</div>
			<div class="icon-set__id">
				<span>navigation</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><polygon fill="#78909C" points="14.5,12 6,18 6,6 	"/><rect x="16" y="6" fill="#546E7A" width="2" height="12"/><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><polygon fill="#78909C" points="14.5,12 6,12 6,6 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>skip_next</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><g><path fill="#ECEFF1" d="M21.025,4.932v14c0,1.1-0.912,2-2.023,2h-7.045v-18h7.045C20.113,2.932,21.025,3.832,21.025,4.932z"/><path fill="#263238" d="M11.973,2.932L4.968,2.984c-1.089,0-1.98,0.9-1.98,2v14c0,1.1,0.775,1.994,1.865,1.994h7.156l-0.143-2.004l0.112-7.824l0,0L11.973,2.932z"/><polygon fill="#ECEFF1" points="12.025,10.979 12.009,20.979 4.853,20.979 		"/><polygon fill="#263238" points="11.978,10.979 11.994,20.979 19.15,20.979 		"/></g><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><rect x="10.98" y="1.02" fill="#64B5F6" width="2.02" height="21.95"/><rect x="11.98" y="1.02" opacity="0.5" fill="#42A5F5" width="1.049" height="21.95"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>compare</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#00695C" points="15,7.5 15,2 9,2 9,7.5 12,10.5 "/><polygon fill="#3F51B5" points="7.5,9 2,9 2,15 7.5,15 10.5,12 "/><polygon fill="#1976D2" points="9,16.5 9,22 15,22 15,16.5 12,13.5 "/><polygon fill="#512DA8" points="16.5,9 13.5,12 16.5,15 22,15 22,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>games</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="#9575CD" d="M12,1c-4.97,0-9,4.03-9,9v7c0,1.308,0.836,2.405,2,2.817V12v-2c0-3.87,3.13-7,7-7s7,3.13,7,7v2v7.817c1.164-0.412,2-1.51,2-2.817v-7C21,5.03,16.971,1,12,1z"/><path fill="#3F51B5" d="M9,20v-8H5v7.817C5.314,19.929,5.647,20,6,20H9z"/><path fill="#3F51B5" d="M15,12v8h3c0.353,0,0.686-0.071,1-0.183V12H15z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>headset</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#3F51B5" d="M3,5v4h2V5h4V3H5C3.9,3,3,3.9,3,5z"/><path fill="#9575CD" d="M5,15H3v4c0,1.1,0.9,2,2,2h4v-2H5V15z"/><path fill="#3F51B5" d="M19,19h-4v2h4c1.1,0,2-0.9,2-2v-4h-2V19z"/><path fill="#9575CD" d="M19,3h-4v2h4v4h2V5C21,3.9,20.1,3,19,3z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_free</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="10" fill="#EF5350" width="4" height="4"/><rect x="4" y="15" fill="#42A5F5" width="4" height="4"/><rect x="4" y="5" fill="#AB47BC" width="4" height="4"/><rect x="9" y="10" fill="#EF5350" width="12" height="4"/><rect x="9" y="15" fill="#42A5F5" width="12" height="4"/><rect x="9" y="5" fill="#AB47BC" width="12" height="4"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="9" y="5" fill="#BA68C8" width="5.95" height="4"/><rect x="9" y="10" fill="#E57373" width="5.95" height="4"/><rect x="9" y="15" fill="#64B5F6" width="5.95" height="4"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_list</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4DD0E1" d="M22,4v12c0,1.1-0.9,2-2,2H6l-4,4l0.01-11V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><rect x="6" y="12" fill="#CFD8DC" width="12" height="2"/><path opacity="0.3" fill="#26C6DA" enable-background="new    " d="M22,4v7H2.01V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><rect x="6" y="9" fill="#CFD8DC" width="12" height="2"/><rect x="6" y="6" fill="#CFD8DC" width="12" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>message</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#D1C4E9" d="M22,6v12c0,1.1-0.9,2-2,2H4c-1.1,0-2-0.9-2-2L2.01,6C2.01,4.9,2.9,4,4,4h16C21.1,4,22,4.9,22,6z"/><polygon fill="#B39DDB" points="22,6 21.979,7.938 12,13 2,7.917 2,6.042 12,11 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>mail</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.6" fill="#90A4AE" d="M22,5.72l-4.6-3.86l-1.291,1.53l4.601,3.86L22,5.72z M8.02,3.28L6.6,1.86L1.87,5.83l1.42,1.42L8.02,3.28z"/><path fill="#90A4AE" d="M12,4c-4.97,0-9,4.03-9,9s4.02,9,9,9c4.971,0,9-4.03,9-9S16.971,4,12,4z"/><path fill="#E0E0E0" d="M12,20c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,20,12,20z"/><polygon fill="#9FA8DA" points="20.09,22 1.65,3.57 2.92,2.29 21.359,20.73 "/></svg>			</div>
			<div class="icon-set__id">
				<span>alarm_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#5C6BC0" d="M20,4h-4l-4-4L8,4H4C2.9,4,2,4.9,2,6v14c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4z"/><polygon fill="#5C6BC0" points="20,20 4,20 4,6 8.52,6 12.04,2.5 15.52,6 20,6 "/><polyline fill="#9FA8DA" points="18,8 6,8 6,18 18,18 "/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_frames</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#0097A7" d="M10,13H5V7h6L10,13z M19,7h-6v6h5L19,7z"/><path fill="#00BCD4" d="M9,17H6l5-10v6L9,17z M19,13V7l-5,10h3L19,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_quote</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path opacity="0.6" fill="#FFA000" d="M20,0H4v2h16V0z M4,24h16v-2H4V24z"/><path fill="#FFA000" d="M20,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4z"/><path fill="#FF6F00" d="M12,6.75c1.24,0,2.25,1.01,2.25,2.25s-1.01,2.25-2.25,2.25S9.75,10.24,9.75,9S10.76,6.75,12,6.75z M17,17H7v-1.5c0-1.67,3.33-2.5,5-2.5s5,0.83,5,2.5V17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>contacts</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#78909C" d="M23.99,19.96v-0.01C22.98,14.96,19.98,10,13,9V5l-7,7l7,7v-4.1c5,0,8.5,1.6,11,5.1C24,19.99,23.99,19.97,23.99,19.96z M7,5l-7,7l7,7v-3l-4-4l4-4V5z"/></g><polygon fill="#546E7A" points="7,8 3,12 7,16 7,19 0,12 7,5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>reply_all</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#DCE775" d="M20,18v1H3v-1l2-2v-5.5c0-3.07,2.13-5.64,5-6.32V3.5C10,2.67,10.67,2,11.5,2S13,2.67,13,3.5v0.68c0.24,0.06,0.47,0.12,0.69,0.21C16.21,5.29,18,7.68,18,10.5V16L20,18z"/><path fill="#DCE775" d="M11.5,22c0.141,0,0.27-0.01,0.4-0.04c0.649-0.13,1.189-0.58,1.439-1.18c0.1-0.24,0.16-0.5,0.16-0.78h-4C9.5,21.1,10.4,22,11.5,22z"/><polygon fill="#FDD835" points="20,18 20,19 3,19 3,18 4.96,16.04 18.04,16.04 	"/><path fill="#FDD835" d="M13.69,4.39c-0.07-0.01-0.15-0.03-0.24-0.06C11.85,3.83,10.52,4.05,10,4.17V3.5C10,2.67,10.67,2,11.5,2S13,2.67,13,3.5v0.68C13.24,4.24,13.47,4.3,13.69,4.39z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>notifications</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#82B1FF" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.11,4,6.6,5.64,5.35,8.04C2.34,8.36,0,10.91,0,14c0,3.311,2.69,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z"/></svg>			</div>
			<div class="icon-set__id">
				<span>cloud</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#64B5F6" d="M22,4v12c0,1.1-0.9,2-2,2H6l-4,4V4c0-1.1,0.9-2,2-2h16C21.1,2,22,2.9,22,4z"/><path fill="none" d="M22,4v12c0,1.1-0.9,2-2,2H6l-4,4V4c0-1.1,0.9-2,2-2h16C21.1,2,22,2.9,22,4z"/><path opacity="0.25" fill="#90CAF9" d="M22,4v7.96H2V4c0-1.1,0.9-2,2-2h16C21.1,2,22,2.9,22,4z"/></svg>			</div>
			<div class="icon-set__id">
				<span>messenger</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFEE58" d="M21,5v14c0,1.1-0.9,2-2,2H5c-1.1,0-2-0.9-2-2V5c0-1.1,0.9-2,2-2h14C20.1,3,21,3.9,21,5z"/><path opacity="0.8" fill="#FDD835" d="M21,5v14c0,1.1-0.9,2-2,2h-7.08V3H19C20.1,3,21,3.9,21,5z"/><polygon fill="#455A64" points="15,9 11,9 11,11 15,11 15,13 11,13 11,15 15,15 15,17 9,17 9,7 15,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>explicit</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#303F9F" d="M10,11.999l2-2L9.64,7.64C9.87,7.14,10,6.589,10,5.999c0-2.21-1.79-4-4-4s-4,1.79-4,4s1.79,4,4,4c0.59,0,1.14-0.13,1.64-0.359L10,11.999z M8,5.999c0,1.109-0.9,2-2,2s-2-0.891-2-2s0.9-2,2-2S8,4.89,8,5.999z"/><path fill="#1A237E" d="M12,14l-2-2l-2.359,2.359C7.141,14.13,6.591,14,6.001,14c-2.21,0-4,1.79-4,4s1.79,4,4,4s4-1.79,4-4c0-0.59-0.13-1.141-0.36-1.641L12,14z M8,17.999c0,1.11-0.9,2-2,2s-2-0.89-2-2s0.9-2,2-2S8,16.889,8,17.999z"/><path fill="#616161" d="M22,3h-3l-7,7l-2,2l0,0l2,2L22,4V3z"/><path fill="#9E9E9E" d="M22,20L12,10l-2,2l0,0l2,2l7,7h3V20z M12,12.5c-0.28,0-0.5-0.22-0.5-0.5s0.22-0.5,0.5-0.5s0.5,0.22,0.5,0.5S12.28,12.5,12,12.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>content_cut</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="none" d="M0,0v24h24V0H0z M17.63,18.16C17.27,18.67,16.67,19,16,19l-2.964-0.003l-1.615,2.276l1.615-2.276L5,18.99c-1.1,0-2-0.891-2-1.99V7c0-1.1,0.9-1.99,2-1.99l8.036-0.007l-1.615-2.276l1.615,2.276L16,5c0.67,0,1.27,0.33,1.63,0.84L22,12L17.63,18.16z"/><path fill="#F57C00" d="M13.036,5.002L5,5.01C3.9,5.01,3,5.9,3,7v10c0,1.1,0.9,1.99,2,1.99l8.036,0.007L18,12L13.036,5.002z"/><path fill="#FF5722" d="M16,5l-2.964,0.002L18,12l-4.964,6.997L16,19c0.67,0,1.27-0.33,1.63-0.84L22,12l-4.37-6.16C17.27,5.33,16.67,5,16,5z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>label</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4CAF50" d="M19,13h-6v6h-2v-6H5v-2h6V5h2v6h6V13z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4FC3F7" d="M15,3c-4.96,0-9,4.04-9,9s4.04,9,9,9s9-4.04,9-9S19.96,3,15,3z"/><path fill="#2196F3" d="M15,19c-3.86,0-7-3.141-7-7c0-3.86,3.14-7,7-7c3.859,0,7,3.14,7,7C22,15.859,18.859,19,15,19z"/><polygon fill="#CFD8DC" points="16,8 14,8 14,11 11,11 11,13 14,13 14,16 16,16 16,13 19,13 19,11 16,11 "/><path fill="#2196F3" d="M2,12c0-2.79,1.64-5.2,4.01-6.32V3.52C2.52,4.76,0,8.09,0,12s2.52,7.24,6.01,8.48v-2.16C3.64,17.2,2,14.79,2,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>control_point_duplicate</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><polygon fill="none" points="21,17 21,10.09 20.94,10.12 20.05,13.17 20.8,17.42 24,17.28 24,24 0,24 0,0 24,0 24,12.12 23,9.0723,17 	"/><polygon fill="#0277BD" points="19,13.18 19,17.18 12,21 5,17.18 5,13.18 12,17 	"/><polygon fill="#0288D1" points="23,9 23,17 21,17 21,10.09 20.94,10.12 12,15 1,9 12,3 22.97,8.98 	"/></g><g id="Capa_2"><polygon fill="#0277BD" points="23,9.07 23,17 21,17 21,10.09 20.94,10.12 20.95,10.08 22.97,8.98 	"/><polygon opacity="0.4" fill="#0277BD" points="12,3 12,15 1,9 	"/><polygon opacity="0.4" fill="#01579B" points="5,13.18 12,17 12,21 5,17.18 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>school</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="2" y="16" fill="#64B5F6" width="20" height="4"/><rect x="4" y="17" fill="#B2EBF2" width="2" height="2"/><rect x="2" y="4" fill="#F06292" width="20" height="4"/><rect x="4" y="5" fill="#F8BBD0" width="2" height="2"/><rect x="2" y="10" fill="#9575CD" width="20" height="4"/><rect x="4" y="11" fill="#D1C4E9" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>storage</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26C6DA" d="M20,2H4C2.9,2,2,2.9,2,4v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2z M20,20H4V4h16V20z"/><path fill="#00ACC1" d="M18,6v12H6V6h4v2H8v8h8V8h-3v2.28c0.05,0.03,0.09,0.06,0.14,0.09c0.051,0.04,0.101,0.08,0.15,0.11c0.11,0.09,0.2,0.19,0.29,0.3C13.84,11.11,14,11.54,14,12c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2c0-0.74,0.4-1.37,1-1.72V8c0-1.1,0.9-2,2-2H18z"/><path fill="none" d="M18,6v12H6V6h4v2H8v8h8V8h-3v2.28c0.05,0.03,0.09,0.06,0.14,0.09c0.051,0.04,0.101,0.08,0.15,0.11c0.11,0.09,0.2,0.19,0.29,0.3C13.84,11.11,14,11.54,14,12c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2c0-0.74,0.4-1.37,1-1.72V8c0-1.1,0.9-2,2-2H18z"/><path fill="none" d="M18,6v12H6V6h4v2H8v8h8V8h-3v2.28c0.05,0.03,0.09,0.06,0.14,0.09c0.051,0.04,0.101,0.08,0.15,0.11c0.11,0.09,0.2,0.19,0.29,0.3C13.84,11.11,14,11.54,14,12c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2c0-0.74,0.4-1.37,1-1.72V8c0-1.1,0.9-2,2-2H18z"/><path fill="none" d="M0,0v24h24V0H0z M4,4h16v16H4V4z"/><path fill="#26C6DA" d="M14,12c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2c0-0.74,0.4-1.37,1-1.72v-0.02c0.29-0.16,0.62-0.24,0.97-0.24c0.37,0,0.72,0.1,1.03,0.26c0.05,0.03,0.09,0.06,0.14,0.09c0.051,0.04,0.101,0.08,0.15,0.11c0.11,0.09,0.2,0.19,0.29,0.3C13.84,11.11,14,11.54,14,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>nfc</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26C6DA" d="M21,6h-2v9H6v2c0,0.55,0.45,1,1,1h11l4,4V7C22,6.45,21.55,6,21,6z"/><path fill="#66BB6A" d="M17,12V3c0-0.55-0.45-1-1-1H3C2.45,2,2,2.45,2,3v14l4-4h10C16.55,13,17,12.55,17,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>question_answer</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26C6DA" d="M4,6h18V4H4C2.9,4,2,4.9,2,6v11H0v3h14v-3H4V6z"/><path fill="#78909C" d="M23,8h-6c-0.55,0-1,0.45-1,1v10c0,0.55,0.45,1,1,1h6c0.55,0,1-0.45,1-1V9C24,8.45,23.55,8,23,8z"/><rect x="18" y="10" fill="#BBDEFB" width="4" height="7"/></svg>			</div>
			<div class="icon-set__id">
				<span>phonelink</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#5C6BC0" points="10,20 5,20 5,22 10,22 10,24 13,21 10,18 "/><rect x="14" y="20" fill="#AB47BC" width="5" height="2"/><path fill="#78909C" d="M19,2v14c0,1.1-0.9,2-2,2H7c-1.1,0-2-0.9-2-2V2c0-1.1,0.9-2,2-2h10C18.1,0,19,0.9,19,2z"/><path opacity="0.4" fill="#546E7A" enable-background="new    " d="M19,2v14c0,1.1-0.9,2-2,2h-5V0h5C18.1,0,19,0.9,19,2z"/><path fill="#FFFFFF" d="M12,6c-1.11,0-2-0.9-2-2s0.89-2,1.99-2c1.1,0,2,0.9,2,2C14,5.1,13.1,6,12,6z"/></svg>			</div>
			<div class="icon-set__id">
				<span>camera_rear</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#00ACC1" d="M23,3v14c0,1.1-0.9,2-2,2H7c-1.1,0-2-0.9-2-2V3c0-1.1,0.9-2,2-2h14C22.1,1,23,1.9,23,3z"/><path fill="#0097A7" d="M3,5H1v16c0,1.1,0.9,2,2,2h16v-2H3V5z"/><path opacity="0.5" fill="#0097A7" d="M23,3v14c0,1.1-0.9,2-2,2h-7.01V1H21C22.1,1,23,1.9,23,3z"/></g><polygon fill="#CFD8DC" points="14,15 16,15 16,5 12,5 12,7 14,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_1</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="7" fill="#F06292" width="14" height="2"/><rect x="3" y="11" fill="#9575CD" width="14" height="2"/><rect x="3" y="15" fill="#64B5F6" width="14" height="2"/><rect x="19" y="15" fill="#B2EBF2" width="2" height="2"/><rect x="19" y="7" fill="#F8BBD0" width="2" height="2"/><rect x="19" y="11" fill="#D1C4E9" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>toc</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M16,11c1.66,0,2.99-1.34,2.99-3S17.66,5,16,5s-3,1.34-3,3S14.34,11,16,11z"/><path fill="#78909C" d="M15.995,13.017c-2.33,0-7,1.17-7,3.5v2.5h14v-2.5C22.995,14.187,18.325,13.017,15.995,13.017z"/><path fill="#90A4AE" d="M8,11c1.66,0,2.99-1.34,2.99-3S9.66,5,8,5S5,6.34,5,8S6.34,11,8,11z"/><path fill="#90A4AE" d="M8,13.017c-2.33,0-7,1.17-7,3.5v2.5h14v-2.5C15,14.187,10.33,13.017,8,13.017z"/></svg>			</div>
			<div class="icon-set__id">
				<span>people</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#FFD54F" d="M7,6v12c0,0.55-0.45,1-1,1H3c-0.55,0-1-0.45-1-1V6c0-0.55,0.45-1,1-1h3C6.55,5,7,5.45,7,6z"/><path fill="#FFD54F" d="M21,6v12c0,0.55-0.45,1-1,1h-3c-0.55,0-1-0.45-1-1V6c0-0.55,0.45-1,1-1h3C20.55,5,21,5.45,21,6z"/><path fill="#FFD54F" d="M14,6v12c0,0.55-0.45,1-1,1h-3c-0.55,0-1-0.45-1-1V6c0-0.55,0.45-1,1-1h3C13.55,5,14,5.45,14,6z"/><path fill="#FFF59D" d="M7,6v3.04H2V6c0-0.55,0.45-1,1-1h3C6.55,5,7,5.45,7,6z"/><path fill="#FFF59D" d="M14,6v3.04H9V6c0-0.55,0.45-1,1-1h3C13.55,5,14,5.45,14,6z"/><path fill="#FFF59D" d="M21,6v3.04h-5V6c0-0.55,0.45-1,1-1h3C20.55,5,21,5.45,21,6z"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>view_week</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#4DD0E1" d="M11.917,2c-5.52,0-10,4.48-10,10c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C21.917,6.48,17.437,2,11.917,2z"/><circle fill="#006064" cx="12" cy="6.75" r="2.25"/><path fill="#006064" d="M17,15.061v2.5c-0.45,0.409-0.96,0.77-1.5,1.049v-0.68c0-0.34-0.17-0.649-0.46-0.92c-0.649-0.619-1.89-1.02-3.04-1.02c-0.96,0-1.96,0.279-2.65,0.729l-0.17,0.12l-0.21,0.17c0.78,0.471,1.63,0.721,2.54,0.82l1.33,0.15c0.37,0.039,0.66,0.359,0.66,0.75c0,0.289-0.16,0.529-0.4,0.66c-0.279,0.149-0.64,0.09-0.95,0.09c-0.35,0-0.69-0.011-1.03-0.051c-0.5-0.06-0.99-0.17-1.46-0.33c-0.49-0.16-0.97-0.38-1.42-0.64c-0.22-0.13-0.44-0.271-0.65-0.43l-0.31-0.24C7.24,17.77,7,17.609,7,17.561v-4.28c0-1.58,2.63-2.78,5-2.78s5,1.2,5,2.78V15.061z"/><path fill="none" d="M0,0h24v24H0V0z"/><circle fill="#4DD0E1" cx="12" cy="13.49" r="1.5"/></svg>			</div>
			<div class="icon-set__id">
				<span>supervisor_account</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#00ACC1" d="M12,3C6.48,3,2,7.48,2,13c0,3.7,2.01,6.92,4.99,8.65l1-1.73C5.61,18.53,4,15.96,4,13c0-4.42,3.58-8,8-8s8,3.58,8,8c0,2.96-1.609,5.53-4,6.92l1,1.73c2.99-1.73,5-4.95,5-8.65C22,7.48,17.52,3,12,3z M12,7c-3.31,0-6,2.69-6,6c0,2.22,1.21,4.15,3,5.189l1-1.739c-1.19-0.7-2-1.97-2-3.45c0-2.21,1.79-4,4-4s4,1.79,4,4c0,1.48-0.811,2.75-2,3.45l1,1.739c1.79-1.039,3-2.971,3-5.189C18,9.69,15.311,7,12,7z M12.089,11c-1.09,0-1.99,0.89-2,1.98c0,0.01,0,0.01,0,0.02c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2c0-0.01,0-0.01,0-0.02C14.079,11.89,13.179,11,12.089,11z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>wifi_tethering</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#E64A19" d="M19,2h-2l-3.2,9h1.9l0.7-2H19.6l0.7,2h1.9L19,2z M16.85,7.65L18,4l1.15,3.65H16.85z"/><path fill="#FFD600" d="M3,2v12h3v9l7-12H9l4-9H3z"/><path fill="#FFC107" d="M3,14L8.5,2H3V14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>flash_auto</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#29B6F6" d="M12,2h-0.08C6.44,2.04,2,6.51,2,12s4.44,9.96,9.92,10H12c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M11,16H9V8h2V16z M15,16h-2V8h2V16z"/><path opacity="0.4" fill="#039BE5" d="M22,12c0,5.52-4.48,10-10,10h-0.08V2H12C17.52,2,22,6.48,22,12z"/><rect x="9" y="8" fill="#CFD8DC" width="2" height="8"/><rect x="13" y="8" fill="#CFD8DC" width="2" height="8"/></svg>			</div>
			<div class="icon-set__id">
				<span>pause_circle_fill</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#6D4C41" d="M4,6H2v14c0,1.1,0.9,2,2,2h14v-2H4V6z"/><path fill="#8D6E63" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path fill="#6D4C41" d="M22,4v12c0,1.1-0.9,2-2,2h-6.04V2H20C21.1,2,22,2.9,22,4z"/><rect x="9" y="9" fill="#FFFFFF" width="10" height="2"/><rect x="9" y="13" fill="#FFFFFF" width="6" height="2"/><rect x="9" y="5" fill="#FFFFFF" width="10" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>my_library_books</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="8.998" y="2.995" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -3.9706 12.4142)" fill="#B2EBF2" width="8.005" height="16.009"/><path fill="#78909C" d="M23.25,12.77L13.68,3.2l-2.45-2.45c-0.59-0.59-1.54-0.59-2.12,0L2.75,7.11c-0.59,0.59-0.59,1.54,0,2.12l12.02,12.02c0.591,0.59,1.54,0.59,2.12,0l6.36-6.36C23.84,14.3,23.84,13.35,23.25,12.77z M15.83,19.49L4.51,8.17l5.66-5.66l11.32,11.32L15.83,19.49z"/><path fill="#9575CD" d="M8.47,20.48C5.2,18.939,2.86,15.76,2.5,12H1c0.51,6.16,5.66,11,11.95,11l0.659-0.03L9.8,19.15L8.47,20.48z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFA726" d="M21,3V2.5C21,1.12,19.88,0,18.5,0S16,1.12,16,2.5V3c-0.55,0-1,0.45-1,1v4c0,0.55,0.45,1,1,1h5c0.55,0,1-0.45,1-1V4C22,3.45,21.55,3,21,3z M16.8,3V2.5c0-0.94,0.76-1.7,1.7-1.7s1.7,0.76,1.7,1.7V3H16.8z"/><path fill="#FFCC80" d="M21,2.5V3h-0.8V2.5c0-0.94-0.76-1.7-1.7-1.7s-1.7,0.76-1.7,1.7V3H16V2.5C16,1.12,17.12,0,18.5,0S21,1.12,21,2.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>screen_lock_rotation</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#4DB6AC" d="M20,7H4l-1,5v2h1v6h10v-6h4v6h2v-6h1v-2L20,7z M12,18H6v-4h6V18z M4,4v2h16V4H4z"/></g><g id="Capa_2"><polygon fill="#EF5350" points="21,12 21,14 3,14 3,12 4,7 20,7 20.98,11.9 	"/><polygon fill="#4DB6AC" points="14,14 14,20 4,20 4,14 6,14 6,18 12,18 12,14 	"/><rect x="18" y="14" fill="#4DB6AC" width="2" height="6"/></g><polygon fill="#E53935" points="21,12 21,14 20,14 20,14.02 4,14.02 4,14 3,14 3,12 3.02,11.9 20.98,11.9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>store</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#607D8B" d="M9,2L7.17,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6c0-1.1-0.9-2-2-2h-3.17L15,2H9z"/><circle fill="#90A4AE" cx="12" cy="12" r="6"/><circle fill="#1A237E" cx="12" cy="12" r="3.2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>camera_alt</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1A237E" d="M18,3v2h-2V3H8v2H6V3H4v18h2v-2h2v2h8v-2h2v2h2V3H18z M8,17H6v-2h2V17z M8,13H6v-2h2V13z M8,9H6V7h2V9zM18,17h-2v-2h2V17z M18,13h-2v-2h2V13z M18,9h-2V7h2V9z"/><rect x="4" y="3" opacity="0.5" fill="#1A237E" width="16" height="18"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_movies</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#78909C" points="7,7 17,7 17,10 21,6 17,2 17,5 5,5 5,11 7,11 "/><polygon fill="#78909C" points="17,17 7,17 7,14 3,18 7,22 7,19 19,19 19,13 17,13 "/><polygon fill="#039BE5" points="13,15 13,9 12,9 10,10 10,11 11.5,11 11.5,15 "/></svg>			</div>
			<div class="icon-set__id">
				<span>repeat_one</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#42A5F5" d="M20.943,5.019v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C20.043,3.019,20.943,3.919,20.943,5.019z"/><path opacity="0.5" fill="#1E88E5" enable-background="new    " d="M20.943,5.019v14c0,1.1-0.9,2-2,2h-7.009v-18h7.009C20.043,3.019,20.943,3.919,20.943,5.019z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#CFD8DC" points="14,7 14,17 12,17 12,9 10,9 10,7 "/></svg>			</div>
			<div class="icon-set__id">
				<span>looks_1</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#E57373" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><path fill="#F44336" d="M12,4c-4.417,0-8,3.583-8,8c0,4.414,3.583,8,8,8c4.414,0,8-3.586,8-8C20,7.583,16.414,4,12,4z"/><path fill="#CFD8DC" d="M13,17h-2v-2h2V17z M13,13h-2V7h2V13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>error</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path d="M17,15h2V7c0-1.1-0.9-2-2-2H9v2h8V15z"/><path fill="#9575CD" d="M7,17V1H5v16c0,1.1,0.9,2,2,2h16v-2H7z"/><path fill="#3F51B5" d="M17,23h2V7c0-1.1-0.9-2-2-2H1v2h16V23z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#26A69A" d="M19.939,21.12c-1.1-2.94-1.64-6.03-1.64-9.12s0.55-6.18,1.64-9.12C19.98,2.77,20,2.66,20,2.57C20,2.23,19.77,2,19.37,2H4.63C4.23,2,4,2.23,4,2.57c0,0.1,0.02,0.2,0.06,0.31C5.16,5.82,5.71,8.91,5.71,12s-0.55,6.18-1.64,9.12C4.02,21.23,4,21.34,4,21.43C4,21.76,4.23,22,4.63,22h14.75c0.39,0,0.63-0.24,0.63-0.57C20,21.33,19.98,21.23,19.939,21.12zM6.54,20c0.77-2.6,1.16-5.28,1.16-8S7.31,6.6,6.54,4h10.91c-0.771,2.6-1.16,5.28-1.16,8s0.39,5.4,1.16,8H6.54z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>panorama_vertical</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#7986CB" d="M18,2h-8L4.02,8L4,20c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V4C20,2.9,19.1,2,18,2z"/><rect x="10" y="4" fill="#CFD8DC" width="2" height="4"/><rect x="13" y="4" fill="#CFD8DC" width="2" height="4"/><rect x="16" y="4" fill="#CFD8DC" width="2" height="4"/></svg>			</div>
			<div class="icon-set__id">
				<span>sd_storage</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path d="M12,2C7.58,2,4,2.5,4,6v9.5C4,17.43,5.57,19,7.5,19L6,20.5V21h12v-0.5L16.5,19c1.93,0,3.5-1.57,3.5-3.5V6C20,2.5,16.42,2,12,2z M7.5,17C6.67,17,6,16.33,6,15.5S6.67,14,7.5,14S9,14.67,9,15.5S8.33,17,7.5,17z M11,11H6V6h5V11z M16.5,17c-0.83,0-1.5-0.67-1.5-1.5s0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5S17.33,17,16.5,17z M18,11h-5V6h5V11z"/><g><path fill="#37474F" d="M19.992,6.003v9.5c0,1.93-1.57,3.5-3.5,3.5l1.5,1.5v0.5h-12v-0.5l1.5-1.5c-1.93,0-3.5-1.57-3.5-3.5v-9.5c0-3.5,3.58-4,8-4C16.412,2.003,19.992,2.503,19.992,6.003z"/><circle fill="#FFFFFF" cx="7.493" cy="15.503" r="1.5"/><rect x="5.993" y="6.003" fill="#66BB6A" width="5" height="5"/><circle fill="#FFFFFF" cx="16.492" cy="15.503" r="1.5"/><rect x="12.992" y="6.003" fill="#66BB6A" width="5" height="5"/><polygon fill="#DCE775" points="17.992,20.503 17.992,21.003 5.993,21.003 5.993,20.503 7.443,19.053 16.543,19.053 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>directions_subway</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0-1h24v24H0V-1z"/><path fill="#0D47A1" d="M11.99,1C6.47,1,2,5.48,2,11c0,5.52,4.47,10,9.99,10C17.52,21,22,16.52,22,11C22,5.48,17.52,1,11.99,1z"/><circle fill="#0D47A1" cx="12" cy="11" r="8"/><circle fill="#64B5F6" cx="15.5" cy="8.5" r="1.5"/><circle fill="#64B5F6" cx="8.5" cy="8.5" r="1.5"/><path fill="#64B5F6" d="M12,17c2.609,0,4.83-1.67,5.65-4H6.35C7.17,15.33,9.39,17,12,17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_alt</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#0097A7" d="M4.59,6.89c0.7-0.71,1.4-1.35,1.71-1.22c0.5,0.2,0,1.03-0.3,1.52c-0.25,0.42-2.86,3.89-2.86,6.31c0,1.28,0.48,2.34,1.34,2.98c0.75,0.56,1.74,0.729,2.64,0.459c1.07-0.31,1.95-1.399,3.06-2.77c1.21-1.49,2.83-3.44,4.08-3.44c1.631,0,1.65,1.01,1.76,1.79c-3.779,0.641-5.379,3.67-5.379,5.371c0,1.699,1.44,3.09,3.209,3.09c1.631,0,4.291-1.33,4.69-6.101H21v-2.5h-2.471c-0.149-1.65-1.09-4.2-4.029-4.2c-2.25,0-4.18,1.91-4.94,2.84c-0.58,0.73-2.06,2.48-2.29,2.72c-0.25,0.3-0.68,0.84-1.11,0.84c-0.45,0-0.72-0.83-0.36-1.92c0.35-1.09,1.4-2.86,1.85-3.52C8.43,8,8.95,7.22,8.95,5.86C8.95,3.69,7.31,3,6.44,3C5.12,3,3.97,4,3.72,4.25C3.36,4.61,3.06,4.91,2.84,5.18L4.59,6.89z M13.88,18.55c-0.31,0-0.739-0.26-0.739-0.72c0-0.6,0.729-2.2,2.869-2.76C15.71,17.76,14.58,18.55,13.88,18.55z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>gesture</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#78909C" d="M7.11,8.53C6.58,9.28,6.23,10.13,6.09,11H4.07C4.24,9.61,4.8,8.27,5.7,7.11l0.6,0.6L7.11,8.53z"/><path fill="#78909C" d="M7.1,15.47l-0.48,0.48l-0.93,0.94c-0.9-1.16-1.45-2.5-1.62-3.891h2.02C6.23,13.88,6.58,14.72,7.1,15.47z"/><path fill="#78909C" d="M11,17.9v2.029c-1.39-0.171-2.74-0.711-3.9-1.609l0.79-0.801l0.65-0.649C9.29,17.41,10.13,17.75,11,17.9z"/><path fill="#78909C" d="M20,12c0,4.08-3.05,7.439-7,7.93v-2.02c2.84-0.48,5-2.94,5-5.91s-2.16-5.43-5-5.91V10L8.45,5.55l0.04-0.03L13,1v3.07C16.95,4.56,20,7.92,20,12z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>rotate_left</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><path fill="#AB47BC" d="M20.87,14.37c-0.14-0.28-0.351-0.53-0.63-0.74c-0.28-0.21-0.61-0.39-1.01-0.53c-0.4-0.14-0.852-0.27-1.352-0.38c-0.352-0.069-0.641-0.149-0.87-0.229c-0.229-0.08-0.409-0.16-0.55-0.25c-0.141-0.09-0.229-0.19-0.279-0.3s-0.08-0.24-0.08-0.39c0-0.15,0.029-0.28,0.09-0.41c0.061-0.13,0.149-0.25,0.271-0.34c0.12-0.1,0.271-0.18,0.45-0.24c0.179-0.06,0.4-0.09,0.64-0.09c0.25,0,0.47,0.04,0.66,0.11c0.19,0.07,0.351,0.17,0.479,0.29c0.131,0.12,0.221,0.26,0.291,0.42c0.06,0.16,0.1,0.32,0.1,0.49h1.949c0-0.39-0.079-0.75-0.239-1.09c-0.16-0.34-0.39-0.63-0.69-0.88c-0.3-0.25-0.66-0.44-1.09-0.59C18.58,9.07,18.09,9,17.55,9c-0.51,0-0.979,0.07-1.39,0.21c-0.411,0.14-0.77,0.33-1.061,0.57c-0.289,0.24-0.511,0.52-0.67,0.84c-0.16,0.32-0.229,0.65-0.229,1.01c0,0.36,0.079,0.68,0.229,0.96c0.149,0.28,0.369,0.52,0.641,0.73c0.27,0.21,0.6,0.38,0.979,0.529c0.381,0.141,0.811,0.26,1.271,0.359c0.39,0.08,0.709,0.17,0.949,0.26c0.239,0.092,0.431,0.19,0.569,0.29c0.132,0.101,0.222,0.222,0.271,0.341c0.051,0.119,0.07,0.25,0.07,0.391c0,0.32-0.131,0.57-0.4,0.77c-0.271,0.2-0.659,0.29-1.17,0.29c-0.219,0-0.43-0.02-0.639-0.08c-0.212-0.05-0.4-0.13-0.562-0.239c-0.171-0.109-0.302-0.261-0.41-0.439c-0.108-0.182-0.17-0.41-0.181-0.67H13.93c0,0.359,0.08,0.71,0.24,1.05s0.391,0.649,0.7,0.931c0.31,0.271,0.69,0.489,1.149,0.659c0.461,0.171,0.979,0.25,1.58,0.25c0.529,0,1.01-0.06,1.439-0.188c0.432-0.131,0.8-0.312,1.109-0.54c0.311-0.23,0.539-0.511,0.709-0.83c0.17-0.32,0.25-0.67,0.25-1.06C21.09,14.99,21.02,14.65,20.87,14.37z"/><g><g><g><path fill="#B39DDB" d="M4.146,14.682l2.24-0.271c0.071,0.57,0.264,1.007,0.576,1.309c0.313,0.303,0.692,0.453,1.137,0.453c0.478,0,0.879-0.181,1.206-0.543c0.327-0.363,0.49-0.852,0.49-1.466c0-0.582-0.157-1.043-0.469-1.384c-0.313-0.34-0.695-0.511-1.145-0.511c-0.296,0-0.65,0.058-1.062,0.173l0.255-1.886C8,10.572,8.478,10.437,8.807,10.148C9.137,9.86,9.301,9.477,9.301,9c0-0.406-0.121-0.73-0.362-0.972S8.376,7.666,7.976,7.666c-0.396,0-0.733,0.137-1.013,0.412c-0.28,0.274-0.45,0.675-0.51,1.202L4.319,8.917c0.148-0.73,0.372-1.313,0.671-1.75C5.29,6.731,5.707,6.388,6.242,6.138c0.535-0.25,1.135-0.375,1.799-0.375c1.136,0,2.047,0.362,2.734,1.087c0.565,0.593,0.848,1.263,0.848,2.009c0,1.06-0.579,1.905-1.738,2.536c0.692,0.148,1.245,0.48,1.66,0.997c0.414,0.516,0.622,1.139,0.622,1.87c0,1.06-0.387,1.963-1.161,2.709c-0.774,0.747-1.737,1.12-2.891,1.12c-1.092,0-1.998-0.314-2.717-0.943C4.679,16.52,4.262,15.697,4.146,14.682z"/></g></g></g></svg>			</div>
			<div class="icon-set__id">
				<span>timer_3</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1E88E5" d="M9.958,7.4c1.16,0,2.1,0.94,2.1,2.1s-0.939,2.1-2.1,2.1s-2.1-0.94-2.1-2.1S8.798,7.4,9.958,7.4"/><path fill="#1E88E5" d="M9.958,16.4c2.971,0,6.1,1.459,6.1,2.1v1.1h-12.2v-1.1C3.858,17.859,6.988,16.4,9.958,16.4"/><circle fill="#1E88E5" cx="9.958" cy="9.5" r="4"/><path fill="#1E88E5" d="M9.958,14.5c-2.67,0-8,1.34-8,4v3h16v-3C17.959,15.84,12.629,14.5,9.958,14.5z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFA726" d="M22.792,6.333v4c0,0.55-0.45,1-1,1h-5c-0.55,0-1-0.45-1-1v-4c0-0.55,0.45-1,1-1h5C22.342,5.333,22.792,5.783,22.792,6.333z"/><path fill="#FFCC80" d="M21.792,4.833v0.5h-0.8v-0.5c0-0.94-0.76-1.7-1.7-1.7s-1.7,0.76-1.7,1.7v0.5h-0.8v-0.5c0-1.38,1.12-2.5,2.5-2.5S21.792,3.453,21.792,4.833z"/></svg>			</div>
			<div class="icon-set__id">
				<span>perm_identity</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M15.17,15L12,18.17l-0.03-0.03L8.83,15l-1.41,1.41l4.55,4.56L12,21l4.59-4.59L15.17,15z M12,3L7.41,7.59L8.83,9L12,5.83L15.17,9l1.41-1.41L12,3z M15.17,15L12,18.17l-0.03-0.03L8.83,15l-1.41,1.41l4.55,4.56L12,21l4.59-4.59L15.17,15z"/><polygon fill="#455A64" points="16.59,16.41 12,21 11.97,20.97 7.42,16.41 8.83,15 11.97,18.14 12,18.17 15.17,15 "/></svg>			</div>
			<div class="icon-set__id">
				<span>unfold_more</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B3E5FC" d="M22,4v12c0,1.1-0.9,2-2,2H8c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h12C21.1,2,22,2.9,22,4z"/><path fill="#81D4FA" d="M22,4v12c0,1.1-0.9,2-2,2h-6.03V2H20C21.1,2,22,2.9,22,4z"/><polygon fill="#66BB6A" points="11,12 13.029,14.71 16,11 20,16 8,16 "/><path fill="#81D4FA" d="M2,6v14c0,1.1,0.9,2,2,2h14v-2H4V6H2z"/></svg>			</div>
			<div class="icon-set__id">
				<span>photo_library</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M17,0H7C5.9,0,5,0.9,5,2v14c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V2C19,0.9,18.1,0,17,0z M17,12.5c0-0.05,0-0.1-0.02-0.15C16.79,10.78,13.62,10,12,10c-1.67,0-5,0.83-5,2.5V2h10V12.5z"/><path opacity="0.4" fill="#546E7A" enable-background="new    " d="M19,2v14c0,1.1-0.9,2-2,2h-4.97v-5.311h4.951V8.52H12.03V0H17C18.1,0,19,0.9,19,2z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#AB47BC" points="10,20 5,20 5,22 10,22 10,24 13,21 10,18 "/><rect x="14" y="20" fill="#5C6BC0" width="5" height="2"/><rect x="7" y="2" fill="#B3E5FC" width="9.98" height="10.689"/><path fill="#1E88E5" d="M12,8c1.1,0,2-0.9,2-2s-0.9-2-2-2c-1.1,0-1.99,0.9-1.99,2S10.9,8,12,8z"/><path fill="#1E88E5" d="M16.98,12.35v0.34H7V12.5c0-1.67,3.33-2.5,5-2.5C13.62,10,16.79,10.78,16.98,12.35z"/></svg>			</div>
			<div class="icon-set__id">
				<span>camera_front</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#B0BEC5" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8s8,3.59,8,8S16.41,20,12,20z"/><polygon opacity="0.3" fill="#90A4AE" enable-background="new    " points="16,12 12,15 12,9 	"/><path opacity="0.3" fill="#90A4AE" enable-background="new    " d="M22,12c0,5.52-4.48,10-10,10v-2c4.41,0,8-3.59,8-8s-3.59-8-8-8V2C17.52,2,22,6.48,22,12z"/><polygon fill="#039BE5" points="16,12 10,16.5 10,7.5 	"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>play_circle_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M20.34,6.03C20.23,6.01,20.12,6,20,6h-4V4l-1.98-1.98L14,2h-4L8,4v2H4C3.88,6,3.77,6.01,3.66,6.03C2.72,6.19,2.01,7,2.01,8L2,13.939V19c0,1.109,0.89,2,2,2h16c1.109,0,2-0.891,2-2V8C22,7,21.279,6.19,20.34,6.03z M10,6V4h4v2H10z"/><path opacity="0.2" fill="#90A4AE" enable-background="new    " d="M22,8v5.939H2L2.01,8c0-1,0.71-1.81,1.65-1.97h16.68C21.279,6.19,22,7,22,8z"/><polygon fill="#00E676" points="12,19 7,14 10,14 10,10 14,10 14,14 17,14 "/><polygon fill="#B0BEC5" points="16,4 16,6 14,6 14,4 10,4 10,6 8,6 8,4 10,2 14,2 14.02,2.02 "/><polygon opacity="0.2" fill="#90A4AE" enable-background="new    " points="14.02,2.02 14.02,4 9.96,4 9.96,2.04 10,2 14,2 "/></svg>			</div>
			<div class="icon-set__id">
				<span>play_download</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="7" y="11" fill="#7E57C2" width="2" height="2"/><rect x="11" y="11" fill="#7E57C2" width="2" height="2"/><rect x="11" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="15" fill="#7E57C2" width="2" height="2"/><rect x="3" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="7" fill="#7E57C2" width="2" height="2"/><rect x="11" y="15" fill="#7E57C2" width="2" height="2"/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><rect x="19" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#512DA8" width="18" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><rect x="11" y="7" fill="#7E57C2" width="2" height="2"/><rect x="19" y="19" fill="#7E57C2" width="2" height="2"/><rect x="15" y="11" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_top</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#D7CCC8" d="M3.007,8.001l-0.008,11c0,1.101,0.892,2,2,2h14c1.101,0,2-0.899,2-2v-11H3.007z"/><path fill="#C2185B" d="M20.999,5.001c0-1.1-0.899-2-2-2h-14c-1.108,0-1.99,0.9-1.99,2l-0.002,3h17.992V5.001z"/><path fill="#A1887F" d="M7.999,5.001h-2v-4h2V5.001z M17.999,1.001h-2v4h2V1.001z"/></g><circle fill="#039BE5" cx="12.043" cy="11.188" r="2.438"/><path fill="#0288D1" d="M16.918,18.501h-9.75v-0.813c0-1.625,3.25-2.519,4.875-2.519s4.875,0.894,4.875,2.519V18.501z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>perm_contact_cal</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#E0E0E0" d="M20,19.59V8l-6-6H6C4.9,2,4.01,2.9,4.01,4L4,20c0,1.1,0.89,2,1.99,2H18c0.45,0,0.85-0.15,1.189-0.4"/><path fill="#7986CB" d="M16.17,15.75C16.689,14.96,17,14.02,17,13c0-2.76-2.24-5-5-5s-5,2.24-5,5s2.24,5,5,5c1.02,0,1.96-0.311,2.76-0.83l4.43,4.43C19.678,21.237,20,20.663,20,20.01c0-0.095,0-0.248,0-0.422L16.17,15.75z M12,16c-1.66,0-3-1.34-3-3s1.34-3,3-3s3,1.34,3,3S13.66,16,12,16z"/><path fill="#BDBDBD" d="M14,2l0.01,4c0,1.1,0.891,2,1.99,2h4L14,2z"/></svg>			</div>
			<div class="icon-set__id">
				<span>find_in_page</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M3,2l2.01,18.23C5.13,21.23,5.97,22,7,22h10c1.029,0,1.87-0.77,1.99-1.77L21,2H3z"/><polygon fill="#81D4FA" points="5.67,8 18.33,8 16.719,20.063 7.219,20.031 "/><path fill="#42A5F5" d="M12,19c-1.66,0-3-1.34-3-3c0-2,3-5.4,3-5.4s3,3.4,3,5.4C15,17.66,13.66,19,12,19z"/><polygon fill="#FFFFFF" points="18.33,8 5.67,8 5.23,4 18.76,4 "/></svg>			</div>
			<div class="icon-set__id">
				<span>local_drink</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#00ACC1" d="M6,3H3v3C4.66,6,6,4.66,6,3z"/><path fill="#00ACC1" d="M14,3h-2c0,4.97-4.03,9-9,9v2C9.08,14,14,9.07,14,3z"/><path fill="#00ACC1" d="M10,3H8c0,2.76-2.24,5-5,5v2C6.87,10,10,6.87,10,3z"/><path fill="#43A047" d="M10,21h2c0-4.97,4.029-9,9-9v-2C14.93,10,10,14.93,10,21z"/><path fill="#43A047" d="M18,21h3v-3C19.34,18,18,19.34,18,21z"/><path fill="#43A047" d="M14,21h2c0-2.76,2.24-5,5-5v-2C17.13,14,14,17.13,14,21z"/></svg>			</div>
			<div class="icon-set__id">
				<span>leak_add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#43A047" d="M12,14.3l3.71,2.7l-1.42-4.359L18,10h-4.55L12,5.5L10.55,10H6l3.71,2.641L8.29,17L12,14.3z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>star_rate</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E53935" d="M15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33v15.33C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V5.33C17,4.6,16.4,4,15.67,4z"/><rect x="11.05" y="16.05" fill="#FFFFFF" width="1.9" height="1.9"/><path fill="#FFFFFF" d="M14.3,12.689c0,0-0.38,0.42-0.67,0.711C13.15,13.88,12.8,14.55,12.8,15h-1.6c0-0.83,0.46-1.52,0.93-2l0.931-0.94C13.33,11.79,13.5,11.41,13.5,11c0-0.83-0.67-1.5-1.5-1.5s-1.5,0.67-1.5,1.5H9c0-1.66,1.34-3,3-3s3,1.34,3,3C15,11.66,14.73,12.26,14.3,12.689z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_unknown</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#4CAF50" d="M8.193,3.3c0.191,0.191,0.297,0.438,0.297,0.693C8.533,5.068,8.713,6.518,9.06,7.558C9.181,7.904,9.095,8.314,8.82,8.59L6.239,11.17C4.843,8.449,4.908,5.54,5,3.003l2.487,0C7.742,3.003,7.996,3.103,8.193,3.3z"/><path fill="#4CAF50" d="M20.699,15.807c-0.191-0.191-0.438-0.297-0.693-0.297c-1.074-0.043-2.525-0.223-3.564-0.57c-0.347-0.12-0.757-0.035-1.032,0.24l-2.581,2.582c2.722,1.396,5.631,1.33,8.168,1.238v-2.486C20.996,16.258,20.896,16.004,20.699,15.807z"/><path fill="#388E3C" d="M19.938,19C11.701,19,5,12.299,5,4.062V3.003H3.997c-0.248-0.007-0.502,0.092-0.7,0.29C3.099,3.491,3,3.807,3,4.062C3,13.416,10.583,21,19.938,21c0.254,0,0.572-0.099,0.77-0.297s0.297-0.453,0.289-0.7V19H19.938z"/></svg>			</div>
			<div class="icon-set__id">
				<span>call</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#B0BEC5" d="M20,18c1.1,0,1.99-0.9,1.99-2L22,5c0-1.1-0.9-2-2-2H4C2.9,3,2,3.9,2,5v11c0,1.1,0.9,2,2,2H20z"/><path fill="#90A4AE" d="M4,18H0c0,1.1,0.9,2,2,2h20c1.1,0,2-0.9,2-2h-4H4z"/><rect x="4" y="5" fill="#455A64" width="16" height="11"/><circle fill="#455A64" cx="12" cy="18" r="1"/></svg>			</div>
			<div class="icon-set__id">
				<span>laptop_mac</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#42A5F5" d="M21,5.019v14c0,1.1-0.9,2-2,2H5c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C20.1,3.019,21,3.919,21,5.019z"/><path opacity="0.5" fill="#1E88E5" enable-background="new    " d="M21,5.019v14c0,1.1-0.9,2-2,2h-7.008v-18H19C20.1,3.019,21,3.919,21,5.019z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#CFD8DC" d="M11,13v2h4v2H9v-4c0-1.11,0.9-2,2-2h2V9H9V7h4c1.1,0,2,0.89,2,2v2c0,1.11-0.9,2-2,2H11z"/></svg>			</div>
			<div class="icon-set__id">
				<span>looks_2</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#8BC34A" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><path fill="#4CAF50" d="M12,4c-4.417,0-8,3.583-8,8c0,4.414,3.583,8,8,8c4.414,0,8-3.586,8-8C20,7.583,16.414,4,12,4z"/><polygon fill="#CFD8DC" points="17,13 13,13 13,17 11,17 11,13 7,13 7,11 11,11 11,7 13,7 13,11 17,11 "/></svg>			</div>
			<div class="icon-set__id">
				<span>add_circle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#B0BEC5" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.109,4,6.6,5.64,5.35,8.04C2.34,8.36,0,10.91,0,14c0,3.311,2.689,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z"/><path fill="none" d="M0,0h24v24H0V0z"/></g><g id="Capa_2"><polygon fill="#9FA8DA" points="3,5.281 4.25,4 20.984,20.734 19.734,21.984 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>cloud_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M7,14l5-5l5,5H7z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>arrow_drop_up</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#43A047" points="19,17.01 15,21 14.91,20.91 11,17.01 14,17.01 14,10 16,10 16,17.01 "/><polygon fill="#00ACC1" points="13,6.99 10,6.99 10,14 8,14 8,6.99 5,6.99 9,3 "/><path fill="none" d="M0,0v24h24V0H0z M9,16.689H3.69V1.53H9V3L5,6.99h3V14h1V16.689z"/></svg>			</div>
			<div class="icon-set__id">
				<span>swap_vert</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#E57373" d="M24,5v14c0,1.1-0.9,2-2,2H7.07c-0.69,0-1.3-0.359-1.66-0.891L0,12l5.41-8.12C5.77,3.35,6.31,3,7,3h15C23.1,3,24,3.9,24,5z"/><circle fill="#FFFFFF" cx="9" cy="12" r="1.5"/><circle fill="#FFFFFF" cx="14" cy="12" r="1.5"/><circle fill="#FFFFFF" cx="19" cy="12" r="1.5"/></g><g id="Capa_2"><path fill="#EF5350" d="M8.984,20.984L7.07,21c-0.69,0-1.3-0.359-1.66-0.891L0,12l5.41-8.12C5.77,3.35,6.31,3,7,3l2.016,0.016L3.25,12L8.984,20.984z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>more</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#EF5350" d="M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M19.141,6.91C17.16,5.4,14.68,4.5,12,4.5c-2.68,0-5.16,0.9-7.14,2.41C3.14,8.21,1.8,9.96,1,12c0.82,2.09,2.21,3.88,3.98,5.18c0,0,0,0.012,0.01,0.021c1.96,1.439,4.39,2.3,7.01,2.3s5.05-0.86,7.01-2.3c0.01-0.01,0.01-0.021,0.01-0.021c1.771-1.3,3.16-3.09,3.98-5.18C22.2,9.96,20.859,8.21,19.141,6.91z M12,17c-2.76,0-5-2.24-5-5s2.24-5,5-5s5,2.24,5,5S14.76,17,12,17z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z"/></g><g id="Capa_2"><path fill="#D32F2F" d="M15,12c0,1.66-1.34,3-3,3s-3-1.34-3-3s1.34-3,3-3S15,10.34,15,12z"/><path fill="#EF5350" d="M15,12c0,1.66-1.34,3-3,3s-3-1.34-3-3s1.34-3,3-3S15,10.34,15,12z"/><path fill="#D32F2F" d="M19.141,6.91C17.16,5.4,14.68,4.5,12,4.5c-2.68,0-5.16,0.9-7.14,2.41c-1.02,1.43-1.61,3.17-1.61,5.05c0,1.96,0.64,3.76,1.73,5.22c0,0,0,0.012,0.01,0.021c1.96,1.439,4.39,2.3,7.01,2.3s5.05-0.86,7.01-2.3c0.01-0.01,0.01-0.021,0.01-0.021c1.092-1.46,1.73-3.26,1.73-5.22C20.75,10.08,20.16,8.34,19.141,6.91z M12,17c-2.76,0-5-2.24-5-5s2.24-5,5-5s5,2.24,5,5S14.76,17,12,17z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>remove_red_eye</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#607D8B" d="M2,17h2v0.5H3v1h1V19H2v1h3v-4H2V17z M3,8h1V4H2v1h1V8z M2,11h1.8L2,13.1V14h3v-1H3.2L5,10.9V10H2V11z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#90A4AE" d="M7,5v2h14V5H7z M7,19h14v-2H7V19z M7,13h14v-2H7V13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_list_numbered</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#C5CAE9" d="M7,2v11h3v9l7-12h-4l4-8H7z"/><path fill="#B0BEC5" d="M7,13l5.5-11H7V13z"/><rect x="9.603" y="0.377" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 26.0568 12.2069)" fill="#9FA8DA" width="1.795" height="22.246"/></svg>			</div>
			<div class="icon-set__id">
				<span>flash_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#BBDEFB" d="M21,19V5c0-1.1-0.9-2-2-2H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14C20.1,21,21,20.1,21,19z M8.5,13.5l2.5,3.01L14.5,12l4.5,6H5L8.5,13.5z"/><g><polygon fill="#43A047" points="19,17.979 5,17.979 8.5,13.479 11,16.489 14.5,11.979 	"/><polygon fill="#66BB6A" points="5,17.979 8.5,13.479 12.234,17.979 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>photo</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0v1.56h12.01V15H9.74c0.79-0.95,1.26-2.17,1.26-3.5C11,8.46,8.54,6,5.5,6S0,8.46,0,11.5S2.46,17,5.5,17h6.51v2.58H0V24h24V0H0z M5.5,15C3.57,15,2,13.43,2,11.5C2,9.57,3.57,8,5.5,8S9,9.57,9,11.5C9,13.43,7.43,15,5.5,15z"/><path fill="#7E57C2" d="M18.5,6C15.46,6,13,8.46,13,11.5c0,1.33,0.471,2.55,1.26,3.5H9.74c0.79-0.95,1.26-2.17,1.26-3.5C11,8.46,8.54,6,5.5,6S0,8.46,0,11.5S2.46,17,5.5,17h13c3.04,0,5.5-2.46,5.5-5.5S21.54,6,18.5,6z M5.5,15C3.57,15,2,13.43,2,11.5C2,9.57,3.57,8,5.5,8S9,9.57,9,11.5C9,13.43,7.43,15,5.5,15z M18.5,15c-1.93,0-3.5-1.57-3.5-3.5C15,9.57,16.57,8,18.5,8S22,9.57,22,11.5C22,13.43,20.43,15,18.5,15z"/></g><path fill="#9575CD" d="M9.74,15c0.79-0.95,1.26-2.17,1.26-3.5C11,8.46,8.54,6,5.5,6S0,8.46,0,11.5S2.46,17,5.5,17h6.51v-2H9.74zM5.5,15C3.57,15,2,13.43,2,11.5C2,9.57,3.57,8,5.5,8S9,9.57,9,11.5C9,13.43,7.43,15,5.5,15z"/></svg>			</div>
			<div class="icon-set__id">
				<span>voicemail</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M16,10v12c0,0.55-0.45,1-1,1H9c-0.55,0-1-0.45-1-1V10c0-0.55,0.45-1,1-1h6C15.55,9,16,9.45,16,10z"/><path opacity="0.2" fill="#546E7A" enable-background="new    " d="M16,10v12c0,0.55-0.45,1-1,1h-2.97V9H15C15.55,9,16,9.45,16,10z"/><path fill="#E53935" d="M12,15c-1.1,0-2-0.9-2-2c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C14,14.1,13.1,15,12,15z"/><path fill="#1E88E5" d="M7.05,6.05l1.41,1.41C9.37,6.56,10.62,6,12,6s2.63,0.56,3.54,1.46l1.41-1.41C15.68,4.78,13.93,4,12,4C10.07,4,8.32,4.78,7.05,6.05z"/><path fill="#42A5F5" d="M12,0C8.96,0,6.21,1.23,4.22,3.22l1.41,1.41C7.26,3.01,9.51,2,12,2s4.74,1.01,6.359,2.64l1.41-1.41C17.79,1.23,15.04,0,12,0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_remote</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4FC3F7" d="M17.02,2.99h-10c-0.14,0-0.29,0.02-0.42,0.05C6.47,3.06,6.34,3.1,6.22,3.16C6.05,3.23,5.89,3.33,5.75,3.45C5.7,3.49,5.66,3.53,5.61,3.58C5.56,3.62,5.52,3.67,5.48,3.72C5.2,4.07,5.03,4.51,5.03,4.99l-0.01,16L12,18l0.02-0.01L19,20.98l0.02,0.01v-16C19.02,3.89,18.12,2.99,17.02,2.99z M17,18l-4.98-2.17L12,15.82L7,18V5h10V18z"/><path fill="#66BB6A" d="M19.02,4.99v16L19,20.98l-6.98-2.99v-2.16L17,18V5h-4.98V2.99h5C18.12,2.99,19.02,3.89,19.02,4.99z"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>turned_in_not</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="4" y="12" fill="#26A69A" width="17" height="6"/><rect x="4" y="5" fill="#00ACC1" width="17" height="6"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_stream</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#039BE5" d="M7,9H3v6h4l5,5V4L7,9z M14,7.97v8.05c1.48-0.729,2.5-2.25,2.5-4.021C16.5,10.23,15.48,8.71,14,7.97zM14,3.23v2.06c2.891,0.86,5,3.54,5,6.71s-2.109,5.85-5,6.71v2.06c4.01-0.908,7-4.486,7-8.77C21,7.72,18.01,4.14,14,3.23z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#90A4AE" points="12,4 12,20 7,15 7,9 	"/></g><g id="Capa_2"><rect x="3" y="9" fill="#78909C" width="4" height="6"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>volume_up</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.6" fill="#8BC34A" enable-background="new    " d="M3,19c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2H5C3.89,3,3,3.9,3,5S3,17.167,3,19z M5,5h14v14H5V5z"/><polygon fill="#4CAF50" points="14,3 14,5 17.59,5 7.76,14.83 9.17,16.24 19,6.41 19,10 21,10 21,3 "/></svg>			</div>
			<div class="icon-set__id">
				<span>launch</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#0D47A1" d="M20,5H4C2.9,5,2.01,5.9,2.01,7L2,17c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V7C22,5.9,21.1,5,20,5z"/><path fill="#64B5F6" d="M11,8h2v2h-2V8z M11,11h2v2h-2V11z M8,8h2v2H8V8z M8,11h2v2H8V11z M7,13H5v-2h2V13z M7,10H5V8h2V10z M16,17H8v-2h8V17z M16,13h-2v-2h2V13z M16,10h-2V8h2V10z M19,13h-2v-2h2V13z M19,10h-2V8h2V10z"/><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#42A5F5" d="M20.971,4.992v14c0,1.1-0.9,2-2,2h-14c-1.1,0-2-0.9-2-2v-14c0-1.1,0.9-2,2-2h14C20.07,2.992,20.971,3.892,20.971,4.992z"/><path opacity="0.5" fill="#1E88E5" enable-background="new    " d="M20.971,4.992v14c0,1.1-0.9,2-2,2h-7.009v-18h7.009C20.07,2.992,20.971,3.892,20.971,4.992z"/></g><path fill="#CFD8DC" d="M15,9V7h-4C9.9,7,9,7.89,9,9v6c0,1.11,0.9,2,2,2h2c1.1,0,2-0.89,2-2v-2c0-1.11-0.9-2-2-2h-2V9H15z M13,13v2h-2v-2H13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>looks_6</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#EF5350" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M17,13H7v-2h10V13z"/><path opacity="0.3" fill="#E53935" d="M22,12c0,5.52-4.48,10-10,10V2C17.52,2,22,6.48,22,12z"/><rect x="7" y="11" fill="#CFD8DC" width="10" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>remove_circle</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#64B5F6" d="M20,4H4C2.89,4,2.01,4.89,2.01,6L2,18c0,1.109,0.89,2,2,2h16c1.109,0,2-0.891,2-2V6C22,4.89,21.109,4,20,4z"/><rect x="2" y="8" fill="#263238" width="20" height="4"/><polygon fill="#455A64" points="8,12 2,12 2,8 12,8 "/></svg>			</div>
			<div class="icon-set__id">
				<span>credit_card</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M20,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4z"/><rect x="4" y="12" fill="#CFD8DC" width="4" height="2"/><rect x="4" y="16" fill="#CFD8DC" width="10" height="2"/><rect x="16" y="16" fill="#CFD8DC" width="4" height="2"/><rect x="10" y="12" fill="#CFD8DC" width="10" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>subtitles</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path opacity="0.9" fill="#90A4AE" enable-background="new    " d="M9.174,14.424l3.357-2.321L3.91,3.5c-1.56,1.56-1.56,4.09,0,5.66L9.174,14.424z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#8D6E63" d="M20.29,19.88l-1.41,1.41L12,14.41l-0.58,0.58l-6.31,6.29L3.7,19.87l9.76-9.76c0.24,0.46,0.67,1.06,1.41,1.43L13.41,13L20.29,19.88z"/><polygon fill="#6D4C41" points="20.29,19.88 18.88,21.29 12.3,14.72 12,14.41 11.98,14.391 13.38,12.98 13.41,13 "/><path fill="#B0BEC5" d="M20.15,10.15c-0.42,0.42-0.881,0.76-1.351,1.03c-0.31,0.17-0.62,0.31-0.939,0.42c-0.04,0.01-0.07,0.02-0.101,0.03c-0.14,0.04-0.279,0.08-0.42,0.11c-0.87,0.19-1.729,0.12-2.45-0.21h-0.01v0.01h-0.01c-0.74-0.37-1.17-0.97-1.41-1.43V10.1c-0.06-0.13-0.11-0.25-0.14-0.35c-0.4-1.22-0.12-2.74,0.79-4.05c0.21-0.3,0.46-0.59,0.729-0.86c1.92-1.91,4.66-2.27,6.12-0.81C22.43,5.5,22.06,8.24,20.15,10.15z"/><polygon fill="#B0BEC5" points="14.88,11.53 14.89,11.53 14.88,11.54 "/><path fill="#B0BEC5" d="M17.86,11.6c0.319-0.11,0.63-0.25,0.939-0.42C18.5,11.36,18.18,11.5,17.86,11.6z"/><path fill="#B0BEC5" d="M20.19,10.14c-0.431,0.43-0.9,0.78-1.391,1.04c0.47-0.27,0.931-0.61,1.351-1.03c1.909-1.91,2.279-4.65,0.81-6.12c-1.46-1.46-4.2-1.1-6.12,0.81C14.57,5.11,14.32,5.4,14.11,5.7c0.21-0.31,0.46-0.61,0.739-0.89C16.78,2.89,19.53,2.52,21,3.99C22.47,5.47,22.1,8.22,20.19,10.14z"/></svg>			</div>
			<div class="icon-set__id">
				<span>restaurant_menu</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#64B5F6" d="M19,7v4h-1v2c0,0.74-0.39,1.38-0.98,1.72C16.73,14.9,16.38,15,16,15h-3v3.05c0.71,0.37,1.2,1.101,1.2,1.95c0,1.17-0.91,2.12-2.05,2.19C12.1,22.2,12.05,22.2,12,22.2c-1.21,0-2.2-0.98-2.2-2.2c0-0.85,0.49-1.58,1.2-1.95V15H8c-1.11,0-2-0.89-2-2v-2.07C5.3,10.56,4.8,9.85,4.8,9c0-1.2,0.98-2.19,2.18-2.2c0.01,0,0.01,0,0.02,0c1.21,0,2.2,0.99,2.2,2.2c0,0.85-0.5,1.56-1.2,1.93V13h3V5H9l2.99-3.99L12,1l2.98,3.97L15,5h-2v8h3v-2h-1V7H19z"/><polygon fill="#1E88E5" points="14.98,4.97 14.98,5 13,5 13,13.02 11.99,13.02 11.99,1.01 12,1 "/><path fill="#1E88E5" d="M14.2,20c0,1.17-0.91,2.12-2.05,2.19V14.97H13v3.08C13.71,18.42,14.2,19.15,14.2,20z"/><path fill="#1E88E5" d="M9.2,9c0,0.85-0.5,1.56-1.2,1.93V13H6.98V6.8c0.01,0,0.01,0,0.02,0C8.21,6.8,9.2,7.79,9.2,9z"/><path fill="#1E88E5" d="M19,7v4h-1v2c0,0.74-0.39,1.38-0.98,1.72V7H19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>usb</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,24V0h24v24H0z"/><polygon fill="#607D8B" points="14,6 9.415,10.585 9.415,13.415 14,18 15.41,16.59 10.828,12 15.41,7.41 "/><rect x="8.414" y="11" transform="matrix(0.7071 0.7071 -0.7071 0.7071 11.2436 -3.1424)" fill="#90A4AE" width="2.001" height="2.001"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_arrow_left</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M9,2L7.17,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6c0-1.1-0.9-2-2-2h-3.17L15,2H9z"/><circle fill="#5C6BC0" cx="12" cy="12" r="5"/><circle fill="#9FA8DA" cx="12" cy="12" r="3.2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>photo_camera</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M22.471,15.22l-1.201,0.4L20.1,16c-1.05-3.189-4.06-5.5-7.6-5.5c-1.96,0-3.73,0.72-5.12,1.88L11,16H2V7l3.6,3.6C7.45,8.99,9.85,8,12.5,8C17.15,8,21.08,11.03,22.471,15.22z"/><path fill="#78909C" d="M22.471,15.22l-1.201,0.4l-0.02-0.03c0,0-2.13-6.2-8.641-6.25C6.09,9.3,2,16,2,16V7l3.6,3.6C7.45,8.99,9.85,8,12.5,8C17.15,8,21.08,11.03,22.471,15.22z"/><polygon fill="#78909C" points="2,7 11,16 2,16 "/></svg>			</div>
			<div class="icon-set__id">
				<span>undo</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#B0BEC5" d="M19,8.973c0,5.041-6.45,12.38-6.96,12.96C12.01,21.963,12,21.973,12,21.973s-7-7.75-7-13c0-3.87,3.13-7,7-7h0.04C15.891,1.993,19,5.113,19,8.973z"/><path opacity="0.3" fill="#90A4AE" enable-background="new    " d="M19,8.973c0,5.041-6.45,12.38-6.96,12.96V1.973C15.891,1.993,19,5.113,19,8.973z"/><circle fill="#CFD8DC" cx="12" cy="8.973" r="2.5"/></g><path fill="none" d="M0,0h24v24H0V0z M11.75,11.47l-0.11-0.11L11.75,11.47z"/><rect x="10.102" y="0.17" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 27.2636 12.7075)" fill="#9FA8DA" width="1.796" height="23.66"/></svg>			</div>
			<div class="icon-set__id">
				<span>location_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M18,8h-1V6c0-2.76-2.24-5-5-5S7,3.24,7,6v2H6c-1.1,0-2,0.9-2,2v10c0,1.1,0.9,2,2,2h12c1.1,0,2-0.9,2-2V10C20,8.9,19.1,8,18,8z M8.9,8V6c0-1.71,1.39-3.1,3.1-3.1s3.1,1.39,3.1,3.1v2H8.9z"/><path fill="#607D8B" d="M12,17c-1.1,0-2-0.9-2-2s0.9-2,2-2c1.1,0,2,0.9,2,2S13.1,17,12,17z"/><path fill="#BDBDBD" d="M17,6v2h-1.9V6c0-1.71-1.39-3.1-3.1-3.1S8.9,4.29,8.9,6v2H7V6c0-2.76,2.24-5,5-5S17,3.24,17,6z"/></svg>			</div>
			<div class="icon-set__id">
				<span>https</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4E342E" d="M19.029,3.019h-4.18c-0.42-1.16-1.521-2-2.823-2c-1.299,0-2.4,0.84-2.82,2h-4.18c-1.1,0-2,0.9-2,2v14c0,1.1,0.9,2,2,2h14c1.101,0,2-0.9,2-2v-14C21.029,3.918,20.129,3.019,19.029,3.019z"/><path fill="#D7CCC8" d="M19.736,6.556c0-1.355-1.1-2.454-2.455-2.454H6.817c-1.356,0-2.455,1.099-2.455,2.454v10.676c0,1.354,1.099,2.453,2.455,2.453h10.464c1.355,0,2.455-1.1,2.455-2.453V6.556z"/><circle fill="#F1F8E9" cx="12.029" cy="4.019" r="1"/></g><rect x="11" y="16" fill="#8E24AA" width="2" height="2"/><rect x="11" y="8" fill="#8E24AA" width="2" height="6"/><path fill="#FFA000" d="M7.026,6.224l9.984,0.031l0.002-1.236v-2h-2.18c-0.42-1.16-1.521-2-2.82-2c-1.299,0-2.4,0.84-2.82,2h-2.18v2L7.026,6.224z M12.013,3.019c0.549,0,1,0.45,1,1c0,0.55-0.451,1-1,1c-0.55,0-1-0.45-1-1C11.013,3.469,11.463,3.019,12.013,3.019z"/></svg>			</div>
			<div class="icon-set__id">
				<span>assignment_late</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#B0BEC5" d="M20.295,6.065c-0.109-0.02-0.219-0.03-0.34-0.03h-4v-2l-1.98-1.98l-0.02-0.02h-4l-2,2v2h-4c-0.12,0-0.23,0.01-0.34,0.03c-0.94,0.16-1.65,0.97-1.65,1.97l-0.01,5.939v5.061c0,1.109,0.891,2,2,2h15.999c1.109,0,2-0.891,2-2v-11C21.955,7.035,21.234,6.225,20.295,6.065z M9.956,6.035v-2h4v2H9.956z"/><path opacity="0.2" fill="#90A4AE" enable-background="new    " d="M21.955,8.035v5.939H1.956l0.01-5.939c0-1,0.71-1.81,1.65-1.97h16.679C21.234,6.225,21.955,7.035,21.955,8.035z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#7986CB" points="16.5,13 9,18 9,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>shop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9E9E9E" d="M11,17h10v-2H11V17z M3,21h18v-2H3V21z M3,3v2h18V3H3z M11,9h10V7H11V9z M11,13h10v-2H11V13z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#00BCD4" points="3,12 7,16 7,8 "/></svg>			</div>
			<div class="icon-set__id">
				<span>format_indent_decrease</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#90A4AE" d="M3,21h18v-2H3V21z M3,13h18v-2H3V13z M3,3v2h18V3H3z"/><path fill="#607D8B" d="M7,17h10v-2H7V17z M7,9h10V7H7V9z M7,5h10V3H7V5z M7,13h10v-2H7V13z M7,21h10v-2H7V21z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_align_center</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#43A047" d="M20,12c0,1.57-0.46,3.03-1.24,4.26L17.3,14.8c0.45-0.83,0.7-1.79,0.7-2.8c0-3.31-2.689-6-6-6v3L8.02,5.02c0,0-0.01-0.01-0.02-0.02l4-4v3C16.42,4,20,7.58,20,12z"/><path fill="#00ACC1" d="M12,23v-3c-4.42,0-8-3.58-8-8c0-1.57,0.46-3.03,1.24-4.26L6.7,9.2C6.25,10.03,6,10.99,6,12c0,3.311,2.69,6,6,6v-3l4,4L12,23z"/><path fill="none" d="M0,0h24v24H0V0z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>sync</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#6D4C41" d="M16,16.159v3.88h3v2H3v-5H2v-4c0-0.55,0.45-1,1-1h3c0.55,0,1,0.45,1,1v4H6v3h8V16.1c-3.31-0.551-5.83-3.42-5.83-6.891c0-3.87,3.13-7,7-7s7,3.13,7,7c0,3.37-2.37,6.17-5.529,6.85H16.63c-0.14,0.03-0.29,0.051-0.438,0.07C16.13,16.139,16.07,16.148,16,16.159z"/><circle cx="4.5" cy="9.539" r="1.5"/><rect x="3" y="20.049" fill="#43A047" width="16" height="1.99"/><path fill="#43A047" d="M22.17,9.209c0,3.37-2.37,6.17-5.529,6.85H16.63c-0.14,0.03-0.29,0.051-0.438,0.07c-0.062,0.01-0.121,0.021-0.19,0.03c-0.271,0.03-0.55,0.04-0.83,0.04c-0.399,0-0.79-0.03-1.17-0.09v-0.012c-3.31-0.55-5.83-3.42-5.83-6.89c0-3.87,3.13-7,7-7S22.17,5.339,22.17,9.209z"/><path fill="#546E7A" d="M7,13.039v4H6v3H3v-3H2v-4c0-0.55,0.45-1,1-1h3C6.55,12.039,7,12.489,7,13.039z"/><circle fill="#546E7A" cx="4.5" cy="9.539" r="1.5"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>nature_people</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#455A64" points="9,4 9,7 14,7 14,19 17,19 17,7 22,7 22,4 "/><polygon fill="#607D8B" points="3,12 6,12 6,19 9,19 9,12 12,12 12,9 3,9 "/></svg>			</div>
			<div class="icon-set__id">
				<span>format_size</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#1976D2" points="12,8.41 16.59,13 18,11.59 12,5.59 6,11.59 7.41,13 "/><rect x="6" y="16" fill="#0D47A1" width="12" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_capslock</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#42A5F5" d="M14.24,12.01l2.32,2.32C16.84,13.609,17,12.82,17,12c0-0.82-0.16-1.59-0.43-2.31L14.24,12.01z"/><path fill="#1E88E5" d="M19.529,6.71l-1.26,1.26c0.631,1.21,0.98,2.57,0.98,4.021c0,1.449-0.359,2.82-0.98,4.02l1.201,1.2c0.969-1.54,1.539-3.36,1.539-5.31C21,10.01,20.46,8.23,19.529,6.71z"/><path fill="#1976D2" d="M15.708,7.71L9.998,2h-1v7.59L4.408,5l-1.41,1.41L8.588,12l-5.59,5.59L4.408,19l4.59-4.59V22h1l5.71-5.71l-4.3-4.29L15.708,7.71z M10.998,5.83l1.88,1.88l-1.88,1.88V5.83z M12.878,16.29l-1.88,1.88v-3.76L12.878,16.29z"/></svg>			</div>
			<div class="icon-set__id">
				<span>bluetooth_searching</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#FFCA28" points="23,21 1,21 11.96,2.07 12,2 "/><polygon fill="#FFD54F" points="11.96,2.07 11.96,21 1,21 "/><rect x="11" y="16" fill="#212121" width="2" height="2"/><rect x="11" y="10" fill="#212121" width="2" height="4"/></svg>			</div>
			<div class="icon-set__id">
				<span>report_problem</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#78909C" d="M21.54,14.63c0.19,0.15,0.24,0.42,0.12,0.64l-2,3.461c-0.12,0.22-0.38,0.31-0.61,0.22l-2.49-1c-0.52,0.39-1.079,0.729-1.689,0.979l-0.38,2.65C14.46,21.82,14.25,22,14,22h-4c-0.25,0-0.46-0.18-0.49-0.42l-0.38-2.65c-0.61-0.25-1.17-0.58-1.69-0.979l-2.49,1c-0.22,0.08-0.49,0-0.61-0.22l-2-3.461c-0.12-0.22-0.07-0.489,0.12-0.64l2.11-1.649C4.53,12.66,4.5,12.33,4.5,12s0.03-0.66,0.07-0.98L2.46,9.37C2.27,9.22,2.21,8.95,2.34,8.73l2-3.46c0.12-0.22,0.38-0.31,0.61-0.22l2.49,1c0.52-0.39,1.08-0.73,1.69-0.98l0.38-2.65C9.54,2.18,9.75,2,10,2h4c0.25,0,0.46,0.18,0.49,0.42l0.38,2.65c0.61,0.25,1.17,0.58,1.689,0.98l2.49-1c0.22-0.08,0.49,0,0.61,0.22l2,3.46c0.12,0.22,0.07,0.49-0.12,0.64l-2.11,1.65c0.04,0.32,0.07,0.64,0.07,0.98s-0.03,0.66-0.07,0.98L21.54,14.63z"/><path fill="#90A4AE" d="M12,2v20h-2c-0.25,0-0.46-0.18-0.49-0.42l-0.38-2.65c-0.61-0.25-1.17-0.58-1.69-0.979l-2.49,1c-0.22,0.08-0.49,0-0.61-0.22l-2-3.461c-0.12-0.22-0.07-0.489,0.12-0.64l2.11-1.649C4.53,12.66,4.5,12.33,4.5,12s0.03-0.66,0.07-0.98L2.46,9.37C2.27,9.22,2.21,8.95,2.34,8.73l2-3.46c0.12-0.22,0.38-0.31,0.61-0.22l2.49,1c0.52-0.39,1.08-0.73,1.69-0.98l0.38-2.65C9.54,2.18,9.75,2,10,2H12z"/><circle fill="#546E7A" cx="11.984" cy="11.984" r="4.453"/><path fill="#FFFFFF" d="M12,15.5c-1.93,0-3.5-1.57-3.5-3.5c0-1.93,1.57-3.5,3.5-3.5c1.93,0,3.5,1.57,3.5,3.5C15.5,13.93,13.93,15.5,12,15.5z"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>settings</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><polygon fill="#B39DDB" points="0,7.72 0,9.4 3,8.4 3,18 5,18 5,6 4.75,6 "/><path fill="#5C6BC0" d="M23.779,14.37c-0.139-0.28-0.35-0.53-0.629-0.74c-0.28-0.21-0.61-0.39-1.01-0.53c-0.4-0.14-0.851-0.27-1.351-0.38c-0.351-0.069-0.64-0.149-0.87-0.229s-0.41-0.16-0.55-0.25c-0.14-0.09-0.229-0.19-0.28-0.3c-0.05-0.11-0.08-0.24-0.08-0.39c0-0.14,0.03-0.28,0.09-0.41c0.061-0.13,0.15-0.25,0.271-0.34c0.12-0.1,0.271-0.18,0.45-0.24s0.4-0.09,0.64-0.09c0.25,0,0.47,0.04,0.66,0.11c0.19,0.07,0.351,0.17,0.479,0.29c0.131,0.12,0.221,0.26,0.291,0.42c0.06,0.16,0.1,0.32,0.1,0.49h1.949c0-0.39-0.08-0.75-0.239-1.09c-0.16-0.34-0.39-0.63-0.69-0.88c-0.3-0.25-0.66-0.44-1.09-0.59C21.49,9.07,21,9,20.46,9c-0.51,0-0.979,0.07-1.39,0.21S18.3,9.54,18.01,9.78c-0.289,0.24-0.51,0.52-0.67,0.84c-0.16,0.32-0.23,0.65-0.23,1.01c0,0.36,0.08,0.69,0.23,0.96c0.15,0.28,0.36,0.52,0.641,0.73c0.27,0.21,0.6,0.38,0.979,0.529c0.38,0.141,0.81,0.26,1.271,0.36c0.39,0.08,0.709,0.17,0.949,0.26c0.24,0.091,0.43,0.19,0.57,0.29c0.13,0.1,0.221,0.221,0.27,0.34c0.051,0.12,0.07,0.25,0.07,0.391c0,0.32-0.13,0.57-0.4,0.77c-0.27,0.2-0.66,0.29-1.17,0.29c-0.22,0-0.43-0.02-0.64-0.08c-0.21-0.05-0.399-0.13-0.56-0.239c-0.17-0.11-0.301-0.261-0.41-0.44c-0.11-0.181-0.17-0.41-0.18-0.67H16.84c0,0.36,0.08,0.71,0.24,1.05s0.391,0.65,0.699,0.93c0.311,0.271,0.691,0.49,1.15,0.66c0.461,0.17,0.98,0.25,1.58,0.25c0.53,0,1.01-0.06,1.44-0.189c0.43-0.131,0.8-0.311,1.11-0.54c0.31-0.23,0.539-0.511,0.709-0.83c0.17-0.32,0.25-0.67,0.25-1.06C24,14.99,23.93,14.65,23.779,14.37z"/><path fill="#B39DDB" d="M14.6,8.64c-0.18-0.66-0.439-1.19-0.779-1.59c-0.34-0.4-0.75-0.7-1.23-0.88C12.12,5.99,11.58,5.9,11,5.9S9.89,5.99,9.41,6.17C8.93,6.35,8.52,6.64,8.18,7.05c-0.34,0.41-0.6,0.93-0.79,1.59c-0.18,0.65-0.28,1.45-0.28,2.39v1.92c0,0.939,0.09,1.74,0.28,2.39c0.19,0.66,0.45,1.19,0.8,1.601c0.34,0.409,0.75,0.71,1.23,0.89s1.01,0.28,1.59,0.28c0.59,0,1.12-0.091,1.59-0.28c0.48-0.18,0.881-0.48,1.221-0.89c0.34-0.41,0.6-0.94,0.779-1.601c0.181-0.649,0.28-1.45,0.28-2.39v-1.92C14.88,10.09,14.79,9.29,14.6,8.64z M12.91,13.22H12.9c0,0.601-0.04,1.11-0.12,1.53s-0.2,0.76-0.36,1.02c-0.16,0.261-0.36,0.45-0.59,0.57s-0.51,0.18-0.82,0.18c-0.3,0-0.58-0.06-0.82-0.18c-0.24-0.12-0.44-0.31-0.6-0.57c-0.16-0.26-0.29-0.6-0.38-1.02s-0.13-0.93-0.13-1.53v-2.5c0-0.6,0.04-1.11,0.13-1.52c0.09-0.41,0.21-0.74,0.38-1c0.16-0.25,0.36-0.43,0.6-0.55C10.43,7.54,10.7,7.48,11,7.48c0.31,0,0.58,0.06,0.81,0.17c0.24,0.11,0.44,0.29,0.6,0.55c0.16,0.25,0.29,0.58,0.37,0.99s0.13,0.92,0.13,1.52V13.22z"/></svg>			</div>
			<div class="icon-set__id">
				<span>timer_10</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#AED581" d="M19.35,10.04C18.67,6.59,15.641,4,12,4C9.11,4,6.6,5.64,5.35,8.04C2.34,8.36,0,10.91,0,14c0,3.311,2.69,6,6,6h13c2.76,0,5-2.24,5-5C24,12.36,21.95,10.22,19.35,10.04z"/><polygon fill="#689F38" points="10,17 6.5,13.5 7.91,12.09 10,14.17 15.18,9 16.59,10.41 "/></svg>			</div>
			<div class="icon-set__id">
				<span>cloud_done</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#455A64" d="M7,10l5,5l5-5H7z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>arrow_drop_down</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M15.5,1h-8C6.12,1,5,2.12,5,3.5v17C5,21.88,6.12,23,7.5,23h8c1.38,0,2.5-1.12,2.5-2.5v-17C18,2.12,16.88,1,15.5,1z"/><circle fill="#B0BEC5" cx="11.5" cy="20.5" r="1.5"/><rect x="7" y="4" fill="#78909C" width="9" height="14"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>phone_iphone</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#F06292" d="M20.176,13.266L12,5.09C13.09,3.81,14.76,3,16.5,3C19.58,3,22,5.42,22,8.5C22,10.159,21.345,11.682,20.176,13.266z M7.5,3C4.42,3,2,5.42,2,8.5c0,3.78,3.4,6.859,8.55,11.53L12,21.35V5.09C10.91,3.81,9.24,3,7.5,3z"/><path fill="#EC407A" d="M20.176,13.266L12,5.09v16.26l1.45-1.31C16.341,17.413,18.679,15.29,20.176,13.266z"/></svg>			</div>
			<div class="icon-set__id">
				<span>favorite</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#B39DDB" d="M22.047,6.141v12c0,1.1-0.9,2-2,2h-16c-1.1,0-2-0.9-2-2l0.01-12c0-1.1,0.891-2,1.99-2h16C21.146,4.141,22.047,5.041,22.047,6.141z"/><polygon fill="#9575CD" points="22.047,6.141 22.025,8.079 12.047,13.141 2.047,8.058 2.047,6.183 12.047,11.141 	"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>markunread</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon opacity="0.8" fill="#0288D1" points="7.83,13 20,13 20,11 7.832,11 5.829,12.002 "/><polygon fill="#0288D1" points="12,20 13.41,18.59 6.829,12.002 7.832,11 7.83,11 13.41,5.41 12,4 4,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>arrow_back</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path d="M20.98,13.999v8c0,0.55-0.451,1-1,1h-1c-0.551,0-1-0.45-1-1v-1h-12v1c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-8l2.08-5.99c0.21-0.59,0.76-1.01,1.42-1.01h11c0.66,0,1.219,0.42,1.42,1.01L20.98,13.999z"/><path fill="#455A64" d="M20.98,16.719v5.28c0,0.55-0.451,1-1,1h-1c-0.551,0-1-0.45-1-1v-1h-12v1c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1v-5.28H20.98z"/><path fill="#E53935" d="M20.98,13.999v2.72h-18v-2.72l2.08-5.99c0.21-0.59,0.76-1.01,1.42-1.01h11c0.66,0,1.219,0.42,1.42,1.01L20.98,13.999z"/><circle fill="#FFFFFF" cx="6.48" cy="16.499" r="1.5"/><circle fill="#FFFFFF" cx="17.48" cy="16.499" r="1.5"/><polygon fill="#BBDEFB" points="4.98,12.999 6.48,8.499 17.48,8.499 18.98,12.999 	"/></g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#42A5F5" d="M17,5c0.83,0,1.5-0.67,1.5-1.5c0-1-1.5-2.7-1.5-2.7s-1.5,1.7-1.5,2.7C15.5,4.33,16.17,5,17,5z"/><path fill="#42A5F5" d="M12,5c0.83,0,1.5-0.67,1.5-1.5c0-1-1.5-2.7-1.5-2.7s-1.5,1.7-1.5,2.7C10.5,4.33,11.17,5,12,5z"/><path fill="#42A5F5" d="M7,5c0.83,0,1.5-0.67,1.5-1.5c0-1-1.5-2.7-1.5-2.7S5.5,2.5,5.5,3.5C5.5,4.33,6.17,5,7,5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_car_wash</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M19,8H5c-1.66,0-3,1.34-3,3v6h4v4h12v-4h4v-6C22,9.34,20.66,8,19,8z"/><rect x="8" y="14" fill="#CFD8DC" width="8" height="5"/><circle fill="#CFD8DC" cx="19" cy="11" r="1"/><rect x="6" y="3" fill="#CFD8DC" width="12" height="4"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="5.969" y="12.719" fill="#CFD8DC" width="12.031" height="8.281"/></svg>			</div>
			<div class="icon-set__id">
				<span>print</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="5" y="17" fill="#455A64" width="14" height="2"/><polygon fill="#37474F" points="9.5,12.8 14.5,12.8 15.4,15 17.5,15 12.75,4 11.25,4 6.5,15 8.6,15 "/><polygon fill="#FFFFFF" points="12,5.98 13.87,11 10.13,11 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>text_format</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#26C6DA" points="21,7 9,19 3.41,13.41 4.83,12 9,16.17 19.59,5.59 "/><polygon opacity="0.5" fill="#00ACC1" points="21,7 9,19 8.96,18.96 8.96,16.13 9,16.17 19.59,5.59 "/></svg>			</div>
			<div class="icon-set__id">
				<span>check</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#03A9F4" points="9,16 15,16 15,10 19,10 12,3 5,10 9,10 "/><rect x="5" y="18" fill="#0288D1" width="14" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>file_upload</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M24,24H0V0h24V24z"/><path fill="#2196F3" d="M4,18v-6h8.5L4,18z M21.5,12H13v6L21.5,12z"/><path fill="#82B1FF" d="M12.5,12H4V6L12.5,12z M13,6v6h8.5L13,6z"/></svg>			</div>
			<div class="icon-set__id">
				<span>fast_forward</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1565C0" d="M12,14c1.66,0,2.99-1.34,2.99-3L15,5c0-1.66-1.34-3-3-3S9,3.34,9,5v6C9,12.66,10.34,14,12,14z"/><path fill="#78909C" d="M19,11c0,3.42-2.721,6.24-6,6.72V21h-2v-3.28C7.72,17.23,5,14.41,5,11h1.7c0,2.99,2.52,5.08,5.26,5.1H12c2.76,0,5.3-2.1,5.3-5.1H19z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#546E7A" d="M19,11c0,3.42-2.721,6.24-6,6.72V21h-1.04v-4.9H12c2.76,0,5.3-2.1,5.3-5.1H19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>m24px</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#2196F3" d="M11.5,12H20v6L11.5,12z M11,18v-6H2.5L11,18z"/><path fill="#64B5F6" d="M20,6v6h-8.5L20,6z M2.5,12H11V6L2.5,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>fast_rewind</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="2.208" y="4.583" fill="#C5CAE9" width="19.458" height="15.875"/><path fill="none" d="M0,0.5h24v24H0V0.5z"/><path fill="#C5CAE9" d="M21,3.5H3c-1.1,0-2,0.9-2,2v14c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2v-14C23,4.4,22.1,3.5,21,3.5z M21,19.52H3V5.49h6V5.48h6v0.01h6V19.52z"/><polygon fill="#42A5F5" points="12,16.5 16,12.5 13,12.5 13,3.5 11,3.5 11,12.5 8,12.5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>system_update_tv</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#1565C0" d="M12,13.027c1.66,0,2.99-1.34,2.99-3l0.01-6c0-1.66-1.34-3-3-3s-3,1.34-3,3v6C9,11.688,10.34,13.027,12,13.027z"/><path fill="#78909C" d="M19,10.027c0,3.42-2.721,6.24-6,6.72v3.28h-2v-3.28c-3.279-0.489-6-3.31-6-6.72h1.7c0,2.99,2.521,5.08,5.26,5.1H12c2.76,0,5.3-2.1,5.3-5.1H19z"/><path fill="#546E7A" d="M19,10.027c0,3.42-2.721,6.24-6,6.72v3.28h-1.04v-4.9H12c2.76,0,5.3-2.1,5.3-5.1H19z"/></g><rect x="6.931" y="22" fill="#455A64" width="2" height="2"/><rect x="11" y="22" fill="#546E7A" width="2" height="2"/><rect x="15" y="22" fill="#455A64" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_voice</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#D1C4E9" d="M22,4v16c0,1.1-0.9,2-2,2H4c-0.56,0-1.08-0.24-1.44-0.62C2.21,21.02,2,20.54,2,20V4c0-1.1,0.9-2,2-2h16c0.55,0,1.04,0.22,1.4,0.58C21.77,2.94,22,3.45,22,4z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#E6EE9C" points="20,20 4,20 20,4 	"/><path fill="#9575CD" d="M22,4v16c0,1.1-0.9,2-2,2H4c-0.56,0-1.08-0.24-1.44-0.62L21.4,2.58C21.77,2.94,22,3.45,22,4z"/><polygon fill="#D1C4E9" points="15,17 15,19 17,19 17,17 19,17 19,15 17,15 17,13 15,13 15,15 13,15 13,17 	"/><rect x="5" y="5" fill="#9575CD" width="6" height="2"/></g><g id="Capa_2"></g></svg>			</div>
			<div class="icon-set__id">
				<span>exposure</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path d="M20.939,11C20.48,6.83,17.17,3.52,13,3.06V1h-2v2.06C9.87,3.18,8.81,3.52,7.84,4.03l1.5,1.5C10.16,5.19,11.06,5,12,5c3.87,0,7,3.13,7,7c0,0.939-0.189,1.84-0.52,2.65l1.5,1.5c0.5-0.961,0.84-2.021,0.97-3.15H23v-2H20.939z M3,4.27l2.04,2.04C3.97,7.62,3.25,9.23,3.06,11H1v2h2.06c0.46,4.17,3.77,7.48,7.94,7.939V23h2v-2.061c1.77-0.199,3.38-0.909,4.689-1.979L19.73,21L21,19.73L4.27,3L3,4.27z M16.27,17.54C15.09,18.45,13.609,19,12,19c-3.87,0-7-3.13-7-7c0-1.61,0.55-3.09,1.46-4.27L16.27,17.54z"/><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.5" fill="#90A4AE" enable-background="new    " d="M3.684,12c0-4.158,4.158-8.316,8.316-8.316v16.632c4.159,0,8.316-4.156,8.316-8.315L3.684,12L3.684,12z"/><path fill="#90A4AE" d="M20.939,11C20.48,6.83,17.17,3.52,13,3.06V1h-2v2.06C6.83,3.52,3.52,6.83,3.06,11H1v2h2.06c0.46,4.17,3.77,7.48,7.94,7.939V23h2v-2.061c4.17-0.459,7.48-3.771,7.939-7.939H23v-2H20.939z M12,19c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,19,12,19z"/><rect x="11.102" y="0.17" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 28.9707 12.0004)" fill="#9FA8DA" width="1.796" height="23.66"/></svg>			</div>
			<div class="icon-set__id">
				<span>location_disabled</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M7,19h14v-2H7V19z M7,13h14v-2H7V13z M7,5v2h14V5H7z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#607D8B" d="M4,10.5c-0.83,0-1.5,0.67-1.5,1.5s0.67,1.5,1.5,1.5s1.5-0.67,1.5-1.5S4.83,10.5,4,10.5z M4,4.5C3.17,4.5,2.5,5.17,2.5,6S3.17,7.5,4,7.5S5.5,6.83,5.5,6S4.83,4.5,4,4.5z M4,16.67c-0.74,0-1.33,0.6-1.33,1.33s0.6,1.33,1.33,1.33s1.33-0.6,1.33-1.33S4.74,16.67,4,16.67z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_list_bulleted</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M23,4v12c0,1.1-0.9,2-2,2h-7v2h2v2H8v-2h2v-2H3c-1.1,0-2-0.9-2-2V4c0-1.1,0.9-2,2-2h18C22.1,2,23,2.9,23,4z"/><rect x="3" y="4" fill="#B3E5FC" width="18" height="12"/><rect x="10" y="17.98" fill="#455A64" width="4" height="2.02"/></svg>			</div>
			<div class="icon-set__id">
				<span>desktop_windows</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M24,0v24H0v-2.38h11.5V21H3c-0.55,0-1-0.45-1-1v-6c0-0.55,0.45-1,1-1h8.5v-2H3c-0.55,0-1-0.45-1-1V4c0-0.55,0.45-1,1-1h8.5V0.49H0V0H24z"/><path fill="#9CCC65" d="M21,14v6c0,0.55-0.45,1-1,1H3c-0.55,0-1-0.45-1-1v-6c0-0.55,0.45-1,1-1h17C20.55,13,21,13.45,21,14z"/><path fill="#26C6DA" d="M21,4v6c0,0.55-0.45,1-1,1H3c-0.55,0-1-0.45-1-1V4c0-0.55,0.45-1,1-1h17C20.55,3,21,3.45,21,4z"/><path fill="#4DD0E1" d="M11.5,3v8H3c-0.55,0-1-0.45-1-1V4c0-0.55,0.45-1,1-1H11.5z"/><path fill="#AED581" d="M3,13h8.5v8H3c-0.55,0-1-0.45-1-1v-6C2,13.45,2.45,13,3,13z"/></svg>			</div>
			<div class="icon-set__id">
				<span>view_agenda</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M20,5v10.5c0,1.81-1.38,3.3-3.14,3.48C16.74,18.99,16.62,19,16.5,19l1.5,1.5V21H6v-0.5L7.5,19c-0.12,0-0.24-0.01-0.36-0.02C5.38,18.8,4,17.31,4,15.5V5c0-3.5,3.58-4,8-4S20,1.5,20,5z"/><path fill="#FFFFFF" d="M12,17c-1.1,0-2-0.9-2-2s0.9-2,2-2c1.1,0,2,0.9,2,2S13.1,17,12,17z"/><rect x="6" y="5" fill="#90CAF9" width="12" height="5"/><path fill="#DCE775" d="M18,20.5V21H6v-0.5L7.5,19c-0.12,0-0.24-0.01-0.36-0.02h9.72C16.74,18.99,16.62,19,16.5,19L18,20.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>directions_train</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#78909C" d="M17,1H7C5.9,1,5,1.9,5,3v18c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V3C19,1.9,18.1,1,17,1z"/><rect x="7" y="5" fill="#B2EBF2" width="10" height="14"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFA726" d="M14,11v-1c0-1.11-0.9-2-2-2c-1.11,0-2,0.9-2,2v1c-0.55,0-1,0.45-1,1v3c0,0.55,0.45,1,1,1h4c0.55,0,1-0.45,1-1v-3C15,11.45,14.55,11,14,11z M10.8,11v-1c0-0.66,0.54-1.2,1.2-1.2s1.2,0.54,1.2,1.2v1H10.8z"/><path fill="#FFCC80" d="M14,10v1h-0.8v-1c0-0.66-0.54-1.2-1.2-1.2s-1.2,0.54-1.2,1.2v1H10v-1c0-1.1,0.89-2,2-2C13.1,8,14,8.89,14,10z"/></svg>			</div>
			<div class="icon-set__id">
				<span>screen_lock_portrait</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="1" y="11" fill="#90CAF9" width="6" height="2"/><rect x="5.906" y="6.408" transform="matrix(0.7072 0.707 -0.707 0.7072 7.4037 -3.0673)" fill="#64B5F6" width="2.998" height="1.994"/><rect x="11" y="1" fill="#90CAF9" width="2" height="6"/><rect x="15.598" y="5.906" transform="matrix(0.707 0.7072 -0.7072 0.707 10.0994 -9.5666)" fill="#64B5F6" width="1.993" height="2.998"/><rect x="17" y="11" fill="#90CAF9" width="6" height="2"/><path fill="#42A5F5" d="M12,9c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,9,12,9z"/><rect x="15.096" y="15.599" transform="matrix(-0.7073 -0.7069 0.7069 -0.7073 16.5998 40.064)" fill="#64B5F6" width="2.998" height="1.993"/><rect x="6.408" y="15.096" transform="matrix(-0.7072 -0.707 0.707 -0.7072 0.9095 33.5671)" fill="#64B5F6" width="1.993" height="2.998"/><rect x="11" y="17" fill="#90CAF9" width="2" height="6"/></svg>			</div>
			<div class="icon-set__id">
				<span>flare</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#263238" d="M1.01,7L1,17c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2V7c0-1.1-0.9-2-2-2H3C1.9,5,1.01,5.9,1.01,7z"/><rect x="5" y="7" fill="#546E7A" width="14" height="10"/></svg>			</div>
			<div class="icon-set__id">
				<span>stay_primary_landscape</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_2_1_"><rect x="3.162" y="7.269" fill="#FFEE58" width="17.531" height="12.229"/></g><g id="Capa_1"><path fill="#FFEE58" d="M12,14v3h4.13v-3H12z M12,14v3h4.13v-3H12z M20,6h-2.18C17.93,5.69,18,5.35,18,5c0-1.66-1.34-3-3-3c-1.05,0-1.96,0.54-2.5,1.35L12,4.02l-0.04-0.05L11.5,3.34C10.96,2.54,10.05,2,9,2C7.34,2,6,3.34,6,5c0,0.35,0.07,0.69,0.18,1H4C2.89,6,2.01,6.89,2.01,8L2,14v5c0,1.11,0.89,2,2,2h16c1.11,0,2-0.89,2-2V8C22,6.89,21.11,6,20,6z M20,14H4V8h5.08L7.64,9.96L7,10.83l0.64,0.46L8.62,12l1.77-2.41L11,8.76l0.96-1.31L12,7.4l1,1.36l0.38,0.52l2,2.72l0.75-0.54L17,10.83l-0.87-1.18L14.92,8H20V14z M20,19H4v-2h16V19z M15,4c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S14.45,4,15,4z M9,4c0.55,0,1,0.45,1,1S9.55,6,9,6S8,5.55,8,5S8.45,4,9,4z M12,14v3h4.13v-3H12z M12,14v3h4.13v-3H12z"/><path fill="none" d="M20,6h-2.18C17.93,5.69,18,5.35,18,5c0-1.66-1.34-3-3-3c-1.05,0-1.96,0.54-2.5,1.35L12,4.02l-0.04-0.05L11.5,3.34C10.96,2.54,10.05,2,9,2C7.34,2,6,3.34,6,5c0,0.35,0.07,0.69,0.18,1H4C2.89,6,2.01,6.89,2.01,8L2,14v5c0,1.11,0.89,2,2,2h16c1.11,0,2-0.89,2-2V8C22,6.89,21.11,6,20,6z M4,19v-2h7.96v2H4z M4,8h5.08L7.64,9.96L7,10.83l0.64,0.46L8.62,12l1.77-2.41L11,8.76l0.96-1.31V14H4V8z M9,4c0.55,0,1,0.45,1,1S9.55,6,9,6S8,5.55,8,5S8.45,4,9,4z"/><rect x="2" y="14" fill="#E53935" width="20" height="3"/><path fill="#E53935" d="M15,2c-1.05,0-1.96,0.54-2.5,1.35L12,4.02l-0.04-0.05L11.5,3.34C10.96,2.54,10.05,2,9,2C7.34,2,6,3.34,6,5c0,0.35,0.07,0.69,0.18,1c0,0,0.01,0.02,0.02,0.05c0.07,0.17,0.32,0.74,0.86,1.22c0.16,0.14,0.36,0.28,0.58,0.4C8.02,7.87,8.5,8,9.08,8L7.64,9.96L7,10.83l0.64,0.46L8.62,12l1.77-2.41L11,8.76l0.96-1.31L12,7.4l1,1.36l0.38,0.52l2,2.72l0.75-0.54L17,10.83l-0.87-1.18L14.92,8c0,0,0.17,0.02,0.42,0c0.22-0.02,0.49-0.06,0.79-0.18c0.3-0.1,0.62-0.27,0.92-0.55s0.57-0.68,0.75-1.22C17.81,6.03,17.81,6.02,17.82,6C17.93,5.69,18,5.35,18,5C18,3.34,16.66,2,15,2z M15,4c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S14.45,4,15,4z M9,4c0.55,0,1,0.45,1,1S9.55,6,9,6S8,5.55,8,5S8.45,4,9,4z"/><rect x="4.635" y="6.052" fill="#E53935" width="3" height="14.922"/><rect x="10.385" y="6.115" fill="#E53935" width="3" height="14.875"/><rect x="16.135" y="6.052" fill="#E53935" width="3" height="14.875"/><path opacity="0.15" fill="#BDBDBD" enable-background="new    " d="M11.944,3.97l-0.46-0.63c-0.54-0.8-1.45-1.34-2.5-1.34c-1.66,0-3,1.34-3,3c0,0.35,0.07,0.69,0.18,1h-2.18c-1.11,0-1.99,0.89-1.99,2l-0.01,6v5c0,1.11,0.89,2,2,2h8V4.02L11.944,3.97zM8.984,4c0.55,0,1,0.45,1,1s-0.45,1-1,1s-1-0.45-1-1S8.435,4,8.984,4z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>wallet_giftcard</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#FFEE58" d="M7,6v6H1V6h2V2c0-0.55,0.45-1,1-1s1,0.45,1,1v4H7z"/><path fill="#455A64" d="M15,14v2c0,1.3-0.83,2.4-1.98,2.81C13.01,18.82,13.01,18.82,13,18.82V23h-2v-4.18c-0.01,0-0.01-0.011-0.02-0.011C9.83,18.39,9,17.29,9,16v-2H15z"/><path fill="#455A64" d="M7,14v2c0,1.29-0.83,2.39-1.98,2.81c-0.01,0-0.01,0.011-0.02,0.011V23H3v-4.18c-0.01,0-0.01-0.011-0.02-0.011C1.83,18.39,1,17.29,1,16v-2H7z"/><path fill="#B0BEC5" d="M23,6v6h-6V6h2V2c0-0.55,0.45-1,1-1s1,0.45,1,1v4H23z"/><path fill="#E53935" d="M15,6v6H9V6h2V2c0-0.55,0.45-1,1-1s1,0.45,1,1v4H15z"/><path fill="#455A64" d="M23,14v2c0,1.3-0.83,2.4-1.98,2.81C21.01,18.82,21.01,18.82,21,18.82V23h-2v-4.18c-0.01,0-0.01-0.011-0.02-0.011C17.83,18.39,17,17.29,17,16v-2H23z"/><path fill="#90A4AE" d="M5,2.009v3.988H3V2.009C3,1.455,3.45,1,4,1S5,1.455,5,2.009z"/><path fill="#90A4AE" d="M13,2.009v3.988h-2V2.009C11,1.455,11.45,1,12,1S13,1.455,13,2.009z"/><path fill="#90A4AE" d="M21,2.009v3.988h-2V2.009C19,1.455,19.45,1,20,1S21,1.455,21,2.009z"/><path fill="#FDD835" d="M1,14h6v2c0,1.29-0.83,2.39-1.98,2.81H2.98C1.83,18.39,1,17.29,1,16V14z"/><path fill="#C62828" d="M9,14h6v2c0,1.3-0.83,2.4-1.98,2.81h-2.04C9.83,18.39,9,17.29,9,16V14z"/><path fill="#90A4AE" d="M23,14v2c0,1.3-0.83,2.4-1.98,2.81H18.98C17.83,18.39,17,17.29,17,16v-2H23z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_input_composite</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#4FC3F7" cx="10" cy="10" r="1"/><circle fill="#4FC3F7" cx="10" cy="14" r="1"/><path fill="#81D4FA" d="M7,9.5c-0.28,0-0.5,0.22-0.5,0.5s0.22,0.5,0.5,0.5s0.5-0.22,0.5-0.5S7.28,9.5,7,9.5z"/><path fill="#81D4FA" d="M10,16.5c-0.28,0-0.5,0.22-0.5,0.5s0.22,0.5,0.5,0.5s0.5-0.22,0.5-0.5S10.28,16.5,10,16.5z"/><path fill="#81D4FA" d="M7,13.5c-0.28,0-0.5,0.22-0.5,0.5s0.22,0.5,0.5,0.5s0.5-0.22,0.5-0.5S7.28,13.5,7,13.5z"/><path fill="#81D4FA" d="M10,7.5c0.28,0,0.5-0.22,0.5-0.5S10.28,6.5,10,6.5S9.5,6.72,9.5,7S9.72,7.5,10,7.5z"/><circle fill="#4FC3F7" cx="14" cy="10" r="1"/><path fill="#81D4FA" d="M14,7.5c0.279,0,0.5-0.22,0.5-0.5S14.279,6.5,14,6.5S13.5,6.72,13.5,7S13.721,7.5,14,7.5z"/><path fill="#81D4FA" d="M17,13.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S17.279,13.5,17,13.5z"/><path fill="#81D4FA" d="M17,9.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S17.279,9.5,17,9.5z"/><path fill="#039BE5" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M12,20c-4.42,0-8-3.58-8-8s3.58-8,8-8s8,3.58,8,8S16.42,20,12,20z"/><path fill="#81D4FA" d="M14,16.5c-0.279,0-0.5,0.22-0.5,0.5s0.221,0.5,0.5,0.5s0.5-0.22,0.5-0.5S14.279,16.5,14,16.5z"/><circle fill="#4FC3F7" cx="14" cy="14" r="1"/></svg>			</div>
			<div class="icon-set__id">
				<span>blur_circular</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0-0.25h24v24H0V-0.25z"/><polygon fill="#607D8B" points="10,18 14.585,13.415 14.585,10.585 10,6 8.59,7.41 13.172,12 8.59,16.59 "/><rect x="13.584" y="10.998" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 16.4135 30.7967)" fill="#90A4AE" width="2.001" height="2.001"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_arrow_right</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#2196F3" d="M15.67,4H14V2h-4v2H8.33C7.6,4,7,4.6,7,5.33v15.33C7,21.4,7.6,22,8.33,22h7.33C16.4,22,17,21.4,17,20.67V5.33C17,4.6,16.4,4,15.67,4z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_std</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#1976D2" points="11.59,7.41 15.17,11 1,11 1,13 15.17,13 11.58,16.59 13,18 19,12 13,6 "/><rect x="20" y="6" fill="#0D47A1" width="2" height="12"/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_tab</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><polygon fill="#66BB6A" points="10,7 8,7 8,11 4,11 4,13 8,13 8,17 10,17 10,13 14,13 14,11 10,11 "/><polygon fill="#546E7A" points="20,18 18,18 18,7.38 15,8.4 15,6.7 19.7,5 20,5 "/></svg>			</div>
			<div class="icon-set__id">
				<span>exposure_plus_1</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#8E24AA" d="M19.51,3.08L3.08,19.51c0.09,0.34,0.27,0.65,0.51,0.9c0.25,0.24,0.56,0.42,0.9,0.51L20.93,4.49C20.74,3.8,20.2,3.26,19.51,3.08z"/><polygon fill="#BA68C8" points="11.88,3 3,11.88 3,14.71 14.71,3 "/><path fill="#8E24AA" d="M5,3C3.9,3,3,3.9,3,5v2l4-4H5z"/><path fill="#8E24AA" d="M19,21c0.55,0,1.05-0.22,1.41-0.59C20.779,20.05,21,19.55,21,19v-2l-4,4H19z"/><polygon fill="#BA68C8" points="9.29,21 12.12,21 21,12.12 21,9.29 "/></svg>			</div>
			<div class="icon-set__id">
				<span>texture</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path d="M10.18,9"/><path fill="#43A047" d="M21,16v-2l-8-5V3.5C13,2.67,12.33,2,11.5,2S10,2.67,10,3.5V9l-8,5v2l8-2.5V19l-2,1.5V22l3.5-1l3.5,1v-1.5L13,19v-5.5L21,16z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="none" d="M15.292-17.625h24v24h-24V-17.625z"/></svg>			</div>
			<div class="icon-set__id">
				<span>airplanemode_on</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#4FC3F7" d="M19,5v16l-7-3l-7,3L5.01,5C5.01,3.9,5.9,3,7,3h10C18.1,3,19,3.9,19,5z"/><path fill="#66BB6A" d="M19,5v16l-7-3V3h5C18.1,3,19,3.9,19,5z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>turned_in</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#90A4AE" d="M10,5v2h12V5H10z M10,19h12v-2H10V19z M10,13h12v-2H10V13z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#2196F3" points="6,7 8.5,7 5,3.5 1.5,7 4,7 4,17 1.5,17 5,20.5 8.5,17 6,17 "/></svg>			</div>
			<div class="icon-set__id">
				<span>format_line_spacing</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M21,3H3C1.9,3,1,3.9,1,5v14c0,1.1,0.9,2,2,2h18c1.1,0,2-0.9,2-2V5C23,3.9,22.1,3,21,3z"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="3" y="4.99" fill="#E1BEE7" width="18" height="14.02"/><path fill="#9575CD" d="M9,16h6.5c1.38,0,2.5-1.12,2.5-2.5S16.88,11,15.5,11h-0.05C15.21,9.31,13.76,8,12,8c-1.4,0-2.6,0.83-3.16,2.02H8.68C7.17,10.18,6,11.45,6,13C6,14.66,7.34,16,9,16z"/></svg>			</div>
			<div class="icon-set__id">
				<span>settings_system_daydream</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#1976D2" d="M16.5,13c-1.2,0-3.07,0.34-4.5,1c-1.43-0.67-3.3-1-4.5-1C5.33,13,1,14.08,1,16.25V19h22v-2.75C23,14.08,18.67,13,16.5,13z M12.5,17.5h-10v-1.25c0-0.54,2.56-1.75,5-1.75s5,1.21,5,1.75V17.5z M21.5,17.5H14v-1.25c0-0.06,0-0.12-0.02-0.17c0.01-0.05,0-0.09-0.011-0.12c-0.01-0.05-0.04-0.14-0.08-0.26c-0.029-0.09-0.069-0.181-0.13-0.271c-0.029-0.05-0.06-0.1-0.09-0.149c-0.01-0.021-0.03-0.05-0.04-0.07c-0.05-0.06-0.1-0.12-0.149-0.18c0.88-0.3,1.96-0.53,3.02-0.53c2.44,0,5,1.21,5,1.75V17.5z"/><path fill="#1976D2" d="M7.5,5C5.57,5,4,6.57,4,8.5S5.57,12,7.5,12S11,10.43,11,8.5S9.43,5,7.5,5z M7.5,10.5c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2S8.6,10.5,7.5,10.5z"/><path d="M16.5,5C14.57,5,13,6.57,13,8.5s1.57,3.5,3.5,3.5S20,10.43,20,8.5S18.43,5,16.5,5z M16.5,10.5c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2S17.6,10.5,16.5,10.5z"/><path fill="#00BCD4" d="M16.5,5C14.57,5,13,6.57,13,8.5s1.57,3.5,3.5,3.5S20,10.43,20,8.5S18.43,5,16.5,5z M16.5,10.5c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2S17.6,10.5,16.5,10.5z"/><path fill="#00BCD4" d="M23,16.25V19h-9.03l0.011-2.92C14,16.13,14,16.19,14,16.25v1.25h7.5v-1.25c0-0.54-2.56-1.75-5-1.75c-1.06,0-2.14,0.23-3.02,0.53C13.19,14.68,12.72,14.29,12,14c1.43-0.66,3.3-1,4.5-1C18.67,13,23,14.08,23,16.25z"/></svg>			</div>
			<div class="icon-set__id">
				<span>people_outline</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#FFC107" d="M2,18c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V8H2.008L2,18z"/><path fill="#FF9800" d="M20,6h-8l-2-2H4C2.9,4,2.01,4.9,2.01,6L2.008,8H22C22,6.9,21.1,6,20,6z"/></svg>			</div>
			<div class="icon-set__id">
				<span>folder</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M17,2H7C5.9,2,5,2.9,5,4v16c0,1.1,0.9,1.99,2,1.99L17,22c1.1,0,2-0.9,2-2V4C19,2.9,18.1,2,17,2z"/><path fill="#B0BEC5" d="M12,4c1.1,0,2,0.9,2,2s-0.9,2-2,2c-1.11,0-2-0.9-2-2S10.89,4,12,4z"/><circle fill="#B0BEC5" cx="12" cy="15" r="5"/><path fill="#78909C" d="M12,12c-1.66,0-3,1.34-3,3s1.34,3,3,3s3-1.34,3-3S13.66,12,12,12z"/></svg>			</div>
			<div class="icon-set__id">
				<span>speaker</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.5" fill="#F57C00" d="M13.908,3.909C9.942,3.909,4,7.03,4,12s4.02,9,9,9c4.971,0,8.092-5.942,8.092-9.909S17.876,3.909,13.908,3.909z"/><path fill="#E0E0E0" d="M13,19c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S16.87,19,13,19z"/><polygon fill="#FFA000" points="13.5,7 12,7 12,13 16.75,15.85 17.5,14.62 13.5,12.25 "/><path fill="#F57C00" d="M13,3c-4.97,0-9,4.03-9,9H1l3.89,3.891l0.07,0.14L9,12H6c0-3.87,3.13-7,7-7s7,3.13,7,7s-3.13,7-7,7c-1.93,0-3.68-0.79-4.94-2.061l-1.42,1.42C8.27,19.99,10.51,21,13,21c4.971,0,9-4.03,9-9S17.971,3,13,3z"/></svg>			</div>
			<div class="icon-set__id">
				<span>history</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#455A64" points="7.88,3.39 6.6,1.86 2,5.71 3.29,7.24 "/><polygon fill="#455A64" points="22,5.72 17.4,1.86 16.109,3.39 20.71,7.25 "/><path fill="#78909C" d="M12,4c-4.97,0-9,4.03-9,9s4.02,9,9,9c4.971,0,9-4.03,9-9S16.971,4,12,4z"/><path fill="#CFD8DC" d="M12,20c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,20,12,20z"/><polygon fill="#EF5350" points="13,9 11,9 11,12 8,12 8,14 11,14 11,17 13,17 13,14 16,14 16,12 13,12 "/></svg>			</div>
			<div class="icon-set__id">
				<span>add_alarm</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#546E7A" d="M20,18c1.1,0,2-0.9,2-2V6c0-1.1-0.9-2-2-2H4C2.9,4,2,4.9,2,6v10c0,1.1,0.9,2,2,2H0v2h24v-2H20z"/><rect x="4" y="6" fill="#E3F2FD" width="16" height="10"/></g><path fill="none" d="M-618-1720H782v3600H-618V-1720z M0,0h24v24H0V0z"/><rect y="18" fill="#37474F" width="24" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>laptop</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="13" y="13" fill="#66BB6A" width="8" height="8"/><rect x="3" y="13" fill="#AB47BC" width="8" height="8"/><rect x="3" y="3" fill="#EC407A" width="8" height="8"/><polygon fill="#5E35B1" points="16.66,1.69 11,7.34 16.66,13 22.32,7.34 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>now_widgets</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_2_1_"><rect x="2.313" y="4.281" fill="#B3E5FC" width="19.438" height="13.031"/></g><g id="Capa_1_2_"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#37474F" d="M21,3H3C1.9,3,1,3.9,1,5v12c0,1.1,0.9,2,2,2h5v2h8v-2h5c1.1,0,1.99-0.9,1.99-2L23,5C23,3.9,22.1,3,21,3zM21,17H3V5h18V17z"/></g><g id="Capa_2"><path fill="#455A64" d="M21,3H3C1.9,3,1,3.9,1,5v12c0,1.1,0.9,2,2,2h18c1.1,0,1.99-0.9,1.99-2L23,5C23,3.9,22.1,3,21,3z M21,17H3V5h18V17z"/></g></g></svg>			</div>
			<div class="icon-set__id">
				<span>tv</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0v24h24V0H0z M22.53,22.43H12.14C14.48,20.31,17.59,19,21,19V8c-3.48,0-6.63,1.35-8.99,3.54V9.13l2.16-1.16h8.36V22.43z"/><path fill="#E57373" d="M21,8v11c-3.41,0-6.52,1.31-8.86,3.43c-0.05,0.04-0.09,0.08-0.14,0.12C9.64,20.35,6.48,19,3,19V8c3.48,0,6.64,1.35,9,3.55l0.01-0.01C14.37,9.35,17.52,8,21,8z"/><path fill="#546E7A" d="M12,8c1.66,0,3-1.34,3-3s-1.34-3-3-3S9,3.34,9,5S10.34,8,12,8z"/><path fill="#EF5350" d="M21,8v11c-3.41,0-6.52,1.31-8.86,3.43h-0.13V11.54C14.37,9.35,17.52,8,21,8z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_library</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#81C784" d="M9,19L21,7l-1.41-1.41L9,16.17V19z"/><path fill="#4CAF50" d="M9,16.17L4.83,12l-1.42,1.41L9,19V16.17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>done</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#1976D2" cx="12" cy="8" r="4"/><path fill="#1565C0" d="M12,14c-2.67,0-8,1.34-8,4v2h16v-2C20,15.34,14.67,14,12,14z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="none" points="62,-15 62,9 52.88,9 52.88,-12.92 38,-12.92 38,-15 "/></svg>			</div>
			<div class="icon-set__id">
				<span>person</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.5" fill="#00BCD4" d="M3.684,12c0-4.158,4.158-8.316,8.316-8.316v16.633c4.159,0,8.316-4.157,8.316-8.316H3.684z"/><path fill="#0097A7" d="M20.939,11C20.48,6.83,17.17,3.52,13,3.06V1h-2v2.06C6.83,3.52,3.52,6.83,3.06,11H1v2h2.06c0.46,4.17,3.77,7.48,7.94,7.939V23h2v-2.061c4.17-0.459,7.48-3.77,7.939-7.939H23v-2H20.939z M12,19c-3.87,0-7-3.13-7-7s3.13-7,7-7s7,3.13,7,7S15.87,19,12,19z"/></svg>			</div>
			<div class="icon-set__id">
				<span>location_searching</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><g><path fill="#CFD8DC" d="M6,3c0,1.66-1.34,3-3,3V4.28h1.31V3H6z"/><path fill="#CFD8DC" d="M13.999,2.999h-2c0,4.97-4.029,9-9,9v2C9.079,13.999,13.999,9.069,13.999,2.999z"/><path fill="#CFD8DC" d="M9.999,2.999h-2c0,2.76-2.24,5-5,5v2C6.869,9.999,9.999,6.869,9.999,2.999z"/><path fill="#B0BEC5" d="M9.999,20.999h2c0-4.97,4.029-9,9-9v-2C14.929,9.999,9.999,14.929,9.999,20.999z"/><path fill="#B0BEC5" d="M21,18v1.7h-1.31V21H18C18,19.34,19.34,18,21,18z"/><path fill="#B0BEC5" d="M13.999,20.999h2c0-2.76,2.24-5,5-5v-2C17.129,13.999,13.999,17.129,13.999,20.999z"/></g></g><g id="Capa_2"><polygon fill="#9FA8DA" points="2.984,4.297 4.297,2.984 20.969,19.719 19.719,21.016 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>leak_remove</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="16" y="3" fill="#00ACC1" width="1" height="5"/><polygon fill="#00ACC1" points="15,5 13,5 13,4 15,4 15,3 12,3 12,6 14,6 14,7 12,7 12,8 15,8 "/><path fill="#00ACC1" d="M18,3v5h1V6h2V3H18z M20,5h-1V4h1V5z"/><g><path fill="#78909C" d="M20.96,16.5V20c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h3.5c0.55,0,1,0.45,1,1c0,1.25,0.2,2.45,0.569,3.57c0.109,0.35,0.029,0.74-0.25,1L6.58,10.78c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.319,0.439,0.63,0.682,0.94c0.129,0.158,0.25,0.3,0.368,0.449c0.29,0.34,0.591,0.658,0.9,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.12,0.109,0.24,0.221,0.37,0.311c0.132,0.102,0.25,0.2,0.382,0.3c0.131,0.11,0.26,0.2,0.398,0.29c0.143,0.102,0.277,0.189,0.42,0.29c0.277,0.2,0.58,0.381,0.88,0.54c0.188,0.109,0.392,0.221,0.58,0.32l2.2-2.2c0.277-0.271,0.67-0.351,1.021-0.24c1.118,0.37,2.318,0.57,3.568,0.57C20.51,15.5,20.96,15.95,20.96,16.5z"/><g id="Capa_2"><path fill="#455A64" d="M20.96,19.68V20c0,0.55-0.45,1-1,1c-9.392,0-17-7.609-17-17c0-0.55,0.45-1,1-1h0.688L4.66,3.06c0,0-0.131,3.77,1.92,7.72c0.271,0.52,0.568,1.04,0.92,1.56c0.21,0.319,0.439,0.63,0.682,0.94c0.129,0.158,0.25,0.3,0.368,0.449c0.29,0.34,0.591,0.658,0.9,0.959c0.22,0.211,0.448,0.42,0.688,0.631c0.12,0.101,0.24,0.199,0.37,0.311c0.132,0.102,0.25,0.2,0.382,0.3c0.131,0.103,0.271,0.19,0.398,0.29c0.143,0.102,0.277,0.189,0.42,0.29c0.277,0.183,0.58,0.37,0.88,0.54c0.188,0.109,0.392,0.221,0.58,0.32c2.05,1.068,4.603,1.869,7.763,2.21L20.96,19.68z"/></g></g></svg>			</div>
			<div class="icon-set__id">
				<span>dialer_sip</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><g><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#90A4AE" d="M21,6.5v11l-4-4V17c0,0.55-0.45,1-1,1H4c-0.55,0-1-0.45-1-1V7c0-0.55,0.45-1,1-1h12c0.55,0,1,0.45,1,1v3.5l0.02-0.02L21,6.5z"/></g><g id="Capa_2"><polygon fill="#90A4AE" points="21,6.5 21,17.5 17,13.5 17,10.5 17.02,10.48 			"/></g><g id="Capa_3"><path fill="#B0BEC5" d="M17.02,10.48v1.49H3V7c0-0.55,0.45-1,1-1h12c0.55,0,1,0.45,1,1v3.5L17.02,10.48z"/><polygon fill="#B0BEC5" points="21,6.5 21,11.984 17.02,11.97 17,10.5 17.02,10.48 			"/></g></g></g><g id="Capa_2_1_"><polygon fill="#9FA8DA" points="1.969,3.266 3.266,1.984 20.984,19.719 19.719,21 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>videocam_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M12,8l-6,6l1.41,1.41L12,10.83l4.59,4.58L18,14L12,8z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>expand_less</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path fill="#0D47A1" d="M20.002,3h-16c-1.1,0-1.99,0.9-1.99,2l-0.01,10c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V5C22.002,3.9,21.102,3,20.002,3z"/><path fill="#64B5F6" d="M11.002,6h2v2h-2V6z M11.002,9h2v2h-2V9z M8.002,6h2v2h-2V6z M8.002,9h2v2h-2V9z M7.002,11h-2V9h2V11zM7.002,8h-2V6h2V8z M16.002,15h-8v-2h8V15z M16.002,11h-2V9h2V11z M16.002,8h-2V6h2V8z M19.002,11h-2V9h2V11z M19.002,8h-2V6h2V8z"/></g><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#64B5F6" points="12,23 16,19 8,19 "/></svg>			</div>
			<div class="icon-set__id">
				<span>keyboard_hide</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#263238" d="M17,1.01L7,1C5.9,1,5,1.9,5,3v18c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V3C19,1.9,18.1,1.01,17,1.01z"/><rect x="7" y="5" fill="#546E7A" width="10" height="14"/><polygon fill="#4CAF50" points="15.41,16.59 20,12 15.41,7.41 14,8.83 17.17,12 14,15.17 "/><polygon fill="#4CAF50" points="10,15.17 6.83,12 10,8.83 8.59,7.41 4,12 8.59,16.59 "/></svg>			</div>
			<div class="icon-set__id">
				<span>developer_mode</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><circle fill="#00796B" cx="12" cy="12" r="11.594"/><path fill="#B2DFDB" d="M12,2C8.13,2,5,5.13,5,9c0,5.25,7,13,7,13s7-7.75,7-13C19,5.13,15.87,2,12,2z M12,11.5c-1.38,0-2.5-1.12-2.5-2.5s1.12-2.5,2.5-2.5s2.5,1.12,2.5,2.5S13.38,11.5,12,11.5z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>place</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="17" fill="#78909C" width="6" height="2"/><rect x="3" y="5" fill="#78909C" width="10" height="2"/><polygon fill="#455A64" points="13,21 13,19 21,19 21,17 13,17 13,15 11,15 11,21 "/><polygon fill="#455A64" points="7,9 7,11 3,11 3,13 7,13 7,15 9,15 9,9 "/><rect x="11" y="11" fill="#78909C" width="10" height="2"/><polygon fill="#455A64" points="15,9 17,9 17,7 21,7 21,5 17,5 17,3 15,3 "/></svg>			</div>
			<div class="icon-set__id">
				<span>tune</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#546E7A" d="M14.24,12.01l2.32,2.32C16.84,13.609,17,12.82,17,12c0-0.82-0.16-1.59-0.43-2.31L14.24,12.01z"/><path fill="#455A64" d="M19.529,6.71l-1.26,1.26c0.631,1.21,0.98,2.57,0.98,4.021c0,1.449-0.359,2.82-0.98,4.02l1.201,1.2c0.969-1.54,1.539-3.36,1.539-5.31C21,10.01,20.46,8.23,19.529,6.71z"/><path fill="#1976D2" d="M15.719,7.731l-5.71-5.71h-1v7.59l-4.59-4.59l-1.41,1.41l5.59,5.59l-5.59,5.59l1.41,1.41l4.59-4.59v7.59h1l5.71-5.71l-4.3-4.29L15.719,7.731z M11.009,5.852l1.88,1.88l-1.88,1.88V5.852z M12.889,16.312l-1.88,1.88v-3.76L12.889,16.312z"/></svg>			</div>
			<div class="icon-set__id">
				<span>bluetooth_audio</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M19.967,7.972v12c0,1.1-0.9,2-2,2H5.958c-1.1,0-1.99-0.9-1.99-2l0.01-16c0-1.1,0.89-2,1.99-2h8L19.967,7.972z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#455A64" points="16,16 13,16 13,19 11,19 11,16 8,16 8,14 11,14 11,11 13,11 13,14 16,14 "/><polygon opacity="0.5" fill="#90A4AE" enable-background="new    " points="19.967,8.067 19.967,9.067 13.967,9.067 13.967,2.067"/></svg>			</div>
			<div class="icon-set__id">
				<span>note_add</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#D32F2F" d="M17,3H7C5.9,3,5.01,3.9,5.01,5L5,21l7-3l7,3V5C19,3.9,18.1,3,17,3z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>bookmark</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M3,5v6h5L7,7l4,1V3H5C3.9,3,3,3.9,3,5z"/><path opacity="0.8" fill="#B0BEC5" d="M8,13H3v6c0,1.1,0.9,2,2,2h6v-5l-4,1L8,13z"/><path fill="#CFD8DC" d="M17,17l-4-1v5h6c1.1,0,2-0.9,2-2v-6h-5L17,17z"/><path opacity="0.8" fill="#B0BEC5" d="M19,3h-6v5l4-1l-1,4h5V5C21,3.9,20.1,3,19,3z"/></svg>			</div>
			<div class="icon-set__id">
				<span>pages</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#78909C" d="M22,7v9h-9l3.62-3.62c-1.39-1.16-3.17-1.88-5.12-1.88c-3.55,0-6.55,2.311-7.6,5.5l-1.39-0.46l-0.97-0.32C2.92,11.03,6.85,8,11.5,8c2.65,0,5.05,0.99,6.9,2.6L22,7z"/></g><g id="Capa_2"><path fill="#78909C" d="M22,7v8.96l-0.02,0.021c0,0-11.465-14.871-19.34-0.418l-1.1-0.343C2.92,11.03,6.85,8,11.5,8c2.65,0,5.05,0.99,6.9,2.6L22,7z"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>redo</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M24,11.31V0H0v24h24v-8h-1v-1.5c0-1.38-1.12-2.5-2.5-2.5S18,13.12,18,14.5V16h-0.52v-4.69H24z M19,14.5c0-0.83,0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5V16h-3V14.5z"/><path fill="#B0BEC5" d="M20.5,9.5c0.279,0,0.55,0.04,0.811,0.08L24,6c-3.34-2.51-7.5-4-12-4S3.34,3.49,0,6l12,16l3.5-4.67V14.5C15.5,11.74,17.74,9.5,20.5,9.5z"/><path fill="#FFA726" d="M23,16v-1.5c0-1.38-1.12-2.5-2.5-2.5S18,13.12,18,14.5V16c-0.55,0-1,0.45-1,1v4c0,0.55,0.45,1,1,1h5c0.55,0,1-0.45,1-1v-4C24,16.45,23.55,16,23,16z M19,16v-1.5c0-0.83,0.67-1.5,1.5-1.5s1.5,0.67,1.5,1.5V16H19z"/><path fill="#FFCC80" d="M23,14.5V16h-1v-1.5c0-0.83-0.67-1.5-1.5-1.5S19,13.67,19,14.5V16h-1v-1.5c0-1.38,1.12-2.5,2.5-2.5S23,13.12,23,14.5z"/></svg>			</div>
			<div class="icon-set__id">
				<span>wifi_lock</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9FA8DA" d="M11,4.07V2.05c-2.01,0.2-3.84,1-5.32,2.21L7.1,5.69C8.21,4.83,9.54,4.25,11,4.07z"/><path fill="#9FA8DA" d="M18.32,4.26C16.84,3.05,15.01,2.25,13,2.05v2.02c1.46,0.18,2.79,0.76,3.9,1.62L18.32,4.26z"/><path fill="#9FA8DA" d="M19.93,11h2.021c-0.2-2.01-1-3.84-2.21-5.32l-1.43,1.42C19.17,8.21,19.75,9.54,19.93,11z"/><path fill="#9FA8DA" d="M5.69,7.1L4.26,5.68C3.05,7.16,2.25,8.99,2.05,11h2.02C4.25,9.54,4.83,8.21,5.69,7.1z"/><path fill="#9FA8DA" d="M4.07,13H2.05c0.2,2.01,1,3.84,2.21,5.32l1.43-1.43C4.83,15.79,4.25,14.46,4.07,13z"/><path fill="#5C6BC0" d="M15,12c0-1.66-1.34-3-3-3s-3,1.34-3,3s1.34,3,3,3S15,13.66,15,12z"/><path fill="#9FA8DA" d="M18.311,16.9l1.43,1.43c1.21-1.48,2.01-3.32,2.21-5.32H19.93C19.75,14.46,19.17,15.79,18.311,16.9z"/><path fill="#9FA8DA" d="M13,19.93v2.021c2.01-0.2,3.84-1,5.32-2.21l-1.43-1.43C15.79,19.17,14.46,19.75,13,19.93z"/><path fill="#9FA8DA" d="M5.68,19.74C7.16,20.95,9,21.75,11,21.95V19.93c-1.46-0.18-2.79-0.76-3.9-1.619L5.68,19.74z"/></svg>			</div>
			<div class="icon-set__id">
				<span>filter_tilt_shift</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#78909C" points="12,7 12,3 2,3 2,21 22,21 22,7 "/><rect x="4" y="17" fill="#E6EE9C" width="2" height="2"/><rect x="4" y="13" fill="#E6EE9C" width="2" height="2"/><rect x="4" y="9" fill="#E6EE9C" width="2" height="2"/><rect x="4" y="5" fill="#E6EE9C" width="2" height="2"/><rect x="8" y="17" fill="#E6EE9C" width="2" height="2"/><rect x="8" y="13" fill="#E6EE9C" width="2" height="2"/><rect x="8" y="9" fill="#E6EE9C" width="2" height="2"/><rect x="8" y="5" fill="#E6EE9C" width="2" height="2"/><polygon fill="#E6EE9C" points="20,19 12,19 12,17 14,17 14,15 12,15 12,13 14,13 14,11 12,11 12,9 20,9 "/><rect x="16" y="11" fill="#78909C" width="2" height="2"/><rect x="16" y="15" fill="#78909C" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>business</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#009688" points="11.709,10.881 7,15.59 7,9 5,9 5,19 15,19 15,17 8.41,17 13.119,12.291 12.969,11.031 "/><rect x="14.857" y="3.28" transform="matrix(0.7071 0.7071 -0.7071 0.7071 10.4034 -8.8251)" fill="#4DB6AC" width="1.994" height="9.731"/></svg>			</div>
			<div class="icon-set__id">
				<span>call_received</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#37474F" d="M10,10v5h2V4h2v11h2V4h2V2h-8C7.79,2,6,3.79,6,6S7.79,10,10,10z"/><polygon fill="#F57C00" points="8,17 8,14 4,18 8,22 8,19 20,19 20,17 "/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="none" d="M-22.917,3.208h24v24h-24V3.208z"/></svg>			</div>
			<div class="icon-set__id">
				<span>functions</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z"/><g><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#B0BEC5" d="M20,2H4C2.9,2,2,2.9,2,4v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2z"/><rect x="4" y="16" fill="#ECEFF1" width="4" height="4"/><rect x="4" y="10" fill="#ECEFF1" width="4" height="4"/><rect x="4" y="4" fill="#ECEFF1" width="4" height="4"/><rect x="10" y="16" fill="#ECEFF1" width="4" height="4"/><rect x="10" y="10" fill="#ECEFF1" width="4" height="4"/><rect x="10" y="4" fill="#ECEFF1" width="4" height="4"/><rect x="16" y="16" fill="#ECEFF1" width="4" height="4"/><rect x="16" y="10" fill="#ECEFF1" width="4" height="4"/><rect x="16" y="4" fill="#ECEFF1" width="4" height="4"/></g></g><g id="Capa_2"><polygon fill="#9FA8DA" points="0,2.547 1.281,1.25 22.734,22.719 21.453,24 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>grid_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M24,9.79V24H0V9.79h2.01V4C2.01,2.9,2.9,2,4,2h16c1.1,0,2,0.9,2,2v5.79H24z"/><path fill="#FFAB91" d="M22,4v12c0,1.1-0.9,2-2,2H6l-4,4L2.01,9.79V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><path opacity="0.3" fill="#FF8A65" enable-background="new    " d="M22,4v5.79H2.01V4C2.01,2.9,2.9,2,4,2h16C21.1,2,22,2.9,22,4z"/><polygon fill="#455A64" points="18,14 14,10.8 14,14 6,14 6,6 14,6 14,9.2 18,6 "/></svg>			</div>
			<div class="icon-set__id">
				<span>voice_chat</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="#B0BEC5" d="M5.996,10.997h-2v-4h-2v10h2v-4h2v4h2v-10h-2V10.997z M12.996,6.997h-4v10h4c1.1,0,2-0.9,2-2v-6C14.996,7.897,14.096,6.997,12.996,6.997z M12.996,14.997h-2v-6h2V14.997z M21.996,10.997v-2c0-1.1-0.9-2-2-2h-4v10h2v-4h1l1,4h2l-1.189-4.17C21.506,12.517,21.996,11.817,21.996,10.997z M19.996,10.997h-2v-2h2V10.997z"/><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/></g><g id="Capa_2"><polygon fill="#9FA8DA" points="1.984,3.531 20.938,22.484 22.219,21.219 3.234,2.25 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>hdr_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9CCC65" fill-opacity="0.3" d="M17,5.33V11h-4V7l-2.13,4H7V5.33C7,4.6,7.6,4,8.33,4H10V2h4v2h1.67C16.4,4,17,4.6,17,5.33z"/><polygon fill="#FFFFFF" points="11,20 11,14.5 9,14.5 10.87,11 13,7 13,12.5 15,12.5 "/><path fill="#AED581" d="M17,11v9.67C17,21.4,16.4,22,15.66,22H8.33C7.6,22,7,21.4,7,20.67V11h3.87L9,14.5h2V20l4-7.5h-2V11H17z"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>battery_charging_60</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#607D8B" d="M3,21h18v-2H3V21z M3,13h18v-2H3V13z M3,3v2h18V3H3z"/><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#90A4AE" d="M3,17h18v-2H3V17z M3,9h18V7H3V9z"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_align_justify</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="3" y="11" fill="#26C6DA" width="2" height="2"/><rect x="3" y="15" fill="#26C6DA" width="2" height="2"/><path fill="#26C6DA" d="M5,21v-2H3C3,20.1,3.89,21,5,21z"/><rect x="3" y="7" fill="#26C6DA" width="2" height="2"/><rect x="15" y="19" fill="#26C6DA" width="2" height="2"/><path fill="#0097A7" d="M19,3H9C7.89,3,7,3.9,7,5v10c0,1.1,0.89,2,2,2h10c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M18.9,15h-10V5h10V15z"/><rect x="11" y="19" fill="#26C6DA" width="2" height="2"/><rect x="7" y="19" fill="#26C6DA" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>flip_to_front</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#455A64" points="9.984,18.984 13.984,18.984 13.969,13.5 10,13.531 "/><polygon fill="#455A64" points="5,4 5,7 10,7 10.031,12.297 13.984,12.25 14,7 19,7 19,4 "/><rect x="3" y="12" fill="#E53935" width="18" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>format_strikethrough</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#CFD8DC" d="M21.687,6.948L12,13L2.31,6.946C2.118,7.253,2,7.611,2,8v10c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2L21.99,8C21.99,7.611,21.876,7.254,21.687,6.948z"/><path fill="#90A4AE" d="M21.687,6.948C21.525,6.687,21.313,6.461,21.05,6.3L12,1L2.95,6.3C2.688,6.46,2.473,6.686,2.31,6.946L12,13L21.687,6.948z"/><polygon fill="#546E7A" points="12,13 3.74,7.84 12,3 20.26,7.84 "/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>drafts</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1_1_"><path fill="#455A64" d="M7,18c-1.1,0-1.99,0.9-1.99,2S5.9,22,7,22s2-0.9,2-2S8.1,18,7,18z"/><path fill="#7CB342" d="M7.2,14.63l-0.03,0.12c0,0.141,0.109,0.25,0.25,0.25H19v2H7c-1.1,0-2-0.9-2-2c0-0.35,0.09-0.68,0.25-0.96l1.35-2.45l-3.57-7.53L3,4H1V2h3.27l0.94,2H20c0.55,0,1,0.45,1,1c0,0.17-0.04,0.34-0.12,0.48l-3.58,6.49c-0.34,0.62-1,1.03-1.75,1.03H8.1L7.2,14.63z"/><path fill="#455A64" d="M17,18c-1.1,0-1.99,0.9-1.99,2S15.9,22,17,22s2-0.9,2-2S18.1,18,17,18z"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#607D8B" points="5.21,4 1,4 1,2 4.27,2 	"/><path fill="#607D8B" d="M19,15v2H7c-1.1,0-2-0.9-2-2c0-0.35,0.09-0.68,0.25-0.96l1.35-2.45c0,0,0.44,1.36,1.45,1.41H8.1L7.2,14.63l-0.03,0.12c0,0.141,0.109,0.25,0.25,0.25H19z"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_grocery_store</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#5E35B1" d="M14,5c0-1.1-0.9-2-2-2h-1V2c0-0.55-0.45-1-1-1H6C5.45,1,5,1.45,5,2v1H4C2.9,3,2,3.9,2,5v15c0,1.1,0.9,2,2,2h8c1.1,0,2-0.9,2-2h8V5H14z"/><rect x="7.969" y="5" fill="#311B92" enable-background="new    " width="14.031" height="14.969"/><rect x="10" y="16" opacity="0.5" fill="#B39DDB" enable-background="new    " width="2" height="2"/><rect x="10" y="7" opacity="0.5" fill="#B39DDB" enable-background="new    " width="2" height="2"/><rect x="14" y="16" opacity="0.5" fill="#B39DDB" enable-background="new    " width="2" height="2"/><rect x="14" y="7" opacity="0.5" fill="#B39DDB" enable-background="new    " width="2" height="2"/><rect x="18" y="16" opacity="0.5" fill="#B39DDB" enable-background="new    " width="2" height="2"/><rect x="18" y="7" opacity="0.5" fill="#B39DDB" enable-background="new    " width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>camera_roll</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#006064" d="M12,19c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S13.1,19,12,19z"/><path opacity="0.8" fill="#006064" d="M6,13c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S7.1,13,6,13z M12,13c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S13.1,13,12,13z M18,13c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S19.1,13,18,13z"/><path opacity="0.7" fill="#006064" d="M6,7C4.9,7,4,7.9,4,9s0.9,2,2,2s2-0.9,2-2S7.1,7,6,7z M18,7c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S19.1,7,18,7z M12,7c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S13.1,7,12,7z"/><path opacity="0.6" fill="#006064" d="M6,1C4.9,1,4,1.9,4,3s0.9,2,2,2s2-0.9,2-2S7.1,1,6,1z M18,5c1.1,0,2-0.9,2-2s-0.9-2-2-2s-2,0.9-2,2S16.9,5,18,5z M12,1c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S13.1,1,12,1z"/></svg>			</div>
			<div class="icon-set__id">
				<span>dialpad</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g id="Capa_1"><path fill="none" d="M0,0h24v24H0V0z M0,0h24v24H0V0z"/><g><path fill="#CFD8DC" d="M17.632,19.227c-1.561,1.559-3.61,2.34-5.66,2.34s-4.1-0.781-5.66-2.34c-3.12-3.121-3.12-8.191,0-11.311l5.66-5.66l5.66,5.66C20.752,11.036,20.752,16.105,17.632,19.227z"/><path fill="#B0BEC5" d="M17.632,19.227c-1.561,1.559-3.61,2.34-5.66,2.34V2.256l5.66,5.66C20.752,11.036,20.752,16.105,17.632,19.227z"/></g></g><g id="Capa_2"><polygon fill="#9FA8DA" points="2.984,5.781 4.266,4.484 20.984,21.219 19.703,22.516 	"/></g></svg>			</div>
			<div class="icon-set__id">
				<span>invert_colors_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#37474F" points="12.45,16 14.54,16 9.43,3 7.57,3 2.46,16 4.55,16 5.67,13 11.31,13 "/><polygon fill="#FFFFFF" points="6.43,11 8.5,5.48 10.57,11 "/><polygon fill="#43A047" points="21.59,11.59 13.5,19.68 9.83,16 8.42,17.41 13.51,22.5 23,13 "/></svg>			</div>
			<div class="icon-set__id">
				<span>spellcheck</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon opacity="0.6" fill="#455A64" points="21,17.31 21,21 15.2,21 17.82,17.31 "/><polygon fill="#90A4AE" points="17.82,17.31 15.2,21 3,21 3,17.31 "/><g><polygon fill="#CFD8DC" points="17.266,10.453 19.094,8.656 15.344,4.891 13.703,6.547 	"/><polygon fill="#78909C" points="17.811,9.94 6.75,21 3,21 3,17.25 14.061,6.19 	"/><path fill="#B0BEC5" d="M20.71,7.04c0.39-0.39,0.39-1.02,0-1.41l-2.34-2.34c-0.39-0.39-1.021-0.39-1.41,0l-1.83,1.83l3.75,3.75L20.71,7.04z"/><polygon fill="#B0BEC5" points="6.75,21 3,21 3,17.25 	"/></g><g opacity="0.4"><polygon fill="#78909C" points="17.82,17.31 15.2,21 9.88,21 13.281,17.313 	"/></g><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>mode_edit</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="1" y="7" fill="#C5CAE9" width="2" height="2"/><rect x="1" y="11" fill="#C5CAE9" width="2" height="2"/><path fill="#C5CAE9" d="M1,5h2V3C1.9,3,1,3.9,1,5z"/><rect x="9" y="19" fill="#C5CAE9" width="2" height="2"/><rect x="1" y="15" fill="#C5CAE9" width="2" height="2"/><path fill="#C5CAE9" d="M3,21v-2H1C1,20.1,1.9,21,3,21z"/><path fill="#0097A7" d="M21,3h-8v6h10V5C23,3.9,22.1,3,21,3z"/><rect x="21" y="15" fill="#C5CAE9" width="2" height="2"/><rect x="9" y="3" fill="#C5CAE9" width="2" height="2"/><rect x="5" y="19" fill="#C5CAE9" width="2" height="2"/><rect x="5" y="3" fill="#C5CAE9" width="2" height="2"/><path fill="#C5CAE9" d="M21,21c1.1,0,2-0.9,2-2h-2V21z"/><rect x="21" y="11" fill="#C5CAE9" width="2" height="2"/><rect x="13" y="19" fill="#C5CAE9" width="2" height="2"/><rect x="17" y="19" fill="#C5CAE9" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>tab_unselected</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#263238" d="M17,1.01L7,1C5.9,1,5.01,1.9,5.01,3v18c0,1.1,0.89,2,1.99,2h10c1.1,0,2-0.9,2-2V3C19,1.9,18.1,1.01,17,1.01z"/><rect x="7" y="5" fill="#546E7A" width="10" height="14"/></svg>			</div>
			<div class="icon-set__id">
				<span>stay_primary_portrait</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path opacity="0.6" fill="#FF5722" d="M3,19c0,1.1,0.89,2,2,2h14c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2H5C3.89,3,3,3.9,3,5S3,17.167,3,19z M5,5h14v14H5V5z"/><polygon fill="#FF5722" points="10.09,15.59 11.5,17 16.5,12 11.5,7 10.09,8.41 12.67,11 3,11 3,13 12.67,13 "/></svg>			</div>
			<div class="icon-set__id">
				<span>exit_to_app</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#C2185B" d="M5,6C3.9,6,3.01,6.9,3.01,8L3,20c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V8c0-1.1-0.9-2-2-2H5z"/><path fill="#F06292" d="M12,13c-2.76,0-5-2.24-5-5h2c0,1.66,1.34,3,3,3s3-1.34,3-3h2C17,10.76,14.76,13,12,13z"/><path fill="#90A4AE" d="M17,6c0-2.76-2.24-5-5-5S7,3.24,7,6h2c0-1.66,1.34-3,3-3s3,1.34,3,3H17z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_mall</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#5C6BC0" d="M20,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6C22,4.9,21.1,4,20,4z"/><rect x="4" y="6" fill="#C5CAE9" width="16" height="12"/><path fill="none" d="M0,0h24v24H0V0z"/><rect x="14" y="10" fill="#9FA8DA" width="2" height="2"/><rect x="14" y="14" fill="#9FA8DA" width="2" height="2"/><rect x="6" y="10" fill="#9FA8DA" width="2" height="2"/><rect x="10" y="10" fill="#9FA8DA" width="2" height="2"/></svg>			</div>
			<div class="icon-set__id">
				<span>image_aspect_ratio</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#78909C" points="12,7 8,11 11,11 11,21 13,21 13,11 16,11 "/><rect x="4" y="3" fill="#546E7A" width="16" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/><polygon fill="#78909C" points="12.02,7.02 12.02,21 11,21 11,11 8,11 12,7 "/><rect x="4" y="3" fill="#546E7A" width="16" height="1.03"/></svg>			</div>
			<div class="icon-set__id">
				<span>vertical_align_top</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#42A5F5" d="M9,2L7.17,4H4C2.9,4,2,4.9,2,6v12c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V6c0-1.1-0.9-2-2-2h-3.17L15,2H9z"/><circle fill="#9575CD" cx="12" cy="12" r="5"/><circle fill="#B39DDB" cx="12" cy="12" r="3.2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>local_see</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><rect x="7" y="3" fill="#7E57C2" width="2" height="2"/><rect x="7" y="11" fill="#7E57C2" width="2" height="2"/><rect x="7" y="19" fill="#7E57C2" width="2" height="2"/><rect x="11" y="15" fill="#7E57C2" width="2" height="2"/><rect x="11" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="19" fill="#7E57C2" width="2" height="2"/><rect x="3" y="15" fill="#7E57C2" width="2" height="2"/><rect x="3" y="11" fill="#7E57C2" width="2" height="2"/><rect x="3" y="7" fill="#7E57C2" width="2" height="2"/><rect x="3" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="11" fill="#7E57C2" width="2" height="2"/><rect x="19" y="15" fill="#7E57C2" width="2" height="2"/><rect x="19" y="11" fill="#7E57C2" width="2" height="2"/><rect x="19" y="19" fill="#7E57C2" width="2" height="2"/><rect x="19" y="7" fill="#7E57C2" width="2" height="2"/><rect x="11" y="7" fill="#7E57C2" width="2" height="2"/><rect x="19" y="3" fill="#7E57C2" width="2" height="2"/><rect x="11" y="3" fill="#7E57C2" width="2" height="2"/><rect x="15" y="19" fill="#7E57C2" width="2" height="2"/><rect x="15" y="11" fill="#7E57C2" width="2" height="2"/><rect x="15" y="3" fill="#7E57C2" width="2" height="2"/><path fill="none" d="M0,0h24v24H0V0z"/></svg>			</div>
			<div class="icon-set__id">
				<span>border_clear</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19,7H5C3.9,7,3,7.9,3,9v6c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V9C21,7.9,20.1,7,19,7z M19,15H5V9h14V15z"/><path fill="none" d="M29.5-3.813h24v24h-24V-3.813z"/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_7_5</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#64B5F6" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z"/><path fill="#2196F3" d="M12,4c-4.417,0-8,3.583-8,8c0,4.414,3.583,8,8,8c4.414,0,8-3.586,8-8C20,7.583,16.414,4,12,4z"/><path fill="#CFD8DC" d="M13,19h-2v-2h2V19z M15.07,11.25l-0.9,0.92C13.45,12.9,13,13.5,13,15h-2v-0.5c0-1.1,0.45-2.1,1.17-2.83l1.24-1.26C13.779,10.05,14,9.55,14,9c0-1.1-0.9-2-2-2c-1.1,0-2,0.9-2,2H8c0-2.21,1.79-4,4-4s4,1.79,4,4C16,9.88,15.641,10.68,15.07,11.25z"/></svg>			</div>
			<div class="icon-set__id">
				<span>help</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#9575CD" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z"/><rect x="5" y="5" fill="#B3E5FC" width="14" height="14"/><polygon fill="#66BB6A" points="13.96,12.29 11.21,15.83 9.25,13.47 6.5,17 17.5,17 "/></svg>			</div>
			<div class="icon-set__id">
				<span>crop_original</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="#E53935" d="M12,2C6.48,2,2,6.48,2,12c0,5.52,4.48,10,10,10c5.52,0,10-4.48,10-10C22,6.48,17.52,2,12,2z M12,20c-4.42,0-8-3.58-8-8s3.58-8,8-8s8,3.58,8,8S16.42,20,12,20z"/><path fill="none" d="M0,0v24h24V0H0z M12,20c-4.42,0-8-3.58-8-8s3.58-8,8-8s8,3.58,8,8S16.42,20,12,20z"/></svg>			</div>
			<div class="icon-set__id">
				<span>radio_button_off</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><path fill="none" d="M0,0h24v24H0V0z"/><path fill="#D32F2F" d="M12.216,12.083L13,16h7V6h-5.6L12.216,12.083z"/><path fill="#5D4037" d="M5,14v7h2v-8L5,14z"/><polygon fill="#F44336" points="16.002,14 5,14 5,4 14,4 "/></svg>			</div>
			<div class="icon-set__id">
				<span>flag</span>
			</div>
		</div>
	</div>	 
							
	<div class="col_xs_6 col_sm_4 col_md_3">
		<div class="icon-set__inner">
			<div class="icon-set__icon">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><polygon fill="#26C6DA" points="14,4 16.29,6.29 12,10.625 13,11.59 12.984,12.406 17.71,7.71 20,10 20,4 "/><polygon fill="#66BB6A" points="10,4 4,4 4,10 6.29,7.71 11,12.41 11,20 13,20 13,11.59 7.71,6.29 "/></svg>			</div>
			<div class="icon-set__id">
				<span>call_split</span>
			</div>
		</div>
	</div>	 
								</div>
	</div>
</div>
	
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>