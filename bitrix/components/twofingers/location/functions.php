<?php 
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);

use \TwoFingers\Location\Location;
use Bitrix\Main\Application;
use \TwoFingers\Location\SessionStorage;

require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule('twofingers.location')
    || ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'))
{
    require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/epilog_after.php");
    return;
}

$request = Application::getInstance()->getContext()->getRequest();

if ($request->get('request') == 'getcities')
{
    try{
        $defaultCities  = Location::getDefaultList();
        $cities         = Location::getList();
        $sameNames      = Location::getSameNames($cities);
    } catch (\Exception $e) {
        $cities = $defaultCities = $sameNames = [];
    }

    foreach ($sameNames as $name => $citiesIds){
        if (count($citiesIds) < 2) continue;

        foreach ($citiesIds as $cityId)
            $cities[$cityId]['SHOW_REGION'] = 'Y';
    }

    $response = [
        'CITIES'            => array_values($cities),
        'DEFAULT_CITIES'    => $defaultCities
    ];

    print Bitrix\Main\Web\Json::encode($response);
}

if ($request->get('request') == 'setcity')
{
    $locationId     = trim($request->get('location_id'));

    if (!strlen($locationId))
        $locationId     = trim($request->get('city')); // old style

    if (!strlen($locationId)) return;

    $locationName   = trim($request->get('location_name'));
    if (!strlen($locationName))
        $locationName = Location::getNameById($locationId);

    if ($locationName){
        SessionStorage::setLocationId($locationId);
        SessionStorage::setLocationName($locationName);
        SessionStorage::setRegionId($request->get('region_id') == 'undefined' ? '' : $request->get('region_id'));
        SessionStorage::setRegionName($request->get('region_name') == 'undefined' ? '' : $request->get('region_name'));
        SessionStorage::setCountryId($request->get('country_id') == 'undefined' ? '' : $request->get('country_id'));
        SessionStorage::setCountryName($request->get('country_name') == 'undefined' ? '' : $request->get('country_name'));


    } else {
        SessionStorage::clear();
    }
}

if ($request->get('request') == 'close_confirm_popup') {
    SessionStorage::setConfirmPopupClosedByUser('Y');
}

require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/epilog_after.php");