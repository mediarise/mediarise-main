<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use TwoFingers\Location\Settings,
    Bitrix\Main\Loader,
    \TwoFingers\Location\SessionStorage,
    \TwoFingers\Location\Current,
    \TwoFingers\Location\Location;

if(!Loader::IncludeModule('twofingers.location'))
{
    ShowError('Module twofingers.location not installed');
    return;
}

$arResult['CALL_CONFIRM_POPUP'] = 'N';
$arResult['CALL_LOCATION_POPUP']= 'N';

$settings = Settings::getList();

// getting new data
if (SessionStorage::isEmpty())
{
    SessionStorage::setFromCurrent(Current::getInstance());

    if (($settings['TF_LOCATION_SHOW_CONFIRM_POPUP'] == 'Y')
        || ($settings['TF_LOCATION_SHOW_CONFIRM_POPUP'] == 'A'))
    {
        $arResult['CALL_CONFIRM_POPUP'] = 'Y';
    }
} elseif (($settings['TF_LOCATION_SHOW_CONFIRM_POPUP'] == 'A')
    && (SessionStorage::getConfirmPopupClosedByUser() == 'N'))
{
    $arResult['CALL_CONFIRM_POPUP'] = 'Y';
}

// try to get info
if (!SessionStorage::isEmpty())
{
    $arResult['CITY_ID']    = SessionStorage::getLocationId();
    $arResult['CITY_NAME']  = SessionStorage::getLocationName();

    if (empty($arResult['CITY_NAME'])){
        $arResult['CITY_NAME'] = Location::getNameById($arResult['CITY_ID']);

        if ($arResult['CITY_NAME'])
            SessionStorage::setLocationName($arResult['CITY_NAME']);
        else
            SessionStorage::clear();
    }
}

if (empty($arResult['CITY_ID']) || empty($arResult['CITY_NAME']) )
{
    $arResult['CITY_NAME']  = GetMessage("TF_LOCATION_CHOOSE");
    $arResult['CITY_ID']    = false;

    if (($settings['TF_LOCATION_ONUNKNOWN'] == 'Y')
        && ($arResult['CALL_CONFIRM_POPUP'] == 'N'))
        $arResult['CALL_LOCATION_POPUP'] = 'Y';

    SessionStorage::clear();
}

$arResult['SETTINGS']       = $settings;
$arResult['COMPONENT_PATH'] = $componentPath;

$this->IncludeComponentTemplate();

return $arResult;
