<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

$this->createFrame()->begin(Loc::getMessage('TF_LOCATION_LOADING'));

$jsCallback = str_replace("'", "\'", $arResult['SETTINGS']['TF_LOCATION_CALLBACK']).'; ';

if($arParams['ORDER_TEMPLATE'] == 'Y'):
    if(!empty($arParams['PARAMS']['ONCITYCHANGE']))
        $jsCallback .= $arParams['PARAMS']['ONCITYCHANGE'].'(); ';

    if(!empty($arParams['PARAMS']['JS_CALLBACK']))
        $jsCallback .= $arParams['PARAMS']['JS_CALLBACK'].'(); ';

    $jsCallback = str_replace(';;', ';', $jsCallback);

    ?><span class="tfl__link-container">
         <a href="#tfLocationPopup"
            data-location-id="<?= $arResult['CITY_ID']?>"
            class="<?= $arResult['SETTINGS']['TF_LOCATION_ORDERLINK_CLASS']?> tfl__link tfl__link_order"
            onclick="return tfLocationPopupOpen('<?= $arResult['COMPONENT_PATH']?>', '<?= $jsCallback?>');"
         ><?= $arResult['CITY_NAME']?></a>
    </span>
    <input type="hidden" name="<?= $arParams['PARAMS']['INPUT_NAME']?>" class="tf_location_city_input" value="<?= $arResult['CITY_ID']?>">
    <input type="hidden" autocomplete="off" class="bx-ui-sls-route" style="padding: 0px; margin: 0px;" value="<?= $arResult['CITY_NAME']?>">
<?php  else: ?>
    <span class="tfl__link-container">
        <?php 
        if( strlen($arResult['SETTINGS']['TF_LOCATION_HEADLINK_TEXT']) > 0 )
            echo $arResult['SETTINGS']['TF_LOCATION_HEADLINK_TEXT'], ': ';
        ?><a href="#tfLocationPopup"
           data-location-id="<?= $arResult['CITY_ID']?>"
           class="<?= $arResult['SETTINGS']['TF_LOCATION_HEADLINK_CLASS']?> tfl__link"
           onclick="return tfLocationPopupOpen('<?= $arResult['COMPONENT_PATH']?>', '<?= $jsCallback?>');"
        ><?= $arResult['CITY_NAME']?></a>
    </span>
<?php endif;

if($arResult['CALL_CONFIRM_POPUP'] == 'Y' ):?>
    <script>
        $(function()
        {
            tflConfirmPopupOpen('<?= $arResult['COMPONENT_PATH']?>', '<?= $jsCallback?>');
        });
    </script>
<?php  endif;

if($arResult['CALL_LOCATION_POPUP'] == 'Y' ):?>
    <script>
        $(function()
        {
            tfLocationPopupOpen('<?= $arResult['COMPONENT_PATH']?>', '<?= $jsCallback?>');
        });
    </script>
<?php  endif;

if ($GLOBALS['TF_LOCATION_TEMPLATE_LOADED'] == 'Y') return;

$GLOBALS['TF_LOCATION_TEMPLATE_LOADED'] = 'Y';

?>
<div class="tfl-popup-overlay" style="display:none;"></div>
<div class="tfl-popup" style="display:none; border-radius:<?= intval($arResult['SETTINGS']['TF_LOCATION_POPUP_RADIUS'])?>px">
    <?php  $title = !is_null($arResult['SETTINGS']['TF_LOCATION_LOCATION_POPUP_HEADER'])
        ? $arResult['SETTINGS']['TF_LOCATION_LOCATION_POPUP_HEADER']
        : GetMessage("TF_LOCATION_CHECK_CITY");

    if (strlen($title)): ?>
        <div class="tfl-popup__title"><?= $title?></div>
    <?php  endif; ?>
    <div class="tfl-popup__search-wrapper">
        <input
                type="text"
                autocomplete="off"
                name="search"
                placeholder="<?= !is_null($arResult['SETTINGS']['TF_LOCATION_LOCATION_POPUP_PLACEHOLDER'])
                    ? $arResult['SETTINGS']['TF_LOCATION_LOCATION_POPUP_PLACEHOLDER']
                    : GetMessage("TF_LOCATION_CHECK_CITY_PLACEHOLDER")?>"
                class="tfl-popup__search-input"><a href="#" class="tfl-popup__clear-field"></a>
    </div>
    <div class="tfl-popup__default-locations-container">
        <div class="tfl-popup__scroll-container">
            <ul class="tfl-popup__default-locations"></ul>
        </div>
    </div>
    <div class="tfl-popup__locations-container">
        <div class="tfl-popup__scroll-container">
            <ul class="tfl-popup__locations"></ul>
        </div>
    </div>
    <div class="tfl-popup__close <?= strlen($title) ? 'tfl-popup__close-big' : 'tfl-popup__close-small'?>"></div>
</div>
<div class="tfl-define-popup" style="display:none; border-radius:<?= intval($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_RADIUS'])?>px">
    <style>

        .tfl-define-popup__main{
            color: <?= $arResult['PRIMARY_COLOR']?>;
            background-color: <?= $arResult['PRIMARY_BG']?>;
        }
        .tfl-define-popup__main:hover{
            color: <?= $arResult['PRIMARY_COLOR_HOVER']?>;
            background-color: <?= $arResult['PRIMARY_BG_HOVER']?>;
        }

        .tfl-define-popup__second:hover{
            color: <?= $arResult['PRIMARY_BG']?>;
        }
    </style>
    <?php  if (intval($arResult['CITY_ID'])):

        $text = is_null($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_TEXT'])
            ? Loc::getMessage('TF_LOCATION_YOUR_CITY')
            : $arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_TEXT'];

        $text = str_replace('#location#', $arResult['CITY_NAME'], $text);

    else:

        $text = is_null($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_ERROR_TEXT'])
            ? Loc::getMessage('TF_LOCATION_NO_CITY')
            : $arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_ERROR_TEXT'];

    endif;

    ?><div class="tfl-define-popup__text"><?= $text?></div>
    <div class="tfl-define-popup__buttons" style="border-radius: 0 0 <?= intval($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_RADIUS'])?>px <?= intval($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_RADIUS'])?>px">
        <?php  if (intval($arResult['CITY_ID'])): ?>
            <a href="#" class="tfl-define-popup__button tfl-define-popup__main tfl-define-popup__yes"><?= Loc::getMessage('TF_LOCATION_YES')?></a>
            <a href="#" class="tfl-define-popup__button tfl-define-popup__second tfl-define-popup__list"><?= Loc::getMessage('TF_LOCATION_LIST')?></a>
        <?php  else: ?>
            <a href="#" class="tfl-define-popup__button tfl-define-popup__main tfl-define-popup__list"><?= Loc::getMessage('TF_LOCATION_LIST')?></a>
            <a href="#" class="tfl-define-popup__button tfl-define-popup__second tfl-define-popup__yes"><?= Loc::getMessage('TF_LOCATION_CLOSE')?></a>
        <?php  endif;?>
    </div>
    <div class="tfl-popup__close tfl-popup__close-small"></div>
</div>