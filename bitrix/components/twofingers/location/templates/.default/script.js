var tf_location_cities_loaded = false,
    speed = 300;

if( !window.BX && top.BX )
    window.BX = top.BX;

/**
 *
 * @param componentPath
 * @param callbackHandler
 */
function tfLocationPopupOpen(componentPath, callbackHandler)
{
    var $body           = $('body'),
        $bodyChildren   = $body.children(),
        $overlay        = $bodyChildren.filter('.tfl-popup-overlay'),
        $popup          = $bodyChildren.filter('.tfl-popup'),
        $locationsList, $defaultsList, $searchInput,
        $locationsContainer, $defaultLocationsContainer, $close, closeHandler, $clear;

    closeConfirmPopup(componentPath);

    if (!$overlay.length){
        $overlay = $('.tfl-popup-overlay');
        $overlay.appendTo('body');
    }

    if (!$popup.length) {
        $popup = $('.tfl-popup');
        $popup.appendTo('body');
    }

    $locationsList      = $popup.find('.tfl-popup__locations');
    $defaultsList       = $popup.find('.tfl-popup__default-locations');
    $searchInput        = $popup.find('.tfl-popup__search-input');
    $locationsContainer = $popup.find(".tfl-popup__locations-container");
    $defaultLocationsContainer  = $popup.find(".tfl-popup__default-locations-container");
    $close              = $popup.find('.tfl-popup__close');
    $clear              = $popup.find('.tfl-popup__clear-field');

    $bodyChildren.addClass('tfl-body-blur');
    $body.addClass('tfl-body-freeze');

    $overlay.fadeIn(speed);
    $popup.fadeIn(speed, function()
    {
        $overlay.removeClass('tfl-body-blur');
        $popup.removeClass('tfl-body-blur');

        if (!tf_location_cities_loaded){
            init();
            tf_location_cities_loaded = true;
        }
    });

    /**
     *
     */
    function init()
    {
        $popup.addClass('tfl-popup_loading');

        $.get(componentPath + '/functions.php', {request: 'getcities'}, function(data)
        {
            if (data.CITIES)
            {
                $.each(data.CITIES, function(key, city)
                {
                    var location;

                    location = '<li><a class="tfl-popup__location-link" '
                        + 'data-id="' + city.ID
                        + '" data-name="' + city.NAME
                        + '" data-region-name="' + city.REGION_NAME
                        + '" data-region-id="' + city.REGION_ID
                        + '" data-country-id="' + city.COUNTRY_ID
                        + '" data-country-name="' + city.COUNTRY_NAME
                        + '" href="#">' + city.NAME + '</a>';

                    if (city.SHOW_REGION == 'Y'){
                        location += '<div class="tf-location__region">' + city.REGION_NAME +'</div>';
                    }

                    location += '</li>';

                    $locationsList.append(location)
                });
            }

            if (data.DEFAULT_CITIES.length)
            {
                $.each(data.DEFAULT_CITIES, function(key, city)
                {
                    $defaultsList.append('<li><a class="tfl-popup__location-link" '
                        +' data-id="' + city.ID
                        + '" data-name="' + city.NAME
                        + '" data-region-name="' + city.REGION_NAME
                        + '" data-region-id="' + city.REGION_ID
                        + '" data-country-name="' + city.COUNTRY_NAME
                        + '" data-country-id="' + city.COUNTRY_ID
                        + '" href="#">' + city.NAME + '</a></li>');
                });

                $popup.addClass('tfl-popup__with-defaults');
                $defaultLocationsContainer.find('.tfl-popup__scroll-container').slimscroll();
            }

            $popup.find('.tfl-popup__location-link').on('click', function()
            {
                var location            = this,
                    selectedCityID      = $(location).data('id'),
                    selectedCityNAME    = $(location).html(),
                    $orderLocation      = $('.tfl__link.tfl__link_order'),
                    $route              = $('.bx-ui-sls-route'),
                    $saleLocationInput  = $('.tf_location_city_input')/*,
                        $fake               = $('.bx-ui-sls-fake')*/;

                if ($orderLocation.length && $route.length) {
                    $route.val(selectedCityNAME);
                }

                /* if ($fake.length) {
                     $fake.val(selectedCityID);
                 }*/

                $('.tfl__link')
                    .html(selectedCityNAME)
                    .data('location-id', selectedCityID)
                    .data('region-id', $(location).data('region-id'))
                    .data('country-id', $(location).data('country-id'));

                if ($saleLocationInput.length)
                    $saleLocationInput.val(selectedCityID);

                $body.removeClass('tfl-body-freeze');

                $.post(componentPath + '/functions.php', {
                    request:        'setcity',
                    location_id:    selectedCityID,
                    location_name:  selectedCityNAME,
                    region_id:      $(location).data('region-id'),
                    region_name:    $(location).data('region-name'),
                    country_id:     $(location).data('country-id'),
                    country_name:   $(location).data('country-name')
                }, function()
                {
                    $overlay.fadeOut(speed);
                    $popup.fadeOut(speed, function()
                    {
                        var actualCallBackHandler = callbackHandler;

                        $bodyChildren.removeClass('tfl-body-blur');

                        if (!!BX) {
                            BX.onCustomEvent('onTFLocationSetLocation', [location]);
                        }

                        try
                        {
                            actualCallBackHandler = actualCallBackHandler.replace('#TF_LOCATION_CITY_ID#', selectedCityID);
                            actualCallBackHandler = actualCallBackHandler.replace('#TF_LOCATION_CITY_NAME#', selectedCityNAME);

                            eval( actualCallBackHandler );
                        }
                        catch(e)
                        {
                            return false;
                        }
                    });
                });
                return false;
            });

            $locationsContainer.find('.tfl-popup__scroll-container').slimscroll();
            $popup.removeClass('tfl-popup_loading');

        }, 'json');

        $searchInput.keyup(function()
        {
            var $searchInputRequest = $(this).val().toUpperCase(),
                $locations = $locationsList.find('a');

            if ($searchInputRequest.length > 0){
                $clear.fadeIn();
            } else {
                $clear.fadeOut();
            }

            if (!$locations.length) return;

            $locations.each(function()
            {
                var city = $(this).html().toUpperCase();

                if (city.indexOf($searchInputRequest) < 0) {
                    $(this).parent().hide();
                } else {
                    $(this).parent().show();
                }
            });
        });

        $clear.click(function()
        {
            $(this).fadeOut();
            $searchInput.val('');
        });
    }

    /// close
    closeHandler = function closeLocationsPopup()
    {
        $overlay.fadeOut(speed);
        $popup.fadeOut(speed);
        $bodyChildren.removeClass('tfl-body-blur');
        $body.removeClass('tfl-body-freeze');
    };

    $overlay.on('click', closeHandler);
    $close.on('click', closeHandler);

    return false;
}

function tflConfirmPopupOpen(componentPath, callbackHandler)
{
    var $link           = $('.tfl__link:visible').not('.tfl__link_order'),
        $body           = $('body'),
        $bodyChildren   = $body.children(),
        $popup          = $bodyChildren.filter('.tfl-define-popup'),
        $close, $confirm, $list, closeHandler;

    if (!$popup.length) {
        $popup = $('.tfl-define-popup');
        $popup.appendTo('body');
    }

    if (!$link.length || !$popup.length) return;

    if (($link.offset().left + $link.width() < 0)) return;

    setConfirmPosition();

    $(window).on('resize', function () {
        setConfirmPosition();
    });

    if ($popup.is(':visible')) return;

    $close      = $popup.find('.tfl-popup__close');
    $confirm    = $popup.find('.tfl-define-popup__yes');
    $list       = $popup.find('.tfl-define-popup__list');

    $popup.fadeIn(speed);

    closeHandler = function(e){
        e.preventDefault();
        e.stopPropagation();
        closeConfirmPopup(componentPath);
    };

    $close.on('click', closeHandler);
    $confirm.on('click', closeHandler);
    $list.on('click', closeHandler);
    $list.on('click', function(){
        tfLocationPopupOpen(componentPath, callbackHandler);
    });

    function setConfirmPosition() {

        $popup.offset({
            top: $link.offset().top + $link.outerHeight() + 12/*,
            left: $link.offset().left + ($link.width() / 2)*/});
        $popup.css('left', ($link.offset().left + ($link.width() / 2)) + 'px')
    }
}



function closeConfirmPopup(componentPath, $popup)
{
    if (!$popup) {
        $popup = $('.tfl-define-popup');
    }

    if ($popup.length) {
        $popup.fadeOut(speed);
    }

    $.post(componentPath + '/functions.php', {request: 'close_confirm_popup'}, function()
    {

    });
}

/*

EVENT TEST

BX.addCustomEvent("onTFLocationSetLocation", function(location)
{
    var $location = $(location);

    console.log('location id: ' + $location.data('id'));
    console.log('location name: ' + $location.data('name'));
    console.log('location region name: ' + $location.data('region-name'));
});
*/