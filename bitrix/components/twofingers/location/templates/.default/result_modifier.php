<?php 
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 13.03.2019
 * Time: 11:26
 *
 * @author Pavel Shulaev (https://rover-it.me)
 */
$arResult['PRIMARY_COLOR'] = is_null($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_COLOR'])
    ? '#ffffff'
    : $arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_COLOR'];

$arResult['PRIMARY_COLOR_HOVER'] = is_null($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_COLOR_HOVER'])
    ? '#333333'
    : $arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_COLOR_HOVER'];

$arResult['PRIMARY_BG'] = is_null($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_BG'])
    ? '#2b7de0'
    : $arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_BG'];

$arResult['PRIMARY_BG_HOVER'] = is_null($arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_BG_HOVER'])
    ? '#468de4'
    : $arResult['SETTINGS']['TF_LOCATION_CONFIRM_POPUP_PRIMARY_BG_HOVER'];