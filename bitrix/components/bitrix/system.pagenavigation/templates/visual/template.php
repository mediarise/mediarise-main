<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"])
{
	if (0 == $arResult["NavRecordCount"] || (1 == $arResult["NavPageCount"] && false == $arResult["NavShowAll"]))
		return;
}
if ('' != $arResult["NavTitle"])
	$arResult["NavTitle"] .= ' ';

$strSelectPath = $arResult['sUrlPathParams'].($arResult["bSavePage"] ? '&PAGEN_'.$arResult["NavNum"].'='.(true !== $arResult["bDescPageNumbering"] ? 1 : '').'&' : '').'SHOWALL_'.$arResult["NavNum"].'=0&SIZEN_'.$arResult["NavNum"].'=';

?>
<div class="bx_pagination_bottom">
	<div class="bx_pagination_section_two">
		<div class="bx_pg_section bx_pg_show_col">
			<span class="bx_wsnw"><?php 
			if ($arParams['USE_PAGE_SIZE'] == 'Y' && !$arResult["NavShowAll"])
			{
			?>
				<span class="bx_pg_text"><?php  echo GetMessage('nav_size_descr'); ?></span>
				<div class="bx_pagination_select_container">
					<select onchange="if (-1 < this.selectedIndex) {location.href='<?php  echo $strSelectPath; ?>'+this[selectedIndex].value};"><?php 
					foreach ($arResult['TPL_DATA']['PAGE_SIZES'] as &$intOneSize)
					{
						?><option value="<?php  echo $intOneSize; ?>"<?php  echo ($arResult['NavPageSize'] == $intOneSize ? ' selected="selected"' : ''); ?>><?php  echo $intOneSize; ?></option>
						<?php 
					}
					unset($intOneSize);
					?>
					</select>
				</div><?php 
			}
			?>
				<?php  echo $arResult["NavTitle"]; ?><?= $arResult["NavFirstRecordShow"]; ?> - <?= $arResult["NavLastRecordShow"]?> <?= GetMessage("nav_of")?> <?= $arResult["NavRecordCount"]?>
			</span>
		</div>
	</div>

	<div class="bx_pagination_section_one">
		<div class="bx_pg_section pg_pagination_num">
			<div class="bx_pagination_page"><?php 
if ($arResult["NavShowAll"])
{
?>
				<span class="bx_pg_text"><?php  echo GetMessage('nav_all_descr'); ?></span>
				<ul>
					<li><a href="<?= $arResult['sUrlPathParams']; ?>SHOWALL_<?= $arResult["NavNum"]?>=0&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>"><?php  echo GetMessage('nav_show_pages'); ?></a></li>
				</ul>
<?php 
}
else
{
?>
				<span class="bx_pg_text"><?php  echo GetMessage('nav_pages'); ?></span>
				<ul>
<?php 
	if (true === $arResult["bDescPageNumbering"])
	{
		?><li><?php 
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
		{
			?><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]+1)?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo GetMessage('nav_prev_title'); ?>">&#8592;</a><?php 
		}
		else
		{
			?>&#8592;<?php 
		}
		?></li><?php 
		$NavRecordGroup = $arResult["NavPageCount"];
		while ($NavRecordGroup >= 1)
		{
			$NavRecordGroupPrint = $arResult["NavPageCount"] - $NavRecordGroup + 1;
			$strTitle = GetMessage(
				'nav_page_num_title',
				array('#NUM#' => $NavRecordGroupPrint)
			);
			if ($NavRecordGroup == $arResult["NavPageNomer"])
			{
				?><li class="bx_active" title="<?php  echo GetMessage('nav_page_current_title'); ?>"><?php  echo $NavRecordGroupPrint; ?></li><?php 
			}
			elseif ($NavRecordGroup == $arResult["NavPageCount"] && $arResult["bSavePage"] == false)
			{
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>"><?= $NavRecordGroupPrint?></a></li><?php 
			}
			else
			{
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= $NavRecordGroup?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>"><?= $NavRecordGroupPrint?></a></li><?php 
			}
			if (1 == ($arResult["NavPageCount"] - $NavRecordGroup) && 2 < ($arResult["NavPageCount"] - $arResult["nStartPage"]))
			{
				$middlePage = floor(($arResult["nStartPage"] + $NavRecordGroup)/2);
				$NavRecordGroupPrint = $arResult["NavPageCount"] - $middlePage + 1;
				$strTitle = GetMessage(
					'nav_page_num_title',
					array('#NUM#' => $NavRecordGroupPrint)
				);
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= $middlePage?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>">...</a></li><?php 
				$NavRecordGroup = $arResult["nStartPage"];
			}
			elseif ($NavRecordGroup == $arResult["nEndPage"] && 3 < $arResult["nEndPage"])
			{
				$middlePage = ceil(($arResult["nEndPage"] + 2)/2);
				$NavRecordGroupPrint = $arResult["NavPageCount"] - $middlePage + 1;
				$strTitle = GetMessage(
					'nav_page_num_title',
					array('#NUM#' => $NavRecordGroupPrint)
				);
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= $middlePage?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>">...</a></li><?php 
				$NavRecordGroup = 2;
			}
			else
			{
				$NavRecordGroup--;
			}
		}
		?><li><?php 
		if ($arResult["NavPageNomer"] > 1)
		{
			?><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]+1)?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo GetMessage('nav_next_title'); ?>">&#8594;</a><?php 
		}
		else
		{
			?>&#8594;<?php 
		}
		?></li><?php 
	}
	else
	{
?>
					<li><?php 
		if (1 < $arResult["NavPageNomer"])
		{
			?><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]-1)?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo GetMessage('nav_prev_title'); ?>">&#8592;</a><?php 
		}
		else
		{
			?>&#8592;<?php 
		}
		?></li><?php 
		$NavRecordGroup = 1;
		while($NavRecordGroup <= $arResult["NavPageCount"])
		{
			$strTitle = GetMessage(
				'nav_page_num_title',
				array('#NUM#' => $NavRecordGroup)
			);
			if ($NavRecordGroup == $arResult["NavPageNomer"])
			{
				?><li class="bx_active" title="<?php  echo GetMessage('nav_page_current_title'); ?>"><?php  echo $NavRecordGroup; ?></li><?php 
			}
			elseif ($NavRecordGroup == 1 && $arResult["bSavePage"] == false)
			{
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>"><?= $NavRecordGroup?></a></li><?php 
			}
			else
			{
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= $NavRecordGroup?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>"><?= $NavRecordGroup?></a></li><?php 
			}
			if ($NavRecordGroup == 2 && $arResult["nStartPage"] > 3 && $arResult["nStartPage"] - $NavRecordGroup > 1)
			{
				$middlePage = ceil(($arResult["nStartPage"] + $NavRecordGroup)/2);
				$strTitle = GetMessage(
					'nav_page_num_title',
					array('#NUM#' => $middlePage)
				);
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= $middlePage?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>">...</a></li><?php 
				$NavRecordGroup = $arResult["nStartPage"];
			}
			elseif ($NavRecordGroup == $arResult["nEndPage"] && $arResult["nEndPage"] < ($arResult["NavPageCount"] - 2))
			{
				$middlePage = floor(($arResult["NavPageCount"] + $arResult["nEndPage"] - 1)/2);
				$strTitle = GetMessage(
					'nav_page_num_title',
					array('#NUM#' => $middlePage)
				);
				?><li><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= $middlePage?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo $strTitle; ?>">...</a></li><?php 
				$NavRecordGroup = $arResult["NavPageCount"]-1;
			}
			else
			{
				$NavRecordGroup++;
			}
		}
			?>
					<li><?php 
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
		{
			?><a href="<?= $arResult['sUrlPathParams']; ?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]+1)?>&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult['NavPageSize']; ?>" title="<?php  echo GetMessage('nav_next_title'); ?>">&#8594;</a><?php 
		}
		else
		{
			?>&#8594;<?php 
		}
		?></li><?php 
		if ($arResult["bShowAll"])
		{
			?><li><a href="<?= $arResult['sUrlPathParams']; ?>SHOWALL_<?= $arResult["NavNum"]?>=1&SIZEN_<?= $arResult["NavNum"]?>=<?= $arResult["NavPageSize"]?>"><?php  echo GetMessage('nav_all'); ?></a></li><?php 
		}
	}
?>
				</ul><?php 
}
?>
			</div>
		</div>
	</div>
</div>