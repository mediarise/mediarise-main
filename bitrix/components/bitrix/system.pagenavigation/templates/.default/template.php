<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<font class="text"><?= $arResult["NavTitle"]?> 

<?php if($arResult["bDescPageNumbering"] === true):?>

	<?= $arResult["NavFirstRecordShow"]?> <?= GetMessage("nav_to")?> <?= $arResult["NavLastRecordShow"]?> <?= GetMessage("nav_of")?> <?= $arResult["NavRecordCount"]?><br /></font>

	<font class="text">

	<?php if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?php if($arResult["bSavePage"]):?>
			<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= $arResult["NavPageCount"]?>"><?= GetMessage("nav_begin")?></a>
			|
			<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]+1)?>"><?= GetMessage("nav_prev")?></a>
			|
		<?php else:?>
			<a href="<?= $arResult["sUrlPath"]?><?= $strNavQueryStringFull?>"><?= GetMessage("nav_begin")?></a>
			|
			<?php if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
				<a href="<?= $arResult["sUrlPath"]?><?= $strNavQueryStringFull?>"><?= GetMessage("nav_prev")?></a>
				|
			<?php else:?>
				<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]+1)?>"><?= GetMessage("nav_prev")?></a>
				|
			<?php endif?>
		<?php endif?>
	<?php else:?>
		<?= GetMessage("nav_begin")?>&nbsp;|&nbsp;<?= GetMessage("nav_prev")?>&nbsp;|
	<?php endif?>

	<?php while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
		<?php $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

		<?php if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<b><?= $NavRecordGroupPrint?></b>
		<?php elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
			<a href="<?= $arResult["sUrlPath"]?><?= $strNavQueryStringFull?>"><?= $NavRecordGroupPrint?></a>
		<?php else:?>
			<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= $arResult["nStartPage"]?>"><?= $NavRecordGroupPrint?></a>
		<?php endif?>

		<?php $arResult["nStartPage"]--?>
	<?php endwhile?>

	|

	<?php if ($arResult["NavPageNomer"] > 1):?>
		<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]-1)?>"><?= GetMessage("nav_next")?></a>
		|
		<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=1"><?= GetMessage("nav_end")?></a>
	<?php else:?>
		<?= GetMessage("nav_next")?>&nbsp;|&nbsp;<?= GetMessage("nav_end")?>
	<?php endif?>

<?php else:?>

	<?= $arResult["NavFirstRecordShow"]?> <?= GetMessage("nav_to")?> <?= $arResult["NavLastRecordShow"]?> <?= GetMessage("nav_of")?> <?= $arResult["NavRecordCount"]?><br /></font>

	<font class="text">

	<?php if ($arResult["NavPageNomer"] > 1):?>

		<?php if($arResult["bSavePage"]):?>
			<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=1"><?= GetMessage("nav_begin")?></a>
			|
			<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]-1)?>"><?= GetMessage("nav_prev")?></a>
			|
		<?php else:?>
			<a href="<?= $arResult["sUrlPath"]?><?= $strNavQueryStringFull?>"><?= GetMessage("nav_begin")?></a>
			|
			<?php if ($arResult["NavPageNomer"] > 2):?>
				<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]-1)?>"><?= GetMessage("nav_prev")?></a>
			<?php else:?>
				<a href="<?= $arResult["sUrlPath"]?><?= $strNavQueryStringFull?>"><?= GetMessage("nav_prev")?></a>
			<?php endif?>
			|
		<?php endif?>

	<?php else:?>
		<?= GetMessage("nav_begin")?>&nbsp;|&nbsp;<?= GetMessage("nav_prev")?>&nbsp;|
	<?php endif?>

	<?php while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

		<?php if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<b><?= $arResult["nStartPage"]?></b>
		<?php elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
			<a href="<?= $arResult["sUrlPath"]?><?= $strNavQueryStringFull?>"><?= $arResult["nStartPage"]?></a>
		<?php else:?>
			<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= $arResult["nStartPage"]?>"><?= $arResult["nStartPage"]?></a>
		<?php endif?>
		<?php $arResult["nStartPage"]++?>
	<?php endwhile?>
	|

	<?php if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]+1)?>"><?= GetMessage("nav_next")?></a>&nbsp;|
		<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= $arResult["NavPageCount"]?>"><?= GetMessage("nav_end")?></a>
	<?php else:?>
		<?= GetMessage("nav_next")?>&nbsp;|&nbsp;<?= GetMessage("nav_end")?>
	<?php endif?>

<?php endif?>


<?php if ($arResult["bShowAll"]):?>
<noindex>
	<?php if ($arResult["NavShowAll"]):?>
		|&nbsp;<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>SHOWALL_<?= $arResult["NavNum"]?>=0" rel="nofollow"><?= GetMessage("nav_paged")?></a>
	<?php else:?>
		|&nbsp;<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>SHOWALL_<?= $arResult["NavNum"]?>=1" rel="nofollow"><?= GetMessage("nav_all")?></a>
	<?php endif?>
</noindex>
<?php endif?>

</font>