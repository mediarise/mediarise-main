<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php if (!empty($arResult)):?>

<?php foreach($arResult as $arItem):?>
	<?php if ($arItem["PERMISSION"] > "D"):?>
		<?php if ($arItem["SELECTED"]):?>
			<span><?= $arItem["TEXT"]?></span>&nbsp;&nbsp;
		<?php else:?>
			<a href="<?= $arItem["LINK"]?>"><?= $arItem["TEXT"]?></a>&nbsp;&nbsp;
		<?php endif?>
	<?php endif?>
<?php endforeach?>

<?php endif?>