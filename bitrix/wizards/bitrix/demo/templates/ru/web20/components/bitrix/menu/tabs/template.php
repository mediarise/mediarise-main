<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php if (!empty($arResult)):?>
<div class="image-load-left"></div>
<div class="image-load-right"></div>
<div class="image-load-bg"></div>

<div class="web-blue-tabs-menu" id="web-blue-tabs-menu">

	<ul>
<?php foreach($arResult as $arItem):?>

	<?php if ($arItem["PERMISSION"] > "D"):?>
		<li<?php if ($arItem["SELECTED"]):?> class="selected"<?php endif?>><a href="<?= $arItem["LINK"]?>"><nobr><?= $arItem["TEXT"]?></nobr></a></li>
	<?php endif?>

<?php endforeach?>

	</ul>
</div>
<div class="menu-clear-left"></div>
<?php endif?>