<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php if (!empty($arResult)):?>
<div class="left-menu">

<?php foreach($arResult as $arItem):?>
	<?php if($arItem["SELECTED"]):?>
		<div class="bl"><div class="br"><div class="tl"><div class="tr"><a href="<?= $arItem["LINK"]?>" class="selected"><?= $arItem["TEXT"]?></a></div></div></div></div>
	<?php else:?>
		<div class="bl"><div class="br"><div class="tl"><div class="tr"><a href="<?= $arItem["LINK"]?>"><?= $arItem["TEXT"]?></a></div></div></div></div>
	<?php endif?>
	
<?php endforeach?>

</div>
<?php endif?>