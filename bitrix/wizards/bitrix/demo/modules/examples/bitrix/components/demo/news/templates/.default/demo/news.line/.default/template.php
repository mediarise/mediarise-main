<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="news-line">
	<?php foreach($arResult["ITEMS"] as $arItem):?>
		<span class="news-date-time"><?php echo $arItem["ACTIVE_FROM"]?>&nbsp;&nbsp;</span><a href="<?php echo $arItem["DETAIL_PAGE_URL"]?>"><?php echo $arItem["NAME"]?></a><br />
	<?php endforeach;?>
</div>
