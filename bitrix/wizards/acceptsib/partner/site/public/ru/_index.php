<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Многоцелевой одностраничник - \"Партнер\"");
?>
<!-- Present display  -->
<?php $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"present", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "#PRESENT_IBLOCK_ID#",
		"NEWS_COUNT" => "5",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "TEXT_BUTTON_PRESENT",
			1 => "ICON_BUTTON_PRESENT",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

<!-- Navigation -->
<?php $APPLICATION->IncludeComponent("bitrix:menu", "menu_top", Array(
	"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => "",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
	),
	false
);?>

<!-- Advantages -->
<div id="advantages" class="section section_cols-promo">
	<div class="section__name">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/advantages.php",
				Array(),
				Array("MODE"=>"html")
			);?>				
			</h2>
		</div>
	</div>
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "advantages", Array(
		"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "#ADVANTAGES_IBLOCK_ID#",
			"NEWS_COUNT" => "6",	
			"SORT_BY1" => "SORT",	
			"SORT_ORDER1" => "DESC",	
			"SORT_BY2" => "ACTIVE_FROM",	
			"SORT_ORDER2" => "ASC",	
			"FILTER_NAME" => "",	
			"FIELD_CODE" => array(	
				0 => "NAME",
				1 => "PREVIEW_TEXT",
				2 => "",
			),
			"PROPERTY_CODE" => array(	
				0 => "ICON_ADVANTAGES",
				1 => "",
			),
			"CHECK_DATES" => "Y",	
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",	
			"AJAX_OPTION_STYLE" => "N",
			"AJAX_OPTION_HISTORY" => "N",	
			"CACHE_TYPE" => "A",	
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",	
			"CACHE_GROUPS" => "Y",	
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	
			"SET_TITLE" => "N",	
			"SET_BROWSER_TITLE" => "N",	
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",	
			"SET_STATUS_404" => "N",	
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
			"ADD_SECTIONS_CHAIN" => "N",	
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
			"PARENT_SECTION" => "",	
			"PARENT_SECTION_CODE" => "",	
			"INCLUDE_SUBSECTIONS" => "N",	
			"DISPLAY_DATE" => "N",	
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "N",	
			"DISPLAY_PREVIEW_TEXT" => "Y",	
			"PAGER_TEMPLATE" => ".default",	
			"DISPLAY_TOP_PAGER" => "N",	
			"DISPLAY_BOTTOM_PAGER" => "N",	
			"PAGER_TITLE" => "",	
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",	
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
			"PAGER_SHOW_ALL" => "N",	
			"AJAX_OPTION_ADDITIONAL" => "",	
		),
		false
	);?>	
</div>

<!-- Services -->
<div id="services" class="section section_inv">
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "services", Array(
		"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "#SERVICES_IBLOCK_ID#",	
			"NEWS_COUNT" => "9",	
			"SORT_BY1" => "SORT",	
			"SORT_ORDER1" => "DESC",	
			"SORT_BY2" => "ACTIVE_FROM",	
			"SORT_ORDER2" => "ASC",	
			"FILTER_NAME" => "",	
			"FIELD_CODE" => array(	
				0 => "NAME",
				1 => "PREVIEW_TEXT",
				2 => "",
			),
			"PROPERTY_CODE" => array(	
				0 => "ICON_SERVICES",
				1 => "",
			),
			"CHECK_DATES" => "Y",	
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",	
			"AJAX_OPTION_STYLE" => "N",
			"AJAX_OPTION_HISTORY" => "N",	
			"CACHE_TYPE" => "A",	
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",	
			"CACHE_GROUPS" => "Y",	
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	
			"SET_TITLE" => "N",	
			"SET_BROWSER_TITLE" => "N",	
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",	
			"SET_STATUS_404" => "N",	
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
			"ADD_SECTIONS_CHAIN" => "N",	
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
			"PARENT_SECTION" => "",	
			"PARENT_SECTION_CODE" => "",	
			"INCLUDE_SUBSECTIONS" => "N",	
			"DISPLAY_DATE" => "N",	
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "N",	
			"DISPLAY_PREVIEW_TEXT" => "Y",	
			"PAGER_TEMPLATE" => ".default",	
			"DISPLAY_TOP_PAGER" => "N",	
			"DISPLAY_BOTTOM_PAGER" => "N",	
			"PAGER_TITLE" => "",	
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",	
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
			"PAGER_SHOW_ALL" => "N",	
			"AJAX_OPTION_ADDITIONAL" => "",	
		),
		false
	);?>
</div>

<!-- Why we -->
<div id="whywe" class="section section_about">
	<div class="section__name">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/whywe.php",
				Array(),
				Array("MODE"=>"html")
			);?>			
			</h2>
		</div>
	</div>
	<div class="section__container">
		<div class="container">
			<div class="about">
				<div class="row">
					<div class="col_md_5">
						<!-- Slider -->
						<?php $APPLICATION->IncludeComponent("bitrix:news.list", "whywe_slider", Array(
							"IBLOCK_TYPE" => "content",
								"IBLOCK_ID" => "#SLIDER_IBLOCK_ID#",	
								"NEWS_COUNT" => "9",	
								"SORT_BY1" => "SORT",	
								"SORT_ORDER1" => "DESC",	
								"SORT_BY2" => "ACTIVE_FROM",	
								"SORT_ORDER2" => "ASC",	
								"FILTER_NAME" => "",	
								"FIELD_CODE" => array(	
									0 => "NAME",
									1 => "PREVIEW_PICTURE",
									2 => "",
								),
								"PROPERTY_CODE" => array(	
									0 => "",
									1 => "",
									2 => "",
								),
								"CHECK_DATES" => "Y",	
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",	
								"AJAX_OPTION_STYLE" => "N",
								"AJAX_OPTION_HISTORY" => "N",	
								"CACHE_TYPE" => "A",	
								"CACHE_TIME" => "36000000",
								"CACHE_FILTER" => "N",	
								"CACHE_GROUPS" => "Y",	
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",	
								"SET_TITLE" => "N",	
								"SET_BROWSER_TITLE" => "N",	
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",	
								"SET_STATUS_404" => "N",	
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
								"ADD_SECTIONS_CHAIN" => "N",	
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
								"PARENT_SECTION" => "",	
								"PARENT_SECTION_CODE" => "",	
								"INCLUDE_SUBSECTIONS" => "N",	
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "Y",
								"DISPLAY_PICTURE" => "Y",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",	
								"DISPLAY_TOP_PAGER" => "N",	
								"DISPLAY_BOTTOM_PAGER" => "N",	
								"PAGER_TITLE" => "",	
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",	
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
								"PAGER_SHOW_ALL" => "N",	
								"AJAX_OPTION_ADDITIONAL" => "",	
							),
							false
						);?>
					</div>
					<div class="col_md_7">
						<!-- List -->
						<?php $APPLICATION->IncludeComponent("bitrix:news.list", "whywe", Array(
							"IBLOCK_TYPE" => "content",
								"IBLOCK_ID" => "#WHYWE_IBLOCK_ID#",	
								"NEWS_COUNT" => "10",	
								"SORT_BY1" => "SORT",	
								"SORT_ORDER1" => "DESC",	
								"SORT_BY2" => "ACTIVE_FROM",	
								"SORT_ORDER2" => "ASC",	
								"FILTER_NAME" => "",	
								"FIELD_CODE" => array(	
									0 => "NAME",
									2 => "",
								),
								"PROPERTY_CODE" => array(	
									0 => "ICON_WHYWE",
									1 => "",
								),
								"CHECK_DATES" => "Y",	
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",	
								"AJAX_OPTION_STYLE" => "N",
								"AJAX_OPTION_HISTORY" => "N",	
								"CACHE_TYPE" => "A",	
								"CACHE_TIME" => "36000000",
								"CACHE_FILTER" => "N",	
								"CACHE_GROUPS" => "Y",	
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",	
								"SET_TITLE" => "N",	
								"SET_BROWSER_TITLE" => "N",	
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",	
								"SET_STATUS_404" => "N",	
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
								"ADD_SECTIONS_CHAIN" => "N",	
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
								"PARENT_SECTION" => "",	
								"PARENT_SECTION_CODE" => "",	
								"INCLUDE_SUBSECTIONS" => "N",	
								"DISPLAY_DATE" => "N",	
								"DISPLAY_NAME" => "Y",
								"DISPLAY_PICTURE" => "N",	
								"DISPLAY_PREVIEW_TEXT" => "N",	
								"PAGER_TEMPLATE" => ".default",	
								"DISPLAY_TOP_PAGER" => "N",	
								"DISPLAY_BOTTOM_PAGER" => "N",	
								"PAGER_TITLE" => "",	
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",	
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
								"PAGER_SHOW_ALL" => "N",	
								"AJAX_OPTION_ADDITIONAL" => "",	
							),
							false
						);?>					
					</div>						
				</div>	
			</div>		
		</div>
	</div>
</div>

<!-- Our works -->
<div id="portfolio" class="section section_inv">
	<div class="section__name section__name_inv">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/portfolio.php",
				Array(),
				Array("MODE"=>"html")
			);?>				
			</h2>
		</div>
	</div>
	<div class="section__container">
		<div class="container">
			<div class="gallery-isotope">
				<?php $APPLICATION->IncludeComponent(
					"bitrix:catalog.section.list", 
					"portfolio_filter", 
					array(
						"IBLOCK_TYPE" => "content",
						"IBLOCK_ID" => "#PORTFOLIO_IBLOCK_ID#",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"COUNT_ELEMENTS" => "N",				
						"TOP_DEPTH" => "1",
						"SECTION_FIELDS" => array(
							0 => "CODE",
							1 => "NAME",
							2 => "",
						),
						"SECTION_USER_FIELDS" => array(
							0 => "",
							1 => "",
						),
						"SECTION_URL" => "",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "Y",
						"ADD_SECTIONS_CHAIN" => "N",
						"VIEW_MODE" => "LINE",
						"SHOW_PARENT_NAME" => "Y"
					),
					false
				);?>			
				<?php $APPLICATION->IncludeComponent("bitrix:news.list", "portfolio", Array(
					"IBLOCK_TYPE" => "content",
						"IBLOCK_ID" => "#PORTFOLIO_IBLOCK_ID#",	
						"NEWS_COUNT" => "18",	
						"SORT_BY1" => "SORT",	
						"SORT_ORDER1" => "DESC",	
						"SORT_BY2" => "ACTIVE_FROM",	
						"SORT_ORDER2" => "ASC",	
						"FILTER_NAME" => "",	
						"FIELD_CODE" => array(	
							0 => "NAME",
							1 => "PREVIEW_PICTURE",
							2 => "",
						),
						"PROPERTY_CODE" => array(	
							0 => "",
							1 => "",
						),
						"CHECK_DATES" => "Y",	
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",	
						"AJAX_OPTION_STYLE" => "N",
						"AJAX_OPTION_HISTORY" => "N",	
						"CACHE_TYPE" => "A",	
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",	
						"CACHE_GROUPS" => "Y",	
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "d.m.Y",	
						"SET_TITLE" => "N",	
						"SET_BROWSER_TITLE" => "N",	
						"SET_META_KEYWORDS" => "N",
						"SET_META_DESCRIPTION" => "N",	
						"SET_STATUS_404" => "N",	
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
						"ADD_SECTIONS_CHAIN" => "N",	
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
						"PARENT_SECTION" => "",	
						"PARENT_SECTION_CODE" => "",	
						"INCLUDE_SUBSECTIONS" => "N",	
						"DISPLAY_DATE" => "N",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "N",
						"PAGER_TEMPLATE" => ".default",	
						"DISPLAY_TOP_PAGER" => "N",	
						"DISPLAY_BOTTOM_PAGER" => "N",	
						"PAGER_TITLE" => "",	
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",	
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
						"PAGER_SHOW_ALL" => "N",	
						"AJAX_OPTION_ADDITIONAL" => "",	
					),
					false
				);?>			  	
			</div>					
		</div>
	</div>
</div>

<!-- How we work -->
<div id="howwork" class="section">
	<div class="section__name">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/howwork.php",
				Array(),
				Array("MODE"=>"html")
			);?>				
			</h2>
		</div>
	</div>
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "howwork", Array(
	"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "#NOWWORK_IBLOCK_ID#",	
		"NEWS_COUNT" => "8",	
		"SORT_BY1" => "SORT",	
		"SORT_ORDER1" => "DESC",	
		"SORT_BY2" => "ACTIVE_FROM",	
		"SORT_ORDER2" => "ASC",	
		"FILTER_NAME" => "",	
		"FIELD_CODE" => array(	
			0 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "",
		),
		"PROPERTY_CODE" => array(	
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",	
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",	
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",	
		"CACHE_TYPE" => "A",	
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",	
		"CACHE_GROUPS" => "Y",	
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	
		"SET_TITLE" => "N",	
		"SET_BROWSER_TITLE" => "N",	
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",	
		"SET_STATUS_404" => "N",	
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
		"ADD_SECTIONS_CHAIN" => "N",	
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
		"PARENT_SECTION" => "",	
		"PARENT_SECTION_CODE" => "",	
		"INCLUDE_SUBSECTIONS" => "N",	
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",	
		"DISPLAY_TOP_PAGER" => "N",	
		"DISPLAY_BOTTOM_PAGER" => "N",	
		"PAGER_TITLE" => "",	
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",	
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
		"PAGER_SHOW_ALL" => "N",	
		"AJAX_OPTION_ADDITIONAL" => "",	
	),
	false
);?>
</div>

<!-- Benefits -->
<div id="benefits" class="section section_bg section_benefits">
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "benefits", Array(
		"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "#BENEFITS_IBLOCK_ID#",	
			"NEWS_COUNT" => "9",	
			"SORT_BY1" => "SORT",	
			"SORT_ORDER1" => "DESC",	
			"SORT_BY2" => "ACTIVE_FROM",	
			"SORT_ORDER2" => "ASC",	
			"FILTER_NAME" => "",	
			"FIELD_CODE" => array(	
				0 => "NAME",
			),
			"PROPERTY_CODE" => array(	
				0 => "AMOUNT_BENEFITS",
				1 => "",
			),
			"CHECK_DATES" => "Y",	
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",	
			"AJAX_OPTION_STYLE" => "N",
			"AJAX_OPTION_HISTORY" => "N",	
			"CACHE_TYPE" => "A",	
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",	
			"CACHE_GROUPS" => "Y",	
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	
			"SET_TITLE" => "N",	
			"SET_BROWSER_TITLE" => "N",	
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",	
			"SET_STATUS_404" => "N",	
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
			"ADD_SECTIONS_CHAIN" => "N",	
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
			"PARENT_SECTION" => "",	
			"PARENT_SECTION_CODE" => "",	
			"INCLUDE_SUBSECTIONS" => "N",	
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "N",
			"DISPLAY_PREVIEW_TEXT" => "N",
			"PAGER_TEMPLATE" => ".default",	
			"DISPLAY_TOP_PAGER" => "N",	
			"DISPLAY_BOTTOM_PAGER" => "N",	
			"PAGER_TITLE" => "",	
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",	
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
			"PAGER_SHOW_ALL" => "N",	
			"AJAX_OPTION_ADDITIONAL" => "",	
		),
		false
	);?>
</div>

<!-- FAQ -->
<div id="faq" class="section">
	<div class="section__name">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/faq.php",
				Array(),
				Array("MODE"=>"html")
			);?>				
			</h2>
		</div>
	</div>
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "faq", Array(
		"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "#FAQ_IBLOCK_ID#",	
			"NEWS_COUNT" => "9",	
			"SORT_BY1" => "SORT",	
			"SORT_ORDER1" => "DESC",	
			"SORT_BY2" => "ACTIVE_FROM",	
			"SORT_ORDER2" => "ASC",	
			"FILTER_NAME" => "",	
			"FIELD_CODE" => array(	
				0 => "NAME",
				1 => "DETAIL_TEXT",
			),
			"PROPERTY_CODE" => array(	
				0 => "",
				1 => "",
			),
			"CHECK_DATES" => "Y",	
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",	
			"AJAX_OPTION_STYLE" => "N",
			"AJAX_OPTION_HISTORY" => "N",	
			"CACHE_TYPE" => "A",	
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",	
			"CACHE_GROUPS" => "Y",	
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	
			"SET_TITLE" => "N",	
			"SET_BROWSER_TITLE" => "N",	
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",	
			"SET_STATUS_404" => "N",	
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
			"ADD_SECTIONS_CHAIN" => "N",	
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
			"PARENT_SECTION" => "",	
			"PARENT_SECTION_CODE" => "",	
			"INCLUDE_SUBSECTIONS" => "N",	
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "N",
			"DISPLAY_PREVIEW_TEXT" => "N",
			"PAGER_TEMPLATE" => ".default",	
			"DISPLAY_TOP_PAGER" => "N",	
			"DISPLAY_BOTTOM_PAGER" => "N",	
			"PAGER_TITLE" => "",	
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",	
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
			"PAGER_SHOW_ALL" => "N",	
			"AJAX_OPTION_ADDITIONAL" => "",	
		),
		false
	);?>
</div>

<!-- Reviews -->
<div id="reviews" class="section section_bg section_reviews-carousel">
	<div class="section__name section__name_bg">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/reviews.php",
				Array(),
				Array("MODE"=>"html")
			);?>				
			</h2>
		</div>
	</div>
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "reviews", Array(
		"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "#REVIEWS_IBLOCK_ID#",	
			"NEWS_COUNT" => "6",	
			"SORT_BY1" => "SORT",	
			"SORT_ORDER1" => "DESC",	
			"SORT_BY2" => "ACTIVE_FROM",	
			"SORT_ORDER2" => "ASC",	
			"FILTER_NAME" => "",	
			"FIELD_CODE" => array(	
				0 => "NAME",
				1 => "PREVIEW_TEXT",
				2 => "PREVIEW_PICTURE",
			),
			"PROPERTY_CODE" => array(	
				0 => "",
				1 => "",
			),
			"CHECK_DATES" => "Y",	
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",	
			"AJAX_OPTION_STYLE" => "N",
			"AJAX_OPTION_HISTORY" => "N",	
			"CACHE_TYPE" => "A",	
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",	
			"CACHE_GROUPS" => "Y",	
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	
			"SET_TITLE" => "N",	
			"SET_BROWSER_TITLE" => "N",	
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",	
			"SET_STATUS_404" => "N",	
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
			"ADD_SECTIONS_CHAIN" => "N",	
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
			"PARENT_SECTION" => "",	
			"PARENT_SECTION_CODE" => "",	
			"INCLUDE_SUBSECTIONS" => "N",	
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"PAGER_TEMPLATE" => ".default",	
			"DISPLAY_TOP_PAGER" => "N",	
			"DISPLAY_BOTTOM_PAGER" => "N",	
			"PAGER_TITLE" => "",	
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",	
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
			"PAGER_SHOW_ALL" => "N",	
			"AJAX_OPTION_ADDITIONAL" => "",	
		),
		false
	);?>
</div>

<!-- How much -->
<div id="prices" class="section section_prices">
	<div class="section__name">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/prices.php",
				Array(),
				Array("MODE"=>"html")
			);?>				
			</h2>
		</div>
	</div>
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "prices", Array(
		"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "#PRICES_IBLOCK_ID#",	
			"NEWS_COUNT" => "3",	
			"SORT_BY1" => "SORT",	
			"SORT_ORDER1" => "DESC",	
			"SORT_BY2" => "ACTIVE_FROM",	
			"SORT_ORDER2" => "ASC",	
			"FILTER_NAME" => "",	
			"FIELD_CODE" => array(	
				0 => "NAME",
			),
			"PROPERTY_CODE" => array(	
				0 => "PRICE_HOWMUCH",
				1 => "TEXT_BUTTON_HOWMUCH",
				2 => "HOWMUCH_PLUS",
				3 => "HOWMUCH_MINUS",
			),
			"CHECK_DATES" => "Y",	
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",	
			"AJAX_OPTION_STYLE" => "N",
			"AJAX_OPTION_HISTORY" => "N",	
			"CACHE_TYPE" => "A",	
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",	
			"CACHE_GROUPS" => "Y",	
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	
			"SET_TITLE" => "N",	
			"SET_BROWSER_TITLE" => "N",	
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",	
			"SET_STATUS_404" => "N",	
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
			"ADD_SECTIONS_CHAIN" => "N",	
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
			"PARENT_SECTION" => "",	
			"PARENT_SECTION_CODE" => "",	
			"INCLUDE_SUBSECTIONS" => "N",	
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "N",
			"DISPLAY_PREVIEW_TEXT" => "N",
			"PAGER_TEMPLATE" => ".default",	
			"DISPLAY_TOP_PAGER" => "N",	
			"DISPLAY_BOTTOM_PAGER" => "N",	
			"PAGER_TITLE" => "",	
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",	
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
			"PAGER_SHOW_ALL" => "N",	
			"AJAX_OPTION_ADDITIONAL" => "",	
		),
		false
	);?>	
</div>

<!-- Call to action -->
<div id="calltoaction" class="section section_bg section_call-to-act">
	<div class="section__container section__container_bg">
		<div class="container">
			<div class="call-to-act">
				<div class="row">
					<div class="col_xs_12">
						<div class="call-to-act__descr">
							<?php $APPLICATION->IncludeFile(
								SITE_DIR."include/calltoaction_descr.php",
								Array(),
								Array("MODE"=>"html")
							);?>
						</div>
					</div>
					<div class="col_xs_12">
						<div class="call-to-act__button">
							<span data-dialog="callback" class="button button_large button_call-to-act">
								<?php $APPLICATION->IncludeFile(
									SITE_DIR."include/calltoaction_button.php",
									Array(),
									Array("MODE"=>"html")
								);?>							
							</span>
						</div>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>

<!-- Our Partners -->
<div id="partners" class="section">
	<div class="section__name">
		<div class="container">
			<h2>
			<?php $APPLICATION->IncludeFile(
				SITE_DIR."include/title_sections/partners.php",
				Array(),
				Array("MODE"=>"html")
			);?>				
			</h2>
		</div>
	</div>
	<?php $APPLICATION->IncludeComponent("bitrix:news.list", "partners", Array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "#PARTNERS_IBLOCK_ID#",	
		"NEWS_COUNT" => "15",	
		"SORT_BY1" => "SORT",	
		"SORT_ORDER1" => "DESC",	
		"SORT_BY2" => "ACTIVE_FROM",	
		"SORT_ORDER2" => "ASC",	
		"FILTER_NAME" => "",	
		"FIELD_CODE" => array(	
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(	
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",	
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",	
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",	
		"CACHE_TYPE" => "A",	
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",	
		"CACHE_GROUPS" => "Y",	
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	
		"SET_TITLE" => "N",	
		"SET_BROWSER_TITLE" => "N",	
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",	
		"SET_STATUS_404" => "N",	
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
		"ADD_SECTIONS_CHAIN" => "N",	
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
		"PARENT_SECTION" => "",	
		"PARENT_SECTION_CODE" => "",	
		"INCLUDE_SUBSECTIONS" => "N",	
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",	
		"DISPLAY_TOP_PAGER" => "N",	
		"DISPLAY_BOTTOM_PAGER" => "N",	
		"PAGER_TITLE" => "",	
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",	
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
		"PAGER_SHOW_ALL" => "N",	
		"AJAX_OPTION_ADDITIONAL" => "",	
	),
	false
);?>
</div>

<!-- Contact -->
<div id="contact" class="section section_map">	
	<div class="section__container section__container_map">
		<div class="contact">
			<div class="container container_contact">
				<div class="contact-card">
					<div class="contact-card__name">
						<h2>
							<?php $APPLICATION->IncludeFile(
								SITE_DIR."include/title_sections/contact.php",
								Array(),
								Array("MODE"=>"html")
							);?>							
						</h2>
					</div>
					<div class="contact-card__container">
						<div class="contact-card__row contact-card__row_address">
							<p>
								<?php $APPLICATION->IncludeFile(
									SITE_DIR."include/address.php",
									Array(),
									Array("MODE"=>"html")
								);?>								
							</p>
						</div>	
						<div class="contact-card__row contact-card__row_phones modificated-to-mobile">
								<span>
									<?php $APPLICATION->IncludeFile(
										SITE_DIR."include/phone_01.php",
										Array(),
										Array("MODE"=>"html")
									);?>								
								</span>	
								<span>
									<?php $APPLICATION->IncludeFile(
										SITE_DIR."include/phone_02.php",
										Array(),
										Array("MODE"=>"html")
									);?>								
								</span>
						</div>
						<div class="contact-card__row contact-card__row_email">
							<?php $APPLICATION->IncludeFile(
								SITE_DIR."include/email.php",
								Array(),
								Array("MODE"=>"html")
							);?>							
						</div>							
					</div>
					<?php $APPLICATION->IncludeComponent("bitrix:news.list", "social", Array(
						"IBLOCK_TYPE" => "settings",
							"IBLOCK_ID" => "#SOCIAL_IBLOCK_ID#",	
							"NEWS_COUNT" => "4",	
							"SORT_BY1" => "SORT",	
							"SORT_ORDER1" => "DESC",	
							"SORT_BY2" => "ACTIVE_FROM",	
							"SORT_ORDER2" => "ASC",	
							"FILTER_NAME" => "",	
							"FIELD_CODE" => array(	
								0 => "NAME",
								1 => "",
							),
							"PROPERTY_CODE" => array(	
								0 => "LINK_SOCIAL",
								1 => "ICON_SOCIAL",
							),
							"CHECK_DATES" => "Y",	
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",	
							"AJAX_OPTION_STYLE" => "N",
							"AJAX_OPTION_HISTORY" => "N",	
							"CACHE_TYPE" => "A",	
							"CACHE_TIME" => "36000000",
							"CACHE_FILTER" => "N",	
							"CACHE_GROUPS" => "Y",	
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",	
							"SET_TITLE" => "N",	
							"SET_BROWSER_TITLE" => "N",	
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",	
							"SET_STATUS_404" => "N",	
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	
							"ADD_SECTIONS_CHAIN" => "N",	
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",	
							"PARENT_SECTION" => "",	
							"PARENT_SECTION_CODE" => "",	
							"INCLUDE_SUBSECTIONS" => "N",	
							"DISPLAY_DATE" => "N",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "N",
							"DISPLAY_PREVIEW_TEXT" => "N",
							"PAGER_TEMPLATE" => ".default",	
							"DISPLAY_TOP_PAGER" => "N",	
							"DISPLAY_BOTTOM_PAGER" => "N",	
							"PAGER_TITLE" => "",	
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",	
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	
							"PAGER_SHOW_ALL" => "N",	
							"AJAX_OPTION_ADDITIONAL" => "",	
						),
						false
					);?>					
									</div>
			</div>			
		</div>
		<?php $APPLICATION->IncludeComponent(
			"bitrix:map.yandex.view", 
			"map", 
			array(
				"INIT_MAP_TYPE" => "MAP",
				"MAP_DATA" => "a:3:{s:10:\"yandex_lat\";d:55.75999999999371;s:10:\"yandex_lon\";d:37.63999999999997;s:12:\"yandex_scale\";i:10;}",
				"MAP_WIDTH" => "100%",
				"MAP_HEIGHT" => "100%",
				"CONTROLS" => array(
				),
				"OPTIONS" => array(
					0 => "ENABLE_DBLCLICK_ZOOM",
					1 => "ENABLE_DRAGGING",
				),
				"MAP_ID" => "map"
			),
			false
		);?>
				</div>		
	</div>
</div>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>