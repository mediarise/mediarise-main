<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");?>
<div class="dialog__header">
	<span>Заполните форму</span>
	<button class="icon-font-cross" data-dialog-close></button>
</div>
<?php $APPLICATION->IncludeComponent(
	"asib:main.callback", 
	"callback", 
	array(
		"USE_CAPTCHA" => "Y",
		"OK_TEXT" => "Спасибо, ваша заявка принята:)",
		"EMAIL_TO" => "",
		"EVENT_MESSAGE_ID" => array(
		),
		"IBLOCK_ID_ADD" => "#CALLBACK_IBLOCK_ID#"
	),
	false
);?>