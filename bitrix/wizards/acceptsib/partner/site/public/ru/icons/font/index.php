<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Набор шрифтовых иконок");
?>

<!-- Section -->
<div class="section section_bg section_page-info">
	<div class="section__container section__container_bg">
		<div class="container">
			<div class="page-info">
				<div class="row">
					<div class="col_sm_7">
							<div class="page-info__title">
								<h1><?php $APPLICATION->ShowTitle();?></h1>
							</div>											
					</div>
					<div class="col_sm_5">
						<div class="page-info__back">
							<a href="<?= SITE_DIR?>">Вернуться на главную</a>
						</div>						
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Icon set -->
<div class="icon-set">
	<div class="container">
		<div class="row">				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-home">

						</div>
						<div class="icon-set__id">
							<span>home</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-home2">

						</div>
						<div class="icon-set__id">
							<span>home2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-home3">

						</div>
						<div class="icon-set__id">
							<span>home3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-newspaper">

						</div>
						<div class="icon-set__id">
							<span>newspaper</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-office">

						</div>
						<div class="icon-set__id">
							<span>office</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pencil">

						</div>
						<div class="icon-set__id">
							<span>pencil</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pencil2">

						</div>
						<div class="icon-set__id">
							<span>pencil2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-quill">

						</div>
						<div class="icon-set__id">
							<span>quill</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pen">

						</div>
						<div class="icon-set__id">
							<span>pen</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-blog">

						</div>
						<div class="icon-set__id">
							<span>blog</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-eyedropper">

						</div>
						<div class="icon-set__id">
							<span>eyedropper</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-droplet">

						</div>
						<div class="icon-set__id">
							<span>droplet</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paint-format">

						</div>
						<div class="icon-set__id">
							<span>paint-format</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-image">

						</div>
						<div class="icon-set__id">
							<span>image</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-images">

						</div>
						<div class="icon-set__id">
							<span>images</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-camera">

						</div>
						<div class="icon-set__id">
							<span>camera</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-headphones">

						</div>
						<div class="icon-set__id">
							<span>headphones</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-music">

						</div>
						<div class="icon-set__id">
							<span>music</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-play">

						</div>
						<div class="icon-set__id">
							<span>play</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-film">

						</div>
						<div class="icon-set__id">
							<span>film</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-video-camera">

						</div>
						<div class="icon-set__id">
							<span>video-camera</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-dice">

						</div>
						<div class="icon-set__id">
							<span>dice</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pacman">

						</div>
						<div class="icon-set__id">
							<span>pacman</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spades">

						</div>
						<div class="icon-set__id">
							<span>spades</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-clubs">

						</div>
						<div class="icon-set__id">
							<span>clubs</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-diamonds">

						</div>
						<div class="icon-set__id">
							<span>diamonds</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bullhorn">

						</div>
						<div class="icon-set__id">
							<span>bullhorn</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-connection">

						</div>
						<div class="icon-set__id">
							<span>connection</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-podcast">

						</div>
						<div class="icon-set__id">
							<span>podcast</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-feed">

						</div>
						<div class="icon-set__id">
							<span>feed</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mic">

						</div>
						<div class="icon-set__id">
							<span>mic</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-book">

						</div>
						<div class="icon-set__id">
							<span>book</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-books">

						</div>
						<div class="icon-set__id">
							<span>books</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-library">

						</div>
						<div class="icon-set__id">
							<span>library</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-text">

						</div>
						<div class="icon-set__id">
							<span>file-text</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-profile">

						</div>
						<div class="icon-set__id">
							<span>profile</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-empty">

						</div>
						<div class="icon-set__id">
							<span>file-empty</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-files-empty">

						</div>
						<div class="icon-set__id">
							<span>files-empty</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-text2">

						</div>
						<div class="icon-set__id">
							<span>file-text2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-picture">

						</div>
						<div class="icon-set__id">
							<span>file-picture</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-music">

						</div>
						<div class="icon-set__id">
							<span>file-music</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-play">

						</div>
						<div class="icon-set__id">
							<span>file-play</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-video">

						</div>
						<div class="icon-set__id">
							<span>file-video</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-zip">

						</div>
						<div class="icon-set__id">
							<span>file-zip</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-copy">

						</div>
						<div class="icon-set__id">
							<span>copy</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paste">

						</div>
						<div class="icon-set__id">
							<span>paste</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stack">

						</div>
						<div class="icon-set__id">
							<span>stack</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-folder">

						</div>
						<div class="icon-set__id">
							<span>folder</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-folder-open">

						</div>
						<div class="icon-set__id">
							<span>folder-open</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-folder-plus">

						</div>
						<div class="icon-set__id">
							<span>folder-plus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-folder-minus">

						</div>
						<div class="icon-set__id">
							<span>folder-minus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-folder-download">

						</div>
						<div class="icon-set__id">
							<span>folder-download</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-folder-upload">

						</div>
						<div class="icon-set__id">
							<span>folder-upload</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-price-tag">

						</div>
						<div class="icon-set__id">
							<span>price-tag</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-price-tags">

						</div>
						<div class="icon-set__id">
							<span>price-tags</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-barcode">

						</div>
						<div class="icon-set__id">
							<span>barcode</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-qrcode">

						</div>
						<div class="icon-set__id">
							<span>qrcode</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-ticket">

						</div>
						<div class="icon-set__id">
							<span>ticket</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cart">

						</div>
						<div class="icon-set__id">
							<span>cart</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-coin-dollar">

						</div>
						<div class="icon-set__id">
							<span>coin-dollar</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-coin-euro">

						</div>
						<div class="icon-set__id">
							<span>coin-euro</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-coin-pound">

						</div>
						<div class="icon-set__id">
							<span>coin-pound</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-coin-yen">

						</div>
						<div class="icon-set__id">
							<span>coin-yen</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-credit-card">

						</div>
						<div class="icon-set__id">
							<span>credit-card</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-calculator">

						</div>
						<div class="icon-set__id">
							<span>calculator</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-lifebuoy">

						</div>
						<div class="icon-set__id">
							<span>lifebuoy</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-phone">

						</div>
						<div class="icon-set__id">
							<span>phone</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-phone-hang-up">

						</div>
						<div class="icon-set__id">
							<span>phone-hang-up</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-address-book">

						</div>
						<div class="icon-set__id">
							<span>address-book</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-envelop">

						</div>
						<div class="icon-set__id">
							<span>envelop</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pushpin">

						</div>
						<div class="icon-set__id">
							<span>pushpin</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-location">

						</div>
						<div class="icon-set__id">
							<span>location</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-location2">

						</div>
						<div class="icon-set__id">
							<span>location2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-compass">

						</div>
						<div class="icon-set__id">
							<span>compass</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-compass2">

						</div>
						<div class="icon-set__id">
							<span>compass2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-map">

						</div>
						<div class="icon-set__id">
							<span>map</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-map2">

						</div>
						<div class="icon-set__id">
							<span>map2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-history">

						</div>
						<div class="icon-set__id">
							<span>history</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-clock">

						</div>
						<div class="icon-set__id">
							<span>clock</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-clock2">

						</div>
						<div class="icon-set__id">
							<span>clock2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-alarm">

						</div>
						<div class="icon-set__id">
							<span>alarm</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bell">

						</div>
						<div class="icon-set__id">
							<span>bell</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stopwatch">

						</div>
						<div class="icon-set__id">
							<span>stopwatch</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-calendar">

						</div>
						<div class="icon-set__id">
							<span>calendar</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-printer">

						</div>
						<div class="icon-set__id">
							<span>printer</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-keyboard">

						</div>
						<div class="icon-set__id">
							<span>keyboard</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-display">

						</div>
						<div class="icon-set__id">
							<span>display</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-laptop">

						</div>
						<div class="icon-set__id">
							<span>laptop</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mobile">

						</div>
						<div class="icon-set__id">
							<span>mobile</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mobile2">

						</div>
						<div class="icon-set__id">
							<span>mobile2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tablet">

						</div>
						<div class="icon-set__id">
							<span>tablet</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tv">

						</div>
						<div class="icon-set__id">
							<span>tv</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-drawer">

						</div>
						<div class="icon-set__id">
							<span>drawer</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-drawer2">

						</div>
						<div class="icon-set__id">
							<span>drawer2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-box-add">

						</div>
						<div class="icon-set__id">
							<span>box-add</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-box-remove">

						</div>
						<div class="icon-set__id">
							<span>box-remove</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-download">

						</div>
						<div class="icon-set__id">
							<span>download</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-upload">

						</div>
						<div class="icon-set__id">
							<span>upload</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-floppy-disk">

						</div>
						<div class="icon-set__id">
							<span>floppy-disk</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-drive">

						</div>
						<div class="icon-set__id">
							<span>drive</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-database">

						</div>
						<div class="icon-set__id">
							<span>database</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-undo">

						</div>
						<div class="icon-set__id">
							<span>undo</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-redo">

						</div>
						<div class="icon-set__id">
							<span>redo</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-undo2">

						</div>
						<div class="icon-set__id">
							<span>undo2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-redo2">

						</div>
						<div class="icon-set__id">
							<span>redo2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-forward">

						</div>
						<div class="icon-set__id">
							<span>forward</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-reply">

						</div>
						<div class="icon-set__id">
							<span>reply</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bubble">

						</div>
						<div class="icon-set__id">
							<span>bubble</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bubbles">

						</div>
						<div class="icon-set__id">
							<span>bubbles</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bubbles2">

						</div>
						<div class="icon-set__id">
							<span>bubbles2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bubble2">

						</div>
						<div class="icon-set__id">
							<span>bubble2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bubbles3">

						</div>
						<div class="icon-set__id">
							<span>bubbles3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bubbles4">

						</div>
						<div class="icon-set__id">
							<span>bubbles4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-user">

						</div>
						<div class="icon-set__id">
							<span>user</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-users">

						</div>
						<div class="icon-set__id">
							<span>users</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-user-plus">

						</div>
						<div class="icon-set__id">
							<span>user-plus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-user-minus">

						</div>
						<div class="icon-set__id">
							<span>user-minus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-user-check">

						</div>
						<div class="icon-set__id">
							<span>user-check</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-user-tie">

						</div>
						<div class="icon-set__id">
							<span>user-tie</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-quotes-left">

						</div>
						<div class="icon-set__id">
							<span>quotes-left</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-quotes-right">

						</div>
						<div class="icon-set__id">
							<span>quotes-right</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-hour-glass">

						</div>
						<div class="icon-set__id">
							<span>hour-glass</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner">

						</div>
						<div class="icon-set__id">
							<span>spinner</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner2">

						</div>
						<div class="icon-set__id">
							<span>spinner2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner3">

						</div>
						<div class="icon-set__id">
							<span>spinner3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner4">

						</div>
						<div class="icon-set__id">
							<span>spinner4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner5">

						</div>
						<div class="icon-set__id">
							<span>spinner5</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner6">

						</div>
						<div class="icon-set__id">
							<span>spinner6</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner7">

						</div>
						<div class="icon-set__id">
							<span>spinner7</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner8">

						</div>
						<div class="icon-set__id">
							<span>spinner8</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner9">

						</div>
						<div class="icon-set__id">
							<span>spinner9</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner10">

						</div>
						<div class="icon-set__id">
							<span>spinner10</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spinner11">

						</div>
						<div class="icon-set__id">
							<span>spinner11</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-binoculars">

						</div>
						<div class="icon-set__id">
							<span>binoculars</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-search">

						</div>
						<div class="icon-set__id">
							<span>search</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-zoom-in">

						</div>
						<div class="icon-set__id">
							<span>zoom-in</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-zoom-out">

						</div>
						<div class="icon-set__id">
							<span>zoom-out</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-enlarge">

						</div>
						<div class="icon-set__id">
							<span>enlarge</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-shrink">

						</div>
						<div class="icon-set__id">
							<span>shrink</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-enlarge2">

						</div>
						<div class="icon-set__id">
							<span>enlarge2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-shrink2">

						</div>
						<div class="icon-set__id">
							<span>shrink2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-key">

						</div>
						<div class="icon-set__id">
							<span>key</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-key2">

						</div>
						<div class="icon-set__id">
							<span>key2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-lock">

						</div>
						<div class="icon-set__id">
							<span>lock</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-unlocked">

						</div>
						<div class="icon-set__id">
							<span>unlocked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-wrench">

						</div>
						<div class="icon-set__id">
							<span>wrench</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-equalizer">

						</div>
						<div class="icon-set__id">
							<span>equalizer</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-equalizer2">

						</div>
						<div class="icon-set__id">
							<span>equalizer2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cog">

						</div>
						<div class="icon-set__id">
							<span>cog</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cogs">

						</div>
						<div class="icon-set__id">
							<span>cogs</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-hammer">

						</div>
						<div class="icon-set__id">
							<span>hammer</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-magic-wand">

						</div>
						<div class="icon-set__id">
							<span>magic-wand</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-aid-kit">

						</div>
						<div class="icon-set__id">
							<span>aid-kit</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bug">

						</div>
						<div class="icon-set__id">
							<span>bug</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pie-chart">

						</div>
						<div class="icon-set__id">
							<span>pie-chart</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stats-dots">

						</div>
						<div class="icon-set__id">
							<span>stats-dots</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stats-bars">

						</div>
						<div class="icon-set__id">
							<span>stats-bars</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stats-bars2">

						</div>
						<div class="icon-set__id">
							<span>stats-bars2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-trophy">

						</div>
						<div class="icon-set__id">
							<span>trophy</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-gift">

						</div>
						<div class="icon-set__id">
							<span>gift</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-glass">

						</div>
						<div class="icon-set__id">
							<span>glass</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-glass2">

						</div>
						<div class="icon-set__id">
							<span>glass2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mug">

						</div>
						<div class="icon-set__id">
							<span>mug</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spoon-knife">

						</div>
						<div class="icon-set__id">
							<span>spoon-knife</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-leaf">

						</div>
						<div class="icon-set__id">
							<span>leaf</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-rocket">

						</div>
						<div class="icon-set__id">
							<span>rocket</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-meter">

						</div>
						<div class="icon-set__id">
							<span>meter</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-meter2">

						</div>
						<div class="icon-set__id">
							<span>meter2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-hammer2">

						</div>
						<div class="icon-set__id">
							<span>hammer2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-fire">

						</div>
						<div class="icon-set__id">
							<span>fire</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-lab">

						</div>
						<div class="icon-set__id">
							<span>lab</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-magnet">

						</div>
						<div class="icon-set__id">
							<span>magnet</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bin">

						</div>
						<div class="icon-set__id">
							<span>bin</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bin2">

						</div>
						<div class="icon-set__id">
							<span>bin2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-briefcase">

						</div>
						<div class="icon-set__id">
							<span>briefcase</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-airplane">

						</div>
						<div class="icon-set__id">
							<span>airplane</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-truck">

						</div>
						<div class="icon-set__id">
							<span>truck</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-road">

						</div>
						<div class="icon-set__id">
							<span>road</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-accessibility">

						</div>
						<div class="icon-set__id">
							<span>accessibility</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-target">

						</div>
						<div class="icon-set__id">
							<span>target</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-shield">

						</div>
						<div class="icon-set__id">
							<span>shield</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-power">

						</div>
						<div class="icon-set__id">
							<span>power</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-switch">

						</div>
						<div class="icon-set__id">
							<span>switch</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-power-cord">

						</div>
						<div class="icon-set__id">
							<span>power-cord</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-clipboard">

						</div>
						<div class="icon-set__id">
							<span>clipboard</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-list-numbered">

						</div>
						<div class="icon-set__id">
							<span>list-numbered</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-list">

						</div>
						<div class="icon-set__id">
							<span>list</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-list2">

						</div>
						<div class="icon-set__id">
							<span>list2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tree">

						</div>
						<div class="icon-set__id">
							<span>tree</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-menu">

						</div>
						<div class="icon-set__id">
							<span>menu</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-menu2">

						</div>
						<div class="icon-set__id">
							<span>menu2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-menu3">

						</div>
						<div class="icon-set__id">
							<span>menu3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-menu4">

						</div>
						<div class="icon-set__id">
							<span>menu4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cloud">

						</div>
						<div class="icon-set__id">
							<span>cloud</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cloud-download">

						</div>
						<div class="icon-set__id">
							<span>cloud-download</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cloud-upload">

						</div>
						<div class="icon-set__id">
							<span>cloud-upload</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cloud-check">

						</div>
						<div class="icon-set__id">
							<span>cloud-check</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-download2">

						</div>
						<div class="icon-set__id">
							<span>download2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-upload2">

						</div>
						<div class="icon-set__id">
							<span>upload2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-download3">

						</div>
						<div class="icon-set__id">
							<span>download3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-upload3">

						</div>
						<div class="icon-set__id">
							<span>upload3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sphere">

						</div>
						<div class="icon-set__id">
							<span>sphere</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-earth">

						</div>
						<div class="icon-set__id">
							<span>earth</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-link">

						</div>
						<div class="icon-set__id">
							<span>link</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-flag">

						</div>
						<div class="icon-set__id">
							<span>flag</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-attachment">

						</div>
						<div class="icon-set__id">
							<span>attachment</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-eye">

						</div>
						<div class="icon-set__id">
							<span>eye</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-eye-plus">

						</div>
						<div class="icon-set__id">
							<span>eye-plus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-eye-minus">

						</div>
						<div class="icon-set__id">
							<span>eye-minus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-eye-blocked">

						</div>
						<div class="icon-set__id">
							<span>eye-blocked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bookmark">

						</div>
						<div class="icon-set__id">
							<span>bookmark</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bookmarks">

						</div>
						<div class="icon-set__id">
							<span>bookmarks</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sun">

						</div>
						<div class="icon-set__id">
							<span>sun</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-contrast">

						</div>
						<div class="icon-set__id">
							<span>contrast</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-brightness-contrast">

						</div>
						<div class="icon-set__id">
							<span>brightness-contrast</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-star-empty">

						</div>
						<div class="icon-set__id">
							<span>star-empty</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-star-half">

						</div>
						<div class="icon-set__id">
							<span>star-half</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-star-full">

						</div>
						<div class="icon-set__id">
							<span>star-full</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-heart">

						</div>
						<div class="icon-set__id">
							<span>heart</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-heart-broken">

						</div>
						<div class="icon-set__id">
							<span>heart-broken</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-man">

						</div>
						<div class="icon-set__id">
							<span>man</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-woman">

						</div>
						<div class="icon-set__id">
							<span>woman</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-man-woman">

						</div>
						<div class="icon-set__id">
							<span>man-woman</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-happy">

						</div>
						<div class="icon-set__id">
							<span>happy</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-happy2">

						</div>
						<div class="icon-set__id">
							<span>happy2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-smile">

						</div>
						<div class="icon-set__id">
							<span>smile</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-smile2">

						</div>
						<div class="icon-set__id">
							<span>smile2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tongue">

						</div>
						<div class="icon-set__id">
							<span>tongue</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tongue2">

						</div>
						<div class="icon-set__id">
							<span>tongue2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sad">

						</div>
						<div class="icon-set__id">
							<span>sad</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sad2">

						</div>
						<div class="icon-set__id">
							<span>sad2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-wink">

						</div>
						<div class="icon-set__id">
							<span>wink</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-wink2">

						</div>
						<div class="icon-set__id">
							<span>wink2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-grin">

						</div>
						<div class="icon-set__id">
							<span>grin</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-grin2">

						</div>
						<div class="icon-set__id">
							<span>grin2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cool">

						</div>
						<div class="icon-set__id">
							<span>cool</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cool2">

						</div>
						<div class="icon-set__id">
							<span>cool2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-angry">

						</div>
						<div class="icon-set__id">
							<span>angry</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-angry2">

						</div>
						<div class="icon-set__id">
							<span>angry2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-evil">

						</div>
						<div class="icon-set__id">
							<span>evil</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-evil2">

						</div>
						<div class="icon-set__id">
							<span>evil2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-shocked">

						</div>
						<div class="icon-set__id">
							<span>shocked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-shocked2">

						</div>
						<div class="icon-set__id">
							<span>shocked2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-baffled">

						</div>
						<div class="icon-set__id">
							<span>baffled</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-baffled2">

						</div>
						<div class="icon-set__id">
							<span>baffled2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-confused">

						</div>
						<div class="icon-set__id">
							<span>confused</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-confused2">

						</div>
						<div class="icon-set__id">
							<span>confused2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-neutral">

						</div>
						<div class="icon-set__id">
							<span>neutral</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-neutral2">

						</div>
						<div class="icon-set__id">
							<span>neutral2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-hipster">

						</div>
						<div class="icon-set__id">
							<span>hipster</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-hipster2">

						</div>
						<div class="icon-set__id">
							<span>hipster2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-wondering">

						</div>
						<div class="icon-set__id">
							<span>wondering</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-wondering2">

						</div>
						<div class="icon-set__id">
							<span>wondering2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sleepy">

						</div>
						<div class="icon-set__id">
							<span>sleepy</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sleepy2">

						</div>
						<div class="icon-set__id">
							<span>sleepy2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-frustrated">

						</div>
						<div class="icon-set__id">
							<span>frustrated</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-frustrated2">

						</div>
						<div class="icon-set__id">
							<span>frustrated2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-crying">

						</div>
						<div class="icon-set__id">
							<span>crying</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-crying2">

						</div>
						<div class="icon-set__id">
							<span>crying2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-point-up">

						</div>
						<div class="icon-set__id">
							<span>point-up</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-point-right">

						</div>
						<div class="icon-set__id">
							<span>point-right</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-point-down">

						</div>
						<div class="icon-set__id">
							<span>point-down</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-point-left">

						</div>
						<div class="icon-set__id">
							<span>point-left</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-warning">

						</div>
						<div class="icon-set__id">
							<span>warning</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-notification">

						</div>
						<div class="icon-set__id">
							<span>notification</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-question">

						</div>
						<div class="icon-set__id">
							<span>question</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-plus">

						</div>
						<div class="icon-set__id">
							<span>plus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-minus">

						</div>
						<div class="icon-set__id">
							<span>minus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-info">

						</div>
						<div class="icon-set__id">
							<span>info</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cancel-circle">

						</div>
						<div class="icon-set__id">
							<span>cancel-circle</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-blocked">

						</div>
						<div class="icon-set__id">
							<span>blocked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-cross">

						</div>
						<div class="icon-set__id">
							<span>cross</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-checkmark">

						</div>
						<div class="icon-set__id">
							<span>checkmark</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-checkmark2">

						</div>
						<div class="icon-set__id">
							<span>checkmark2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-spell-check">

						</div>
						<div class="icon-set__id">
							<span>spell-check</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-enter">

						</div>
						<div class="icon-set__id">
							<span>enter</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-exit">

						</div>
						<div class="icon-set__id">
							<span>exit</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-play2">

						</div>
						<div class="icon-set__id">
							<span>play2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pause">

						</div>
						<div class="icon-set__id">
							<span>pause</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stop">

						</div>
						<div class="icon-set__id">
							<span>stop</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-previous">

						</div>
						<div class="icon-set__id">
							<span>previous</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-next">

						</div>
						<div class="icon-set__id">
							<span>next</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-backward">

						</div>
						<div class="icon-set__id">
							<span>backward</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-forward2">

						</div>
						<div class="icon-set__id">
							<span>forward2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-play3">

						</div>
						<div class="icon-set__id">
							<span>play3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pause2">

						</div>
						<div class="icon-set__id">
							<span>pause2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stop2">

						</div>
						<div class="icon-set__id">
							<span>stop2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-backward2">

						</div>
						<div class="icon-set__id">
							<span>backward2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-forward3">

						</div>
						<div class="icon-set__id">
							<span>forward3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-first">

						</div>
						<div class="icon-set__id">
							<span>first</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-last">

						</div>
						<div class="icon-set__id">
							<span>last</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-previous2">

						</div>
						<div class="icon-set__id">
							<span>previous2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-next2">

						</div>
						<div class="icon-set__id">
							<span>next2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-eject">

						</div>
						<div class="icon-set__id">
							<span>eject</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-volume-high">

						</div>
						<div class="icon-set__id">
							<span>volume-high</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-volume-medium">

						</div>
						<div class="icon-set__id">
							<span>volume-medium</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-volume-low">

						</div>
						<div class="icon-set__id">
							<span>volume-low</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-volume-mute">

						</div>
						<div class="icon-set__id">
							<span>volume-mute</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-volume-mute2">

						</div>
						<div class="icon-set__id">
							<span>volume-mute2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-volume-increase">

						</div>
						<div class="icon-set__id">
							<span>volume-increase</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-volume-decrease">

						</div>
						<div class="icon-set__id">
							<span>volume-decrease</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-loop">

						</div>
						<div class="icon-set__id">
							<span>loop</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-loop2">

						</div>
						<div class="icon-set__id">
							<span>loop2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-infinite">

						</div>
						<div class="icon-set__id">
							<span>infinite</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-shuffle">

						</div>
						<div class="icon-set__id">
							<span>shuffle</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-up-left">

						</div>
						<div class="icon-set__id">
							<span>arrow-up-left</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-up">

						</div>
						<div class="icon-set__id">
							<span>arrow-up</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-up-right">

						</div>
						<div class="icon-set__id">
							<span>arrow-up-right</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-right">

						</div>
						<div class="icon-set__id">
							<span>arrow-right</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-down-right">

						</div>
						<div class="icon-set__id">
							<span>arrow-down-right</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-down">

						</div>
						<div class="icon-set__id">
							<span>arrow-down</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-down-left">

						</div>
						<div class="icon-set__id">
							<span>arrow-down-left</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-left">

						</div>
						<div class="icon-set__id">
							<span>arrow-left</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-up-left2">

						</div>
						<div class="icon-set__id">
							<span>arrow-up-left2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-up2">

						</div>
						<div class="icon-set__id">
							<span>arrow-up2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-up-right2">

						</div>
						<div class="icon-set__id">
							<span>arrow-up-right2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-right2">

						</div>
						<div class="icon-set__id">
							<span>arrow-right2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-down-right2">

						</div>
						<div class="icon-set__id">
							<span>arrow-down-right2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-down2">

						</div>
						<div class="icon-set__id">
							<span>arrow-down2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-down-left2">

						</div>
						<div class="icon-set__id">
							<span>arrow-down-left2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-arrow-left2">

						</div>
						<div class="icon-set__id">
							<span>arrow-left2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-circle-up">

						</div>
						<div class="icon-set__id">
							<span>circle-up</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-circle-right">

						</div>
						<div class="icon-set__id">
							<span>circle-right</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-circle-down">

						</div>
						<div class="icon-set__id">
							<span>circle-down</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-circle-left">

						</div>
						<div class="icon-set__id">
							<span>circle-left</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tab">

						</div>
						<div class="icon-set__id">
							<span>tab</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-move-up">

						</div>
						<div class="icon-set__id">
							<span>move-up</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-move-down">

						</div>
						<div class="icon-set__id">
							<span>move-down</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sort-alpha-asc">

						</div>
						<div class="icon-set__id">
							<span>sort-alpha-asc</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sort-alpha-desc">

						</div>
						<div class="icon-set__id">
							<span>sort-alpha-desc</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sort-numeric-asc">

						</div>
						<div class="icon-set__id">
							<span>sort-numeric-asc</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sort-numberic-desc">

						</div>
						<div class="icon-set__id">
							<span>sort-numberic-desc</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sort-amount-asc">

						</div>
						<div class="icon-set__id">
							<span>sort-amount-asc</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sort-amount-desc">

						</div>
						<div class="icon-set__id">
							<span>sort-amount-desc</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-command">

						</div>
						<div class="icon-set__id">
							<span>command</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-shift">

						</div>
						<div class="icon-set__id">
							<span>shift</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-ctrl">

						</div>
						<div class="icon-set__id">
							<span>ctrl</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-opt">

						</div>
						<div class="icon-set__id">
							<span>opt</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-checkbox-checked">

						</div>
						<div class="icon-set__id">
							<span>checkbox-checked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-checkbox-unchecked">

						</div>
						<div class="icon-set__id">
							<span>checkbox-unchecked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-radio-checked">

						</div>
						<div class="icon-set__id">
							<span>radio-checked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-radio-checked2">

						</div>
						<div class="icon-set__id">
							<span>radio-checked2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-radio-unchecked">

						</div>
						<div class="icon-set__id">
							<span>radio-unchecked</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-crop">

						</div>
						<div class="icon-set__id">
							<span>crop</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-make-group">

						</div>
						<div class="icon-set__id">
							<span>make-group</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-ungroup">

						</div>
						<div class="icon-set__id">
							<span>ungroup</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-scissors">

						</div>
						<div class="icon-set__id">
							<span>scissors</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-filter">

						</div>
						<div class="icon-set__id">
							<span>filter</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-font">

						</div>
						<div class="icon-set__id">
							<span>font</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-ligature">

						</div>
						<div class="icon-set__id">
							<span>ligature</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-ligature2">

						</div>
						<div class="icon-set__id">
							<span>ligature2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-text-height">

						</div>
						<div class="icon-set__id">
							<span>text-height</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-text-width">

						</div>
						<div class="icon-set__id">
							<span>text-width</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-font-size">

						</div>
						<div class="icon-set__id">
							<span>font-size</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-bold">

						</div>
						<div class="icon-set__id">
							<span>bold</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-underline">

						</div>
						<div class="icon-set__id">
							<span>underline</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-italic">

						</div>
						<div class="icon-set__id">
							<span>italic</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-strikethrough">

						</div>
						<div class="icon-set__id">
							<span>strikethrough</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-omega">

						</div>
						<div class="icon-set__id">
							<span>omega</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-sigma">

						</div>
						<div class="icon-set__id">
							<span>sigma</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-page-break">

						</div>
						<div class="icon-set__id">
							<span>page-break</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-superscript">

						</div>
						<div class="icon-set__id">
							<span>superscript</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-subscript">

						</div>
						<div class="icon-set__id">
							<span>subscript</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-superscript2">

						</div>
						<div class="icon-set__id">
							<span>superscript2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-subscript2">

						</div>
						<div class="icon-set__id">
							<span>subscript2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-text-color">

						</div>
						<div class="icon-set__id">
							<span>text-color</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pagebreak">

						</div>
						<div class="icon-set__id">
							<span>pagebreak</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-clear-formatting">

						</div>
						<div class="icon-set__id">
							<span>clear-formatting</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-table">

						</div>
						<div class="icon-set__id">
							<span>table</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-table2">

						</div>
						<div class="icon-set__id">
							<span>table2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-insert-template">

						</div>
						<div class="icon-set__id">
							<span>insert-template</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pilcrow">

						</div>
						<div class="icon-set__id">
							<span>pilcrow</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-ltr">

						</div>
						<div class="icon-set__id">
							<span>ltr</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-rtl">

						</div>
						<div class="icon-set__id">
							<span>rtl</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-section">

						</div>
						<div class="icon-set__id">
							<span>section</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paragraph-left">

						</div>
						<div class="icon-set__id">
							<span>paragraph-left</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paragraph-center">

						</div>
						<div class="icon-set__id">
							<span>paragraph-center</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paragraph-right">

						</div>
						<div class="icon-set__id">
							<span>paragraph-right</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paragraph-justify">

						</div>
						<div class="icon-set__id">
							<span>paragraph-justify</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-indent-increase">

						</div>
						<div class="icon-set__id">
							<span>indent-increase</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-indent-decrease">

						</div>
						<div class="icon-set__id">
							<span>indent-decrease</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-share">

						</div>
						<div class="icon-set__id">
							<span>share</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-new-tab">

						</div>
						<div class="icon-set__id">
							<span>new-tab</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-embed">

						</div>
						<div class="icon-set__id">
							<span>embed</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-embed2">

						</div>
						<div class="icon-set__id">
							<span>embed2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-terminal">

						</div>
						<div class="icon-set__id">
							<span>terminal</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-share2">

						</div>
						<div class="icon-set__id">
							<span>share2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mail">

						</div>
						<div class="icon-set__id">
							<span>mail</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mail2">

						</div>
						<div class="icon-set__id">
							<span>mail2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mail3">

						</div>
						<div class="icon-set__id">
							<span>mail3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-mail4">

						</div>
						<div class="icon-set__id">
							<span>mail4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-google">

						</div>
						<div class="icon-set__id">
							<span>google</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-google-plus">

						</div>
						<div class="icon-set__id">
							<span>google-plus</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-google-plus2">

						</div>
						<div class="icon-set__id">
							<span>google-plus2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-google-plus3">

						</div>
						<div class="icon-set__id">
							<span>google-plus3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-google-drive">

						</div>
						<div class="icon-set__id">
							<span>google-drive</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-facebook">

						</div>
						<div class="icon-set__id">
							<span>facebook</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-facebook2">

						</div>
						<div class="icon-set__id">
							<span>facebook2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-facebook3">

						</div>
						<div class="icon-set__id">
							<span>facebook3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-ello">

						</div>
						<div class="icon-set__id">
							<span>ello</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-instagram">

						</div>
						<div class="icon-set__id">
							<span>instagram</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-twitter">

						</div>
						<div class="icon-set__id">
							<span>twitter</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-twitter2">

						</div>
						<div class="icon-set__id">
							<span>twitter2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-twitter3">

						</div>
						<div class="icon-set__id">
							<span>twitter3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-feed2">

						</div>
						<div class="icon-set__id">
							<span>feed2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-feed3">

						</div>
						<div class="icon-set__id">
							<span>feed3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-feed4">

						</div>
						<div class="icon-set__id">
							<span>feed4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-youtube">

						</div>
						<div class="icon-set__id">
							<span>youtube</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-youtube2">

						</div>
						<div class="icon-set__id">
							<span>youtube2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-youtube3">

						</div>
						<div class="icon-set__id">
							<span>youtube3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-youtube4">

						</div>
						<div class="icon-set__id">
							<span>youtube4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-twitch">

						</div>
						<div class="icon-set__id">
							<span>twitch</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-vimeo">

						</div>
						<div class="icon-set__id">
							<span>vimeo</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-vimeo2">

						</div>
						<div class="icon-set__id">
							<span>vimeo2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-vimeo3">

						</div>
						<div class="icon-set__id">
							<span>vimeo3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-lanyrd">

						</div>
						<div class="icon-set__id">
							<span>lanyrd</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-flickr">

						</div>
						<div class="icon-set__id">
							<span>flickr</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-flickr2">

						</div>
						<div class="icon-set__id">
							<span>flickr2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-flickr3">

						</div>
						<div class="icon-set__id">
							<span>flickr3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-flickr4">

						</div>
						<div class="icon-set__id">
							<span>flickr4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-picassa">

						</div>
						<div class="icon-set__id">
							<span>picassa</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-picassa2">

						</div>
						<div class="icon-set__id">
							<span>picassa2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-dribbble">

						</div>
						<div class="icon-set__id">
							<span>dribbble</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-dribbble2">

						</div>
						<div class="icon-set__id">
							<span>dribbble2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-dribbble3">

						</div>
						<div class="icon-set__id">
							<span>dribbble3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-forrst">

						</div>
						<div class="icon-set__id">
							<span>forrst</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-forrst2">

						</div>
						<div class="icon-set__id">
							<span>forrst2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-deviantart">

						</div>
						<div class="icon-set__id">
							<span>deviantart</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-deviantart2">

						</div>
						<div class="icon-set__id">
							<span>deviantart2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-steam">

						</div>
						<div class="icon-set__id">
							<span>steam</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-steam2">

						</div>
						<div class="icon-set__id">
							<span>steam2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-dropbox">

						</div>
						<div class="icon-set__id">
							<span>dropbox</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-onedrive">

						</div>
						<div class="icon-set__id">
							<span>onedrive</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-github">

						</div>
						<div class="icon-set__id">
							<span>github</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-github2">

						</div>
						<div class="icon-set__id">
							<span>github2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-github3">

						</div>
						<div class="icon-set__id">
							<span>github3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-github4">

						</div>
						<div class="icon-set__id">
							<span>github4</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-github5">

						</div>
						<div class="icon-set__id">
							<span>github5</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-wordpress">

						</div>
						<div class="icon-set__id">
							<span>wordpress</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-wordpress2">

						</div>
						<div class="icon-set__id">
							<span>wordpress2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-joomla">

						</div>
						<div class="icon-set__id">
							<span>joomla</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-blogger">

						</div>
						<div class="icon-set__id">
							<span>blogger</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-blogger2">

						</div>
						<div class="icon-set__id">
							<span>blogger2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tumblr">

						</div>
						<div class="icon-set__id">
							<span>tumblr</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tumblr2">

						</div>
						<div class="icon-set__id">
							<span>tumblr2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-yahoo">

						</div>
						<div class="icon-set__id">
							<span>yahoo</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-tux">

						</div>
						<div class="icon-set__id">
							<span>tux</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-apple">

						</div>
						<div class="icon-set__id">
							<span>apple</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-finder">

						</div>
						<div class="icon-set__id">
							<span>finder</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-android">

						</div>
						<div class="icon-set__id">
							<span>android</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-windows">

						</div>
						<div class="icon-set__id">
							<span>windows</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-windows8">

						</div>
						<div class="icon-set__id">
							<span>windows8</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-soundcloud">

						</div>
						<div class="icon-set__id">
							<span>soundcloud</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-soundcloud2">

						</div>
						<div class="icon-set__id">
							<span>soundcloud2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-skype">

						</div>
						<div class="icon-set__id">
							<span>skype</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-reddit">

						</div>
						<div class="icon-set__id">
							<span>reddit</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-linkedin">

						</div>
						<div class="icon-set__id">
							<span>linkedin</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-linkedin2">

						</div>
						<div class="icon-set__id">
							<span>linkedin2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-lastfm">

						</div>
						<div class="icon-set__id">
							<span>lastfm</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-lastfm2">

						</div>
						<div class="icon-set__id">
							<span>lastfm2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-delicious">

						</div>
						<div class="icon-set__id">
							<span>delicious</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stumbleupon">

						</div>
						<div class="icon-set__id">
							<span>stumbleupon</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stumbleupon2">

						</div>
						<div class="icon-set__id">
							<span>stumbleupon2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-stackoverflow">

						</div>
						<div class="icon-set__id">
							<span>stackoverflow</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pinterest">

						</div>
						<div class="icon-set__id">
							<span>pinterest</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-pinterest2">

						</div>
						<div class="icon-set__id">
							<span>pinterest2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-xing">

						</div>
						<div class="icon-set__id">
							<span>xing</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-xing2">

						</div>
						<div class="icon-set__id">
							<span>xing2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-flattr">

						</div>
						<div class="icon-set__id">
							<span>flattr</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-foursquare">

						</div>
						<div class="icon-set__id">
							<span>foursquare</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paypal">

						</div>
						<div class="icon-set__id">
							<span>paypal</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paypal2">

						</div>
						<div class="icon-set__id">
							<span>paypal2</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-paypal3">

						</div>
						<div class="icon-set__id">
							<span>paypal3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-yelp">

						</div>
						<div class="icon-set__id">
							<span>yelp</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-pdf">

						</div>
						<div class="icon-set__id">
							<span>file-pdf</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-openoffice">

						</div>
						<div class="icon-set__id">
							<span>file-openoffice</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-word">

						</div>
						<div class="icon-set__id">
							<span>file-word</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-file-excel">

						</div>
						<div class="icon-set__id">
							<span>file-excel</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-libreoffice">

						</div>
						<div class="icon-set__id">
							<span>libreoffice</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-html5">

						</div>
						<div class="icon-set__id">
							<span>html5</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-html52">

						</div>
						<div class="icon-set__id">
							<span>html52</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-css3">

						</div>
						<div class="icon-set__id">
							<span>css3</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-git">

						</div>
						<div class="icon-set__id">
							<span>git</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-svg">

						</div>
						<div class="icon-set__id">
							<span>svg</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-codepen">

						</div>
						<div class="icon-set__id">
							<span>codepen</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-chrome">

						</div>
						<div class="icon-set__id">
							<span>chrome</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-firefox">

						</div>
						<div class="icon-set__id">
							<span>firefox</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-IE">

						</div>
						<div class="icon-set__id">
							<span>IE</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-opera">

						</div>
						<div class="icon-set__id">
							<span>opera</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-safari">

						</div>
						<div class="icon-set__id">
							<span>safari</span>
						</div>
					</div>
				</div>	 			
				
				<div class="col_xs_2">
					<div class="icon-set__inner">
						<div class="icon-set__icon icon-font-IcoMoon">

						</div>
						<div class="icon-set__id">
							<span>IcoMoon</span>
						</div>
					</div>
				</div>	 			
			
		</div>
	</div>
</div>	
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>