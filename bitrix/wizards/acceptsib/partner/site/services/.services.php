<?php 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	"main" => Array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => Array(
			"files.php",
			"template.php",
			"theme.php",
			"group.php",
			"settings.php",	
			"mail.php",	
		),
	),
	"iblock" => Array(
		"NAME" => GetMessage("SERVICE_IBLOCK"),
		"STAGES" => Array(
			"types.php",
			"present.php",
			"advantages.php",
			"services.php",
			"slider.php",
			"whywe.php",
			"portfolio.php",
			"howwork.php",
			"benefits.php",
			"faq.php",
			"reviews.php",
			"prices.php",
			"partners.php",
			"social.php",	
			"callback.php"	
		),
	),
);
?>