<?php 
$MESS["PORTFOLIO_TAB_NAME"] = "Элемент";
$MESS["PORTFOLIO_TAB_02_NAME"] = "Альбомы";
$MESS["PORTFOLIO_FIELD_ACTIVE"] = "Активность";
$MESS["PORTFOLIO_FIELD_ACTIVE_FROM"] = "Начало активности";
$MESS["PORTFOLIO_FIELD_NAME"] = "Название";
$MESS["PORTFOLIO_FIELD_CODE"] = "Символьный код";
$MESS["PORTFOLIO_FIELD_SORT"] = "Сортировка";
$MESS["PORTFOLIO_FIELD_DETAIL_TEXT"] = "Описание";
$MESS["PORTFOLIO_FIELD_PREVIEW_PICTURE"] = "Картинка для анонса";
$MESS["PORTFOLIO_FIELD_IBLOCK_SECTION"] = "Альбомы";
?>