<?php 
$MESS["CONTENT_TYPE_NAME"] = "Content";
$MESS["CONTENT_SECTION_NAME"] = "Section";
$MESS["CONTENT_ELEMENT_NAME"] = "Section";
$MESS["SETTINGS_TYPE_NAME"] = "Content";
$MESS["SETTINGS_SECTION_NAME"] = "Section";
$MESS["SETTINGS_ELEMENT_NAME"] = "Section";
$MESS["FORM_TYPE_NAME"] = "Content";
$MESS["FORM_SECTION_NAME"] = "Section";
$MESS["FORM_ELEMENT_NAME"] = "Section";
?>