<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="promo-list">
	<ul>
	<?php foreach($arResult["ITEMS"] as $arItem):?>	
		<?php if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<li class="promo-list__item">
				<?php if(!empty($arItem["DISPLAY_PROPERTIES"]['ICON_WHYWE']['VALUE'])):?>
					<span class="promo-list__icon icon-font-<?= $arItem["DISPLAY_PROPERTIES"]['ICON_WHYWE']['VALUE']?>"></span>
				<?php endif;?>
				<div class="promo-list__text">
					<p><?= $arItem["NAME"]?></p>
				</div>
			</li>		
		<?php endif;?>	
	<?php endforeach;?>				
	</ul>
</div>