<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php if(!empty($arResult)):?>
	<div class="navbar-wrapper">
		<div class="navbar" role="navigation">
			<div class="navbar__header">
				<div class="container">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar__collapse">
					<span class="navbar-toggle__icon-bar"></span>
					<span class="navbar-toggle__icon-bar"></span>
					<span class="navbar-toggle__icon-bar"></span>
				  </button>   	    		
				</div>     	
			</div>
			<div class="navbar__collapse collapse">
				<div class="container">
				  <ul class="nav navbar__nav">			
					<?php 
						$count = 0;
						foreach($arResult as $arItem):
						$count++;
					?>   
						<li>
							<a <?php if($count == 0) echo "class='active-item'";?> href="<?= $arItem["LINK"]?>">
								<?= $arItem["TEXT"]?>
							</a>
						</li>
					<?php endforeach?>
				  </ul>					
				</div>	
			</div>
		</div>
	</div>
<?php endif?>