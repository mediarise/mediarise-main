<?php 
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

if(class_exists("acceptsib_partner")) return;

Class acceptsib_partner extends CModule
{
	var $MODULE_ID = "acceptsib.partner";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function acceptsib_partner()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");	

		$this->MODULE_VERSION = $arModuleVersion["VERSION"]; 
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"]; 

		$this->MODULE_NAME = GetMessage("AS_MODUL_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("AS_MODUL_INSTALL_DESCRIPTION");
		$this->PARTNER_NAME = GetMessage("AS_MODUL_PARTNER");
		$this->PARTNER_URI = GetMessage("AS_MODUL_PARTNER_URI");
	}

	function InstallDB($install_wizard = true)
	{
		global $DB, $DBType, $APPLICATION;
		
		RegisterModule("acceptsib.partner");
		RegisterModuleDependences("main", "OnBeforeProlog", "acceptsib.partner", "CSiteAS", "ShowPanel");
		
		return true;
	}

	function UnInstallDB($arParams = Array())
	{
		global $DB, $DBType, $APPLICATION;
		
		UnRegisterModule("acceptsib.partner");
		UnRegisterModuleDependences("main", "OnBeforeProlog", "acceptsib.partner", "CSiteAS", "ShowPanel"); 
		
		return true;
	}

	function InstallEvents()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/acceptsib.partner/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles()
	{
		return true;
	}

	function InstallPublic()
	{
	}

	function UnInstallFiles()
	{
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION, $step;

		$this->InstallFiles();
		$this->InstallDB(false);
		$this->InstallEvents();
		$this->InstallPublic();

		$APPLICATION->IncludeAdminFile(GetMessage("AS_MODUL_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/acceptsib.partner/install/step.php");
	}

	function DoUninstall()
	{
		global $APPLICATION, $step;

		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->UnInstallEvents();

		$APPLICATION->IncludeAdminFile(GetMessage("AS_MODUL_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/acceptsib.partner/install/unstep.php");
	}
}
?>