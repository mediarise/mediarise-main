<?php 
$MESS ['MCT_TITLE_FORM'] = "Заполните форму";
$MESS ['MCT_NAME'] = "Ваше имя";
$MESS ['MCT_PHONE'] = "Телефон";
$MESS ['MCT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MCT_CAPTCHA_CODE'] = "Введите символы";
$MESS ['MCT_SUBMIT'] = "Отправить";
$MESS ['MCT_FIELDS_NUMBERS'] = "Допустимы только цифры";
$MESS ['MCT_FIELDS_NONE'] = "Поле не заполнено";
$MESS ['MCT_FIELDS_MIN_3'] = "мин. 3 символов";
$MESS ['MCT_FIELDS_MIN_7'] = "мин. 7 символов";
?>