<?php 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

IncludeModuleLangFile(__FILE__);

$arFilter = array("TYPE_ID" => "CALLBACK_FORM");
$rsET = CEventType::GetList($arFilter);

$arET = $rsET->Fetch();

if(empty($arET)) {

	$et = new CEventType;
	
	$idET = $et->Add(array(
		"LID"           => "ru",
		"EVENT_NAME"    => "CALLBACK_FORM",
		"NAME"          => GetMessage("EVENT_TYPE_NAME"),
		"DESCRIPTION"   => GetMessage("EVENT_TYPE_DESCRIPTION"),
		));	
}

if($idET || $arET) {
	
	$arFilter = Array(
		"TYPE_ID"       => "CALLBACK_FORM",
		"ACTIVE"        => "Y",
		"SUBJECT"       => GetMessage("EVENT_TAMP_SUBJECT"),
		"SITE_ID"       => WIZARD_SITE_ID,
		);	
		
	$rsETamp = CEventMessage::GetList($by="site_id", $order="desc", $arFilter);	
	$arETamp = $rsETamp->Fetch();
	
	if(empty($arETamp)){
		
		$tamplate = new CEventMessage;
		
		$arMessage = Array(
		"EVENT_NAME" => "CALLBACK_FORM",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#EMAIL_TO#",
		"SUBJECT" =>  GetMessage("EVENT_TAMP_SUBJECT"),
		"MESSAGE" => GetMessage("EVENT_TAMP_MESSAGE"),
		);
		
		$templateID = $tamplate->Add($arMessage);
		
	}
}