<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="shell">
	<div class="fs-present-display" >
		<div class="container">
			<div class="present-info">
				<div class="present-carousel owl-carousel">		
					<?php  	$count = 0;
						foreach($arResult["ITEMS"] as $arItem):
						$count++;
					?>			
						<div class="present-carousel__item">					
							<div class="present-info__inner">
								<?php if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
									<div class="it-aimation">
										<?php if($count == 1):?>
											<h1><?= $arItem["NAME"]?></h1>
										<?php else:?>
											<h2><?= $arItem["NAME"]?></h2>
										<?php endif;?>
									</div>
								<?php endif;?>
								<?php if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<div class="it-aimation">
										<p><?= $arItem["PREVIEW_TEXT"]?></p>
									</div>
								<?php endif;?>
								<?php if(!empty($arItem["DISPLAY_PROPERTIES"]['TEXT_BUTTON_PRESENT']['VALUE'])):?>
									<div class="it-aimation">
										<span data-dialog="callback" class="button button_large button_fs-present-display">
										<?php if(!empty($arItem["DISPLAY_PROPERTIES"]['ICON_BUTTON_PRESENT']['VALUE'])):?>
											<small class="icon-font-<?= $arItem["DISPLAY_PROPERTIES"]['ICON_BUTTON_PRESENT']['VALUE']?>"></small>
										<?php endif;?>
										<?= $arItem["DISPLAY_PROPERTIES"]['TEXT_BUTTON_PRESENT']['VALUE']?>
										</span>									
									</div>
								<?php endif;?>		
							</div>
						</div>					
					<?php endforeach;?>						
				</div>				
			</div>
		</div>
	</div>
</div>