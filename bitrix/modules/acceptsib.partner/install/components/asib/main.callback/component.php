<?php 
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");

$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if($arParams["EVENT_NAME"] == '')
	$arParams["EVENT_NAME"] = "CALLBACK_FORM";

$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
if($arParams["EMAIL_TO"] == '')
	$arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");

$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '')
	$arParams["OK_TEXT"] = GetMessage("MC_OK_MESSAGE");

if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] <> '' && (!isset($_POST["PARAMS_HASH"]) || $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
	$arResult["ERROR_MESSAGE"] = array();
	
	if(LANG_CHARSET == "windows-1251") {
		$_POST["NAME_CALLBACK"] = iconv( 'UTF-8', 'Windows-1251',  $_POST["NAME_CALLBACK"]);  
		$_POST["PHONE_CALLBACK"] = iconv( 'UTF-8', 'Windows-1251',  $_POST["PHONE_CALLBACK"]); 		
	}
		
	if(check_bitrix_sessid())
	{
		
		if(strlen($_POST["NAME_CALLBACK"]) <= 1)
			$arResult["ERROR_MESSAGE"][] = GetMessage("MC_REQ_NAME");
		
		if(strlen($_POST["PHONE_CALLBACK"]) <= 1)
			$arResult["ERROR_MESSAGE"][] = GetMessage("MC_REQ_PHONE");
	
		if($arParams["USE_CAPTCHA"] == "Y")
		{
			include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
			$captcha_code = $_POST["captcha_sid"];
			$captcha_word = $_POST["captcha_word"];
			$cpt = new CCaptcha();
			$captchaPass = COption::GetOptionString("main", "captcha_password", "");
			if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0)
			{
				if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass))
					$arResult["ERROR_MESSAGE"][] = GetMessage("MC_CAPTCHA_WRONG");
			}
			else
				$arResult["ERROR_MESSAGE"][] = GetMessage("MC_CAPTHCA_EMPTY");

		}
		if(empty($arResult["ERROR_MESSAGE"]))
		{	
			//Add block
			$iBlockId = $arParams["IBLOCK_ID_ADD"];
			if(!empty($iBlockId) && CModule::IncludeModule("iblock")) {
					
					$res = CIBlock::GetProperties($iBlockId, Array(), Array());
					
					$arrPropCode = array();
					
					while($arrRes = $res->GetNext()) {
						$relCode = $arrRes["CODE"];
						$arrPropCode[$relCode] = htmlspecialcharsbx($_POST[$relCode]);
					}
					
					$element = new CIBlockElement;
					
					$arrElementFields = Array(  
					   'MODIFIED_BY' => $GLOBALS['USER']->GetID(),  
					   'IBLOCK_ID' => $iBlockId,
					   "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL"),
					   'PROPERTY_VALUES' => $arrPropCode,
					   'NAME' => $arrPropCode["NAME_CALLBACK"].GetMessage('MC_SEND_NAME'),  
					   'ACTIVE' => 'Y',
					);
					
					if (!$PRODUCT_ID = $element->Add($arrElementFields))
					{
						$arResult["ERROR_MESSAGE"][] = $element->LAST_ERROR;
					}				
			}	
			
			//Send email
			$arFields = Array(
				"AUTHOR" =>  $_POST["NAME_CALLBACK"],
				"AUTHOR_PHONE" => $_POST["PHONE_CALLBACK"],
				"EMAIL_TO" => $arParams["EMAIL_TO"],
			);
			if(!empty($arParams["EVENT_MESSAGE_ID"]))
			{
				foreach($arParams["EVENT_MESSAGE_ID"] as $v)
					if(IntVal($v) > 0)
						CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
			}
			else 
					CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
				$_SESSION["MC_NAME"] = htmlspecialcharsbx($_POST["NAME_CALLBACK"]);
				$_SESSION["MC_PHONE"] = htmlspecialcharsbx($_POST["PHONE_CALLBACK"]);
				LocalRedirect($APPLICATION->GetCurPageParam("success=".$arResult["PARAMS_HASH"], Array("success")));				
								
		}
	
		$arResult["AUTHOR_NAME"] = htmlspecialcharsbx($_POST["NAME_CALLBACK"]);
		$arResult["AUTHOR_PHONE"] = htmlspecialcharsbx($_POST["PHONE_CALLBACK"]);			
		
	}
	else
		$arResult["ERROR_MESSAGE"][] = GetMessage("MC_SESS_EXP");
}
elseif($_REQUEST["success"] == $arResult["PARAMS_HASH"])
{
	$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];		
}

if(empty($arResult["ERROR_MESSAGE"]))
{
	if(strlen($_SESSION["MC_NAME"]) > 0)
		$arResult["AUTHOR_NAME"] = htmlspecialcharsbx($_SESSION["MC_NAME"]);
	if(strlen($_SESSION["MC_PHONE"]) > 0)
		$arResult["AUTHOR_PHONE"] = htmlspecialcharsbx($_SESSION["MC_PHONE"]);
}

if($arParams["USE_CAPTCHA"] == "Y")
	$arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());

$this->IncludeComponentTemplate();