<?php 
IncludeModuleLangFile(__FILE__);

class CSiteAS
{
	function ShowPanel()
	{
		if ($GLOBALS["USER"]->IsAdmin() && COption::GetOptionString("main", "wizard_solution", "", SITE_ID) == "partner")
		{
			$GLOBALS["APPLICATION"]->AddPanelButton(array(
				"HREF" => "/bitrix/admin/wizard_install.php?lang=".LANGUAGE_ID."&wizardName=acceptsib:partner&wizardSiteID=".SITE_ID."&".bitrix_sessid_get(),
				"ID" => "partner_wizard",
				"ICON" => "bx-panel-site-wizard-icon",
				"MAIN_SORT" => 2500,
				"TYPE" => "BIG",
				"SORT" => 10,	
				"ALT" => GetMessage("AS_BUTTON_DESCRIPTION"),
				"TEXT" => GetMessage("AS_BUTTON_NAME"),
				"MENU" => array(),
			));
		}
	}
}
?>