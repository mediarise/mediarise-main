<?php 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?php 
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="bx-auth">
<?php if($arResult["AUTH_SERVICES"]):?>
	<div class="bx-auth-title"><?php echo GetMessage("AUTH_TITLE")?></div>
<?php endif?>
	<div class="bx-auth-note"><?= GetMessage("AUTH_PLEASE_AUTH")?></div>

	<form name="form_auth" method="post" target="_top" action="<?= $arResult["AUTH_URL"]?>">

		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<?php if (strlen($arResult["BACKURL"]) > 0):?>
		<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"]?>" />
		<?php endif?>
		<?php foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?= $key?>" value="<?= $value?>" />
		<?php endforeach?>

		<table class="bx-auth-table">
			<tr>
				<td class="bx-auth-label"><?= GetMessage("AUTH_LOGIN")?></td>
				<td><input class="bx-auth-input form-control" type="text" name="USER_LOGIN" maxlength="255" value="<?= $arResult["LAST_LOGIN"]?>" /></td>
			</tr>
			<tr>
				<td class="bx-auth-label"><?= GetMessage("AUTH_PASSWORD")?></td>
				<td><input class="bx-auth-input form-control" type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" />
<?php if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?php echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?php echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure').style.display = 'inline-block';
</script>
<?php endif?>
				</td>
			</tr>
			<?php if($arResult["CAPTCHA_CODE"]):?>
				<tr>
					<td></td>
					<td><input type="hidden" name="captcha_sid" value="<?php echo $arResult["CAPTCHA_CODE"]?>" />
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?php echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></td>
				</tr>
				<tr>
					<td class="bx-auth-label"><?php echo GetMessage("AUTH_CAPTCHA_PROMT")?>:</td>
					<td><input class="bx-auth-input form-control" type="text" name="captcha_word" maxlength="50" value="" size="15" /></td>
				</tr>
			<?php endif;?>
<?php if ($arResult["STORE_PASSWORD"] == "Y"):?>
			<tr>
				<td></td>
				<td><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" /><label for="USER_REMEMBER">&nbsp;<?= GetMessage("AUTH_REMEMBER_ME")?></label></td>
			</tr>
<?php endif?>
			<tr>
				<td></td>
				<td class="authorize-submit-cell"><input type="submit" class="btn btn-primary" name="Login" value="<?= GetMessage("AUTH_AUTHORIZE")?>" /></td>
			</tr>
		</table>

<?php if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
		<noindex>
			<p>
				<a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?= GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
			</p>
		</noindex>
<?php endif?>

<?php if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
		<noindex>
			<p>
				<a href="<?= $arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?= GetMessage("AUTH_REGISTER")?></a><br />
				<?= GetMessage("AUTH_FIRST_ONE")?>
			</p>
		</noindex>
<?php endif?>

	</form>
</div>

<script type="text/javascript">
<?php if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?php else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?php endif?>
</script>