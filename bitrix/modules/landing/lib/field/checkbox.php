<?php 
namespace Bitrix\Landing\Field;

class Checkbox extends \Bitrix\Landing\Field
{
	/**
	 * Vew field.
	 * @param array $params Array params:
	 * name - field name
	 * class - css-class for this element
	 * additional - some additional params as is.
	 * @return void
	 */
	public function viewForm(array $params = array())
	{
		$name = \htmlspecialcharsbx(isset($params['name_format'])
				? str_replace('#field_code#', $this->code, $params['name_format'])
				: $this->code);
		?>
		<input type="hidden" name="<?=  $name?>" value="N" />
		<input type="checkbox" <?php 
		?><?=  isset($params['additional']) ? $params['additional'] . ' ' : ''?><?php 
		?><?=  isset($params['id']) ? 'id="' . \htmlspecialcharsbx($params['id']) . '" ' : ''?><?php 
		?>class="<?=  isset($params['class']) ? \htmlspecialcharsbx($params['class']) : ''?>" <?php 
		?>data-code="<?=  \htmlspecialcharsbx($this->code)?>" <?php 
		?>name="<?=  $name?>" <?php 
		?>value="Y"<?php if ($this->value == 'Y'){?> checked="checked"<?php }?> <?php 
		?> />
		<?php 
	}

	/**
	 * Magic method return value as string.
	 * @return string
	 */
	public function __toString()
	{
		return $this->value == 'Y' ? 'Y' : 'N';
	}
}
