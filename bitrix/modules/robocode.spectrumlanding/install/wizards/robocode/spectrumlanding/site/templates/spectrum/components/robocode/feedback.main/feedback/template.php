<?php 
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?php if($arParams['USE_IN_COMPONENT']!="Y"):?>
<div class="row">
	<?php endif?>


	<?php  if(strlen($arResult["OK_MESSAGE"]) > 0):?>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="alert alert-success">
					<strong><?= $arParams['OK_TEXT']?></strong><br>
				</div>
			</div>
		</div>
	<?php endif?>
	<?php  if(!empty($arResult["ERROR_MESSAGE"])):?>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="alert alert-danger">
					<strong><?php foreach($arResult["ERROR_MESSAGE"] as $v){ echo $v;}?></strong><br>
				</div>
			</div>
		</div>
	<?php endif?>

	<form class="form-horizontal" id="contact-form" role="form" data-toggle="validator" action="<?= POST_FORM_ACTION_URI?>" method="POST">
		<?= bitrix_sessid_post()?>

		<?php if(in_array("NAME", $arParams["USED_FIELDS"]) or in_array("EMAIL", $arParams["USED_FIELDS"]) or in_array("PHONE", $arParams["USED_FIELDS"])):?>
			<div class="col-md-6">
				<?php if(in_array("NAME", $arParams["USED_FIELDS"])):?>
					<input type="text" name="names" required="" class="contact-input white-input" placeholder="<?= $arParams['NAME_HINT_TEXT']?><?php if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>*<?php endif?>" value="<?= $arResult["AUTHOR_NAME"]?>">
				<?php endif?>
				<?php if(in_array("EMAIL", $arParams["USED_FIELDS"])):?>
					<input class="contact-input white-input" required="" name="email" placeholder="<?= $arParams['EMAIL_HINT_TEXT']?><?php if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>*<?php endif?>" type="email" value="<?= $arResult["AUTHOR_EMAIL"]?>">
				<?php endif?>
				<?php if(in_array("PHONE", $arParams["USED_FIELDS"])):?>
					<input class="contact-input white-input" required="" name="phone" placeholder="<?= $arParams['PHONE_HINT_TEXT']?><?php if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>*<?php endif?>" type="text"  value="">
				<?php endif?>
			</div>
		<?php endif?>

		<?php if(in_array("MESSAGE", $arParams["USED_FIELDS"])):?>
			<div class="col-md-6">
				<textarea class="contact-commnent white-input" rows="<?= $arParams['MESSAGE_HIDTH']?>" placeholder="<?= $arParams['MESSAGE_HINT_TITLE']?><?php if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>*<?php endif?>" name="message"></textarea>
			</div>
		<?php endif?>

		<br/>



		<?php if($arParams["USE_CAPTCHA"] == "Y"):?>
			<div class="col-lg-12">
				<div class="col-lg-6">
					<p style="text-align: center;"><?= GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></p>
					<div class="col-lg-6">
						<p style="text-align: center;"><input type="hidden" name="captcha_sid" value="<?= $arResult["capCode"]?>">
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["capCode"]?>" width="158" height="35" alt="CAPTCHA"></p>
					</div>
					<div class="col-lg-6">
						<input class="required form-control" type="text" name="captcha_word" size="30" maxlength="50" value="">
					</div>
				</div>
				<div class="col-lg-6">
					<br/>
					<p style="text-align:  center;">
						<input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"]?>">
						<input class="contact-submit" type="submit" name="submit" value="<?= $arParams['BUTTON_MESSAGE']?>" style="padding-left: 50px; padding-right: 50px;">
					</p>
				</div>
			</div>
		<?php else:?>

			<div class="col-md-12">
				<input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"]?>">
				<input value="<?= $arParams['BUTTON_MESSAGE']?>" id="submit-button" class="contact-submit" type="submit" name="submit">
			</div>

		<?php endif;?>

	</form>

	<?php if($arParams['USE_IN_COMPONENT']!="Y"):?>
</div>
<?php endif?>
