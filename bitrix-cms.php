<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Продуктами 1С-Битрикс");
$APPLICATION->SetPageProperty("keywords", "Продуктами 1С-Битрикс");
$APPLICATION->SetPageProperty("description", "Продуктами 1С-Битрикс");
$APPLICATION->SetTitle("Продуктами 1С-Битрикс");
?><style>
td {
 text-align: center;
}
p {
 text-align: left;
}

li {
 text-align: left;
}

td:first-child {
 text-align: left;
}

h6 {
font-size: 24px;
}
</style>

<div  class="section section_cols-promo">
        <div class="section__name">
            <div class="container">

 <img alt="Сравните лицензии" src="/images/box.png" title="1С-Битрикс: Управление сайтом" valign="middle" height="250" border="0" align="middle">
 
<p> «1С-Битрикс: Управление сайтом» включает все необходимые инструменты для создания и управления интернет-ресурсами: от эффектных лендингов и визиток до крупных интернет-магазинов. <br>
		 Просто выберите лицензию, подходящую по возможностям:<br></p>
<ul>
			<li>«Старт»</li>
			<li>«Стандарт»</li>
			<li>«Малый бизнес»</li>
			<li>«Бизнес»</li>
			<li>«Энтерпрайз»</li>
		</ul>

<p>
	 Сравните возможности, входящие в каждую лицензию и выберите для себя наиболее подходящую!
</p>
 <br>
 <br>
<table width="100%">
<tbody>
<tr>
	<td width="200">
 <br>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Старт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Стандарт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Малый бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Энтерпрайз</b>
	</td>
</tr>
<tr>
	<td>
		<h5>Стоимость</h5>
	</td>
	<td height="30" align="center">
 <span style="color: red"><b>	<b>5 400 руб.</b></b></span>
	</td>
	<td height="30" align="center">
 <span style="color: red"><b>	<b>15 900 руб.</b></b></span>
	</td>
	<td height="30" align="center">
 <span style="color: red"><b>	<b>35 900 руб.</b></b></span>
	</td>
	<td height="30" align="center">
 <span style="color: red"><b>	<b>72 900 руб.</b></b></span>
	</td>
	<td height="30" align="center">
 <span style="color: red"><b> </b></span>
	</td>
</tr>

<tr>
	<td>
 <br>
	</td>
	<td colspan="4" align="center">
		<h6>CMS</h6>
	</td>
</tr>
<tr>
	<td height="40">
 Главный модуль
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="https://release18.1c-bitrix.ru/" title="sites">Сайты24</a>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
		 Число сайтов
	</td>
	<td align="center">
		 2
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">*
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">*
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">*
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">*
	</td>
</tr>
<tr>
	<td height="40">
		 Число страниц
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/filemanager.php" title="filemanager">Управление структурой</a>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/infoblocks.php" title="infoblocks">Информационные блоки</a>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/highloadblock.php" title="highload">Highload-блоки</a>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/search.php" title="search">Поиск</a>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/translate.php" title="translate">Перевод</a>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Форумы
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/blog.php" title="blog">Блоги</a>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/socialnet.php" title="socialnet">Социальная сеть</a>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Обучение, тестирование
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/messenger.php" title="messenger">Веб-мессенджер</a>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Бизнес-процессы
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Почта
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Техподдержка
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Календари
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/workflow.php" title="workflow">Документооборот</a>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
AD/LDAP интеграция
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Универсальные списки
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Контроллер сайтов
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
</tbody>
</table>
 <br>
 <br>
<table width="100%">
<tbody>
<tr>
	<td>
 <br>
	</td>
	<td colspan="4" height="60" align="center">
		<h6>Интернет-магазин</h6>
	</td>
</tr>
<tr>
	<td width="200">
 <br>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Старт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Стандарт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Малый бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Энтерпрайз</b>
	</td>
</tr>
<tr>
	<td height="40">
Интернет-магазин
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
		 Базовая
	</td>
	<td align="center">
		 Расширенная
	</td>
	<td align="center">
		 Расширенная
	</td>
</tr>
<tr>
	<td height="40">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td colspan="2" height="60" align="center">
		 Сравнить комплектации	</td>
</tr>
<tr>
	<td height="40">
Торговый каталог
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Интеграция с CRM
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Мастер управления магазином
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Складской учет
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
		 1 склад
	</td>
	<td align="center">
		 неограниченно
	</td>
	<td align="center">
		 неограниченно
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/currency.php" title="currency">Валюты</a>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Конструктор отчетов
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="90">
		 Разделение заказов между филиалами компании
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
</tbody>
</table>
 <br>
<table width="100%">
<tbody>
<tr>
	<td>
 <br>
	</td>
	<td colspan="4" height="60" align="center">
		<h6>Поддержка и консалтинг 1С-Битрикс</h6>
	</td>
</tr>
<tr>
	<td width="200">
 <br>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Старт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Стандарт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Малый бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Энтерпрайз</b>
	</td>
</tr>
<tr>
	<td height="40">
Время реакции в техподдержке
	</td>
	<td align="center">
		 6 часов
	</td>
	<td align="center">
		 6 часов
	</td>
	<td align="center">
		 6 часов
	</td>
	<td align="center">
		 6 часов
	</td>
	<td align="center">
		 2 часа
	</td>
</tr>
<tr>
	<td height="40">
VIP-поддержка 24/7
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="60">
Аудит производительности от 1С-Битрикс
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="70">
Сопровождение проекта от 1С-Битрикс
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
</tbody>
</table>
 <br>
<table width="100%">
<tbody>
<tr>
	<td>
 <br>
	</td>
	<td colspan="4" height="60" align="center">
		<h6>Маркетинг</h6>
	</td>
</tr>
<tr>
	<td width="200">
 <br>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Старт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Стандарт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Малый бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Энтерпрайз</b>
	</td>
</tr>
<tr>
	<td height="40">
 SEO-модуль
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Социальные сервисы
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Интеграция с Битрикс24
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Веб-формы
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Опросы
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Подписка и рассылки
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
A/B тестирование
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
E-mail маркетинг
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Реклама
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Веб-аналитика
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
</tbody>
</table>
 <br>
<table width="100%">
<tbody>
<tr>
	<td>
 <br>
	</td>
	<td colspan="4" height="60" align="center">
		<h6>Производительность</h6>
	</td>
</tr>
<tr>
	<td width="200">
 <br>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Старт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Стандарт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Малый бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Энтерпрайз</b>
	</td>
</tr>
<tr>
	<td height="40">
Композитный сайт
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
 <a href="/products/cms/features/compress.php" title="compress">Компрессия</a>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Ежемесячный трафик CDN
	</td>
	<td align="center">
		 5 Гб
	</td>
	<td align="center">
		 10 Гб
	</td>
	<td align="center">
		 20 Гб
	</td>
	<td align="center">
		 40 Гб
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">
	</td>
</tr>
<tr>
	<td height="40">
		 Допустимое число серверов
	</td>
	<td align="center">
		 1
	</td>
	<td align="center">
		 1
	</td>
	<td align="center">
		 1
	</td>
	<td align="center">
		 1
	</td>
	<td align="center">
		 от 4
	</td>
</tr>
<tr>
	<td height="60">
Монитор производительности
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="60">
 <a href="/products/cms/features/scaling.php" title="scaling">Визуальное масштабирование</a>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Пульс конверсии
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Модуль "Веб-кластер"
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="70">
Балансировка нагрузки между серверами
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="70">
Отказоустойчивость на уровне кластера серверов
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="70">
Географически распределенный кластер
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="70">
Автоматическое масштабирование
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
</tbody>
</table>
 <br>
<table width="100%">
<tbody>
<tr>
	<td>
 <br>
	</td>
	<td colspan="4" height="60" align="center">
		<h6>Безопасность и мобильность</h6>
	</td>
</tr>
<tr>
	<td width="200">
 <br>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Старт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Стандарт</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Малый бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Бизнес</b>
	</td>
	<td width="180" bgcolor="#DBDADD" align="center">
 <b>Энтерпрайз</b>
	</td>
</tr>
<tr>
	<td height="40">
Облачные хранилища
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Автоматический бекап в облако
	</td>
	<td align="center">
		 2 Гб
	</td>
	<td align="center">
		 2 Гб
	</td>
	<td align="center">
		 4 Гб
	</td>
	<td align="center">
		 10 Гб
	</td>
	<td align="center">
 <img src="/images/Infinity.png" width="17">
	</td>
</tr>
<tr>
	<td height="40">
Маркетплейс
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="60">
Защита от DDOS
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="40">
Базы данных
	</td>
	<td align="center">
		 MySQL
	</td>
	<td align="center">
		 MySQL
	</td>
	<td align="center">
		 MySQL
	</td>
	<td align="center">
		 MySQL
	</td>
	<td align="center">
		 MySQL
	</td>
</tr>
<tr>
	<td height="40">
Проактивная защита
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="60">
Мобильная платформа
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="60">
Мобильное приложение интернет-магазина
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
<tr>
	<td height="60">
Технология Push &amp; Pull
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <br>
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
	<td align="center">
 <img src="https://opt-560835.ssl.1c-bitrix-cdn.ru/images/icons/bullet-n.gif">
	</td>
</tr>
</tbody>
</table>
 <br>
<p>
 <b>*</b>Все сайты, работающие на одной лицензии, должны размещаться на одном хостинге и использовать одну копию программного продукта «1С-Битрикс: Управления сайтом».&nbsp;С подробной информацией вы можете ознакомиться в разделе&nbsp;<noindex><a href="https://www.1c-bitrix.ru/products/cms/multisite.php#tab-license-link" target="_blank">"Многосайтовость"</a></noindex>.
</p>
 </div><div id="tab-transition-body" class="tab-off" style="display: none; opacity: 1; height: auto; overflow-y: visible;"><div class="tab-title tab-off">Переход между лицензиями</div>
<h2>Как перейти на другую лицензию</h2>
<p>
	 В любой момент вы можете перейти c используемой вами лицензии на «старшую» лицензию и получить дополнительные возможности. Или же продлить лицензию для получения всех новинок продукта.
</p>
<table>
<tbody>
<tr>
	<td>
 <img alt="Сравните лицензии" src="/images/eshop.png" title="1С-Битрикс: Управление сайтом" valign="top" width="350" border="0">
	</td>
	<td>
		<h4>Переход между лицензиями</h4>
		<div style="text-align: left; margin-top: 20px; font-size: 15px">
			 Если вы уже используете «Старт», «Стандарт» или другую лицензию продукта, вы можете <b>расширить возможности вашего сайта</b>: открыть интернет-магазин, управлять партнерскими сетями, анализировать статистику посещений, определять эффективность ваших рекламных кампаний и многое другое.
		</div>
	</td>
</tr>
</tbody>
</table>
<p>
	 Достаточно выбрать необходимую лицензию продукта и приобрести ее. При переходе вся информация на сайте сохранится. Вам не потребуется создавать его заново. C помощью <a href="/products/cms/siteupdate.php#tab-technology-link" title="Update">технологии SiteUpdate</a> вы получите новые модули продукта и сможете установить их без помощи разработчиков. <a href="/buy/cms.php#tab-upgrade-link" title="Стоимость перехода">Стоимость перехода</a> на другую лицензию продукта составляет разницу в цене лицензий.<br>
</p>
 <br>
<h4>
<div style="text-align: center">
	 Как выбрать лицензию
</div>
 </h4>
<table>
<tbody>
<tr>
	<td>
 <b>1. Определите задачи вашего сайта</b><br>
 <br>
		 При выборе лицензии стоит руководствоваться теми задачами, которые предстоит выполнять созданному на базе «1С-Битрикс: Управление сайтом» интернет-проекту. Будет ли это <b>корпоративный сайт</b>, <b>интернет-магазин</b> или <b>крупный интернет-портал</b> с многотысячными каталогами товаров - решать вам, и от этого решения зависит выбор конкретной лицензии продукта.
	</td>
	<td>
 <img src="/images/note-moduls.png" alt="модули" title="1С-Битрикс: Управление сайтом" valign="top" width="350" border="0" align="middle">
	</td>
</tr>
</tbody>
</table>
 <br>
 <br>
<p>
 <b>2. Выберите функциональные возможности</b><br>
 <br>
	 Лицензции продукта различаются набором функциональных возможностей - <b>модулей</b> (для управления контентом сайта, форумами, блогами, рассылками, пользователями, правами доступа и многим другим).<br>
 <br>
	 «1С-Битрикс: Управление сайтом» чрезвычайно упрощает разработку веб-сайтов «из коробки», предлагая сразу при установке продукта <b>набор готовых к использованию решений</b> для создания сайтов. Это сокращает процесс разработки и позволяет запустить полностью персонализированный веб-сайт в считанные часы.
</p>
 <br>
 <br>
 <br>
 <a href="/buy/cms.php#tab-upgrade-link" target="_blank" class="bt_green dib"><nobr>Перейти на другую лицензию</nobr></a> </div><div id="tab-prolong-body" class="tab-off" style="display: none; opacity: 1; height: auto; overflow-y: visible;"><div class="tab-title tab-off">Продление лицензии</div>
<h2>Как продлить лицензию</h2>
<p>
	 Продукт постоянно обновляется: выпускаются <a href="/products/cms/versions.php#tab-changes-link">новые функциональные возможности модулей</a>, обновления для интерфейса, новые версии. <br>
	 Все пользователи продукта <b>в течение 1 года</b> с момента приобретения могут <b>бесплатно</b> скачивать все обновления. В дальнейшем срок действия обновлений можно продлить по льготному или стандартному варианту. <br>
 <br>
 <i>Если решите не продлевать обновления, ваш сайт не закроется, вы продолжите работать с той версией продукта, которая будет у вас на этот момент.</i>
</p>

</div></div></div>
			</div>			<div class="clearboth"></div>
	</div>

	</div>
	</div>
	</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>