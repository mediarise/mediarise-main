<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
?>
<div class="dialog__contain">

	<?
	if(!empty($arResult["ERROR_MESSAGE"]))
	{
		?>
		<div class="dialog__error">
			<?
				foreach($arResult["ERROR_MESSAGE"] as $v)
					ShowError($v);			
			?>
		</div>
		<?	
	}
	if(strlen($arResult["OK_MESSAGE"]) > 0)
		{
			?>
			<div class="dialog__success">
				<span><?=$arParams["OK_TEXT"];?></span>
			</div>		
			<?
		}
	?>	

	<form id="callback-form" action="<?=POST_FORM_ACTION_URI?>" method="POST">
	
	<?=bitrix_sessid_post()?>
	<div>
		<input 
			type="text" 
			name="NAME_CALLBACK" 
			placeholder="<?=GetMessage("MCT_NAME")?>" 
			value="<?=$arResult["AUTHOR_NAME"]?>">
	</div>
	<div>
		<input type="text" 
			name="PHONE_CALLBACK" 
			placeholder="<?=GetMessage("MCT_PHONE")?>" 
			value="<?=$arResult["AUTHOR_PHONE"]?>">
	</div>

	
	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
	<div>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">	
		<input type="text" placeholder="<?=GetMessage("MCT_CAPTCHA_CODE")?>"  name="captcha_word" value="">
	</div>	
	<div class="dialog__captcha">	
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
	</div>		
	<?endif;?>
	
	<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
	<div class="dialog_button-container">
		<input class="button" type="submit" name="submit" value="<?=GetMessage("MCT_SUBMIT")?>">
	</div>		

	</form>

</div>
<script>
$(function(){

	//Add new mothod to object for checking other character () +
	$.validator.addMethod("laxEmail", function(value, element) {

	return this.optional( element ) || /^([+]?[0-9\s-\(\)]{3,25})*$/.test( value );

	}, '<?=GetMessage("MCT_FIELDS_NUMBERS")?>');	
	
	//Validate callback form
	$("#callback-form").validate({ 
		submitHandler: function(form) {
			$(form).ajaxSubmit({
				target:'.dialog__content', 
				url: "<?=SITE_DIR?>include/callback_form.php",	
				beforeSubmit:function(){
					$(".dialog__content").addClass(".dialog__content_loading");
				},
				success:function(){		
					$('[data-dialog-close]').click(dlg.toggle.bind(dlg));	
					$(".dialog__content").removeClass(".dialog__content_loading");
				}			
			});
		},
		focusInvalid: true,
		focusCleanup: false,
		rules: {			
		'NAME_CALLBACK': {
			required: true,
			maxlength: 40,
			minlength: 3,	
		},
		'PHONE_CALLBACK': {
			required: true,
			maxlength: 20,
			minlength: 7,
			laxEmail: true,				
		},						
		},
		messages: {
		'NAME_CALLBACK': {
			required: "<?=GetMessage("MCT_FIELDS_NONE")?>",
			minlength: "<?=GetMessage("MCT_FIELDS_MIN_3")?>",	
		},	
		'PHONE_CALLBACK': {
			required: "<?=GetMessage("MCT_FIELDS_NONE")?>",				
			minlength: "<?=GetMessage("MCT_FIELDS_MIN_7")?>",		
		},				
		},
		errorPlacement: function(error, element) {
			var er = element.attr("name");
			error.appendTo( element.parent());	    
		}		
	});
});
</script>
