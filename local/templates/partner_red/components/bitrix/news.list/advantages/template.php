<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="section__container">


    <div class="widget shapely_home_parallax">
        <section class="">
            <div class="container">
                <div class="row align-children">
                    <div class="col_md_5 col_sm_6 text-center mb_xs_24">
                        <img class="img-responsive" alt="SEO Friendly"
                             src="/images/mr/create_siter_mediarise.jpg">
                    </div>
                    <div class="col_md_6 col_md_offset_1 col_sm_5 col_sm_offset_1">
                        <div class="">
                            <h3>Разработка сайта</h3>
                            <h3>Разработка лендинг пейдж (посадочной страницы)</h3>
                            <h3>Техническая поддержка сайтов</h3>

                            <div class="mb32">

                                <p>Создание сайтов для бизнеса</p>
                                <p>Создадим сайт с учетом современных тенденций в дизайне и юзабилити. Оптимизируем его
                                    для продвижения в поисковых системах Яндекс и Google и увеличим Ваши продажи.</p>

                            </div>

                            <div class="easy-header__callback" style="float: left;">
						    <span data-dialog="callback" class="button">
							<i class="fas fa-phone"></i> Заказать сайт</span>
                            </div>


                        </div>

                    </div>

        </section>
        <div class="clearfix"></div>
    </div>

    <div class="widget shapely_home_parallax">
        <section class="bg-secondary" style="    background: #f5f5f5;">
            <div class="container">
                <div class="row align-children">
                       <div class="col_md_5 col_md_offset_1 col_sm_6 col_sm_offset_1 text-center">
                        <div class="">
                            <h3>SEO-продвижение сайтов</h3>
                            <h3>Контекстная реклама</h3>
                            <h3>Продвижение в социальных сетях (SMM)</h3>
                            <h3>Медийная реклама</h3>


                            <div class="mb32">

                                <p></p>


                            </div>

                            <div class="easy-header__callback" style="float: left;">
						<span data-dialog="callback" class="button">
							<i class="fas fa-phone"></i> Заказать рекламу</span>
                            </div>
                    </div>
                    </div>
                    <div class="col_md_6 col_sm_5 mb_xs_24">
                    <img class="img-responsive" alt="Portfolio Section"
                             src="/images/mr/seo_mediarise.jpg">
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    </div>

    <div class="widget shapely_home_parallax">
        <section class="">
            <div class="container">
                <div class="row align-children">
                    <div class="col_md_5 col_sm_6 text-center mb_xs_24">
                        <img class="img-responsive" alt="SEO Friendly"
                             src="/images/mr/design_mediarise.jpg">
                    </div>
                    <div class="col_md_6 col_md_offset_1 col_sm_5 col_sm_offset_1">
                        <div class="">
                            <h3>SEO-продвижение сайтов</h3>
                            <h3>Контекстная реклама</h3>
                            <h3>Продвижение в социальных сетях (SMM)</h3>
                            <h3>Медийная реклама</h3>


                            <div class="mb32">

                                <p></p>

                            </div>

                            <div class="easy-header__callback" style="float: left;">
						<span data-dialog="callback" class="button">
							<i class="fas fa-phone"></i> Заказать продвижение</span>
                            </div>


                        </div>

                    </div>

        </section>
        <div class="clearfix"></div>
    </div>

    <div class="widget shapely_home_parallax">
        <section class="bg-secondary" style="    background: #f5f5f5;">
            <div class="container">
                <div class="row align-children">
                    <div class="col_md_5 col_md_offset_1 col_sm_6 col_sm_offset_1 text-center">
                        <div class="">
                            <h3>ИТ аутсорсинг для Вашего бизнеса</h3>



                            <div class="mb32">

                                <p></p>


                            </div>

                            <div class="easy-header__callback" style="float: left;">
						<span data-dialog="callback" class="button">
							<i class="fas fa-phone"></i> Заказать услугу</span>
                            </div>
                        </div>
                    </div>
                    <div class="col_md_6 col_sm_5 mb_xs_24">
                        <img class="img-responsive" alt="Portfolio Section"
                             src="/images/mr/seo_mediarise.jpg">
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    </div>


</div>
</div>


