<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<div class="fs-present-display" >



        <video id="bgvid" playsinline autoplay muted>
            <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->

            <source src="<?= SITE_DIR.'include/video/video.mp4' ?>" type="video/mp4">
        </video>



		<div class="container mr-slider">
			<div class="present-info">
				<div class="present-carousel owl-carousel">		
					<? 	$count = 0;
						foreach($arResult["ITEMS"] as $arItem):
						$count++;
					?>			
						<div class="present-carousel__item">					
							<div class="present-info__inner">
								<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
									<div class="it-aimation">
										<?if($count == 1):?>
											<h1><?=$arItem["NAME"]?></h1>
										<?else:?>
											<h2><?=$arItem["NAME"]?></h2>
										<?endif;?>
									</div>
								<?endif;?>
								<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
									<div class="it-aimation">
										<p><?=$arItem["PREVIEW_TEXT"]?></p>
									</div>
								<?endif;?>
								<?if(!empty($arItem["DISPLAY_PROPERTIES"]['TEXT_BUTTON_PRESENT']['VALUE'])):?>
									<div class="it-aimation">
										<span data-dialog="callback" class="button button_large button_fs-present-display">
										<?if(!empty($arItem["DISPLAY_PROPERTIES"]['ICON_BUTTON_PRESENT']['VALUE'])):?>
											<small class="icon-font-<?=$arItem["DISPLAY_PROPERTIES"]['ICON_BUTTON_PRESENT']['VALUE']?>"></small>
										<?endif;?>
										<?=$arItem["DISPLAY_PROPERTIES"]['TEXT_BUTTON_PRESENT']['VALUE']?>
										</span>									
									</div>
								<?endif;?>		
							</div>
						</div>					
					<?endforeach;?>						
				</div>				
			</div>
		</div>
	</div>
