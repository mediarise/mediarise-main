<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult)):?>
	<div class="navbar-wrapper">




        <!-- Header -->
        <div class="easy-header">
            <div class="easy-header__container">
                <div class="container">
                    <div class="row">
                        <div class="col_md_3 col_sm_4 col_xs_12">
                            <div class="easy-header__logo">
                                <?$APPLICATION->IncludeFile(
                                    SITE_DIR."include/logo_company.php",
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
                            </div>
                        </div>


                        <div class="col_md_2 col_sm_4 col_xs_12">

                        <!--
                            <nav>
                                <input id='link-top' type='checkbox'>
                                <label class='down' for='link-top'>
                                    <span class="mr-header__service">Услуги и цены</span>
                                </label>
                                <ul>
                                    <li>
                                        <a>Все услуги</a>
                                    </li>
                                    <li>
                                        <a>SEO-Продвижение сайтов</a>
                                    </li>
                                    <li>
                                        <a>Контекстная реклама</a>
                                    </li>
                                    <li>
                                        <a>Управление репутацие</a>
                                    </li>
                                    <li>
                                        <a>Редизайн сайта</a>
                                    </li>
                                    <li>
                                        <a>SMM</a>
                                    </li>
                                    <li>
                                        <a>Юзабилити</a>
                                    </li>
                                    <li>
                                        <a>Медийная реклама</a>
                                    </li>
                                    <li>
                                        <a>Создание сайтов</a>
                                    </li>
                                    <li>
                                        <a>Техническая поддержка</a>
                                    </li>
                                    <li>
                                        <a>Разработка landing page</a>
                                    </li>
                                </ul>
                            </nav>
                         -->

                        </div>


                        <div class="col_md_4 col_sm_4 col_xs_12">
                            <div class="easy-header__phones modificated-to-mobile">
							<span>
								<?$APPLICATION->IncludeFile(
                                    SITE_DIR."include/phone_01.php",
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
							</span>
                                <span>
								<?$APPLICATION->IncludeFile(
                                    SITE_DIR."include/phone_02.php",
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
							</span>
                            </div>
                        </div>
                        <div class="col_md_3 col_sm_4 col_xs_12">
                            <div class="easy-header__callback">
						<span data-dialog="callback" class="button">
							<?$APPLICATION->IncludeFile(
                                SITE_DIR."include/callback_button.php",
                                Array(),
                                Array("MODE"=>"html")
                            );?>
						</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



		<div class="navbar" role="navigation">
			<div class="navbar__header">
				<div class="container">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar__collapse">
					<span class="navbar-toggle__icon-bar"></span>
					<span class="navbar-toggle__icon-bar"></span>
					<span class="navbar-toggle__icon-bar"></span>
				  </button>   	    		
				</div>     	
			</div>
			<div class="navbar__collapse collapse">
				<div class="container">
				  <ul class="nav navbar__nav">			
					<?	
						$count = 0;
						foreach($arResult as $arItem):
						$count++;
					?>   
						<li>
							<a <?if($count == 0) echo "class='active-item'";?> href="<?=$arItem["LINK"]?>">
								<?=$arItem["TEXT"]?>
							</a>
						</li>
					<?endforeach?>	
				  </ul>					
				</div>	
			</div>
		</div>
	</div>
<?endif?>