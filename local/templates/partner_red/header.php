<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE HTML>
<html lang="ru-RU">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?$APPLICATION->ShowHead();?>
<title><?$APPLICATION->ShowTitle()?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700|Playfair+Display|Roboto:300|Scada:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

<!-- IcoMoon -->
<link href="<?=SITE_TEMPLATE_PATH?>/css/fonts/icomoon.css" rel="stylesheet" type="text/css">

<!-- OwlCarousel core stylesheet -->
<link href="<?=SITE_TEMPLATE_PATH?>/css/plugins/owl-carousel.css" rel="stylesheet" type="text/css">

<!-- Magnific PopUp css -->
<link href="<?=SITE_TEMPLATE_PATH?>/css/plugins/magnific-popup.css" media="screen, projection" rel="stylesheet" type="text/css" />

<!-- Main styles -->
<link href="<?=SITE_TEMPLATE_PATH?>/css/style.css" media="screen, projection" rel="stylesheet" type="text/css" />
<link href="<?=SITE_TEMPLATE_PATH?>/colors.css" media="screen, projection" rel="stylesheet" type="text/css" />

<!-- Libraries -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.min.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.viewport.js" type="text/javascript"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->			
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Muli|Staatliches|Vollkorn" rel="stylesheet">


</head>
<body>
<?$APPLICATION->ShowPanel();?>
<!-- Loader -->
<?
$APPLICATION->IncludeFile(
	SITE_DIR."include/loader.php",
	Array(),
	Array("MODE"=>"html")
);
?>

<!-- Navigation -->
<?$APPLICATION->IncludeComponent("bitrix:menu", "menu_top", Array(
	"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => "",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
	),
	false
);?>