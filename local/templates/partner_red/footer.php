<!-- Footer -->
<div class="footer">
<style>

.bx-composite-btn {
    background: url(/bitrix/images/main/composite/sprite-1x.png) no-repeat right 0 #e94524;
    border-radius: 15px;
    color: #fff !important;
    display: inline-block;
    line-height: 30px;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
    font-size: 12px !important;
    font-weight: bold !important;
    height: 31px !important;
    padding: 0 42px 0 17px !important;
    vertical-align: middle !important;
    text-decoration: none !important;
}
</style>
	<div class="container">
		<div class="row">
			<div class="col_sm_6 col_md_6">


				<div class="footer__copyright">
					<?$APPLICATION->IncludeFile(
						SITE_DIR."include/copyright_company.php",
						Array(),
						Array("MODE"=>"html")
					);?>




				</div>
			</div>
			<div class="col_sm_6 col_md_6">
                <noindex>
                <img  alt="Бизнес партнер 1С-Битрикс" src="//partners.1c-bitrix.ru/images/partners/business_sm.png" title="Бизнес партнер 1С-Битрикс" class="img-responsive" align="left"><br><br>
                    <p>Работает на <a href="//www.1c-bitrix.ru/products/cms/">«1С-Битрикс: Управление сайтом»</a></p>

	<span id="composite-banner" class="bx-composite-btn bx-btn-white" style="background-color: rgb(233, 69, 36);">Быстро с 1С-Битрикс</span>
                </noindex>
            </div>
		</div>
	</div>
</div>

<!-- Callback dialog -->
<div id="callback" class="dialog">
<div class="dialog__overlay"></div>
	<div class="dialog__content">
		<?$APPLICATION->IncludeFile(
			SITE_DIR."include/callback_form.php",
			Array(),
			Array("MODE"=>"php")
		);?>
	</div>
</div>

<!-- Libraries -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.min.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.viewport.js" type="text/javascript"></script>

<!-- Sticky -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/jquery.sticky.js" type="text/javascript"></script>

<!-- ScrollTo -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/jquery.scrollTo.min.js" type="text/javascript"></script>

<!-- Isotope -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/isotope.pkgd.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/imagesloaded.pkgd.min.js" type="text/javascript"></script>

<!-- Waypoints -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/jquery.waypoints.min.js" type="text/javascript"></script>

<!-- Owlcaroulsel -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/owl.carousel.js" type="text/javascript"></script>

<!-- Bootstrap plugins -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/collapse.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/transition.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/tab.js" type="text/javascript"></script>

<!-- Counter -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/countUp.min.js" type="text/javascript"></script>

<!-- Magnific PopUp -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/jquery.magnific-popup.js" type="text/javascript"></script>

<!-- Jquery form validation and Ajax form -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/jquery.validate.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/jquery.form.min.js" type="text/javascript"></script>

<!-- SmoothScroll -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/SmoothScroll.js" type="text/javascript"></script>

<!-- Dialog plugins -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/classie.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/plugins/dialogFx.js" type="text/javascript"></script>

<!-- Init -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/init.js" type="text/javascript"></script>
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b9777617/crm/site_button/loader_1_56yk58.js');
</script>
</body>
</html>