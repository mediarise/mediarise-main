<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;

use Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class mediarisegroup_yasovetblock extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();
        
        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_ID = 'mediarisegroup.yasovetblock';
        $this->MODULE_NAME = Loc::getMessage('MEDIARISEGROUP_YASOVETBLOCK_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MEDIARISEGROUP_YASOVETBLOCK_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('MEDIARISEGROUP_YASOVETBLOCK_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = 'https://mediarise.ru';
    }

    public function DoInstall(){

        global $APPLICATION;

        if(CheckVersion(ModuleManager::getVersion("main"), "14.00.00")){

            $this->InstallFiles();
            $this->InstallDB();

            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallEvents();
        }else{

            $APPLICATION->ThrowException(
                Loc::getMessage("MEDIARISEGROUP_YASOVETBLOCK_INSTALL_ERROR_VERSION")
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("MEDIARISEGROUP_YASOVETBLOCK_INSTALL_TITLE")." \"".Loc::getMessage("MEDIARISEGROUP_YASOVETBLOCK_NAME")."\"",
            __DIR__."/step.php"
        );

        return false;
    }

    public function DoUninstall(){

        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallEvents();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("MEDIARISEGROUP_YASOVETBLOCK_UNINSTALL_TITLE")." \"".Loc::getMessage("MEDIARISEGROUP_YASOVETBLOCK_NAME")."\"",
            __DIR__."/unstep.php"
        );

        return false;
    }

    public function installDB()
    {
        return false;
    }

    public function UnInstallDB(){

        Option::delete($this->MODULE_ID);

        return false;
    }



    public function InstallFiles(){

        CopyDirFiles(
            __DIR__."/assets/scripts",
            Application::getDocumentRoot()."/bitrix/js/".$this->MODULE_ID."/",
            true,
            true
        );

        return false;
    }

    public function InstallEvents(){

        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnBeforeEndBufferContent",
            $this->MODULE_ID,
            "MediaRiseGroup\YaSovetBlock\Main",
            "appendScriptsYaSovetBlock"
        );

        return false;
    }

    public function UnInstallEvents(){

        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnBeforeEndBufferContent",
            $this->MODULE_ID,
            "MediaRiseGroup\YaSovetBlock\Main",
            "appendScriptsYaSovetBlock"
        );

        return false;
    }

    public function UnInstallFiles(){

        Directory::deleteDirectory(
            Application::getDocumentRoot()."/bitrix/js/".$this->MODULE_ID
        );

        return false;
    }
}
