<?php

$MESS['MEDIARISEGROUP_YASOVETBLOCK_MODULE_NAME'] = 'Блокировка Советник Яндекс.Маркета';
$MESS['MEDIARISEGROUP_YASOVETBLOCK_MODULE_DESCRIPTION'] = 'Блокирует Советник Яндекс.Маркета';
$MESS['MEDIARISEGROUP_YASOVETBLOCK_MODULE_PARTNER_NAME'] = 'MediaRise Group';

$MESS["MEDIARISEGROUP_YASOVETBLOCK_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["MEDIARISEGROUP_YASOVETBLOCK_INSTALL_TITLE"]   		= "Установка модуля";

$MESS["MEDIARISEGROUP_YASOVETBLOCK_UNINSTALL_TITLE"]   		= "Деинсталляция модуля";
