<?php

namespace MediaRiseGroup\YaSovetBlock;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;

class Main
{

    public function appendScriptsYaSovetBlock()
    {

        if (!defined("ADMIN_SECTION") && $ADMIN_SECTION !== true) {

            $module_id = pathinfo(dirname(__DIR__))["basename"];
            Asset::getInstance()->addJs("/bitrix/js/" . $module_id . "/script.js");
        }

        return false;
    }
}